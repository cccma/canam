!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
module phys_consts
  !
  !     * jun 05/2013 - j.li/     new version for gcm17+:
  !     *               m.lazare. - "ACTFRC" variable added for semi-
  !     *                           direct effect.
  !     *                         - "HS" changed to be consistent with
  !     *                           what is used else where in code.
  !     * nov 14/2006 - m.lazare. previous version spwcon10 for gcm15f+:
  !     *                         - scope variables using
  !     *                           implicit none.
  !     *                         - epsice removed (only
  !     *                           one active was qmin and
  !     *                           is now defined in trinfo4).
  !     * dec 20/2002 - m.lazare. previous version spwcon9.
  !     * may 17/2000 - m.lazare. previous version spwcon8.

  !     * sets constants for gcm17.
  !
  !     *    ww = earth rotation rate (1/sec).
  !     * rayon = earth radius (m).
  !     *  grav = gravity acceleration (m/sec**2).
  !     *  rgas = dry air gas constant (joule/(kg*deg)).
  !     * rgocp = rgas/(dry air specific heat)
  !     * fvort = vorticity of earth rotation.
  !
  implicit none
  !
  !     * parameters used in AGCM (physics and dynamics !).
  !
  real, parameter :: ww     = 7.292e-5
  real, parameter :: tw     = ww + ww
  real, parameter :: rayon  = 6.37122e06        !< earth radius (m).
  real, parameter :: asq    = rayon * rayon
  real, parameter :: grav   = 9.80616           !< Gravity on Earth \f$[m s^{-2}]\f$
  real, parameter :: rgas   = 287.04            !< Ideal gas constant for dry air \f$[J kg^{-1} K^{-1}]\f$
  real, parameter :: mma_kg = 0.0289647         !< Molecular mass of dry air [kg/mol]
  real, parameter :: mma_g  = 28.9647           !< Molecular mass of dry air [g/mol]
  real, parameter :: rgocp  = 2./7.             !< Ideal gas constant divided by heat capacity for dry air \f$[unitless]\f$
  real, parameter :: rgoasq = rgas/asq
  real, parameter :: cpres  = rgas/rgocp        !< Heat capacity of dry air \f$[J kg^{-1} K^{-1}]\f$
  real, parameter :: cpresv = 1870.
  real, parameter :: rgasv  = 461.50            !< Ideal gas constant for water vapor \f$[J kg^{-1} K^{-1}]\f$
  !
  real, parameter :: pi     = 3.1415926535898
  ! real, parameter :: pi     = 4. * atan(1.0)

  real, parameter :: rvord  = 0.622
  real, parameter :: tfrez  = 273.16            !< freezing temperature of fresh water or melting point of snow (k)
  real, parameter :: hs     = 2.835e6           !< latent heat of sublimation (j kg-1)
  real, parameter :: hv     = 2.501e6           !< latent heat of vaporization (j kg-1)
  real, parameter :: daylnt = 86400.            !< length of day in seconds

  real, parameter :: csno   = 9.6e4
  real, parameter :: cpack  = 2.1e5             !< heat capacity of 10cm 'upper layer' of pack ice (J m-2 K-1)
  real, parameter :: gtfsw  = 271.2             !< freezing point of sea water (k)
  real, parameter :: rkhi   = 0.7
  real, parameter :: cice   = cpack*sqrt(2.)    !< heat capacity of pack ice (normalized by sqrt(2)) (j m-2 k-1).

  real, parameter :: sbc    = 5.66796e-8
  real, parameter :: snomax = 10.

  real, parameter :: coni   = 2.25              !< thermal conductivity of ice (w m k-1)
  real, parameter :: deni   = 913.              !< sea ice density (kg m-3)
  real, parameter :: sicmin = 45.0
  real, parameter :: dfsic  = 0.05              !< thickness of 'thin' ice

  real, parameter :: conf   = 1.00

  real, parameter :: actfrc = 0.9

  real, parameter :: tvfa = 0.608

  real, parameter :: vkc    = 0.4
  real, parameter :: vmin   = 0.1

  !
  !     * parameter for dynamical spectral transforms.
  !
  real, parameter :: fvort  = tw * sqrt(2./3.)
  !
  !
  !     * Other physical parameters.
  !
  real, parameter :: rauw   = 1.e+3
  real, parameter :: cp     = 1004.5     !>  heat capacity of air at constant pressure (j/kg/k)
  !
  !     * parameters used by  function  htvocp.
  !
  real, parameter :: t1s    = 273.16
  real, parameter :: t2s    = 233.16
  real, parameter :: aw     = 3.15213e+6/cp
  real, parameter :: bw     = 2.38e+3/cp
  real, parameter :: ai     = 2.88053e+6/cp
  real, parameter :: bi     = 0.167e+3/cp
  !     real, parameter :: aice   =24.292
  !     real, parameter :: bice   =6141.
  !     real, parameter :: tice   =233.16
  !     real, parameter :: qmin   =1.e-40
  real, parameter :: slp    = 1./(t1s - t2s)
  !
  !     * parameters used by  functions  dewpnt,  spchum,  deltaq.
  !
  real, parameter :: a      = 21.656 !< Constant used in saturation vapour pressure calculation \f$e_{s}=exp(A-B/T)\f$ \f$[units]\f$
  real, parameter :: b      = 5418.  !< Constant used in saturation vapour pressure calculation \f$e_{s}=exp(A-B/T)\f$\f$[units]\f$
  real, parameter :: eps1   = 0.622  !< Ratio of molecular weight of water vapor to the mean molecular weight of dry air \f$[1]\f$
  real, parameter :: eps2   = 0.378  !< 1-EPS1 \f$[1]\f$
  !
  !     * parameters used by  function  gamsat.
  !
  real, parameter :: epss   = eps1
  real, parameter :: capa   = rgocp
  !
  !     * parameters used in moist convective adjustment.
  !
  real, parameter :: hc     = .95
  real, parameter :: hf     = .95
  real, parameter :: hm     = .95
  real, parameter :: lheat  = 1
  real, parameter :: moiadj = 1
  real, parameter :: moiflx = 1
  real, parameter :: depth  = 1./(rauw * grav)
!  real, parameter :: aa     = 0.0
  real, parameter :: aa     = 1./(6. * (1. - hm) ** 2)  !if (hm < 1.)
  !
  !     * ADDED CONSTANTS FOR KNUT'S NEW SATURATION VAPOUR PRESSURE FORMULATION.
  !
  real, parameter :: rw1 = 53.67957
  real, parameter :: rw2 = - 6743.769
  real, parameter :: rw3 = - 4.8451
  real, parameter :: ri1 = 23.33086
  real, parameter :: ri2 = - 6111.72784
  real, parameter :: ri3 = 0.15215
  !
!
! * Monin-Obukov stability theory parameters.
! * These are used in routines vrtdftke.f and drcoef.f.
! COMMENT: CARSTEN will move these in the new atmospheric surface layer module in which 
! stability functions are defined. Once that is harmonized with CLASSIC drag coefficient calculations 
! these can be removed here
  real, parameter :: rmos1 = 16.0
  real, parameter :: rmos2 = 16.0
  !-----------------------------------------------------------------------
end module phys_consts
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
