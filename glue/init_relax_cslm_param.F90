!> \file
!> \brief Read in CSLM relaxation namelist parameters
!!
!! John Scinocca
!

subroutine init_relax_cslm_param(nml_iu, mynode)
  !***********************************************************************
  ! read cslm relaxation parameter values from the model input namelist file
  !
  ! input
  !   integer*4 :: mynode  ...mpi node (used to restrict i/o to node 0)
  !
  !***********************************************************************

  use relax_cslm_param_defs

  implicit none

  !--- input
  integer*4, intent(in) :: mynode !< Variable description\f$[units]\f$
  integer,   intent(in) :: nml_iu !< Input namelist file unit number\f$[unit-less]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !-----------------------------------------------------------------------
  !--- set the relaxation parameter defaults

  lake_param=0 ! 0 - use lake parameters in restart, 1 - read in new set, 2 - read in just LLAK (scaled)
  iperiod_lake_cor=24 ! lake nudging/bc cycle [hr] - ie how often to read in the bias correction data
  lake_ref_year=2004 ! time stamp of repeated reference year of lake data

  tau_lake_sref=24.  ! lake skin-layer relaxation time scale [hr]
  tau_lake_bref=24.  ! lake base-layer relaxation time scale [hr]
  ithresh=0.05 ! ice threshold below which which reference temperature is set to tfrez
  llak_max=0. ! if llak_max > 0., llak=min(llak,llak_max).  unaltered otherwise

  !--- read relaxation parameters from a namelist

  rewind(nml_iu)
  read(nml_iu, nml=relax_cslm_param)
  !        close(iu)
  if (mynode==0) then
    write(6,*)' '
    write(6,relax_cslm_param)
    write(6,*)' '
    call flush(6)
  end if

end subroutine init_relax_cslm_param
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
