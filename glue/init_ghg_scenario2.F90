!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

subroutine init_ghg_scenario2(mynode)
  !***********************************************************************
  ! read ghg scenario values from a file named ghg_scenario
  !
  ! input
  !   integer*4 :: mynode  ...mpi node (used to restrict i/o to node 0)
  !
  ! output
  !   logical :: interp_ghgts ...flags interpolation of ghg time series in
  !                           the gcm driver.
  !
  ! jason cole....... jan 112016. removed initialization from internal
  !                                data statements.  always from file.
  !                                removed stuff that seemed unnecessary.
  ! larry solheim ... jun 23,2013. add interpolation of rcp6 and use
  !                                revised module "ghgts2_defs".
  ! larry solheim  ...jul 15,2009. original version init_ghg_scenario.
  !
  !***********************************************************************
  use ghgts_defs
  use em_scenarios_defs, only : ghg_co2_scn_offset, ghg_ch4_scn_offset,  &
                                ghg_n2o_scn_offset, ghg_f11_scn_offset,  &
                                ghg_f12_scn_offset, ghg_f113_scn_offset, &
                                ghg_f114_scn_offset
  !--- ghg constants used by the radiation
  use radcons_mod,       only : co2_ppm, ch4_ppm, n2o_ppm, f11_ppm, f12_ppm, &
                                f113_ppm, f114_ppm
  use agcm_types_mod,    only : phys_options


  implicit none

  !--- input
  integer*4, intent(in) :: mynode !< Variable description\f$[units]\f$

  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !--- local
  integer, parameter :: verbose=1 !<

  !-----------------------------------------------------------------------

  !--- set default ghg values to use when parameters
  !--- co2_scn_mode, ch4_scn_mode, ... are set to zero
  !--- these will be the values set in radcons2
  co2_scn_default  = co2_ppm
  ch4_scn_default  = ch4_ppm
  n2o_scn_default  = n2o_ppm
  f11_scn_default  = f11_ppm
  f12_scn_default  = f12_ppm
  f113_scn_default = f113_ppm
  f114_scn_default = f114_ppm

  if (mynode==0 .and. verbose>1) then
    write(6,*)' co2_scn_default=',co2_scn_default
    write(6,*)' ch4_scn_default=',ch4_scn_default
    write(6,*)' n2o_scn_default=',n2o_scn_default
    write(6,*)' f11_scn_default=',f11_scn_default
    write(6,*)' f12_scn_default=',f12_scn_default
    write(6,*)'f113_scn_default=',f113_scn_default
    write(6,*)'f114_scn_default=',f114_scn_default
  end if

  !--- if there is interpolation then we need to read the information
  !--- from file ghg_scenario" and assign as appropriate.

  !--- put the data just read into the appropriate array
  co2_scn(1:n_co2_scn)      = ghgts_co2(1:n_co2_scn)
  co2_scn_time(1:n_co2_scn) = ghgts_time(1:n_co2_scn) &
                                    + ghg_co2_scn_offset

  ch4_scn(1:n_ch4_scn)      = ghgts_ch4(1:n_ch4_scn)
  ch4_scn_time(1:n_ch4_scn) = ghgts_time(1:n_ch4_scn) &
                                    + ghg_ch4_scn_offset

  n2o_scn(1:n_n2o_scn)      = ghgts_n2o(1:n_n2o_scn)
  n2o_scn_time(1:n_n2o_scn) = ghgts_time(1:n_n2o_scn) &
                                    + ghg_n2o_scn_offset

  ! for cfc-11 there is an option to use it directly or to use
  ! an "effective" cfc-11 that rolls in the effective of many
  ! trace gases not explicitly treated in the radiative transfer.
  ! controlled by a switch in the basefile.

  if (phys_options%cfc11_effective) then
     f11_scn(1:n_f11_scn)      = ghgts_e11(1:n_f11_scn)
     f11_scn_time(1:n_f11_scn) = ghgts_time(1:n_f11_scn) &
                               + ghg_f11_scn_offset
  else
     f11_scn(1:n_f11_scn)      = ghgts_f11(1:n_f11_scn)
     f11_scn_time(1:n_f11_scn) = ghgts_time(1:n_f11_scn) &
                               + ghg_f11_scn_offset
  end if

  f12_scn(1:n_f12_scn)      = ghgts_f12(1:n_f12_scn)
  f12_scn_time(1:n_f12_scn) = ghgts_time(1:n_f12_scn) &
                                    + ghg_f12_scn_offset

  !--- note: f113 or f114 are initialized non-zero here but the
  !--- default mode is "0", that is, by default they will be fixed
  !--- at a prescribed value, namely the value set in radcons2.

  f113_scn(1:n_f113_scn)      = ghgts_f113(1:n_f113_scn)
  f113_scn_time(1:n_f113_scn) = ghgts_time(1:n_f113_scn) &
                                      + ghg_f113_scn_offset

  f114_scn(1:n_f114_scn)      = ghgts_f113(1:n_f114_scn)
  f114_scn_time(1:n_f114_scn) = ghgts_time(1:n_f114_scn) &
                                      + ghg_f114_scn_offset

  if (verbose>2 .and. mynode==0) then
    write(6,*)'CO2 time: n=',n_co2_scn
    write(6,'(1p10e12.4)')co2_scn_time(1:n_co2_scn)

    write(6,*)'CO2: n=',n_co2_scn
    write(6,'(1p10e12.4)')co2_scn(1:n_co2_scn)

    write(6,*)'CH4: n=',n_ch4_scn
    write(6,'(1p10e12.4)')ch4_scn(1:n_ch4_scn)

    write(6,*)'N2O: n=',n_n2o_scn
    write(6,'(1p10e12.4)')n2o_scn(1:n_n2o_scn)

    write(6,*)'F11: n=',n_f11_scn
    write(6,'(1p10e12.4)')f11_scn(1:n_f11_scn)

    write(6,*)'F12: n=',n_f12_scn
    write(6,'(1p10e12.4)')f12_scn(1:n_f12_scn)

    write(6,*)'F113: n=',n_f113_scn
    write(6,'(1p10e12.4)')f113_scn(1:n_f113_scn)

    write(6,*)'F114: n=',n_f114_scn
    write(6,'(1p10e12.4)')f114_scn(1:n_f114_scn)
  end if

  return

end subroutine init_ghg_scenario2
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
