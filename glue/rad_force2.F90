!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

module rad_force2
  !
  !     * feb 07/2011 - m.lazare. unused "curr_time" removed.
  !     * may 08/2010 - m.lazare. radforce update directive changed to cpp.
  !     * feb 15/2009 - l.solheim. previous version radforce.
  implicit none

  !--- character variable to identify the set of perturbations to use
  character(len = 10) :: rf_scenario = " "

  !--- rf_adj and rf_inst flag the use of adjusted or instantaneous
  !--- radiative forcing, respectively
  logical :: rf_adj
  logical :: rf_inst

  !--- character variables used to form variable names for output
  character(len = 4) :: rfname = "    "
  character(len = 2) :: rfp_id

  contains

  subroutine init_radforce_scenario(mynode)
  use psizes_19,      only : ilev, levrf, nrfp, max_sam
  use agcm_types_mod, only : phys_options
  use agcm_types_mod, only : phys_diag
  implicit none

  integer, intent(in) :: mynode

  if (trim(phys_diag%radforce_scenario) == '') then
     if (mynode == 0) then
        write(6, *) 'Radiative forcing switch is enabled without appropriate scenario set'
        write(6, *) 'Please set radforce_scenario to a proper string in canam_settings namelist'
     end if
     call xit('AGCM:RF', -2)
  end if

  rf_scenario = trim(phys_diag%radforce_scenario)

  ! Set number of radiative forcing perturbations employed
  select case (rf_scenario)
    case('2X4XCO2', '4XCO2_GHAN')
       nrfp = 2
    case('CFMIPA')
       nrfp = 3
    case('AERORF', 'PAMRF')
       nrfp = 6
    case default
       nrfp = 1
  end select

  ! Set number of levels in diagnostic output fields for radiative forcing.
  levrf = ilev + 2

  if (phys_options%use_mcica) then
! potential maximum number of samples that will be taken in the mcica computations
     max_sam = 600
  end if

  ! Enable the option to save radiative flux profiles
  phys_diag%rad_flux_profs = .true.

  return
  end subroutine init_radforce_scenario

  subroutine set_radforce_trop_idx(mynode, lh)
  use psizes_19,      only : ilev
  use agcm_types_mod, only : phys_diag
  implicit none

  integer, intent(in) :: mynode
  integer, intent(in), dimension(ilev+1) :: lh

  integer :: ii

  !---todo--- create a subroutine to set the tropopause height
  !---todo--- index array. this could read from a file or calculate
  !---todo--- tropopause height "on the fly" so that time dependent
  !---todo--- and/or a more realistic tropopause height may be used.

  !--- if trop_idx < 0 then the following conditional block will pick a level near 200mb.
  if (phys_diag%radforce_trop_idx < 0) then
    !--- determine the level index closest to 200 mb
    !--- and use this as the tropopause height
    phys_diag%radforce_trop_idx = 0
    do ii = 1,ilev
      if (lh(ii) <= 200 .and. lh(ii+1) >= 200) then
        if (abs(lh(ii)-200) < abs(200-lh(ii+1))) then
          phys_diag%radforce_trop_idx=ii
        else
          phys_diag%radforce_trop_idx=ii+1
        end if
        exit
      end if
    end do
    if (phys_diag%radforce_trop_idx == 0) then
      write(6,*)'CANNOT DETERMINE LEVEL OF TROPOPAUSE'
      call xit('GCM:RF',-1)
    end if
  end if

  if (phys_diag%radforce_trop_idx > 0) then
    rf_adj=.true.
    rf_inst=.false.
  else
    rf_adj=.false.
    rf_inst=.true.
  end if

  if (mynode == 0) then
    write(6,*)'======================================'
    write(6,*)'RF_SCENARIO :: ', rf_scenario
    write(6,*)'TROP_IDX    :: ', phys_diag%radforce_trop_idx
    write(6,*)'RF_ADJ=',rf_adj,'   RF_INST=',rf_inst
    write(6,*)'======================================'
  end if

  return
  end subroutine set_radforce_trop_idx

end module rad_force2
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
