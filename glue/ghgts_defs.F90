!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

module ghgts_defs

  !--- jason cole     ...jan 10, 2018 read ghg time-series from a file rather than
  !---                                data statement.
  !--- larry solheim  ...jun 23,2013. add rcp6.
  !--- larry solheim  ...dec 2,2009.  previous ghgts_defs.

  !-------------------------------------------------------------
  !--- set up space for ghg values assigned according to certain
  !--- predetermined scenarios
  !--- these values may be set to zero, initialized from a file
  !--- or initialized from data in this module
  !-------------------------------------------------------------
  implicit none
  !--- counters to indicate the number of values in each array
  integer :: n_co2_scn = 0 !<
  integer :: n_ch4_scn = 0 !<
  integer :: n_n2o_scn = 0 !<
  integer :: n_f11_scn = 0 !<
  integer :: n_f12_scn = 0 !<
  integer :: n_f113_scn = 0 !<
  integer :: n_f114_scn = 0 !<

  !--- allocate enough space for 800 years of monthly values
  !--- initialize all arrays to zero

  integer, parameter :: max_ghg_scn=9602 !<
  real   , dimension(max_ghg_scn) :: co2_scn = 0.0 !<
  real*8 , dimension(max_ghg_scn) :: co2_scn_time = 0.0 !<
  real   , dimension(max_ghg_scn) :: ch4_scn = 0.0 !<
  real*8 , dimension(max_ghg_scn) :: ch4_scn_time = 0.0 !<
  real   , dimension(max_ghg_scn) :: n2o_scn = 0.0 !<
  real*8 , dimension(max_ghg_scn) :: n2o_scn_time = 0.0 !<
  real   , dimension(max_ghg_scn) :: f11_scn = 0.0 !<
  real*8 , dimension(max_ghg_scn) :: f11_scn_time = 0.0 !<
  real   , dimension(max_ghg_scn) :: f12_scn = 0.0 !<
  real*8 , dimension(max_ghg_scn) :: f12_scn_time = 0.0 !<
  real   , dimension(max_ghg_scn) :: f113_scn = 0.0!<
  real*8 , dimension(max_ghg_scn) :: f113_scn_time = 0.0 !<
  real   , dimension(max_ghg_scn) :: f114_scn = 0.0 !<
  real*8 , dimension(max_ghg_scn) :: f114_scn_time = 0.0 !<

  !--- these arrays hold the variables that might be read in
  !--- from a file.

  real, dimension(max_ghg_scn) :: ghgts_time !<
  real, dimension(max_ghg_scn) :: ghgts_co2 !<
  real, dimension(max_ghg_scn) :: ghgts_ch4 !<
  real, dimension(max_ghg_scn) :: ghgts_n2o !<
  real, dimension(max_ghg_scn) :: ghgts_f11 !<
  real, dimension(max_ghg_scn) :: ghgts_e11 !<
  real, dimension(max_ghg_scn) :: ghgts_f12 !<
  real, dimension(max_ghg_scn) :: ghgts_f113 !<
  real, dimension(max_ghg_scn) :: ghgts_f114 !<

  !--- default values to use when ghg_xxx_mode=0
  !--- these default values are set in init_ghg_scenario
  real :: co2_scn_default !<
  real :: ch4_scn_default !<
  real :: n2o_scn_default !<
  real :: f11_scn_default !<
  real :: f12_scn_default !<
  real :: f113_scn_default !<
  real :: f114_scn_default !<

  !--- name of file with the ghg time-series.  note that
  !--- this is currently hard-coded in the agcm setup to
  !--- be "GHG_SCENARIO".  however, we define it here
  !--- to make it easier to find in the future.
  character(len=12) :: ghgts_fn = "GHG_SCENARIO" !<

end module ghgts_defs
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
