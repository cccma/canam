!>\file
!>\brief Routines to write the outputs from the radiative transfer 
!! calculations.
!!
!! @author Jason Cole
!

module write_output_mod
  implicit none
  
  private :: write_rfmip_output
  private :: write_canam_output
  private :: check
  private :: write_full_gcm_output
  
contains
  
  subroutine write_output(outdir,      & ! Input
                          outfilename, &
                          cfg          )
  
    use parameters_mod, only : maxlen_char
    use read_config_file_mod, only : cfg_values
    
    implicit none
    
    ! input
    type(cfg_values), intent(in) :: cfg !< Configuration

    character(len=maxlen_char), intent(in) :: outdir      !< Output directory
    character(len=maxlen_char), intent(in) :: outfilename !< Output filename

    ! local
    
    if (cfg%output_type == 1) then ! RFMIP type output
      ! write the flux data needed for RFMIP
      call write_rfmip_output(outdir,      &
                              outfilename, &
                              cfg          )
    elseif (cfg%output_type == 2) then ! CanAM output using ASCII files
      ! write all radiative data to file
      call write_full_gcm_output(outdir,      &
                                 outfilename, &
                                 cfg          )
    elseif (cfg%output_type == 3) then ! CanAM type output
      ! write all radiative data to file
      call write_canam_output(outdir,      &
                              outfilename, &
                              cfg          )
    endif ! output_type
    
    return
  end subroutine write_output

  subroutine write_rfmip_output(outdir,      & ! input
                                outfilename, &
                                cfg          )
    !>\file
    !>\brief Create and write the output needed for the RFMIP calculations.
    
    use parameters_mod, only : maxlen_char
    use read_config_file_mod, only : cfg_values
    use arrays_mod, only : fladrol, &
                           flaurol, &
                           fsadrol, &
                           fsaurol
    use netcdf
    
    implicit none

    ! Input
    character(len=maxlen_char), intent(in) :: outdir      !< Output directory
    character(len=maxlen_char), intent(in) :: outfilename !< Output filename
        
    type(cfg_values), intent(in) :: cfg !< Configuration
    
    ! Local
    character(len=maxlen_char) :: ofilename ! Output filename

    integer :: ncid             ! Netcdf file ID
    integer :: lev_dimid        ! ID for level dimension
    integer :: profile_dimid    ! ID for profile dimension
    integer :: expt_dimid       ! ID for experiment per profile dimension
    integer :: rld_varid        ! ID for downward thermal flux profiles
    integer :: rlu_varid        ! ID for upward thermal flux profiles
    integer :: rsd_varid        ! ID for downward solar flux profiles
    integer :: rsu_varid        ! ID for upward solar flux profiles
    
    integer, dimension(3) :: dimids
    
    integer :: iexpt    ! Counter over experiments
    integer :: isite    ! Counter over sites
    integer :: ilev     ! Counter over vertical levels
    integer :: il       ! Counter over columns
    integer :: ilev_loc ! Counter over vertical levels
        
    real, dimension(:,:,:), allocatable :: write_rld ! Reshaped variable with results to write to file
    real, dimension(:,:,:), allocatable :: write_rlu ! Reshaped variable with results to write to file
    real, dimension(:,:,:), allocatable :: write_rsd ! Reshaped variable with results to write to file
    real, dimension(:,:,:), allocatable :: write_rsu ! Reshaped variable with results to write to file
    
    !==================================================================
    ! Create the netcdf file for output with some basic information.
    write(ofilename,'(a,a,a)') trim(outdir),'/',trim(outfilename)
    
    call check(nf90_create(trim(ofilename), nf90_clobber, ncid))
    
    ! Add dimensions
    call check(nf90_def_dim(ncid,'levels', cfg%lev, lev_dimid))
    call check(nf90_def_dim(ncid,'profile', cfg%num_profile, profile_dimid))
    call check(nf90_def_dim(ncid,'experiment', cfg%num_experiment, expt_dimid))
    
    dimids = (/ lev_dimid, profile_dimid, expt_dimid /)

    call check(nf90_def_var(ncid, "rld", nf90_float, dimids, rld_varid))
    call check(nf90_def_var(ncid, "rlu", nf90_float, dimids, rlu_varid))
    call check(nf90_def_var(ncid, "rsd", nf90_float, dimids, rsd_varid))
    call check(nf90_def_var(ncid, "rsu", nf90_float, dimids, rsu_varid))

    call check(nf90_enddef(ncid))

    ! Reshape the results so they can be written to the file.
    ! Recall that the caluculations are a 2d array (column versus "height") where
    ! the columns are defined all experiments for site 1, all experiments for site 2,
    ! and so on for all sites.
    
    allocate(write_rld(cfg%lev,cfg%num_profile,cfg%num_experiment), &
             write_rlu(cfg%lev,cfg%num_profile,cfg%num_experiment), &
             write_rsd(cfg%lev,cfg%num_profile,cfg%num_experiment), &
             write_rsu(cfg%lev,cfg%num_profile,cfg%num_experiment)  )
             
    ilev_loc = 1
    do ilev = 2, cfg%lev+1 ! The topmost level is TOA, the second topmost is top of model which is what we want
      il = 1
      do isite = 1, cfg%num_profile
        do iexpt = 1, cfg%num_experiment
          write_rld(ilev_loc,isite,iexpt) = fladrol(il,ilev)
          write_rlu(ilev_loc,isite,iexpt) = flaurol(il,ilev)
          write_rsd(ilev_loc,isite,iexpt) = fsadrol(il,ilev)
          write_rsu(ilev_loc,isite,iexpt) = fsaurol(il,ilev)
          il = il + 1
        end do ! iexpt
      end do ! isite
      ilev_loc = ilev_loc + 1
    end do ! ilev
    
    call check(nf90_put_var(ncid,rld_varid,write_rld))
    call check(nf90_put_var(ncid,rlu_varid,write_rlu))
    call check(nf90_put_var(ncid,rsd_varid,write_rsd))
    call check(nf90_put_var(ncid,rsu_varid,write_rsu))
            
    ! close the netcdf file.
    call check(nf90_close(ncid))
    
    deallocate(write_rld, &
               write_rlu, &
               write_rsd, &
               write_rsu  )

    return
  end subroutine write_rfmip_output

  subroutine write_full_gcm_output(outdir,      & ! Input
                                   outfilename, &
                                   cfg          )
    !>\file
    !>\brief Create and write the output typically used in CanAM.  Use an ASCII
    !        file to avoid dealing with netCDF.
    
    use parameters_mod, only : maxlen_char
    use read_config_file_mod, only : cfg_values
    use arrays_mod, only : fladrol, &
                           flaurol, &
                           fsadrol, &
                           fsaurol, &
                           hrlrow,  &
                           pressg,  &
                           shj,     &
                           shtj
    use netcdf
    
    implicit none

    ! Input
    character(len=maxlen_char), intent(in) :: outdir      !< Output directory
    character(len=maxlen_char), intent(in) :: outfilename !< Output filename
        
    type(cfg_values), intent(in) :: cfg !< Configuration
    
    ! Local
    character(len=maxlen_char) :: ofilename ! Output filename
    
    integer :: ix   
    integer :: iz
    
    real, dimension(cfg%ilg,cfg%ilev) :: p_layer
    real, dimension(cfg%ilg,cfg%lev)  :: p_level
    
    ! Compute the pressure profiles for the output
    do iz = 1, cfg%ilev
      do ix = cfg%il1,cfg%il2
        p_layer(ix,iz) = pressg(ix)*shj(ix,iz)
      end do
    end do

    do iz = 1, cfg%lev
      do ix = cfg%il1,cfg%il2
        p_level(ix,iz) = pressg(ix)*shtj(ix,iz)
      end do
    end do
    
    ! Construct the output file name and open it up
    write(ofilename,'(a,a,a)') trim(outdir),'/',trim(outfilename)
    open(unit=15,file=trim(ofilename))
    
    ! Write out the size information
    write(15,*) 'number layers, number levels, number columns'
    write(15,*) cfg%ilev, cfg%lev,cfg%il2-cfg%il1+1
    
    ! Write out the profiles.  Do it one variable at a time to the same file.
    write(15,*) "Pressure on model levels (layer interfaces)"
    do iz = 1, cfg%lev
      write(15,'(i4,150e14.4)') iz,(p_level(ix,iz),ix=cfg%il1,cfg%il2)
    end do
    
    write(15,*)
    write(15,*) "All-sky downward thermal fluxes on model levels"
    do iz = 2, cfg%lev
      write(15,'(i4,150e14.4)') iz,(fladrol(ix,iz),ix=cfg%il1,cfg%il2)
    end do

    write(15,*)
    write(15,*) "All-sky upward thermal fluxes on model levels"
    do iz = 2, cfg%lev
      write(15,'(i4,150e14.4)') iz,(flaurol(ix,iz),ix=cfg%il1,cfg%il2)
    end do

    write(15,*)
    write(15,*) "All-sky downward solar fluxes on model levels"
    do iz = 2, cfg%lev
      write(15,'(i4,150e14.4)') iz,(fsadrol(ix,iz),ix=cfg%il1,cfg%il2)
    end do

    write(15,*)
    write(15,*) "All-sky upward solar fluxes on model levels"
    do iz = 2, cfg%lev
      write(15,'(i4,150e14.4)') iz,(fsaurol(ix,iz),ix=cfg%il1,cfg%il2)
    end do

    write(15,*) "Pressure on model layers (layer mid-points)"
    do iz = 1, cfg%ilev
      write(15,'(i4,150e14.4)') iz,(p_layer(ix,iz),ix=cfg%il1,cfg%il2)
    end do

    write(15,*)
    write(15,*) "All-sky thermal heating rates for model layers"
    do iz = 1, cfg%ilev
      write(15,'(i4,150e14.4)') iz,(hrlrow(ix,iz),ix=cfg%il1,cfg%il2)
    end do
            
    return
  end subroutine write_full_gcm_output
  
  subroutine write_canam_output(outdir,      & ! Input
                                outfilename, &
                                cfg          )
    !>\file
    !>\brief Create and write the output typically used in CanAM.
    
    use parameters_mod, only : maxlen_char
    use read_config_file_mod, only : cfg_values
    use arrays_mod, only : fladrol, flcdrol, &
                           flaurol, flcurol, &
                           fsadrol, fscdrol, &
                           fsaurol, fscurol, &
                           hrlrow,  hrlcrow, &
                           hrsrow, hrscrow
    use netcdf
    
    implicit none

    ! Input
    character(len=maxlen_char), intent(in) :: outdir      !< Output directory
    character(len=maxlen_char), intent(in) :: outfilename !< Output filename
        
    type(cfg_values), intent(in) :: cfg !< Configuration
    
    ! Local
    character(len=maxlen_char) :: ofilename ! Output filename

    integer :: ncid             ! netcdf file ID
    integer :: lev_dimid        ! ID for level dimension
    integer :: lay_dimid        ! ID for layers dimension
    integer :: lat_dimid        ! ID for latitude dimension
    integer :: lon_dimid        ! ID for longitude dimension
    integer :: rld_varid        ! ID for downward thermal flux profiles
    integer :: rldc_varid       ! ID for downward clear-sky thermal flux profiles
    integer :: rlu_varid        ! ID for upward thermal flux profiles
    integer :: rluc_varid       ! ID for upward clear-sky thermal flux profiles
    integer :: rsd_varid        ! ID for downward solar flux profiles
    integer :: rsdc_varid       ! ID for downward clear-sky solar flux profiles
    integer :: rsu_varid        ! ID for upward solar flux profiles
    integer :: rsuc_varid       ! ID for upward clear-sky solar flux profiles
    integer :: hrl_varid        ! ID for thermal heating profiles
    integer :: hrlc_varid       ! ID for thermal clear-sky heating profiles
    integer :: hrs_varid        ! ID for thermal heating profiles
    integer :: hrsc_varid       ! ID for thermal clear-sky heating profiles
    
    integer, dimension(3) :: dimids
            
    real, dimension(:,:,:), allocatable :: write_flux ! Reshaped variable with results to write to file
    real, dimension(:,:,:), allocatable :: write_hr   ! Reshaped variable with results to write to file
    
    !==================================================================
    ! Create the netcdf file for output with some basic information.
    write(ofilename,'(a,a,a)') trim(outdir),'/',trim(outfilename)
    
    call check(nf90_create(trim(ofilename), nf90_clobber, ncid))
    
    ! Add dimensions
    call check(nf90_def_dim(ncid,'levels', cfg%lev, lev_dimid))
    call check(nf90_def_dim(ncid,'layers', cfg%ilev, lay_dimid))
    call check(nf90_def_dim(ncid,'latitude', cfg%nlat, lat_dimid))
    call check(nf90_def_dim(ncid,'longitude', cfg%il2, lon_dimid))
    
    dimids = (/ lat_dimid, lon_dimid, lev_dimid /)

    call check(nf90_def_var(ncid, "rld", nf90_float, dimids, rld_varid))
    call check(nf90_def_var(ncid, "rldc", nf90_float, dimids, rldc_varid))

    call check(nf90_def_var(ncid, "rlu", nf90_float, dimids, rlu_varid))
    call check(nf90_def_var(ncid, "rluc", nf90_float, dimids, rluc_varid))

    call check(nf90_def_var(ncid, "rsd", nf90_float, dimids, rsd_varid))
    call check(nf90_def_var(ncid, "rsdc", nf90_float, dimids, rsdc_varid))

    call check(nf90_def_var(ncid, "rsu", nf90_float, dimids, rsu_varid))
    call check(nf90_def_var(ncid, "rsuc", nf90_float, dimids, rsuc_varid))

    dimids = (/ lat_dimid, lon_dimid, lay_dimid /)
    call check(nf90_def_var(ncid, "hrl", nf90_float, dimids, hrl_varid))
    call check(nf90_def_var(ncid, "hrlc", nf90_float, dimids, hrlc_varid))

    call check(nf90_def_var(ncid, "hrs", nf90_float, dimids, hrs_varid))
    call check(nf90_def_var(ncid, "hrsc", nf90_float, dimids, hrsc_varid))

    call check(nf90_enddef(ncid))

    ! Reshape the results so they can be written to the file.
    ! Eecall that the calculations are a 2d array (column versus "height") where
    ! the columns are defined all experiments for site 1, all experiments for site 2,
    ! and so on for all sites.
    
    allocate(write_flux(cfg%nlat,cfg%il2,cfg%lev), &
             write_hr(cfg%nlat,cfg%il2,cfg%ilev))

    call write_flux_canam(write_flux,fladrol,rld_varid,ncid,cfg)
    call write_flux_canam(write_flux,flcdrol,rldc_varid,ncid,cfg)
    call write_flux_canam(write_flux,flaurol,rlu_varid,ncid,cfg)
    call write_flux_canam(write_flux,flcurol,rluc_varid,ncid,cfg)
    call write_flux_canam(write_flux,fsadrol,rsd_varid,ncid,cfg)
    call write_flux_canam(write_flux,fscdrol,rsdc_varid,ncid,cfg)
    call write_flux_canam(write_flux,fsaurol,rsu_varid,ncid,cfg)
    call write_flux_canam(write_flux,fscurol,rsuc_varid,ncid,cfg)

    call write_hr_canam(write_hr,hrlrow,hrl_varid,ncid,cfg)
    call write_hr_canam(write_hr,hrlcrow,hrlc_varid,ncid,cfg)    
    call write_hr_canam(write_hr,hrsrow,hrs_varid,ncid,cfg)
    call write_hr_canam(write_hr,hrscrow,hrsc_varid,ncid,cfg)    
                
    ! Close the netcdf file.
    call check(nf90_close(ncid))
    
    deallocate(write_flux, &
               write_hr)
    return
  end subroutine write_canam_output

  subroutine check(status)

    use netcdf

    implicit none

    integer, intent(in) :: status !< Status of call to netCDF function

    if(status /= nf90_noerr) then 
       write(6,'(a,i5)') 'netcdf library error code ',status
       write(6,*) trim(adjustl(nf90_strerror(status)))
       stop 
    end if
    
    return
    
  end subroutine check

  subroutine write_flux_canam(data_write, &
                              data_in, &
                              varid, &
                              ncid, &
                              cfg )
                              
  ! Reshape the data from the radiative transfer calculations, then write to 
  ! netcdf file.
  
  use read_config_file_mod, only : cfg_values
  use netcdf
  
  implicit none
  
  ! Input
  real, dimension(:,:,:), intent(inout) :: data_write !< Reshaped data that will be written to file.
  real, dimension(:,:), intent(in) :: data_in         !< Unraveled data that needs to be reshaped.
  
  integer, intent(in) :: varid !< NetCDF variable ID
  integer, intent(in) :: ncid  !< NetCDF file ID
  
  type(cfg_values), intent(in) :: cfg !< Configuration  
  
  ! Local
  integer :: ilat     ! Counter over latitudes
  integer :: ilon     ! Counter over longitudes
  integer :: ilev     ! Counter over vertical levels
  integer :: il       ! Counter over columns
  integer :: ilev_loc ! Counter over vertical levels
    
  ilev_loc = 1
  do ilev = 2, cfg%lev+1 ! The topmost level is TOA, the second topmost is top of model which is what we want
    il = 1
    do ilon = 1, cfg%il2
      do ilat = 1, cfg%nlat
        data_write(ilat,ilon,ilev_loc) = data_in(il,ilev)
        il = il + 1
      end do ! ilat
    end do ! ilon
    ilev_loc = ilev_loc + 1
  end do ! ilev  

  call check(nf90_put_var(ncid,varid,data_write))

  return
end subroutine write_flux_canam

  subroutine write_hr_canam(data_write, &
                            data_in, &
                            varid, &
                            ncid, &
                            cfg )
                              
  ! Reshape the data from the radiative transfer calculations, then write to 
  ! netcdf file.
  
  use read_config_file_mod, only : cfg_values
  use netcdf
  
  implicit none
  
  ! input
  real, dimension(:,:,:), intent(inout) :: data_write !< Reshaped data that will be written to file.
  real, dimension(:,:), intent(in) :: data_in         !< Unraveled data that needs to be reshaped.
  
  integer, intent(in) :: varid !< NetCDF variable ID
  integer, intent(in) :: ncid  !< NetCDF file ID
  
  type(cfg_values), intent(in) :: cfg !< Configuration  
  
  ! local
  integer :: ilat     ! Counter over latitudes
  integer :: ilon     ! Counter over longitudes
  integer :: ilay     ! Counter over vertical layers
  integer :: il       ! Counter over columns

  do ilay = 1, cfg%ilev
    il = 1
    do ilon = 1, cfg%il2
      do ilat = 1, cfg%nlat
        data_write(ilat,ilon,ilay) = data_in(il,ilay)
        il = il + 1
      end do ! ilat
    end do ! ilon
  end do ! ilay
    
  call check(nf90_put_var(ncid,varid,data_write))

  return
end subroutine write_hr_canam

end module write_output_mod

!> \file
!> Routines to write the outputs from the radiative transfer 
!! calculations.
