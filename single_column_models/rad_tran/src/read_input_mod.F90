!>\file
!>\brief Routines to read in the inputs needed for the radiative transfer 
!! calculations.
!!
!! @author Jason Cole
!

module read_input_mod

  implicit none
  
  private :: read_rfmip_input
  private :: read_rfmip_ghg
  private :: prep_rfmip_data
  private :: check
  private :: read_full_gcm_input
  private :: read_full_global_gcm_input
  
contains
  
  
  subroutine read_input_data(infilename, & ! input
                             cfg         )
    
    use parameters_mod, only : maxlen_char
    
    use read_config_file_mod, only : cfg_values
    
    implicit none
    
    ! input
    type(cfg_values), intent(in) :: cfg !< Configuration
    character(len=maxlen_char), intent(in) :: infilename !< Input filename
    
    if (cfg%input_type == 1) then ! RFMIP type input
      ! Read in the data from the netcdf file
      call read_rfmip_input(infilename, & ! Input
                            cfg         )

      ! As needed adjust and derive fields from that read in.
      call prep_rfmip_data(cfg) ! Input
    elseif(cfg%input_type == 2) then ! Gcm input from right before raddriv
      ! read in the data from the ascii file
      call read_full_gcm_input(infilename, & ! Input
                               cfg         )
    elseif(cfg%input_type == 3) then ! Global gcm input from right before raddriv
      ! read in the data from the netcdf file
      call read_full_global_gcm_input(infilename, & ! Input
                                      cfg         )                               
    endif ! input_type
    
    return
    
  end subroutine read_input_data

  subroutine read_rfmip_input(infilename, &
                              cfg)
    
    ! Read in the data for the CMIP6/RFMIP profiles
    ! The data contains a number of sites and a number of experiments for each
    ! site.  The profiles for experiments and sites will be "unfolded" into the
    ! single column number dimension and manipulated as needed for the output.
    
    use arrays_mod
    use parameters_mod, only : maxlen_char, &
                               exit_code_read_input, &
                               deg2rad, &
                               r_zero
                               
    use read_config_file_mod, only : cfg_values
    use netcdf
    
    implicit none
    
    ! input
    type(cfg_values), intent(in) :: cfg !< Configuration

    character(len=maxlen_char), intent(in) :: infilename !< Input filename
    
    ! local
    integer :: ilg     ! Number of profiles
    integer :: n_lay   ! number of vertical layers
    integer :: n_lev   ! Number of vertical layer interfaces
    integer :: n_site  ! Number of sites
    integer :: n_expt  ! Number of experiments
    
    integer :: i_site  ! Counter over site
    integer :: i_expt  ! Counter over experiments
    integer :: il      ! Counter over profiles
    integer :: i_lay   ! Counter over layers
    integer :: i_lev   ! Counter over levels
    
    integer :: ncid ! netcdf file id
    integer :: varid ! netcdf file id
    
    real, allocatable, dimension(:)     :: data_1d
    real, allocatable, dimension(:,:)   :: data_2d
    real, allocatable, dimension(:,:,:) :: data_3d
    
    character(len=maxlen_char) :: varname
    
    ! Open the input file.
    
    call check(nf90_open(trim(infilename), nf90_nowrite, ncid))
    
    ! First read in the dimensions, checking that they match those set in the 
    ! configuration file.  If not exit the program.
    
    call check(nf90_inq_dimid(ncid,"expt",  varid))
    call check(nf90_inquire_dimension(ncid, varid, len = n_expt))
    
    call check(nf90_inq_dimid(ncid,"site",  varid))
    call check(nf90_inquire_dimension(ncid, varid, len = n_site))
    
    call check(nf90_inq_dimid(ncid,"level",  varid))
    call check(nf90_inquire_dimension(ncid, varid, len = n_lev))
    
    call check(nf90_inq_dimid(ncid,"layer",  varid))
    call check(nf90_inquire_dimension(ncid, varid, len = n_lay))
    
    ilg = n_expt*n_site

    if (n_site /= cfg%num_profile) then ! Inconsistent number of profiles between config and data
      write(*,*) 'number of profiles in data ',n_site
      write(*,*) 'number columns in configuration ', cfg%num_profile
      write(*,*) 'exiting the program.'
      stop exit_code_read_input
    end if

    if (n_expt /= cfg%num_experiment) then ! Inconsistent number of experiments per profile between config and data
      write(*,*) 'number of experiments per profiles in data ',n_expt
      write(*,*) 'number of experiments per profiles in configuration ', cfg%num_experiment
      write(*,*) 'exiting the program.'
      stop exit_code_read_input
    end if
    
    if (n_lay /= cfg%ilev) then ! Inconsistent number of layers between config and data
      write(*,*) 'number of layers in data ',n_lay
      write(*,*) 'number layers in configuration ', cfg%ilev
      write(*,*) 'exiting the program.'
      stop exit_code_read_input
    end if

    if (n_lev /= cfg%lev) then ! Inconsistent number of levels (layer interfaces) between config and data
      write(*,*) 'number of levels in data ',n_lev
      write(*,*) 'number levels in configuration ', cfg%lev
      write(*,*) 'exiting the program.'
      stop exit_code_read_input
    end if
    
    ! Now read in the data.
    
    ! The input data will be read in by looping over each site and setting up the 
    ! experiments, then move onto the next one, and so on for all sites and experiments.
    
    ! When writing the results the columns will be "unraveled".
       
    ! Read in the single level data for sites.
    allocate(data_1d(n_site))
    
    ! Surface albedo.  it is spectrally constant for rfmip.
    
    call check(nf90_inq_varid(ncid, "surface_albedo",  varid))
    call check(nf90_get_var(ncid, varid, data_1d))
    
    il = 1
    do i_site = 1, n_site
      do i_expt = 1, n_expt
        salbrol(il,:)   = data_1d(i_site)
        csalrol(il,:)   = data_1d(i_site)
        salbrot(il,:,:) = data_1d(i_site)
        csalrot(il,:,:) = data_1d(i_site)
        il = il + 1
      end do ! i_expt
    end do ! i_site
  
    ! Surface emissivity.  It is spectrally constant for rfmip.
    
    call check(nf90_inq_varid(ncid, "surface_emissivity",  varid))
    call check(nf90_get_var(ncid, varid, data_1d))
        
    il = 1
    do i_site = 1, n_site
      do i_expt = 1, n_expt
        emisrow(il)   = data_1d(i_site)
        emisrot(il,:) = data_1d(i_site)
        il = il + 1
      end do ! i_expt
    end do ! i_site
        
    ! Cosine of solar zenith angle.
    
    call check(nf90_inq_varid(ncid, "solar_zenith_angle",  varid))
    call check(nf90_get_var(ncid, varid, data_1d))
    ! Compute the cosine of the solar zenith angle
    data_1d = cos(deg2rad*data_1d)
    
    il = 1
    do i_site = 1, n_site
      do i_expt = 1, n_expt
        cszrow(il)   = data_1d(i_site)
        il = il + 1
      end do ! i_expt
    end do ! i_site

    ! Make sure csz >= 0.0
    where (cszrow(:) < r_zero)
      cszrow(:) = r_zero
    end where
    
    ! Total solar irradiance
    
    call check(nf90_inq_varid(ncid, "total_solar_irradiance",  varid))
    call check(nf90_get_var(ncid, varid, data_1d))
    
    il = 1
    do i_site = 1, n_site
      do i_expt = 1, n_expt
        tsi(il)   = data_1d(i_site)
        il = il + 1
      end do ! i_expt
    end do ! i_site
    
    ! Deallocate the 1d array used to read in the single level site data
    deallocate(data_1d)
    
    ! Now the 1d data that is defined at a single level for each experiment.
    ! This is mostly global mean ghgs.
    
    ! CO2
    varname="carbon_dioxide_GM"
    call read_rfmip_ghg(co2rox, varname, ncid, ilg, n_lay, n_site, n_expt)
    co2rox = co2rox*1.5188126
                            
    ! CH4
    varname="methane_GM"
    call read_rfmip_ghg(ch4rox, varname, ncid, ilg, n_lay, n_site, n_expt)
    ch4rox = ch4rox*0.5522955
    
    ! N2O
    varname="nitrous_oxide_GM"
    call read_rfmip_ghg(n2orox, varname, ncid, ilg, n_lay, n_site, n_expt)
    n2orox = n2orox*1.5188126
    
    ! CFC11 (this is "effective" cfc11 in the CanAM so it will also the that here.)
    varname="cfc11eq_GM"
    call read_rfmip_ghg(f11rox, varname, ncid, ilg, n_lay, n_site, n_expt)
    f11rox = f11rox*4.7418019
    
    ! CFC12
    varname="cfc12_GM"
    call read_rfmip_ghg(f12rox, varname, ncid, ilg, n_lay, n_site, n_expt)
    f12rox = f12rox*4.1736279
    
    ! CFC113
    varname="cfc113_GM"
    call read_rfmip_ghg(f113rox, varname, ncid, ilg, n_lay, n_site,n_expt)

    ! CFC114
    varname="cfc114_GM"
    call read_rfmip_ghg(f114rox, varname, ncid, ilg, n_lay, n_site,n_expt)
    
    ! Now read in the 2d data
    
    ! First data on levels
    allocate(data_2d(n_lev,n_site))
    
    ! Pressure at levels and surface (taken to be the bottom most level)
    call check(nf90_inq_varid(ncid, "pres_level",  varid))
    call check(nf90_get_var(ncid, varid, data_2d))

    il = 1
    do i_site = 1, n_site
      do i_expt = 1, n_expt
        pressg(il)   = data_2d(n_lev,i_site)
        il = il + 1
      end do ! i_expt
    end do ! i_site
    
    ! The radiative transfer model takes in shtj as the pressure/surface pressure
    ! and uses it to compute the pressure at the levels in the code.  Do the
    ! scaling here.
    do i_lev = 1, n_lev
      il = 1
      do i_site = 1, n_site
        do i_expt = 1, n_expt
          shtj(il,i_lev)   = data_2d(i_lev,i_site)/pressg(il)
          il = il + 1
        end do ! i_expt
      end do ! i_site
    end do ! i_lev
        
    deallocate(data_2d)
    
    ! Now data on layers
    allocate(data_2d(n_lay,n_site))
    
    ! Pressure on model layers
    call check(nf90_inq_varid(ncid, "pres_layer",  varid))
    call check(nf90_get_var(ncid, varid, data_2d))

    ! The radiative transfer model takes in shj as the pressure/surface pressure
    ! and uses it to compute the pressure at the levels in the code.  Do the
    ! scaling here.
    do i_lay = 1, n_lay
      il = 1
      do i_site = 1, n_site
        do i_expt = 1, n_expt
          shj(il,i_lay)   = data_2d(i_lay,i_site)/pressg(il)
          il = il + 1
        end do ! i_expt
      end do ! i_site
    end do ! i_lay

    deallocate(data_2d)
    
    ! Surface skin temperature
    allocate(data_2d(n_site,n_expt))
    
    call check(nf90_inq_varid(ncid, "surface_temperature",  varid))
    call check(nf90_get_var(ncid, varid, data_2d))

    il = 1
    do i_site = 1, n_site
      do i_expt = 1, n_expt
        gtrow(il)   = data_2d(i_site,i_expt)
        gtrot(il,:) = data_2d(i_site,i_expt)
        il = il + 1
      end do ! i_expt
    end do ! i_site

    deallocate(data_2d)
    
    ! now the 3d data 
    
    ! first the temperature on each level
    
    allocate(data_3d(n_lev,n_site,n_expt))
    
    call check(nf90_inq_varid(ncid, "temp_level",  varid))
    call check(nf90_get_var(ncid, varid, data_3d))

    do i_lev = 1, n_lev
      il = 1
      do i_site = 1, n_site
        do i_expt = 1, n_expt
          tfrow(il,i_lev)   = data_3d(i_lev,i_site,i_expt)
          il = il + 1
        end do ! i_expt
      end do ! i_site
    end do ! i_lev
    
    deallocate(data_3d)
    
    ! Note some 3d fields on layers
    allocate(data_3d(n_lay,n_site,n_expt))
    
    call check(nf90_inq_varid(ncid, "temp_layer",  varid))
    call check(nf90_get_var(ncid, varid, data_3d))

    ! Note that the temperature on levels is passed into raddriv using the variable
    ! unotnd due to variable reuse.  This really needs to be fixed...
    do i_lay = 1, n_lay
      il = 1
      do i_site = 1, n_site
        do i_expt = 1, n_expt
          unotnd(il,i_lay)   = data_3d(i_lay,i_site,i_expt)
          il = il + 1
        end do ! i_expt
      end do ! i_site
    end do ! i_lay

    ! Ozone
    call check(nf90_inq_varid(ncid, "ozone",  varid))
    call check(nf90_get_var(ncid, varid, data_3d))

    do i_lay = 1, n_lay
      il = 1
      do i_site = 1, n_site
        do i_expt = 1, n_expt
          ozphs(il,i_lay)   = data_3d(i_lay,i_site,i_expt)*(48. / 28.97)
          il = il + 1
        end do ! i_expt
      end do ! i_site
    end do ! i_lay

    ! Water vapour
    call check(nf90_inq_varid(ncid, "water_vapor",  varid))
    call check(nf90_get_var(ncid, varid, data_3d))

    do i_lay = 1, n_lay
      il = 1
      do i_site = 1, n_site
        do i_expt = 1, n_expt
          qc(il,i_lay)   = data_3d(i_lay,i_site,i_expt)
          il = il + 1
        end do ! i_expt
      end do ! i_site
    end do ! i_lay
    
    deallocate(data_3d)
    
    ! close the input file
    call check(nf90_close(ncid))
    
    return
    
  end subroutine read_rfmip_input

  subroutine read_full_gcm_input(infilename, &
                                 cfg)
    
    ! Read in the ascii data print out right before raddriv in canam.
    ! These were written with a formatted write which we will use here exactly.
    ! It should be possible to read most, if not all, of the data directly into
    ! the various arrays.
    
    use arrays_mod
    use parameters_mod, only : maxlen_char, &
                               exit_code_read_input, &
                               deg2rad, &
                               r_zero, &
                               nbs,    &
                               nbl
                               
    use read_config_file_mod, only : cfg_values
    
    use sdet_mod, only : rrsq
    
    implicit none
    
    ! Input
    type(cfg_values), intent(in) :: cfg !< Configuration

    character(len=maxlen_char), intent(in) :: infilename !< Input filename

    ! Local
    integer :: ix
    integer :: iz
    integer :: icol
    integer :: iband
    integer :: itile
    integer :: ijunk

    ! Open the ascii file
    open(unit=15,file=trim(infilename))
    
    ! Read the data on levels
    read(15,*)
    do iz = 1, cfg%lev
      do ix = 1, cfg%ilg !cfg%il1, cfg%il2
        read(15,*) ijunk, ijunk, shtj(ix,iz),tfrow(ix,iz)
      end do
    end do
    
    ! Read the data on layers
    read(15,*)
    read(15,*)    
    do iz = 1, cfg%ilev
      do ix = 1, cfg%ilg !cfg%il1, cfg%il2
        read(15,'(2i4,2e14.7,2f10.3,11e14.7,2f10.3)') ijunk, ijunk, &
                shj(ix,iz), dshj(ix,iz), dz(ix,iz), unotnd(ix,iz),  &
                ozphs(ix,iz), qc(ix,iz),                            &
                co2rox(ix,iz), ch4rox(ix,iz), n2orox(ix,iz),        &
                f11rox(ix,iz), f12rox(ix,iz), f113rox(ix,iz), f114rox(ix,iz), &
                ccld(ix,iz), rhc(ix,iz), anu(ix,iz), eta(ix,iz)
      end do 
    end do
    
    ! Read in the random number seeds
    read(15,*)
    do ix = 1, cfg%ilg !cfg%il1, cfg%il2
      read(15,'(i4,4i14)') ijunk, (iseedrow(ix,iz),iz=1,4)
    end do
        
    ! Read in the aerosol information
    read(15,*)
    do iz = 1, cfg%ilev
      do ix = 1, cfg%ilg !cfg%il1, cfg%il2
        read(15,'(2i4,9e14.7)') ijunk,ijunk,(aerin(ix,iz,icol),icol=1,9)
      end do
    end do
    
    ! Read in the stratospheric aerosols
    read(15,*)
    do iz = 1, cfg%ilev
      do ix = 1, cfg%ilg !cfg%il1, cfg%il2
        read(15,'(2i4,12e14.7)') ijunk,ijunk,                          &
                                 (sw_ext_sa(ix,iz,iband),iband=1,nbs), &
                                 (sw_ssa_sa(ix,iz,iband),iband=1,nbs), &
                                 (sw_g_sa(ix,iz,iband),iband=1,nbs)
       end do
     end do

    read(15,*)     
    do iz = 1, cfg%ilev
      do ix = 1, cfg%ilg !cfg%il1, cfg%il2
        read(15,'(2i4,9e14.7)') ijunk,ijunk,&
                                (lw_abs_sa(ix,iz,iband),iband=1, nbl)
      end do
    end do
    
    ! Read surface data that is not on tiles
    read(15,*)
    do ix = 1, cfg%ilg !cfg%il1, cfg%il2
       read(15,'(i4,2f10.2,e14.7,5e14.7,i4)') ijunk,           &
           pressg(ix), gtrow(ix), oztop(ix), cszrow(ix),       &
           vtaurow(ix), troprow(ix), emisrow(ix), cldtrol(ix), &
           ncldy(ix)
    end do
    
    read(15,*)    
    do ix = 1, cfg%ilg !cfg%il1, cfg%il2
       read(15,'(i4,8e14.7)') ijunk,                           &
                             (salbrol(ix,iband),iband=1, nbs), &
                             (csalrol(ix,iband),iband=1, nbs)
    end do
    
    ! Read surface data that is on tiles
    read(15,*)
    do ix = 1, cfg%ilg !cfg%il1, cfg%il2
        read(15,'(i4,18e14.7)') ijunk,                             &
                               (emisrot(ix,itile),itile=1,cfg%im), &
                               (gtrot(ix,itile),itile=1,cfg%im),   &
                               (farerot(ix,itile),itile=1,cfg%im)
    end do
    
    read(15,*)
    do ix = 1, cfg%ilg !cfg%il1, cfg%il2
      do itile = 1, cfg%im
        read(15,'(2i4,8e14.7)') ijunk, ijunk,                          &
                               (salbrot(ix,itile,iband),iband=1, nbs), &
                               (csalrot(ix,itile,iband),iband=1, nbs)
      end do
    end do

    ! Read in the subcolumn data
    read(15,*)
    do iz = 1, cfg%ilev
      do ix = 1, cfg%ilg !cfg%il1, cfg%il2
        read(15,'(2i4,300f10.3,300e14.7)') ijunk, ijunk,                  &
                                  (rel_sub(ix,iz,icol),icol=1,cfg%nxloc), &
                                  (rei_sub(ix,iz,icol),icol=1,cfg%nxloc), &
                                  (clw_sub(ix,iz,icol),icol=1,cfg%nxloc), &
                                  (cic_sub(ix,iz,icol),icol=1,cfg%nxloc)
      end do
    end do

    ! There are a few scalars needed for the radiative transfer but we should set
    ! them to appropriate default values here.

    kount = 0
    jlat  = 0
    solv  = 0.0
    rrsq  = 1.0
        
    close(15)
    
    return
  end subroutine read_full_gcm_input
  
  subroutine read_full_global_gcm_input(infilename, &
                                        cfg)
    
    ! Read in the data print out right before raddriv in canam.
    ! This information is all in a netcdf file with all of the
    ! data for the globe.
    ! The data is lat/lon but will be "unravelled" so it is just over
    ! gcm columns.
    
    use arrays_mod
    use parameters_mod, only : maxlen_char, &
                               exit_code_read_input, &
                               deg2rad, &
                               r_zero, &
                               nbs,    &
                               nbl
                               
    use sdet_mod, only : rrsq
    
    use read_config_file_mod, only : cfg_values
    use netcdf
    
    implicit none
    
    ! Input
    type(cfg_values), intent(in) :: cfg !< Configuration

    character(len=maxlen_char), intent(in) :: infilename !< Input filename

    ! Local
    integer :: ix
    integer :: iz
    integer :: ilat
    integer :: ilon
    integer :: icol
    integer :: iband
    integer :: itile
    integer :: ijunk

    integer :: ncid ! netcdf file id
    integer :: varid ! netcdf file id

    real, allocatable, dimension(:,:)   :: data_2d
    integer, allocatable, dimension(:,:,:) :: idata_3d
    
    character(len=maxlen_char) :: varname

    ! Open the input file.
    
    call check(nf90_open(trim(infilename), nf90_nowrite, ncid))
    
    ! Read in the 2d data
    allocate(data_2d(cfg%nlat,cfg%il2))
    
    call check(nf90_inq_varid(ncid, "pressg",  varid))
    call check(nf90_get_var(ncid, varid, data_2d))
    pressg = reshape(data_2d,(/cfg%nlat*cfg%il2/))

    call check(nf90_inq_varid(ncid, "gtrow",  varid))
    call check(nf90_get_var(ncid, varid, data_2d))
    gtrow = reshape(data_2d,(/cfg%nlat*cfg%il2/))
        
    call check(nf90_inq_varid(ncid, "oztop",  varid))
    call check(nf90_get_var(ncid, varid, data_2d))
    oztop = reshape(data_2d,(/cfg%nlat*cfg%il2/))

    call check(nf90_inq_varid(ncid, "cszrow",  varid))
    call check(nf90_get_var(ncid, varid, data_2d))
    cszrow = reshape(data_2d,(/cfg%nlat*cfg%il2/))

    call check(nf90_inq_varid(ncid, "vtaurow",  varid))
    call check(nf90_get_var(ncid, varid, data_2d))
    vtaurow = reshape(data_2d,(/cfg%nlat*cfg%il2/))

    call check(nf90_inq_varid(ncid, "troprow",  varid))
    call check(nf90_get_var(ncid, varid, data_2d))
    troprow = reshape(data_2d,(/cfg%nlat*cfg%il2/))

    call check(nf90_inq_varid(ncid, "emisrow",  varid))
    call check(nf90_get_var(ncid, varid, data_2d))
    emisrow = reshape(data_2d,(/cfg%nlat*cfg%il2/))

    call check(nf90_inq_varid(ncid, "cldtrol",  varid))
    call check(nf90_get_var(ncid, varid, data_2d))
    cldtrol = reshape(data_2d,(/cfg%nlat*cfg%il2/))

    call check(nf90_inq_varid(ncid, "ncldy",  varid))
    call check(nf90_get_var(ncid, varid, data_2d))
    ncldy = reshape(data_2d,(/cfg%nlat*cfg%il2/))

    deallocate(data_2d)
    
    ! Now the 3d data
    varname="shtj"
    call unravel_3d(shtj,ncid,varname,cfg%nlat,cfg%il2,cfg%lev)

    varname="tfrow"
    call unravel_3d(tfrow,ncid,varname,cfg%nlat,cfg%il2,cfg%lev)
    
    varname="shj"
    call unravel_3d(shj,ncid,varname,cfg%nlat,cfg%il2,cfg%ilev)

    varname="dshj"
    call unravel_3d(dshj,ncid,varname,cfg%nlat,cfg%il2,cfg%ilev)

    varname="dz"
    call unravel_3d(dz,ncid,varname,cfg%nlat,cfg%il2,cfg%ilev)

    varname="tlayer"
    call unravel_3d(unotnd,ncid,varname,cfg%nlat,cfg%il2,cfg%ilev)

    varname="ozphs"
    call unravel_3d(ozphs,ncid,varname,cfg%nlat,cfg%il2,cfg%ilev)

    varname="qc"
    call unravel_3d(qc,ncid,varname,cfg%nlat,cfg%il2,cfg%ilev)

    varname="co2rox"
    call unravel_3d(co2rox,ncid,varname,cfg%nlat,cfg%il2,cfg%ilev)
    
    varname="ch4rox"
    call unravel_3d(ch4rox,ncid,varname,cfg%nlat,cfg%il2,cfg%ilev)

    varname="n2orox"
    call unravel_3d(n2orox,ncid,varname,cfg%nlat,cfg%il2,cfg%ilev)

    varname="f11rox"
    call unravel_3d(f11rox,ncid,varname,cfg%nlat,cfg%il2,cfg%ilev)

    varname="f113rox"
    call unravel_3d(f113rox,ncid,varname,cfg%nlat,cfg%il2,cfg%ilev)

    varname="f114rox"
    call unravel_3d(f114rox,ncid,varname,cfg%nlat,cfg%il2,cfg%ilev)

    varname="ccld"
    call unravel_3d(ccld,ncid,varname,cfg%nlat,cfg%il2,cfg%ilev)

    varname="rhc"
    call unravel_3d(rhc,ncid,varname,cfg%nlat,cfg%il2,cfg%ilev)
    
    varname="anu"
    call unravel_3d(anu,ncid,varname,cfg%nlat,cfg%il2,cfg%ilev)

!    varname="eta"
!    call unravel_3d(eta,ncid,varname,cfg%nlat,cfg%il2,cfg%ilev)
    eta = 0.0
    
    varname="salbrol"
    call unravel_3d(salbrol,ncid,varname,cfg%nlat,cfg%il2,nbs)

    varname="csalrol"
    call unravel_3d(csalrol,ncid,varname,cfg%nlat,cfg%il2,nbs)
    
    varname="emisrot"
    call unravel_3d(emisrot,ncid,varname,cfg%nlat,cfg%il2,cfg%im)

    varname="gtrot"
    call unravel_3d(gtrot,ncid,varname,cfg%nlat,cfg%il2,cfg%im)

    varname="farerot"
    call unravel_3d(farerot,ncid,varname,cfg%nlat,cfg%il2,cfg%im)
        
    ! iseed
    allocate(idata_3d(cfg%nlat,cfg%il2,4))
    
    varname="iseed"
    call check(nf90_inq_varid(ncid, trim(varname), varid))
    call check(nf90_get_var(ncid, varid, idata_3d))
  
    do icol = 1, 4
      ix = 1
      do ilon = 1, cfg%il2
        do ilat = 1, cfg%nlat
          iseedrow(ix,icol) = idata_3d(ilat,ilon,icol)
          ix = ix + 1
        end do
      end do
    end do
    
    ! Read in the 4d data
    varname="aerin"
    call unravel_4d(aerin,ncid,varname,cfg%nlat,cfg%il2,cfg%ilev,cfg%idimr)

    varname="sw_ext_sa"
    call unravel_4d(sw_ext_sa,ncid,varname,cfg%nlat,cfg%il2,cfg%ilev,nbs)

    varname="sw_ssa_sa"
    call unravel_4d(sw_ssa_sa,ncid,varname,cfg%nlat,cfg%il2,cfg%ilev,nbs)

    varname="sw_g_sa"
    call unravel_4d(sw_g_sa,ncid,varname,cfg%nlat,cfg%il2,cfg%ilev,nbs)

    varname="lw_abs_sa"
    call unravel_4d(lw_abs_sa,ncid,varname,cfg%nlat,cfg%il2,cfg%ilev,nbl)

    varname="salbrot"
    call unravel_4d(salbrot,ncid,varname,cfg%nlat,cfg%il2,cfg%im,nbs)

    varname="csalrot"
    call unravel_4d(csalrot,ncid,varname,cfg%nlat,cfg%il2,cfg%im,nbs)

    varname="rel_sub"
    call unravel_4d(rel_sub,ncid,varname,cfg%nlat,cfg%il2,cfg%ilev,cfg%nxloc)

    varname="rei_sub"
    call unravel_4d(rei_sub,ncid,varname,cfg%nlat,cfg%il2,cfg%ilev,cfg%nxloc)

    varname="clw_sub"
    call unravel_4d(clw_sub,ncid,varname,cfg%nlat,cfg%il2,cfg%ilev,cfg%nxloc)

    varname="cic_sub"
    call unravel_4d(cic_sub,ncid,varname,cfg%nlat,cfg%il2,cfg%ilev,cfg%nxloc)

    ! There are a few scalars needed for the radiative transfer but we should set
    ! them to appropriate default values here.

    kount = 0
    jlat  = 0
    solv  = 0.0
    rrsq  = 1.0
    
    ! Close the input file
    call check(nf90_close(ncid))
    
    return
  end subroutine read_full_global_gcm_input
  
  subroutine prep_rfmip_data(cfg) ! Input
    
    ! Prepare the various inputs needed for the radiative transfer.
    ! This will either manipulation of data read in or derivation of variables.
    ! If the variable is not set in the configuration, read in or set here then
    ! it has been zeroed in zero_arrays.
    
    use arrays_mod, only : shtj,    &
                           unotnd,  &
                           dshj,    &
                           dz,      &
                           qc,      &
                           farerot, &
                           kount,   &
                           solv,    &
                           jlat
                           
    use parameters_mod, only : r_one,    &
                               m_h2o,    &
                               m_dry_air
                               
    use read_config_file_mod, only : cfg_values
    
    use phys_consts, only : grav, rgas
    use sdet_mod, only : rrsq
    
    implicit none
    
    ! Input
    type(cfg_values), intent(in) :: cfg !< Configuration
    
    ! Local    
    integer :: il      ! counter over profiles
    integer :: i_lay   ! counter over layers
    integer :: i_lev   ! counter over levels
    
    real :: rog ! ratio of gas constant to gravity
    real :: sh ! specific humidity [g/g]
    
    ! Compute the layer thickness in sigma level and the geometric thickness
    rog = rgas/grav
    do i_lay = 1, cfg%ilev
      do il = 1, cfg%ilg
        dshj(il,i_lay) = shtj(il,i_lay+1)-shtj(il,i_lay)
        dz(il,i_lay) = rog * unotnd(il,i_lay) * log(shtj(il,i_lay+1)/shtj(il,i_lay))
      end do ! il
    end do  ! i_lay
    
    ! Adjust the units of the water vapour
    ! It is provided as molar fraction but the radiative transfer expects it in 
    ! specific humidity.
    
    do i_lay = 1, cfg%ilev
      do il = 1, cfg%ilg
        !qc(il,i_lay) = qc(il,i_lay)*0.6219
        sh = qc(il,i_lay)*0.6219
        qc(il,i_lay) = (qc(il,i_lay)*18.01534)&
                     / ((qc(il,i_lay)*18.01534)+(1.0-qc(il,i_lay))*28.9644)
      end do ! il
    end do  ! i_lay
    
    ! Set the fractional area within the grid to 1.0
    ! Set the number of subgrid tiles to 1
    farerot(:,:) = r_one
    
    ! There are a few scalars needed for the radiative transfer but not provided
    ! for rfmip so set them to appropriate values here.
    kount = 0
    jlat  = 0
    solv  = 0.0
    rrsq  = 1.0 
    
    return
  end subroutine prep_rfmip_data

  subroutine read_rfmip_ghg(ghg_data, & ! Output
                            varname,  & ! Input
                            ncid,     &
                            ilg,      &
                            n_lay,    &
                            n_site,   &
                            n_expt    )
                            
    ! Read in the ghg data for the cmip6/rfmip profiles
    ! the data contains a number of sites and a number of experiments for each
    ! site.  The profiles for experiments and sites will be "unfolded" into the
    ! single column number dimension and manipulated as needed for the output.
    
    use parameters_mod, only : maxlen_char
    use netcdf
    
    implicit none
    
    ! output
    real, dimension(ilg,n_lay), intent(out) :: ghg_data !< Array with all of the ghg profiles for all experiments and sites
    
    ! input
    character(len=maxlen_char), intent(in) :: varname !< Name of ghg variable to extract from input
    
    integer, intent(in) :: ncid   !< ID for the netcdf file with the ghgs
    integer, intent(in) :: n_lay  !< Number of layers in the ghg profile
    integer, intent(in) :: n_expt !< Number of experiments
    integer, intent(in) :: n_site !< Number of sites
    integer, intent(in) :: ilg    !< Number of columns (number of sites time number of experiments)
    
    ! local    
    integer :: i_site  ! Counter over site
    integer :: i_expt  ! Counter over experiments
    integer :: il      ! Counter over profiles
    integer :: i_lay   ! Counter over layers
    integer :: varid ! netcdf file id

    real :: ghg_scale_factor !< Factor by which to scale the mole fraction
    
    real, allocatable, dimension(:) :: data_1d
    
    character(len=maxlen_char) :: att_units ! Hold the units attribute values
    character(len=maxlen_char) :: att_name  ! Units attribute name
    
    allocate(data_1d(n_expt))
    
    call check(nf90_inq_varid(ncid, trim(varname),  varid))
    call check(nf90_get_var(ncid, varid, data_1d))
    att_name="units"
    call check(nf90_get_att(ncid, varid, att_name, att_units))
    read(att_units,*) ghg_scale_factor

    do i_lay = 1, n_lay
      il = 1
      do i_site = 1, n_site
        do i_expt = 1, n_expt
          ghg_data(il,i_lay)   = data_1d(i_expt)*ghg_scale_factor
          il = il + 1
        end do ! i_expt
      end do ! i_site
    end do ! i_lay

    deallocate(data_1d)
    
    return
  end subroutine read_rfmip_ghg

  subroutine check(status)

    use netcdf

    implicit none

    integer, intent(in) :: status !< Status of call to netCDF routine

    if(status /= nf90_noerr) then 
       write(6,'(a,i5)') 'netcdf library error code ',status
       write(6,*) trim(adjustl(nf90_strerror(status)))
       stop 
    end if
    
    return
    
  end subroutine check

  subroutine unravel_3d(data_out, &
                        ncid,     &
                        varname,  &
                        d1_in,    &
                        d2_in,    &
                        d3_in     )
                        
  ! Take 3D arrays that are lat,lon,dimension and unroll them to be
  ! 2D arrays that are lat*lon,dimension
  
  use netcdf
  implicit none
  
  ! input
  integer, intent(in) :: ncid  !< ID of netCDF file
  integer, intent(in) :: d1_in !< Size of first dimension of input array
  integer, intent(in) :: d2_in !< Size of second dimension of input array
  integer, intent(in) :: d3_in !< Size of third dimension of input array

  character(len=*), intent(in) :: varname !< Name of variable in the netCDF file

  ! Output
  real, dimension(:,:), intent(out) :: data_out !< The unraveled data (3D to 2D)
  
  ! Local
  integer :: varid
  integer :: ix
  integer :: id1, id2, id3
  
  real, dimension(d1_in,d2_in,d3_in) :: data_3d

  call check(nf90_inq_varid(ncid, trim(varname), varid))
  call check(nf90_get_var(ncid, varid, data_3d))
    
  do id3 = 1, d3_in
    ix = 1
    do id2 = 1, d2_in
      do id1 = 1, d1_in
        data_out(ix,id3) = data_3d(id1,id2,id3)
        ix = ix + 1
      end do
    end do
  end do
  
  return
  
  end subroutine unravel_3d

  subroutine unravel_4d(data_out, &
                        ncid,     &
                        varname,  &
                        d1_in,    &
                        d2_in,    &
                        d3_in,    &
                        d4_in     )
                        
  ! Take 4D arrays that are lat,lon,dimension3,dimension4 and unroll them to be
  ! 3D arrays that are lat*lon,dimension3,dimension4
  
  use netcdf
  implicit none
  
  ! input
  integer, intent(in) :: ncid   !< ID of netCDF file
  integer, intent(in) :: d1_in  !< Size of first dimension of input array
  integer, intent(in) :: d2_in  !< Size of second dimension of input array
  integer, intent(in) :: d3_in  !< Size of third dimension of input array
  integer, intent(in) :: d4_in  !< Size of fourth dimension of input array

  character(len=*), intent(in) :: varname !< Name of variable in the netCDF file

  ! output
  real, dimension(:,:,:), intent(out) :: data_out !< The unraveled data (4D to 3D)
  
  ! local
  integer :: varid
  integer :: ix
  integer :: id1, id2, id3, id4
  
  real, dimension(d1_in,d2_in,d3_in,d4_in) :: data_4d

  call check(nf90_inq_varid(ncid, trim(varname), varid))
  call check(nf90_get_var(ncid, varid, data_4d))
  
  do id4 = 1, d4_in
    do id3 = 1, d3_in
      ix = 1
      do id2 = 1, d2_in
        do id1 = 1, d1_in
          data_out(ix,id3,id4) = data_4d(id1,id2,id3,id4)
          ix = ix + 1
        end do
      end do
    end do
  end do
  
  return
  
end subroutine unravel_4d
  
end module read_input_mod

!> \file
!> Routines to read in the inputs needed for the radiative transfer 
!! calculations.
