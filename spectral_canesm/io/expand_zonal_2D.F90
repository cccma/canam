!> \file
!> \brief Expands a zonal field so it is replicated for all longitudes
!!
!! @author Jason Cole

subroutine expand_zonal_2d(data_out, & ! output 
                            data_in, & ! input
                               nlat, &
                               nlon, &
                             nx_out, &
                              nx_in)

  implicit none

  ! output data
  real, intent(out) , dimension(nx_out) :: data_out !< Output with zonal value expanded to 2D array

  ! input data
  real, intent(in) , dimension(nx_in) :: data_in !< Zonal input
  integer, intent(in) :: nlat !< Number of points along the latitude dimension
  integer, intent(in) :: nlon !< Number of points along the longitude dimension
  integer, intent(in) :: nx_out !< Number of points in the output data
  integer, intent(in) :: nx_in !< Number of points in the input data

  ! local data

  integer :: ilat !<
  integer :: ilon !<
  integer :: icount !<

  ! loop over the data and expand the zonal data so it fills the 2d data, replicating the
  ! zonal data for each longitude point.
  ! not that it is assumed that the 2d array has longitude as the fastest varying dimension,
  ! then the latitude dimension.

  icount = 0
  do ilat = 1, nlat
    do ilon = 1, nlon
      icount = icount + 1
      data_out(icount) = data_in(ilat)
    end do ! i
  end do ! j
  ! there is an extra element in the output data, set it to the first element to avoid it
  ! having junk in it.
  icount = icount + 1
  data_out(icount) = data_in(1)

  return

end subroutine expand_zonal_2d
!> \file
!> Expand a zonal field so it fills a 2D field in which the zonal data
!! is replicated for each point long the longitude dimension.
