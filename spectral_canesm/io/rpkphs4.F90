!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine rpkphs4(nf,g,nlev,ibuf,lon,nlat,ilat,lev,lh,gg,ok)

  !     * jul 10/03 - m. lazare. new routine, based on rpkphs3, but using
  !     *                        code similar to other input routines to
  !     *                        produce "PAK" reordered data relevant
  !     *                        for the particular node. "NLAT" is passed
  !     *                        instead of "ILEV" and work array "GG"
  !     *                        added to call. ** note ** that these
  !     *                        changes are not part of the write
  !     *                        (wpkphs4) revision, since there each node
  !     *                        writes out its own reordered data which
  !     *                        is post-processed outside of the model.

  !     * nov 20/94 - m. lazare. previous version rpkphs3.

  !     * reads from the sequential file nf the packed array g whose
  !     * eight word label is given by ibuf.

  !     * the repeating grenwich longitude is stripped off before
  !     * inserting into array g.
  !     * note that this calculation, to avoid the overhead of going
  !     * through the packer, assumes all grid fields are unpacked
  !     * internally in the gcm (new standard).

  !     * g can be multidimentional, but otherwise nlev has to be set
  !     * to one.

  !     * the fields emd,emm are handled differently because they have
  !     * "LEV" levels and are ordered bottom up.
  !
  !     *
  !     * input: nf    = sequential restart file.
  !     *        nlev  = number of levels. can be either 1,lev,ilev,levs.
  !     *        ibuf  = eight word label.
  !     *         lon  = number of distinct equally-spaced longitudes.
  !     *        nlat  = number of gaussian latitudes.
  !     *        ilat  = number of gaussian latitudes on each node.
  !     *         lev  = ilev+1
  !     *          lh  = vector of model half-level sigma values.
  !     *          gg  = work field to do actual i/o.
  !     *
  !     * output: g    = packed array.
  !     *         ok   = logical :: switch. when the execution is incorrect
  !     *                ok is false.
  !
  implicit none
  integer, intent(in) :: ilat
  integer, intent(in) :: lev  !< Number of vertical levels plus 1 \f$[unitless]\f$
  integer, intent(in) :: lon
  integer, intent(in) :: nf
  integer, intent(in) :: nlat
  integer, intent(in) :: nlev

  integer, intent(inout), dimension(8) :: ibuf !< Variable description\f$[units]\f$
  integer, intent(in), dimension(nlev) :: lh !< Variable description\f$[units]\f$
  real, intent(inout) :: g (lon * ilat + 1, nlev) !< Variable description\f$[units]\f$
  real, intent(in) :: gg( (lon + 1) * nlat) !< Variable description\f$[units]\f$
  logical, intent(inout) :: ok !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  integer :: i
  integer :: l
  integer :: lon1
  integer, external :: nc4to8
  integer, dimension(8) :: kbuf !< Variable description\f$[units]\f$
  !-----------------------------------------------------------------------
  ok = .false.
  lon1 = lon + 1
  !
  do i = 1,8
    kbuf(i) = ibuf(i)
  end do ! loop 30
  !
  do l = 1,nlev
    if (nlev == 1) then
      kbuf(4) = 1
    else if (nlev == lev) then
      if (l == lev) then
        if (kbuf(3) /= nc4to8(" EMD") .and. &
            kbuf(3) /= nc4to8(" EMM")      ) call xit('RPKPHS4', - 1)
        kbuf(4) = 0
      else
        kbuf(4) = lh(lev - l)
      end if
    else
      kbuf(4) = lh(l)
    end if
    !
    call getggbx(g(1,l),kbuf(3),nf,lon1,nlat,kbuf(2),kbuf(4),gg)
  end do ! loop 100
  !
  do i = 1,8
    ibuf(i) = kbuf(i)
  end do ! loop 300
  !
  ok = .true.

  return
  !--------------------------------------------------------------------
end subroutine rpkphs4
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
