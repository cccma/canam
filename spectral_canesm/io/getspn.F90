!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine getspn(nf,g,latotal,lmtotal,la,lm,lsrtotal, &
                  kindc,nstep,name,nlevel,ok)
  !
  !     * nov 03/03 - m.lazare. new routine to read spectral data into
  !     *                       reordered form for loadbalancing, and
  !     *                       which contains subset of data applicable
  !     *                       for each node.
  !
  implicit none
  integer, intent(in) :: kindc
  integer, intent(in) :: la
  integer, intent(in) :: latotal
  integer, intent(in) :: lm
  integer, intent(in) :: lmtotal
  integer, intent(in) :: name
  integer, intent(in) :: nf
  integer, intent(in) :: nlevel
  integer, intent(in) :: nstep
  complex, intent(inout) :: g(la)                 !< Variable description\f$[units]\f$
  integer, intent(in), dimension(2,lmtotal + 1) :: lsrtotal !< Variable description\f$[units]\f$
  logical, intent(inout) :: ok

  complex :: wrk(latotal)          !< Variable description\f$[units]\f$
  integer :: ibuf
  integer :: idat
  integer :: intsize
  integer :: lac
  integer :: lmh
  integer :: m
  integer :: machine
  integer :: maxx
  integer :: mc
  integer :: mend
  integer :: mstart
  integer :: mval
  integer :: n
  integer :: nl
  integer :: nr
  integer*4 :: mynode
  !
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  common /mpinfo/ mynode
  !
  !     * icom is a shared i/o area. it must be large enough
  !     * for an 8 word label followed by a packed gaussian grid.
  !
  common /icom/ ibuf(8),idat(1)
  common /machtyp/ machine, intsize
  !---------------------------------------------------------------------
  maxx = ( 2 * latotal + 8) * machine
  !
  !     * first, read the complete input record into work space "WRK".
  !
  ok = .true.
  call getfld2(nf, wrk,kindc,nstep,name,nlevel,ibuf,maxx,ok)
  if (.not.ok) return
  !     write(6,6025) (ibuf(i),i=1,8)
  !
  lmh = lmtotal/2
  mc = 0
  lac = 0
  !
  !     * mpi hook.
  !
  mstart = mynode * lm + 1
  mend  = mynode * lm + lm
  !
  !     * construct "RECTANGLE" from pairs of low/high zonal wavenumbers.
  !
  do m = 1,lmh
    !
    mval = m
    mc = mc + 1
    if (mc >= mstart .and. mc <= mend) then
      nl = lsrtotal(1,mval)
      nr = lsrtotal(1,mval + 1) - 1
      do n = nl,nr
        lac = lac + 1
        g(lac) = wrk(n)
      end do
    end if
    !
    mval = lmtotal - m + 1
    mc = mc + 1
    if (mc >= mstart .and. mc <= mend) then
      nl = lsrtotal(1,mval)
      nr = lsrtotal(1,mval + 1) - 1
      do n = nl,nr
        lac = lac + 1
        g(lac) = wrk(n)
      end do
    end if
    !
  end do
  !
  return
  !------------------------------------------------------------------
6025 format(' ',a4,i10,1x,a4,5i6)
end subroutine getspn
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
