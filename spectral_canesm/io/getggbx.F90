!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine getggbx (ggpak, name, nf, nlon, nlat, itim, lvl, gg)
  !
  !     * jun 20/03 - m.lazare. - convert grid to ordered s-n pairs
  !     *                         with cyclic longitude removed, as
  !     *                         input to the model.
  !     *                       - npack removed from call.
  !     * sep 27/92 - m.lazare. previous version getggb.
  !
  !     * gets gaussian grid gg(nlon,nlat) called name from file nf.
  !     * and packs it, in ordered s-n pairs, into array ggpak.

  !     * the last point in each row is omitted since it is
  !     * just a copy of the first point in the row.
  !     * itim is the value in ibuf(2).
  !     * lvl  is the value in ibuf(4).
  !
  !     * note that this routine is exactly the same as routine "GETAGB",
  !     * except that the input file is not rewound (to be used to save
  !     * i/o on operations that work with sequential data) and a value
  !     * must be specified for ibuf(4), i.e. the level indicator (rather
  !     * than assuming ibuf(4)=1).
  !
  use msizes, only: ilat
  implicit none
  integer, intent(in) :: itim
  integer, intent(in) :: lvl
  integer, intent(in) :: name
  integer, intent(in) :: nf
  integer, intent(in) :: nlat
  integer, intent(in) :: nlon
  !
  real, intent(out), dimension((nlon-1)*ilat) :: ggpak !< Variable description\f$[units]\f$
  real, intent(in),  dimension(nlon,nlat) :: gg !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  integer :: ibuf
  integer :: idat
  integer :: intsize
  integer :: machine
  integer :: maxx

  integer, external :: nc4to8
  logical :: ok
  !
  !     * icom is a shared scm i/o area. it must be large enough
  !     * for an 8 word label followed by a packed gaussian grid.
  !
  common /icom/ ibuf(8), idat(1)
  common /machtyp/ machine, intsize
  !-----------------------------------------------------------------------
  ! * Read the field.
  ! * Stop if it is not found, or if the wrong size is read.
  !
  maxx = ( nlon * nlat + 8) * machine

  call getfld2 (nf, gg, nc4to8("GRID"), itim, name, lvl, ibuf, maxx, ok)
  if (.not. ok) then
    ! * error exit - stop if grid is missing or is the wrong size.
    write(6,*), "GETGGBX error: Input grid missing or the wrong size!"
    write(6,6020) itim, name, lvl, nlon, nlat
    call xit('GETGGBX', - 1)
  end if
  !     write(6,6025) ibuf
  if (ibuf(5) /= nlon) then
    ! * error exit - longitudes don't match
    write(6,*) "GETGGBX: the read in lon count: ", ibuf(5)
    write(6,*) "         doesn't match the desired count: ", nlon
    write(6,6020) itim,name,lvl,nlon,nlat
    call xit('GETGGBX', - 1)
  end if
  if (ibuf(6) /= nlat) then
    !     * error exit - lats don't match.
    write(6,*) "GETGGBX: the read in lat count: ", ibuf(6)
    write(6,*) "         doesn't match the desired count: ", nlat
    write(6,6020) itim,name,lvl,nlon,nlat
    call xit('GETGGBX', - 1)
  end if
  !
  !     * convert to ordered s-n pairs.
  !
  call rgtocg2(ggpak,gg,nlon - 1,nlat,ilat)

  return
  !

  !-----------------------------------------------------------------------
6020 format('getggbx: GRID',i10,1x,a4,i10,5x,'SIZE ',2i6,5x,'NOT FOUND')
6025 format(1h ,a4,i10,1x,a4,5i6)
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

