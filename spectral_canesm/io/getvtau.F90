!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine getvtau(vtaupak,vtaupal,nlon1,nlat, &
                   irefyr,irefmn,iday,kount,ijpak,nuvt,gg)
  !

  !     * dec 20, 2016 - j. cole.    modified code so it can read the data
  !                                  as fully transient, repeated annual cycle
  !                                  or repeating a particular month.
  !
  !     * april 17, 2015 - m.lazare.     cosmetic revision (same name kept)
  !     *                                to have "keeptim" common block
  !     *                                definition line in upper case,
  !     *                                so that "myrssti" does not conflict
  !     *                                with the cpp directive (compiler
  !     *                                warnings generated).
  !
  !***********************************************************************
  ! read monthly averaged sato et al (1993) stratospheric volcanic aerosol
  ! optical depths from a file every mid month day.
  ! this is a multi year data set (currently for years 1850-2002) so this
  ! routine also depends on iyear, passed in via common /keeptim/.
  !
  ! larry solheim nov,2007
  !***********************************************************************

  implicit none
  real :: curr_year
  real :: day1
  real :: day2
  real :: day3
  integer :: i
  integer :: ibuf2
  integer, intent(in) :: iday  !< Julian calendar day of year \f$[day]\f$
  integer, intent(in) :: ijpak
  integer :: imdh
  integer :: isavdts
  integer :: iyear
  integer, intent(in) :: kount  !< Current model timestep \f$[unitless]\f$
  integer :: myrssti
  integer, intent(in) :: nlat
  integer, intent(in) :: nlon1
  integer, intent(in) :: nuvt
  integer :: nc4to8
  real :: w1
  real :: w2

  integer, intent(in) :: irefyr !< Reference year\f$[units]\f$
  integer, intent(in) :: irefmn !< Reference month\f$[units]\f$

  real, intent(inout), dimension(ijpak) :: vtaupak !< pak is the interpolated value\f$[units]\f$
  real, intent(out),   dimension(ijpak) :: vtaupal !< pal is the target value\f$[units]\f$

  real, intent(in) :: gg( * ) !< Reference year\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !--- local
  integer :: year_prev
  integer :: year_next
  integer :: mon_prev
  integer :: mon_next

  !--- keeptime is required for iyear
  common /keeptim/ iyear,imdh,myrssti,isavdts

  !--- mynode used to control i/o to stdout
  integer*4 :: mynode
  common /mpinfo/ mynode

  !--- a list containing mid month days of the year
  integer, parameter, dimension(12) :: mm_doy = (/ 16,46,75,106,136,167,197,228,259,289,320,350 /)

  !--- a list containing mid month days for each month
  integer, parameter, dimension(12) :: mm_dom = (/ 16,14,16,15,16,15,16,16,15,16,15,16 /)
  !----------------------------------------------------------------------

  !.....determine the year for which data is required

  if (irefyr > 0) then
    !--- when irefyr > 0 read emissions from the data file for
    !--- a repeated annual cycle of year irefyr
    curr_year = irefyr
  else
    !--- otherwise read time dependent emissions from the data
    !--- file using iyear to determine the current year
    curr_year = iyear
  end if

  !.....check on iday
  if (iday < 1 .or. iday > 365) then
    if (mynode == 0) then
      write(6, * )'GETVTAU: IDAY is out of range. IDAY = ',iday
    end if
    call xit("GETVTAU", - 1)
  end if

  !.....determine previous and next year/month, relative to iday
  mon_prev = 0
  if (irefmn < 0) then
    if (iday >= 1 .and. iday < mm_doy(1)) then
      year_prev = curr_year - 1
      year_next = curr_year
      mon_prev = 12
      mon_next = 1
    else if (iday >= mm_doy(12) .and. iday <= 365) then
      year_prev = curr_year
      year_next = curr_year + 1
      mon_prev = 12
      mon_next = 1
    else
      do i = 2,12
        if (iday >= mm_doy(i - 1) .and. iday < mm_doy(i)) then
          year_prev = curr_year
          year_next = curr_year
          mon_prev = i - 1
          mon_next = i
          exit
        end if
      end do
    end if
  else
    !--- the month is not changing.
    mon_prev = irefmn
    mon_next = irefmn
  end if

  if (mon_prev == 0) then
    if (mynode == 0) then
      write(6, * )'GETVTAU: Problem setting year/month on IDAY = ',iday
    end if
    call xit("GETVTAU", - 2)
  end if

  if (kount == 0) then     !--- starting up

    !--- get field for previous mid-month day
    if (mynode == 0) then
      write(6, * )"GETVTAU: Reading volcanic optical depth values", &
       " for year,month: ",year_prev,mon_prev," IDAY = ",iday, &
       " KOUNT = ",kount
    end if
    ibuf2 = 1000000 * year_prev + 10000 * mon_prev + 100 * mm_dom(mon_prev)
    rewind nuvt
    call getggbx(vtaupak,nc4to8("VTAU"),nuvt, &
                 nlon1,nlat,ibuf2,1,gg)

    !--- get field for next mid-month day
    if (mynode == 0) then
      write(6, * )"GETVTAU: Reading volcanic optical depth values", &
       " for year,month: ",year_next,mon_next," IDAY = ",iday, &
       " KOUNT = ",kount
    end if
    ibuf2 = 1000000 * year_next + 10000 * mon_next + 100 * mm_dom(mon_next)
    rewind nuvt
    call getggbx(vtaupal,nc4to8("VTAU"),nuvt, &
                 nlon1,nlat,ibuf2,1,gg)

    !--- interpolate to current day
    day1 = real(mm_doy(mon_prev))
    day2 = real(iday)
    day3 = real(mm_doy(mon_next))
    if (day2 < day1) day2 = day2 + 365.
    if (day3 < day2) day3 = day3 + 365.
    w1 = (day2 - day1)/(day3 - day1)
    w2 = (day3 - day2)/(day3 - day1)
    if (mynode == 0) then
      write(6, * )'GETVTAU: Interpolating between days ', &
       mm_doy(mon_prev),' and ',mm_doy(mon_next),' to day ',iday, &
       ' using weights ',w1,w2
    end if

    do i = 1,ijpak
      vtaupak(i) = w1 * vtaupal(i) + w2 * vtaupak(i)
    end do

  else     !--- continue a run

    if (mynode == 0) then
      write(6, * )"GETVTAU: Reading volcanic optical depth values", &
       " for year,month: ",year_next,mon_next," IDAY = ",iday, &
       " KOUNT = ",kount
    end if
    !--- get field for next mid-month day
    ibuf2 = 1000000 * year_next + 10000 * mon_next + 100 * mm_dom(mon_next)
    rewind nuvt
    call getggbx(vtaupal,nc4to8("VTAU"),nuvt, &
                 nlon1,nlat,ibuf2,1,gg)
  end if

  return
end subroutine getvtau
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
