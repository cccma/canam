!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine getranoz(ozpak,ozpal,name,lo,levoz,nlon,nlat, &
                    incd,irefyr,iday,kount,ijpak,nuan,gg)
  !
  !     * april 17, 2015 - m.lazare.     cosmetic revision (same name kept)
  !     *                                to have "keeptim" common block
  !     *                                definition line in upper case,
  !     *                                so that "myrssti" does not conflict
  !     *                                with the cpp directive (compiler
  !     *                                warnings generated).
  !     * mike lazare - jul 31,2009. new routine for gcm15i to read in
  !     *                            transient ozone. based on routine
  !     *                            getch developed by knut.
  !
  use msizes, only: ilat
  implicit none
  integer, intent(in) :: iday  !< Julian calendar day of year \f$[day]\f$
  integer, intent(in) :: ijpak
  integer, intent(in) :: incd
  integer, intent(in) :: irefyr
  integer, intent(in) :: kount  !< Current model timestep \f$[unitless]\f$
  integer, intent(in) :: levoz  !< Number of vertical layers for ozone input data (other than chemistry) \f$[unitless]\f$
  integer, intent(in) :: name
  integer, intent(in) :: nlat
  integer, intent(in) :: nlon
  integer, intent(in) :: nuan
  !
  !     * surface emissions/concentrations.
  !
  real, intent(inout), dimension(ijpak,levoz) :: ozpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,levoz) :: ozpal !< Variable description\f$[units]\f$
  !
  !     * integer :: level index array for ozone.
  !
  integer, intent(in), dimension(levoz) :: lo !< Variable description\f$[units]\f$
  !
  !     * work space
  !
  real, intent(in) :: gg( * ) !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  real :: day1
  real :: day2
  real :: day3
  integer :: i
  integer :: ijpakx
  integer :: imdh
  integer :: isavdts
  integer :: iyear
  integer :: l
  integer :: myrssti
  real :: w1
  real :: w2

  !--- mynode used to control i/o to stdout
  integer*4 :: mynode
  common /mpinfo/ mynode

  !--- keeptime is required for iyear
  common /keeptim/ iyear,imdh,myrssti,isavdts

  !--- local
  integer :: year_prev
  integer :: year_next
  integer :: mon_prev
  integer :: mon_next
  integer :: ib2p
  integer :: ib2n
  integer :: curr_year

  !--- a list containing mid month days of the year
  integer, parameter, dimension(12) :: mm_doy = (/ 16,46,75,106,136,167,197,228,259,289,320,350 /)

  !--- a list containing mid month days for each month
  integer, parameter, dimension(12) :: mm_dom = (/ 16,14,16,15,16,15,16,16,15,16,15,16 /)
  !----------------------------------------------------------------------
  !
  if (mynode == 0) then
    write(6, * )'    GETRANOZ: NEW OZONE REQUIRED'
  end if

  !.....determine the year for which data is required
  if (irefyr > 0) then
    !--- when irefyr > 0 read emissions from the data file for
    !--- a repeated annual cycle of year irefyr
    curr_year = irefyr
  else
    !--- otherwise read time dependent emissions from the data
    !--- file using iyear to determine the current year
    curr_year = iyear
  end if

  !.....check on iday
  if (iday < 1 .or. iday > 365) then
    if (mynode == 0) then
      write(6, * )'GETRANOZ: IDAY is out of range. IDAY = ',iday
    end if
    call xit("GETRANOZ", - 1)
  end if

  !.....determine previous and next year/month, relative to iday
  mon_prev = 0
  if (iday >= 1 .and. iday < mm_doy(1)) then
    year_prev = curr_year - 1
    year_next = curr_year
    mon_prev = 12
    mon_next = 1
  else if (iday >= mm_doy(12) .and. iday <= 365) then
    year_prev = curr_year
    year_next = curr_year + 1
    mon_prev = 12
    mon_next = 1
  else
    do i = 2,12
      if (iday >= mm_doy(i - 1) .and. iday < mm_doy(i)) then
        year_prev = curr_year
        year_next = curr_year
        mon_prev = i - 1
        mon_next = i
        exit
      end if
    end do
  end if

  if (mon_prev == 0) then
    if (mynode == 0) then
      write(6, * )'GETRANOZ: Problem setting year/month on IDAY = ',iday
    end if
    call xit("GETRANOZ", - 2)
  end if

  ! xxx
  if (year_prev == 1849 .and. mon_prev == 12) then
    year_prev = 1850
    mon_prev = 12
  end if
  if (year_next == 2001 .and. mon_next == 1) then
    year_next = 2000
    mon_next = 1
  end if
  ! xxx

  !--- determine time stamps (ibuf2 values) to be used to read
  !--- appropriate data from the file
  ! ib2p=1000000*year_prev+10000*mon_prev+100*mm_dom(mon_prev)
  ! ib2n=1000000*year_next+10000*mon_next+100*mm_dom(mon_next)
  ib2p = year_prev * 100 + mon_prev
  ib2n = year_next * 100 + mon_next

  if (mynode == 0) then
    write(6, * )'GETRANOZ: year_prev,mon_prev,year_next,mon_next: ', &
                        year_prev,mon_prev,year_next,mon_next
    write(6, * )'GETRANOZ: ib2p,ib2n: ',ib2p,ib2n
  end if
  !
  !     * get new boundary fields for next mid-month day.
  !
  if (incd == 0) then
    !
    !       * model is stationary.
    !
    if (kount == 0) then
      !
      !         * start-up time. read-in fields for average of month.
      !         * initialize target fields as well, although not used.
      !
      rewind nuan
      do l = 1,levoz
        call getggbx(ozpak(1,l),name,nuan,nlon,nlat,ib2n,lo(l),gg)
      end do
      !
      rewind nuan
      do l = 1,levoz
        call getggbx(ozpal(1,l),name,nuan,nlon,nlat,ib2n,lo(l),gg)
      end do

    end if
    !
  else
    !
    !       * the model is moving.
    !
    if (kount == 0) then
      !
      !         * start-up time. get fields for previous and target mid-month days.
      !
      rewind nuan
      do l = 1,levoz
        call getggbx(ozpak(1,l),name,nuan,nlon,nlat,ib2p,lo(l),gg)
      end do
      !
      rewind nuan
      do l = 1,levoz
        call getggbx(ozpal(1,l),name,nuan,nlon,nlat,ib2n,lo(l),gg)
      end do
      !
      !--- interpolate to current day
      day1 = real(mm_doy(mon_prev))
      day2 = real(iday)
      day3 = real(mm_doy(mon_next))
      if (day2 < day1) day2 = day2 + 365.
      if (day3 < day2) day3 = day3 + 365.
      w1 = (day2 - day1)/(day3 - day1)
      w2 = (day3 - day2)/(day3 - day1)
      if (mynode == 0) then
        write(6, * ) &
         'GETRANOZ: Interpolating at ',curr_year,' day',iday, &
         ' between ',year_prev,' day',mm_doy(mon_prev), &
         ' and ',year_next,' day',mm_doy(mon_next), &
         ' using weights ',w1,w2
      end if
      !
      ijpakx = (nlon - 1) * ilat
      do l = 1,levoz
        do i = 1,ijpakx
          ozpak(i,l) = w1 * ozpal(i,l) + w2 * ozpak(i,l)
        end do
      end do
    else
      !
      !         * this is in the middle of a run.
      !
      rewind nuan
      do l = 1,levoz
        call getggbx(ozpal(1,l),name,nuan,nlon,nlat,ib2n,lo(l),gg)
      end do
    end if
  end if
  !
  return
end subroutine getranoz
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
