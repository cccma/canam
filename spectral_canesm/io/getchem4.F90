!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine getchem4(dmsopak,dmsopal,edmspak,edmspal, &
                    suz0pak,suz0pal,pdsfpak,pdsfpal, &
                    ohpak,  ohpal, &
                    h2o2pak,h2o2pal,  o3pak,  o3pal, &
                    no3pak, no3pal,hno3pak,hno3pal, &
                    nh3pak, nh3pal, nh4pak, nh4pal, &
                    nlon,nlat,levox,incd,iday,mday,mday1, &
                    kount,ijpak,nuch,nuox,lx,gg)
  !
  !     * jun 11, 2018 - m.lazare. Read from unit number nuch ("llchem" file)
  !     *                          instead of nugg for non-transient chemistry
  !     *                          forcing (several calls). Also read from unit
  !     *                          number nuox ("oxi" file) for non-transient
  !     *                          oxidants in getchem4 (subroutine also changed).
  !     * may 10/2012 - y.peng/   new version for gcm16:
  !     *               m.lazare. - add suz0,pdsf input fields for new
  !     *                           dust scheme.
  !     *                         - mday and mday1 passed in (calculated
  !     *                           in "GETMDAY") rather than being
  !     *                           determined here.
  !     * knut von salzen - feb 07,2009. previous version getchem3 for
  !     *                                gcm15h/i:
  !     *                                remove eost (now done in getcac).
  !     * jan 17/08 - x.ma/     previous version getchem2 for gcm15g:
  !     *             m.lazare. add eost and remove ebc,eoc,eocf.
  !     * dec 15/03 - m.lazare. previous version getchemx up to gcm15f:
  !     *                       - row fields removed.
  !     *                       - uses mpi-ready i/o interface routines.
  !     *                       - only prints out on node zero.
  !     *                       - "IFIZ" removed.
  !     * jul 16/01 - m.lazare. previous version getchem for gcm15.
  !
  !     * read in chemistry fields (surface emissions and atmospheric
  !     * species concentrations) and interpolate to current iday.
  !
  !     * time label is the julian date of the first of the month
  !     * (nfdm) for which the sst are the monthly average.
  !
  !     * when the model is moving through the year (incd/=0),
  !     * then new fields are read on every mid month day (mmd),
  !     * and interpolation will be done between the two adjacent
  !     * mid month days. "PAK" is the precedent, "PAL" is the target.
  !
  use msizes, only: ilat
  implicit none
  integer, intent(in) :: iday  !< Julian calendar day of year \f$[day]\f$
  integer, intent(in) :: ijpak
  integer, intent(in) :: incd
  integer, intent(in) :: kount  !< Current model timestep \f$[unitless]\f$
  integer, intent(in) :: mday  !< Julian calendar day of next mid-month \f$[day]\f$
  integer, intent(in) :: mday1 !< Variable description\f$[units]\f$
  integer, intent(in) :: nlat
  integer, intent(in) :: nlon
  integer, intent(in) :: nuch
  integer, intent(in) :: nuox
  !
  !     * surface emissions/concentrations.
  !
  real, intent(inout), dimension(ijpak) :: dmsopak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: dmsopal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: edmspak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: edmspal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: suz0pak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: suz0pal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: pdsfpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: pdsfpal !< Variable description\f$[units]\f$
  !
  !     * multi-level species.
  !
  real, intent(inout), dimension(ijpak,levox) :: ohpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,levox) :: ohpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,levox) :: h2o2pak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,levox) :: h2o2pal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,levox) :: o3pak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,levox) :: o3pal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,levox) :: no3pak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,levox) :: no3pal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,levox) :: hno3pak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,levox) :: hno3pal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,levox) :: nh3pak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,levox) :: nh3pal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,levox) :: nh4pak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,levox) :: nh4pal !< Variable description\f$[units]\f$
  !
  real, intent(in) :: gg( * ) !< Variable description\f$[units]\f$
  !
  integer, intent(in), dimension(levox) :: lx !< Variable description\f$[units]\f$
  integer, intent(in) :: levox  !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxyg%s/en format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  real :: day1
  real :: day2
  real :: day3
  integer :: i
  integer :: ijpakx
  integer :: jour
  integer :: jour1
  integer :: l
  integer :: lon
  integer, external :: nc4to8
  real :: w1
  real :: w2

  integer*4 :: mynode
  !
  common /mpinfo/ mynode
  !----------------------------------------------------------------------
  jour = mday
  jour1 = mday1
  ijpakx = (nlon - 1) * ilat
  !
  if (incd == 0) then
    !
    ! * Model is stationary.
    !
    if (kount == 0) then
      !
      !         * start-up time. read-in fields for average of month.
      !         * initialize target fields as well, although not used.
      !
      rewind nuch
      !
      call getagbx(dmsopak,nc4to8("DMSO"),nuch,nlon,nlat,iday,gg)
      call getagbx(dmsopal,nc4to8("DMSO"),nuch,nlon,nlat,iday,gg)
      call getagbx(edmspak,nc4to8("EDMS"),nuch,nlon,nlat,iday,gg)
      call getagbx(edmspal,nc4to8("EDMS"),nuch,nlon,nlat,iday,gg)
      call getagbx(suz0pak,nc4to8("SUZ0"),nuch,nlon,nlat,iday,gg)
      call getagbx(suz0pal,nc4to8("SUZ0"),nuch,nlon,nlat,iday,gg)
      call getagbx(pdsfpak,nc4to8("SLAI"),nuch,nlon,nlat,iday,gg)
      call getagbx(pdsfpal,nc4to8("SLAI"),nuch,nlon,nlat,iday,gg)
      !
      rewind nuox
      !
      do   l = 1,levox
        call getggbx(ohpak(1,l),nc4to8("  OH"),nuox,nlon,nlat,iday, &
                     lx(l),gg)
      end do ! loop 8
      !
      do   l = 1,levox
        call getggbx(h2o2pak(1,l),nc4to8("H2O2"),nuox,nlon,nlat,iday, &
                     lx(l),gg)
      end do ! loop 9
      !
      do  l = 1,levox
        call getggbx(o3pak(1,l),nc4to8("  O3"),nuox,nlon,nlat,iday, &
                     lx(l),gg)
      end do ! loop 10
      !
      do  l = 1,levox
        call getggbx(no3pak(1,l),nc4to8(" NO3"),nuox,nlon,nlat,iday, &
                     lx(l),gg)
      end do ! loop 11
      !
      do  l = 1,levox
        call getggbx(hno3pak(1,l),nc4to8("HNO3"),nuox,nlon,nlat,iday, &
                     lx(l),gg)
      end do ! loop 12
      !
      do  l = 1,levox
        call getggbx(nh3pak(1,l),nc4to8(" NH3"),nuox,nlon,nlat,iday, &
                     lx(l),gg)
      end do ! loop 13
      !
      do  l = 1,levox
        call getggbx(nh4pak(1,l),nc4to8(" NH4"),nuox,nlon,nlat,iday, &
                     lx(l),gg)
      end do ! loop 14
      do l = 1,levox
        do i = 1,ijpakx
          ohpal  (i,l) = ohpak  (i,l)
          h2o2pal(i,l) = h2o2pak(i,l)
          o3pal  (i,l) = o3pak  (i,l)
          no3pal (i,l) = no3pak (i,l)
          hno3pal(i,l) = hno3pak(i,l)
          nh3pal (i,l) = nh3pak (i,l)
          nh4pal (i,l) = nh4pak (i,l)
        end do
      end do ! loop 16

    else
      !
      !         * not a new integration. no new fields required.
      !
    end if

  else
    !
    !       * the model is moving.
    !
    if (kount == 0) then
      !
      ! * Start-up time. Get fields for previous and target mid-month days.
      !
      rewind nuch
      !
      call getggbx(dmsopak,nc4to8("DMSO"),nuch,nlon,nlat,jour1,1,gg)
      call getggbx(edmspak,nc4to8("EDMS"),nuch,nlon,nlat,jour1,1,gg)
      call getggbx(suz0pak,nc4to8("SUZ0"),nuch,nlon,nlat,jour1,1,gg)
      call getggbx(pdsfpak,nc4to8("SLAI"),nuch,nlon,nlat,jour1,1,gg)
      !
      rewind nuox
      !
      do  l = 1,levox
        call getggbx(ohpak(1,l),nc4to8("  OH"),nuox,nlon,nlat,jour1, &
                     lx(l),gg)
      end do ! loop 20
      !
      do  l = 1,levox
        call getggbx(h2o2pak(1,l),nc4to8("H2O2"),nuox,nlon,nlat,jour1, &
                     lx(l),gg)
      end do ! loop 21
      !
      do  l = 1,levox
        call getggbx(o3pak(1,l),nc4to8("  O3"),nuox,nlon,nlat,jour1, &
                     lx(l),gg)
      end do ! loop 22
      !
      do  l = 1,levox
        call getggbx(no3pak(1,l),nc4to8(" NO3"),nuox,nlon,nlat,jour1, &
                     lx(l),gg)
      end do ! loop 23
      !
      do  l = 1,levox
        call getggbx(hno3pak(1,l),nc4to8("HNO3"),nuox,nlon,nlat,jour1, &
                     lx(l),gg)
      end do ! loop 24
      !
      do  l = 1,levox
        call getggbx(nh3pak(1,l),nc4to8(" NH3"),nuox,nlon,nlat,jour1, &
                     lx(l),gg)
      end do ! loop 25
      !
      do  l = 1,levox
        call getggbx(nh4pak(1,l),nc4to8(" NH4"),nuox,nlon,nlat,jour1, &
                     lx(l),gg)
      end do ! loop 26
      !
      rewind nuch
      !
      call getggbx(dmsopal,nc4to8("DMSO"),nuch,nlon,nlat,jour,1,gg)
      call getggbx(edmspal,nc4to8("EDMS"),nuch,nlon,nlat,jour,1,gg)
      call getggbx(suz0pal,nc4to8("SUZ0"),nuch,nlon,nlat,jour,1,gg)
      call getggbx(pdsfpal,nc4to8("SLAI"),nuch,nlon,nlat,jour,1,gg)
      !
      rewind nuox
      !
      do  l = 1,levox
        call getggbx(ohpal(1,l),nc4to8("  OH"),nuox,nlon,nlat,jour, &
                     lx(l),gg)
      end do ! loop 30
      !
      do  l = 1,levox
        call getggbx(h2o2pal(1,l),nc4to8("H2O2"),nuox,nlon,nlat,jour, &
                     lx(l),gg)
      end do ! loop 31
      !
      do  l = 1,levox
        call getggbx(o3pal(1,l),nc4to8("  O3"),nuox,nlon,nlat,jour, &
                     lx(l),gg)
      end do ! loop 32
      !
      do  l = 1,levox
        call getggbx(no3pal(1,l),nc4to8(" NO3"),nuox,nlon,nlat,jour, &
                     lx(l),gg)
      end do ! loop 33
      !
      do  l = 1,levox
        call getggbx(hno3pal(1,l),nc4to8("HNO3"),nuox,nlon,nlat,jour, &
                     lx(l),gg)
      end do ! loop 34
      !
      do  l = 1,levox
        call getggbx(nh3pal(1,l),nc4to8(" NH3"),nuox,nlon,nlat,jour, &
                     lx(l),gg)
      end do ! loop 35
      !
      do  l = 1,levox
        call getggbx(nh4pal(1,l),nc4to8(" NH4"),nuox,nlon,nlat,jour, &
                     lx(l),gg)
      end do ! loop 36
      !
      lon = nlon - 1
      day1 = real(mday1)
      day2 = real(iday)
      day3 = real(mday)
      if (day2 < day1) day2 = day2 + 365.
      if (day3 < day2) day3 = day3 + 365.
      w1 = (day2 - day1)/(day3 - day1)
      w2 = (day3 - day2)/(day3 - day1)
      if (mynode == 0) write(6,6000) iday,mday1,mday,w1,w2
      !
      do i = 1,ijpakx
        dmsopak(i) = w1 * dmsopal(i) + w2 * dmsopak(i)
        edmspak(i) = w1 * edmspal(i) + w2 * edmspak(i)
        suz0pak(i) = w1 * suz0pal(i) + w2 * suz0pak(i)
        pdsfpak(i) = w1 * pdsfpal(i) + w2 * pdsfpak(i)
      end do ! loop 125
      !
      do l = 1,levox
        do i = 1,ijpakx
          ohpak  (i,l) = w1 * ohpal  (i,l) + w2 * ohpak  (i,l)
          h2o2pak(i,l) = w1 * h2o2pal(i,l) + w2 * h2o2pak(i,l)
          o3pak  (i,l) = w1 * o3pal  (i,l) + w2 * o3pak  (i,l)
          no3pak (i,l) = w1 * no3pal (i,l) + w2 * no3pak (i,l)
          hno3pak(i,l) = w1 * hno3pal(i,l) + w2 * hno3pak(i,l)
          nh3pak (i,l) = w1 * nh3pal (i,l) + w2 * nh3pak (i,l)
          nh4pak (i,l) = w1 * nh4pal (i,l) + w2 * nh4pak (i,l)
        end do
      end do ! loop 150

    else
      !
      ! * This is in the middle of a run.
      !
      rewind nuch
      !
      call getggbx(dmsopal,nc4to8("DMSO"),nuch,nlon,nlat,jour,1,gg)
      call getggbx(edmspal,nc4to8("EDMS"),nuch,nlon,nlat,jour,1,gg)
      call getggbx(suz0pal,nc4to8("SUZ0"),nuch,nlon,nlat,jour,1,gg)
      call getggbx(pdsfpal,nc4to8("SLAI"),nuch,nlon,nlat,jour,1,gg)
      !
      rewind nuox
      !
      do  l = 1,levox
        call getggbx(ohpal(1,l),nc4to8("  OH"),nuox,nlon,nlat,jour, &
                     lx(l),gg)
      end do ! loop 230
      !
      do  l = 1,levox
        call getggbx(h2o2pal(1,l),nc4to8("H2O2"),nuox,nlon,nlat,jour, &
                     lx(l),gg)
      end do ! loop 231
      !
      do  l = 1,levox
        call getggbx(o3pal(1,l),nc4to8("  O3"),nuox,nlon,nlat,jour, &
                     lx(l),gg)
      end do ! loop 232
      !
      do  l = 1,levox
        call getggbx(no3pal(1,l),nc4to8(" NO3"),nuox,nlon,nlat,jour, &
                     lx(l),gg)
      end do ! loop 233
      !
      do  l = 1,levox
        call getggbx(hno3pal(1,l),nc4to8("HNO3"),nuox,nlon,nlat,jour, &
                     lx(l),gg)
      end do ! loop 234
      !
      do  l = 1,levox
        call getggbx(nh3pal(1,l),nc4to8(" NH3"),nuox,nlon,nlat,jour, &
                     lx(l),gg)
      end do ! loop 235
      !
      do  l = 1,levox
        call getggbx(nh4pal(1,l),nc4to8(" NH4"),nuox,nlon,nlat,jour, &
                     lx(l),gg)
      end do ! loop 236
    end if

  end if
  return
  !---------------------------------------------------------------------
6000 format(' INTERPOLATING FOR', i5, ' BETWEEN', i5, ' AND', &
        i5, ' WITH WEIGHTS = ', 2f7.3)
end subroutine getchem4
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}


