#define FNAME1 CONCAT(agcm_checksum_array,ARRAYKIND)
#define FNAME  CONCAT(FNAME1,2d)
!> Checksum CanAM 2d arrays of type ARRAYTYPE and kind ARRAYKIND
subroutine FNAME( array, msg, is, ie, js, je, out_unit )
  ARRAYTYPE(kind=ARRAYKIND), dimension(:,:), intent(in) :: array !< The array to be checksummed
  character(len=*),            intent(in) :: msg   !< A message to accompany the checksum
  integer, optional,           intent(in) :: is    !< Starting index to checksum (i-direction)
  integer, optional,           intent(in) :: ie    !< Ending index of array to checksum (i-direction)
  integer, optional,           intent(in) :: js    !< Starting index to checksum (j-direction)
  integer, optional,           intent(in) :: je    !< Ending index of array to checksum (j-direction)
  integer, optional,           intent(in) :: out_unit !< Where to write the message and checksum

  integer(kind=int64) :: array_chksum, global_chksum, write_unit
  integer(kind=MPI_INT_KIND) :: mpi_error

  array_chksum = checksum_low(array, is_in=is, ie_in=ie, js_in=js, je_in=je)

  call MPI_REDUCE(array_chksum, global_chksum, 1, MPI_INTEGER, MPI_SUM, AGCM_COMM_ROOT, AGCM_COMM_WORLD, mpi_error)
  if (AGCM_RANK == AGCM_COMM_ROOT) then
    write_unit = STDOUT
    if (present(out_unit)) write_unit = out_unit

    global_chksum = mod(global_chksum, CHKSUM_MOD)
    write(write_unit,'(A,I8.8,X,A)') "chksum=", array_chksum, TRIM(msg)
    call flush(write_unit)
  endif
end subroutine FNAME
#undef ARRAYTYPE
#undef ARRAYKIND
#undef FNAME1
#undef FNAME