module agcm_checksum

use iso_fortran_env, only : STDOUT => output_unit, int64
use mpi,             only : MPI_INTEGER, MPI_COMM_WORLD, MPI_SUM
use mpi,             only : MPI_INTEGER4, MPI_INTEGER8, MPI_REAL4, MPI_REAL8
use mpi,             only : MPI_COMPLEX8, MPI_COMPLEX16
use checksums,       only : checksum_low => checksum
use iso_fortran_env, only : int32, int64, real32, real64

implicit none; private
interface checksum
  module procedure agcm_checksum_array_real32_1d,    &
                   agcm_checksum_array_real64_1d,    &
                   agcm_checksum_array_complex32_1d, &
                   agcm_checksum_array_complex64_1d, &
                   agcm_checksum_array_int32_1d,     &
                   agcm_checksum_array_int64_1d,     &
                   agcm_checksum_array_real32_2d,    &
                   agcm_checksum_array_real64_2d,    &
                   agcm_checksum_array_complex32_2d, &
                   agcm_checksum_array_complex64_2d, &
                   agcm_checksum_array_int32_2d,     &
                   agcm_checksum_array_int64_2d,     &
                   agcm_checksum_array_real32_3d,    &
                   agcm_checksum_array_real64_3d,    &
                   agcm_checksum_array_complex32_3d, &
                   agcm_checksum_array_complex64_3d, &
                   agcm_checksum_array_int32_3d,     &
                   agcm_checksum_array_int64_3d
end interface checksum

integer, parameter :: complex32 = real32
integer, parameter :: complex64 = real64

integer, parameter :: MPI_INT_KIND = kind(MPI_INTEGER)
integer(kind=int64), parameter :: CHKSUM_MOD = 10000000

integer(kind=MPI_INT_KIND), save :: AGCM_COMM_WORLD = -1
integer(kind=MPI_INT_KIND), save :: AGCM_COMM_ROOT  = -1
integer(kind=MPI_INT_KIND), save :: AGCM_RANK       = -1

public :: init_agcm_checksum
public :: checksum_dynamic_state
public :: checksum

contains

!> Calculate the checksums for variables that describe the dynamic state of the model
subroutine checksum_dynamic_state( P, C, T, ES, PS, out_unit)
  implicit none
  complex, dimension(:), intent(in) :: P !< Vorticity
  complex, dimension(:), intent(in) :: C !< Divergence
  complex, dimension(:), intent(in) :: T !< Temperature
  complex, dimension(:), intent(in) :: ES !< Specific humidity
  complex, dimension(:), intent(in) :: PS !< Surface pressure
  integer, optional, intent(inout)  :: out_unit !< The 'unit' to write to, either a file or by default STDOUT

  integer :: array_chksum
  integer :: out_unit_local

  out_unit_local = STDOUT
  if (present(out_unit)) out_unit_local = out_unit

  call checksum(P,  "P (vorticity) at model finalization",          out_unit=out_unit_local)
  call checksum(C,  "C (divergence) at model finalization",         out_unit=out_unit_local)
  call checksum(T,  "T (temperature) at model finalization",        out_unit=out_unit_local)
  call checksum(ES, "ES (specific humidity) at model finalization", out_unit=out_unit_local)
  call checksum(PS, "PS (surface pressure) at model finalization",  out_unit=out_unit_local)

end subroutine checksum_dynamic_state

!> Store information within the module about the MPI Comm world of the AGCM
subroutine init_agcm_checksum( comm_world, root_pe, rank )
  integer(kind=MPI_INT_KIND), intent(in) :: comm_world !< The MPI_COMM_WORLD this processor belongs to
  integer(kind=MPI_INT_KIND), intent(in) :: root_pe    !< The first processor in this comm_world
  integer(kind=MPI_INT_KIND), intent(in) :: rank       !< The rank of this processor within the comm_world

  AGCM_COMM_WORLD = comm_world
  AGCM_COMM_ROOT  = root_pe
  AGCM_RANK       = rank

end subroutine init_agcm_checksum

#define PASTE(X)         X
#define PASTE2(X)        PASTE(X)_
#define CONCATHELP(X, Y) PASTE2(X)Y
#define CONCAT(X, Y)     CONCATHELP(X,Y)

#define ARRAYTYPE real
#define ARRAYKIND real32
#include "agcm_checksum_1d.inc"

#define ARRAYTYPE real
#define ARRAYKIND real64
#include "agcm_checksum_1d.inc"

#define ARRAYTYPE complex
#define ARRAYKIND complex32
#include "agcm_checksum_1d.inc"

#define ARRAYTYPE complex
#define ARRAYKIND complex64
#include "agcm_checksum_1d.inc"

#define ARRAYTYPE integer
#define ARRAYKIND int32
#include "agcm_checksum_1d.inc"

#define ARRAYTYPE integer
#define ARRAYKIND int64
#include "agcm_checksum_1d.inc"

#define ARRAYTYPE real
#define ARRAYKIND real32
#include "agcm_checksum_2d.inc"

#define ARRAYTYPE real
#define ARRAYKIND real64
#include "agcm_checksum_2d.inc"

#define ARRAYTYPE complex
#define ARRAYKIND complex32
#include "agcm_checksum_2d.inc"

#define ARRAYTYPE complex
#define ARRAYKIND complex64
#include "agcm_checksum_2d.inc"

#define ARRAYTYPE integer
#define ARRAYKIND int32
#include "agcm_checksum_2d.inc"

#define ARRAYTYPE integer
#define ARRAYKIND int64
#include "agcm_checksum_2d.inc"

#define ARRAYTYPE real
#define ARRAYKIND real32
#include "agcm_checksum_3d.inc"

#define ARRAYTYPE real
#define ARRAYKIND real64
#include "agcm_checksum_3d.inc"

#define ARRAYTYPE complex
#define ARRAYKIND complex32
#include "agcm_checksum_3d.inc"

#define ARRAYTYPE complex
#define ARRAYKIND complex64
#include "agcm_checksum_3d.inc"

#define ARRAYTYPE integer
#define ARRAYKIND int32
#include "agcm_checksum_3d.inc"

#define ARRAYTYPE integer
#define ARRAYKIND int64
#include "agcm_checksum_3d.inc"


#undef PASTE(X)
#undef PASTE2(X)
#undef CONCATHELP(X, Y)
#undef CONCAT(X, Y)

end module agcm_checksum
