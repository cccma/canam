!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine radflab(lr,levrf,ptoit,lh,ilev)

  !     * mar 3/2008 - l.solheim,m.lazare.
  !
  !     * defines level index values for radiative forcing diagnostic fields.
  !
  implicit none
  !
  integer, intent(inout), dimension(levrf) :: lr !< Variable description\f$[units]\f$
  integer, intent(in), dimension(ilev) :: lh !< Variable description\f$[units]\f$
  !
  real, intent(in) :: ptoit !< Variable description\f$[units]\f$
  integer, intent(in) :: levrf   !< Number of vertical levels for radiation \f$[unitless]\f$
  integer, intent(in) :: ilev   !< Number of vertical levels \f$[unitless]\f$
  integer :: lhtop
  integer :: l
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !-----------------------------------------------------------------------
  call lvcode(lhtop,1e-5 * ptoit,1)
  do l = 1,levrf
    if (l == 1) then
      !--- toa values are stored in first level.
      lr(l) = lhtop
    else if (l == levrf) then
      !--- surface values are stored in last level.
      lr(l) = 1111
    else
      !--- model levels are stored in between.
      lr(l) = lh(l - 1)
    end if
  end do
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
