!>\file compute_geom.F90
!>\brief Compute AGCM geometries and calculate trignometric arrays.
!!
!! @author Routine author name(s)
!
 subroutine compute_geom(ijpak, lon_coord, lat_coord, gauss_wgt, cell_area, &
                         cld, slda, wlda, wocslda, sla, wla, wocsla)
  use msizes,      only : ilat, ilatd, lonsl, lonsld, nlat, nlatd, nlatj, nlatjd, &
                          ntask_td, ntask_tp
  use compak,      only : ilslpak, jlpak, radjpak, wjpak, &
                          dlonpak, dlatpak, latupak, latdpak, lonupak, londpak
  use phys_consts, only : pi
  implicit none

  integer, intent(in) :: ijpak
  real(kind=8), dimension(lonsl),      intent(out) :: lon_coord !<
  real(kind=8), dimension(nlat),       intent(out) :: lat_coord !<
  real(kind=8), dimension(nlat),       intent(out) :: gauss_wgt !<
  real(kind=8), dimension(lonsl,nlat), intent(out) :: cell_area !<
  real*8, dimension(nlatjd,ntask_td),  intent(out) :: cld    !<
  real*8, dimension(nlat) , intent(out) :: sla    !<
  real*8, dimension(nlat) , intent(out) :: wla    !<
  real*8, dimension(nlat) , intent(out) :: wocsla !<
  real*8, dimension(nlatd), intent(out) :: slda    !<
  real*8, dimension(nlatd), intent(out) :: wlda    !<
  real*8, dimension(nlatd), intent(out) :: wocslda !<
  !
  ! * Local variables
  !
  real :: delta_lon
  real :: djpak
  integer :: ij
  integer :: iother
  integer :: nlath
  integer :: nlathd
  integer :: nntp
  real :: r2d
  real*8 :: wlxaccum
  !
  !     * gaussian fields (physics grid).
  !
  !     * these are complete sets.
  !
  real*8 :: cla   (nlat) !<
  real*8 :: radla (nlat) !<
  !
  real*8 :: clda   (nlatd) !<
  real*8 :: radlda (nlatd) !<
  !
  real*8 :: slx   (nlat) !<
  real*8 :: clx   (nlat) !<
  real*8 :: wlx   (nlat) !<
  real*8 :: wocslx(nlat) !<
  real*8 :: radlx (nlat) !<
  !
  !     * these are defined locally on each node.
  !
  real*8 :: sl   (nlatj,ntask_tp) !<
  real*8 :: cl   (nlatj,ntask_tp) !<
  real*8 :: wl   (nlatj,ntask_tp) !<
  real*8 :: wocsl(nlatj,ntask_tp) !<
  real*8 :: radl (nlatj,ntask_tp) !<
  !
  real*8 :: sld   (nlatjd,ntask_td) !<
  real*8 :: wld   (nlatjd,ntask_td) !<
  real*8 :: wocsld(nlatjd,ntask_td) !<
  real*8 :: radld (nlatjd,ntask_td) !<

  real*8 :: radbu (nlatj,ntask_tp) !<
  real*8 :: radbd (nlatj,ntask_tp) !<
  real*8 :: radbxu(nlat)           !<
  real*8 :: radbxd(nlat)           !<
  real*8 :: radbx (nlat + 1)       !<
  !
  nlath    = nlat/2
  nlathd   = nlatd/2
  djpak    = lonsld*ilatd+1
  ! * Calculate trignometric arrays.
  ! * The "X" variables are working arrays.
  ! * The "A" variables are the re-ordered arrays applicable on the
  ! * complete set of latitudes (for the legendre transform).
  ! * The others apply locally on each node only.
  slx = 0.0_8
  wlx = 0.0_8
  clx = 0.0_8
  radlx = 0.0_8
  wocslx = 0.0_8
  call gauss  (nlath, slx, wlx, clx, radlx, wocslx)
  ! * Need to be able to compute the upper and lower latitude boundaries
  ! * for the gaussian grid.  compute the accumulated weights for the northern
  ! * going from the pole to equator

  r2d           =  180.0/3.141592654
  wlxaccum      =  1.0_8
  radbx(1)      =  1.0_8
  radbx(nlat+1) = -1.0_8
  radbx(nlath)  =  0.0_8
  radbx(nlath+1)  =  -0.0_8
  do ij = 2, nlath
    iother        = nlat-ij+2
    wlxaccum      = wlxaccum - wlx(ij-1)
    radbx(ij)     = wlxaccum
    radbx(iother) = -radbx(ij)
  end do
  do ij = 1, nlat
    radbxu(ij) = asin(radbx(ij))*r2d
    radbxd(ij) = asin(radbx(ij+1))*r2d
  end do ! i

  ! related to CanCPL
  lon_coord = 0.0_8
  lat_coord = 0.0_8
  gauss_wgt = 0.0_8
  cell_area = 0.0_8
  !--- initialize lat_coord and gauss_wgt with values from the call to gauss
  !--- these values are only defined for the lower half of these arrays
  lat_coord(1:nlat) = radlx(1:nlat)
  gauss_wgt(1:nlat) = wlx(1:nlat)

  nntp = nlatj * ntask_tp
  call trigml4(nlat, ilat, sl, wl, cl, radl, wocsl, radbu, radbd, &
               slx, wlx, clx, radlx, wocslx, radbxu, radbxd, nntp, pi)
  call trigpak(ilslpak, jlpak, wjpak, radjpak, dlonpak, latupak, latdpak, &
               wl, radl, radbu, radbd, ilat, lonsl, ijpak)

  call trigml4a(nlath, nlat, sla, wla, cla, radla, wocsla, &
                slx, wlx, clx, radlx, wocslx, pi)

  ! Compute the longitude boundaries for each gridbox
  delta_lon = 0.5*(dlonpak(2)-dlonpak(1)) ! since equi-spaced
  do ij = 1, ijpak - 1
    ! latitudes in degrees rather than radians
    dlatpak(ij) = radjpak(ij)*r2d

    londpak(ij) = dlonpak(ij)-delta_lon
    lonupak(ij) = dlonpak(ij)+delta_lon
    ! checks if wrapped around 0 degree point
    if (londpak(ij) < 0.0) then
      londpak(ij) = londpak(ij)+360.0
    end if
    if (lonupak(ij) > 360.0) then
      lonupak(ij) = lonupak(ij)-360.0
    end if
  end do ! ij
  !
  call gauss (nlathd, slda, wlda, clda, radlda, wocslda)
  call triglx2(nlathd, ilatd, sld, wld, cld, radld, wocsld, &
               slda, wlda, clda, radlda, wocslda, pi)

  return
 end subroutine compute_geom
