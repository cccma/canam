!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine getlday(lday,iday)
  !
  !     * jul 29/07 - m.lazare. new routine to get "LDAY", based
  !     *                       on "GETZON9" without the ozone.
  !
  !     * sets "LDAY", ie first day ofnext target month.
  !
  implicit none
  !
  integer*4 :: mynode !<
  integer, dimension(12) :: nfdm !<
  integer :: l !<
  integer :: n !<
  integer, intent(in) :: iday   !< Julian calendar day of year \f$[day]\f$
  integer, intent(inout) :: lday   !< Julian calendar day of next first-day-of-month \f$[day]\f$
  logical :: ok !<
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  common /mpinfo/ mynode
  !
  data nfdm/1,32,60,91,121,152,182,213,244,274,305,335/
  !---------------------------------------------------------------------
  !     * reset lday to first day of the next month.
  !
  l=0
  do n=1,12
    if (iday>=nfdm(n)) l=n+1
  end do
  if (l==13) l=1
  lday=nfdm(l)
  if (mynode==0) write(6,6020) lday
  !---------------------------------------------------------------------
  6020 format('0',3x,' LDAY RESET TO',i6)
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
