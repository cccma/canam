!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine undotf1(x,xm,f,isiz)
  !
  !     * oct 26/87 - r.laprise.
  !     * undo the effect of tf1.
  !     * f  is the time filter coefficient.
  !     * X  IS X' (K) ON INPUT AND X (K) ON OUTPUT,
  !     * XM IS X''(K-1).
  !
  implicit none
  real, intent(in) :: f
  integer, intent(in) :: isiz

  complex, intent(inout) :: x(isiz) !< Variable description\f$[units]\f$
  complex, intent(in) :: xm(isiz) !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  integer :: i
  !-----------------------------------------------------------------------
  do i=1,isiz
    x(i) = (x(i)-f*xm(i)) / (1.-2.*f)
  end do ! loop 100
  !
  return
end subroutine undotf1
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
