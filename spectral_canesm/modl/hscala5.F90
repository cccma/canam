!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine hscala5 (q, es, il1, il2, ilg, ilgp, ilev, levs, &
                    moist, lowbnd, sref, spow, rmoon, smin)
  !
  !     * may 24/2017 - m.lazare. for conversion to new xc40:
  !     *                         - replace ibm intrinsics by generic
  !     *                           for portability.
  !     * sep 13/2007 - m.lazare. new version for gcm15g:
  !     *                         - il1,il2 passed instead of "LON".
  !     *                         - "FACT" and "EFACT" now internal.
  !     *                         - unused temp,prespa removed.
  !     * sep 13/2006 - m.lazare/ previous version hscala4 for gcm15f:
  !     *               f.majaess.- work arrays now local.
  !
  !     * calculate specific humidity (q) from model moisture
  !     * variable (es), as defined by switch moist.
  !
  implicit none
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: ilgp !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: levs  !< Number of moisture levels in the vertical \f$[unitless]\f$
  integer, intent(in) :: moist
  real, intent(in) :: rmoon
  real, intent(in) :: smin
  real, intent(in) :: spow
  real, intent(in) :: sref

  logical, intent(in) :: lowbnd !< Variable description\f$[units]\f$

  real, intent(out), dimension(ilg,ilev) :: q !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilgp,levs) :: es !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  !     * work arrays.
  !
  real, dimension(ilg,ilev) :: fact
  real, dimension(ilg,ilev) :: efact
  !
  real :: esvalx
  integer :: i
  integer :: l
  integer :: m
  integer :: msg
  integer :: nc4to8
  real :: pinv
  real :: qval
  integer*4 :: len
  integer :: machine
  integer :: intsize
  !
  !     * common block to hold word size.
  !
  common /machtyp/ machine,intsize
  !-----------------------------------------------------------------------
  msg = ilev - levs
  if (msg > 0) then
    do l = 1,msg
      do i = il1,il2
        q(i,l) = 0.
      end do
    end do
  end if
  !
  if (moist == nc4to8("   Q") .or. moist == nc4to8("SL3D")) then
    !
    do l = msg + 1,ilev
      m = l - msg
      do i = il1,il2
        q(i,l) = es(i,m)
      end do
    end do

  else if (moist == nc4to8("QHYB") .or. moist == nc4to8("SLQB")) then
    !
    fact (1:ilg, 1:ilev) = 0.
    efact(1:ilg, 1:ilev) = 1.
    do l = msg + 1,ilev
      m = l - msg
      if (spow == 1.) then
        do i = il1,il2
          esvalx = max(es(i,m),smin)
          fact(i,l) = (1. - (sref/esvalx))
          efact(i,l) = exp(fact(i,l))
        end do
      else
        pinv = 1./spow
        do i = il1,il2
          esvalx = max(es(i,m),smin)
          fact(i,l) = pinv * (1. - (sref/esvalx) ** spow)
          efact(i,l) = exp(fact(i,l))
        end do
      end if
    end do
    !
    do l = msg + 1,ilev
      m = l - msg
      do i = il1,il2
        if (es(i,m) >= sref) then
          qval = es(i,m)
        else
          !             qval = sref*efact(i,l)
          !             if (es(i,m)<=smin) qval=rmoon
          if (es(i,m) <= smin) then
            qval = rmoon
          else
            qval = sref * efact(i,l)
          end if
        end if
        q(i,l) = qval
      end do
    end do

  else
    call xit('HSCALA4', - 1)
  end if
  !
  if (lowbnd) then
    do l = msg + 1,ilev
      do i = il1,il2
        q(i,l) = max(rmoon,q(i,l))
      end do
    end do
  end if

  return
end subroutine hscala5
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
