!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine hydlab(lc,lg,icanp1,ignd)

  !     * aug 13/91 - m.lazare. new version for gcm7 ihyd=2 (ican=4).

  !     * jan 20/91 - m.lazare. previous version getgg7u for gcm7u.
  !
  !     * defines level index values for canopy and sub-surface land-
  !     * surface scheme (ihyd=2) arrays.
  !     * canopy array is extended to include "URBAN" class.
  !
  implicit none
  integer, intent(inout) :: icanp1
  integer, intent(inout) :: ignd
  integer :: l

  integer, intent(inout), dimension(icanp1) :: lc !< Variable description\f$[units]\f$
  integer, intent(inout), dimension(ignd) :: lg !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !-----------------------------------------------------------------------
  do l=1,icanp1
    lc(l)=l
  end do ! loop 100

  do l=1,ignd
    lg(l)=l
  end do ! loop 200

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
