!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine bascalx(sgb,shb, sg,sh, nk,lay)

  !     * sep 23/03 - m.lazare. add "MPINFO" common block to control only
  !     *                       writing to standard output on node 0.
  !     *********************** this means that this routine must now
  !     *********************** be in the "MODEL" archive library instead
  !     *********************** of "COMM", where "BASCAL" will continue to
  !     *********************** be used.
  !     * jan 25/93 - e.chan.   previous version bascal.
  !
  !     * calcul des bases des couches a partir de
  !     * la position des "MILIEUX".
  !     *
  !     * sgb(k)    : bases des couches du vent.
  !     * shb(k)    : bases des couches de temperature.
  !     * sg(k)     : milieu des couches du vent.
  !     * sh(k)     : milieu des couches de temperature.
  !     * nk        : nombre de couches.
  !     *
  !     * LAY = TYPE D'ARRANGEMENT DES INTERFACES PAR RAPPORT
  !     *       aux milieux de couches.
  !     *     = 0, comme lay=2 pour v et lay=4 pour t,
  !     *          pour compatibilite avec version anterieure du gcm.
  !     *     = 1 / 2, interfaces des couches placees a distance egale
  !     *              sigma / geometrique du milieu des couches
  !     *              adjacentes.
  !     *     = 3 / 4, bases des couches extrapolees en montant
  !     *              de sorte que les milieux des couches soient
  !     *              au centre sigma / geometrique des couches.
  !     * DECOUPLAGE DE L'EPAISSEUR DES COUCHES PEUT RESULTER
  !     * DE L'APPLICATION DE LAY=3 OU 4 (AUSSI =0 POUR T) SI
  !     * les milieux de couches ne sont pas definis de facon
  !     * appropries.
  !
  implicit none
  integer :: k
  integer, intent(inout) :: lay  !< Number of vertical layers \f$[unitless]\f$
  integer, intent(inout) :: nk
  !
  real, intent(inout), dimension(nk) :: sgb !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nk) :: shb !< Variable description\f$[units]\f$
  real, intent(in), dimension(nk) :: sg !< Variable description\f$[units]\f$
  real, intent(in), dimension(nk) :: sh !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  integer*4 :: mynode !<
  !
  common /mpinfo/ mynode
  !--------------------------------------------------------------------
  !     * definition des bases des couches.
  !
  if (lay==0) then
    sgb(nk)=1.e0
    shb(nk)=1.e0
    do k=nk-1,1,-1
      sgb(k) = sqrt(sg(k)*sg(k+1))
      shb(k) = sh(k+1)**2/shb(k+1)
    end do ! loop 1
    !
  else if (lay==1) then
    do k=1,nk-1
      sgb(k)=0.5e0*(sg(k)+sg(k+1))
      shb(k)=0.5e0*(sh(k)+sh(k+1))
    end do ! loop 3
    sgb(nk)=1.0e0
    shb(nk)=1.0e0
    !
  else if (lay==2) then
    do k=1,nk-1
      sgb(k)=sqrt(sg(k)*sg(k+1))
      shb(k)=sqrt(sh(k)*sh(k+1))
    end do ! loop 4
    sgb(nk)=1.0e0
    shb(nk)=1.0e0
    !
  else if (lay==3) then
    sgb(nk)=1.0e0
    shb(nk)=1.0e0
    do k=nk-1,1,-1
      sgb(k)=2.e0*sg(k+1)-sgb(k+1)
      shb(k)=2.e0*sh(k+1)-shb(k+1)
    end do ! loop 5
    !
  else if (lay==4) then
    sgb(nk)=1.0e0
    shb(nk)=1.0e0
    do k=nk-1,1,-1
      sgb(k)=sg(k+1)**2/sgb(k+1)
      shb(k)=sh(k+1)**2/shb(k+1)
    end do ! loop 6
    !
  else
    call xit('BASCALX',-1)
  end if
  !
  if (mynode==0) then
    call writlev(sg,nk,' SG ')
    call writlev(sgb,nk,' SGB')
    call writlev(sh,nk,' SH ')
    call writlev(shb,nk,' SHB')
  end if
  !
  return
  !--------------------------------------------------------------
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
