!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

!
!     * feb 26/2009 - m.lazare/    new for gcm15h.
!     *               k.vonsalzen.
!
!     * defines emission fields used by gcm15h.
!
!=========================================================================
!     * multi-month, single level: (read/interpolated by getchem3/intchem3)
!     * -------------------------------------------------------------------
!     * dmso    kettle oceanic dms concentrations (nanomol/litre)
!     * edms    land dms fluxes (kg/m2-s)


!     * multi-month, 6 "levels": (read/interpolated by getwildf/intwf)
!     * --------------------------------------------------------------
!     * ebwa    aerocom wildfire bcc emissions (kg/m2-s) (1997-2002)
!     * eowa    aerocom wildfire pom emissions (kg/m2-s) (1997-2002)
!     * eswa    aerocom wildfire so2 emissions (kg/m2-s) (1997-2002)


!     * annual value, single level: (read by getace)
!     * --------------------------------------------
!     * ebbt    aerocom biofuel bc emissions (kg/m2-s) (2000)
!     * eobt    aerocom biofuel pom emissions (kg/m2-s) (2000)
!     * ebft    aerocom fossil fuel bc emissions (kg/m2-s) (2000)
!     * eoft    aerocom fossil fuel pom emissions (kg/m2-s) (2000)
!     * esdt    aerocom domestic so2 emissions (kg/m2-s) (2000)
!     * esit    aerocom industrial so2 emissions (kg/m2-s) (2000)
!     * esst    aerocom shipping so2 emissions (kg/m2-s) (2000)
!     * esot    aerocom off-road so2 emissions (kg/m2-s) (2000)
!     * espt    aerocom power plant so2 emissions (kg/m2-s) (2000)
!     * esrt    aerocom road so2 emissions (kg/m2-s) (2000)


!     * multi-month, single level: (read/interpolated by getcac/intcac)
!     * -------------------------------------------------------------------
!     * eost    aerocom secondary organic emissions (kg/m2-s) (2000)


!     * volcano fields (annual value, single level): (read by getnae)
!     * -------------------------------------------------------------
!     * escv    aerocom continuous volcano so2 emissions (kg/m2-s)
!     * ehcv    aerocom continuous volcano height (m)
!     * esev    aerocom explosive  volcano so2 emissions (kg/m2-s)
!     * ehev    aerocom explosive  volcano height (m)

!     * multi-month, single level: (read/interpolated by getch/inthem)
!     * these are for "transient historic emissions" (1850-2000)
!     * and activated by "%df histemi"
!     * --------------------------------------------------------------
!     * eboc    open vegetation burning organic carbon emissions (kg/m2-s)
!     * ebbc    open vegetation burning black   carbon emissions (kg/m2-s)
!     * eaoc    fossil and bio fuel organic carbon emissions (kg/m2-s)
!     * eabc    fossil and bio fuel black carbon emissions (kg/m2-s)
!     * easl    anthropogenic sulphur emissions for low level (kg/m2-s)
!     * eash    anthropogenic sulphur emissions for high level (kg/m2-s)

!     * multi-month, single level: (read/interpolated by getch/inthem)
!     * these are for "transient historic emissions" (1850-2000)
!     * and activated by "%df emists", in kg/m2-s (s or c).
!     * --------------------------------------------------------------

!     * sair    aircraft emissions of sulphur dioxide
!     * ssfc    anthropogenic surface emissions of sulphur dioxde
!     * sstk    stack emissions of sulphur dioxide
!     * sfir    forest fire emissions of sulphur dioxide
!     * oair    aircraft emissions of organic carbon aerosol
!     * osfc    anthropogenic surface emissions of organic carbon aerosol
!     * ostk    stack emissions of organic carbon aerosol
!     * ofir    forest fire emissions of organic carbon aerosol
!     * bair    aircraft emissions of black carbon aerosol
!     * bsfc    anthropogenic surface emissions of black carbon aerosol
!     * bstk    stack emissions of black carbon aerosol
!     * bfir    forest fire emissions of black carbon aerosol

!     * the following are for "transient historic emissions" (1850-2000)
!     * regardless of the "%df histemi" or "%df emists" settings

!     * multi-month, levwf levels: (read/interpolated by getwfh/intprf)
!     * ---------------------------------------------------------------
!     * fbbc    fraction of emission by level for open vegetation burning  (2000)

!     * multi-month, levair levels: (read/interpolated by getach/intprf)
!     * ---------------------------------------------------------------
!     * fair   fraction of emission by level for aircraft emissions
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

