!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

!     * aug 23/2018 - v.arora.   additional cmip6 output variables
!                                added to output to tm file
!
!     * jul 17/2018 - v.arora.   no need to output ch4n
!                                and ch4h from specified wetlands.
!                                Also don't output CW2D because
!                                we don't use it. So the only wetland
!                                ch4 emissions are now based on dynamic
!                                wetlands and heterotrophic respiration
!                                and called cw1d.
!
!     * feb 16/2016 - m.lazare.  new routine (common deck) for gcm19.
!     *                          save fields for ctem, analagous to
!     *                          how save normal fields in agcm.
!=========================================================================
!     * save daily ctem output to file nutm (outcm).

k=iymd

!          1. vegetation biomass
call putgg(cvegpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CVEG"),1)

!          2. litter mass
call putgg(cdebpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CDEB"),1)

!          3. soil c mass
call putgg(chumpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CHUM"),1)

!          4. leaf area index
call putgg(claipak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CLAI"),1)

!          5. net primary productivity
call putgg(cfnppak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CFNP"),1)

!          6. net ecosystem productivity
call putgg(cfnepak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CFNE"),1)

!          7. autotrophic respiration
call putgg(cfrvpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CFRV"),1)

!          8. gross primary productivity
call putgg(cfgppak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CFGP"),1)

!          9. net biome productivity
call putgg(cfnbpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CFNB"),1)

!          10. fire losses from vegetation
call putgg(cffvpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CFFV"),1)

!          11. fire losses from litter
call putgg(cffdpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CFFD"),1)

!          12. land use change combustion losses from vegetation
call putgg(cflvpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CFLV"),1)

!          13. land use change litter inputs
call putgg(cfldpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CFLD"),1)

!          14. land use change soil carbon inputs
call putgg(cflhpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CFLH"),1)

!          15. area burned
call putgg(cbrnpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CBRN"),1)

!          16. respiration from soil c pool
call putgg(cfrhpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CFRH"),1)

!          17. humification transfer from litter to soil c pool
call putgg(cfhtpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CFHT"),1)

!          18. litter fall
call putgg(cflfpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CFLF"),1)

!          19. litter respiration
call putgg(cfrdpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CFRD"),1)
!
!          additional cmip5 output
!
!          20. growth respiration
call putgg(cfrgpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CFRG"),1)

!          21. maintenance respiration
call putgg(cfrmpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CFRM"),1)

!          22. biomass in leaves
call putgg(cvglpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CVGL"),1)

!          23. biomass in stem
call putgg(cvgspak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CVGS"),1)

!          24. biomass in roots
call putgg(cvgrpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CVGR"),1)

!          25. npp of leaves
call putgg(cfnlpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CFNL"),1)

!          26. npp of stem
call putgg(cfnspak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CFNS"),1)

!          27. npp of roots
call putgg(cfnrpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CFNR"),1)

!          28. ch4 wetland emissions, heterotrophic resp. based
!          call putgg(ch4hpak,lon1,ilat,khem,npgg,k,nutm,
!    &                  nc4to8("CH4H"),1)

!          29. ch4 wetland emissions, npp based
!          call putgg(ch4npak,lon1,ilat,khem,npgg,k,nutm,
!    &                  nc4to8("CH4N"),1)

!          30. dynamic wetland fraction
call putgg(wfrapak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("WFRA"),1)

!          31. dynamic ch4 wetland emissions, heterotrophic
call putgg(cw1dpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CW1D"),1)

!          32. dynamic ch4 wetland emissions, npp
!          call putgg(cw2dpak,lon1,ilat,khem,npgg,k,nutm,
!    &                  nc4to8("CW2D"),1)

!          33. carbon land flux
call putgg(fcolpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("FCOL"),1)

!          34. current ctem vegetation fraction (ictem levels)
do l=1,ictem
  call putgg(curfpak(1,l),lon1,ilat,khem,npgg,k,nutm, &
                        nc4to8("CURF"),lctem(l))
end do

!          additional ctem related cmip6 output

!          35. brfrpak   ! baresoilfrac
call putgg(brfrpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("BRFR"),1)

!          36. c3crpak   ! cropfracc3
call putgg(c3crpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("C3CR"),1)

!          37. c4crpak   ! cropfracc4
call putgg(c4crpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("C4CR"),1)

!          38. crpfpak   ! cropfrac
call putgg(crpfpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CRPF"),1)

!          39. c3grpak   ! grassfracc3
call putgg(c3grpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("C3GR"),1)

!          40. c4grpak   ! grassfracc4
call putgg(c4grpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("C4GR"),1)

!          41. grsfpak   ! grassfrac
call putgg(grsfpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("GRSF"),1)

!          42. bdtfpak   ! treefracbdldcd
call putgg(bdtfpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("BDTF"),1)

!          43. betfpak   ! treefracbdlevg
call putgg(betfpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("BETF"),1)

!          44. ndtfpak   ! treefracndldcd
call putgg(ndtfpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("NDTF"),1)

!          45. netfpak   ! treefracndlevg
call putgg(netfpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("NETF"),1)

!          46. treepak   ! treefrac
call putgg(treepak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("TREE"),1)

!          47. vegfpak   ! vegfrac
call putgg(vegfpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("VEGF"),1)

!          48. c3pfpak   ! c3pftfrac
call putgg(c3pfpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("C3PF"),1)

!          49. c4pfpak   ! c4pftfrac
call putgg(c4pfpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("C4PF"),1)

!          50. clndpak   ! cland
call putgg(clndpak,lon1,ilat,khem,npgg,k,nutm, &
                      nc4to8("CLND"),1)

!
!     *    zero out accumulated fields.
!     *    note that {cveg,cdeb,chum,clai,cvgl,cvgs,cvgr,curf}
!     *    are only sampled !
!
!     *    the additional cmip6 land cover related fractions
!          and other variables (clnd)
!     *    are zeroed as row variables in sfcproc2.dk so likely
!     *    not needed to be zeroed here.
!
call pkzeros2(cfnppak,ijpak,   1)
call pkzeros2(cfnepak,ijpak,   1)
call pkzeros2(cfrvpak,ijpak,   1)
call pkzeros2(cfgppak,ijpak,   1)
call pkzeros2(cfnbpak,ijpak,   1)
call pkzeros2(cffvpak,ijpak,   1)
call pkzeros2(cffdpak,ijpak,   1)
call pkzeros2(cflvpak,ijpak,   1)
call pkzeros2(cfldpak,ijpak,   1)
call pkzeros2(cflhpak,ijpak,   1)
call pkzeros2(cbrnpak,ijpak,   1)
call pkzeros2(cfrhpak,ijpak,   1)
call pkzeros2(cfhtpak,ijpak,   1)
call pkzeros2(cflfpak,ijpak,   1)
call pkzeros2(cfrdpak,ijpak,   1)
call pkzeros2(cfrgpak,ijpak,   1)
call pkzeros2(cfrmpak,ijpak,   1)
call pkzeros2(cfnlpak,ijpak,   1)
call pkzeros2(cfnspak,ijpak,   1)
call pkzeros2(cfnrpak,ijpak,   1)
call pkzeros2(ch4hpak,ijpak,   1)
call pkzeros2(ch4npak,ijpak,   1)
call pkzeros2(wfrapak,ijpak,   1)
call pkzeros2(cw1dpak,ijpak,   1)
call pkzeros2(cw2dpak,ijpak,   1)
call pkzeros2(fcolpak,ijpak,   1)
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
