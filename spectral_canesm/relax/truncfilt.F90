!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine truncfilt(g,wrk,lm,la,latotal,lmtotal, &
                           lsrtotal,ntrunc)
  !
  !     sunrountine returns g filter
  !     1 < ntrunc
  !     0 > ntrunc
  !
  implicit none
  real :: eh
  real :: gi
  real :: gr
  integer :: ibuf
  integer :: idat
  integer :: intsize
  integer :: k
  integer :: kl
  integer :: kr
  integer, intent(in) :: la
  integer :: lac
  integer, intent(in) :: latotal
  integer, intent(in) :: lm
  integer :: lmh
  integer, intent(in) :: lmtotal
  integer :: m
  integer :: machine
  integer :: mc
  integer :: mend
  integer :: mstart
  integer :: mval
  integer :: n
  integer :: nl
  integer :: nr
  integer :: ns
  integer, intent(in) :: ntrunc
  !     * level 2,g
  complex, intent(inout) :: g(la) !< Variable description\f$[units]\f$
  complex, intent(inout) :: wrk(latotal) !< Variable description\f$[units]\f$
  integer, intent(in) :: lsrtotal(2,lmtotal+1) !< Variable description\f$[units]\f$
  integer*4 :: mynode !<
  common /mpinfo/ mynode
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  !     * icom is a shared i/o area. it must be large enough
  !     * for an 8 word label followed by a packed gaussian grid.
  !
  common /icom/ ibuf(8),idat(1)
  common /machtyp/ machine, intsize

  !-----------------------------------------------------------------------
  do m=1,lmtotal
    kl=lsrtotal(1,m)
    kr=lsrtotal(1,m+1)-1
    do k=kl,kr
      ns=(m-1)+(k-kl)
      !
      !     * eh=11.   (t35=06h -> t47=24h  -> t63=162days)
      !     * eh=15.96 (t21=24h -> t35=2.4d -> t47=17d -> t63=1000d)
      !
      eh=15.96
      if (ns>ntrunc) then
        ! use this line for smooth reduction of filter strength
        !         gr=exp(-(float(ns-ntrunc))**2/(eh**2))
        ! use this line for step-wise filter strength
        gr=0.
      else
        gr=1.0
      end if
      !         gr=float(ns*(ns+1))
      gi=0.e0
      !         write(6,*) "ns, GR ",ns, gr
      wrk(k)=cmplx(gr,gi)
    end do
  end do ! loop 100


  lmh=lmtotal/2
  mc=0
  lac=0
  !
  !     * mpi hook.
  !
  mstart=mynode*lm+1
  mend  =mynode*lm+lm
  !
  !     * construct "RECTANGLE" from pairs of low/high zonal wavenumbers.
  !
  do m=1,lmh
    !
    mval=m
    mc=mc+1

    if (mc>=mstart .and. mc .le. mend) then
      nl=lsrtotal(1,mval)
      nr=lsrtotal(1,mval+1)-1
      do n=nl,nr
        lac=lac+1
        g(lac)=wrk(n)
      end do
    end if
    !
    mval=lmtotal-m+1
    mc=mc+1

    if (mc>=mstart .and. mc .le. mend) then
      nl=lsrtotal(1,mval)
      nr=lsrtotal(1,mval+1)-1
      do n=nl,nr
        lac=lac+1
        g(lac)=wrk(n)
      end do
    end if
    !
  end do


  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
