!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine qpassf3(a,b,c,d,trigs,inc1,inc2,inc3,inc4,lot,n,ndim,ifac, &
                         la,ierr)
  !
  !     * aug 01/03 - m.lazare. generalized using "MAXLOT" passed in
  !     *                       common block, instead of hard-coded
  !     *                       value of 256 only valid for the nec.
  !     * nov 01/92 - j.stacey. previous verstion qpassf2.
  !
  !     SUBROUTINE 'QPASSF3' - PERFORMS ONE PASS THROUGH DATA AS PART
  !     of multiple real :: fft (fourier analysis) routine
  !
  !     a is first real :: input vector
  !         equivalence b(1) with a(ifac*la*inc1+1)
  !     c is first real :: output vector
  !         equivalence d(1) with c(la*inc2+1)
  !     trigs is a precalculated list of sines & cosines
  !     inc1 is the addressing increment for a
  !     inc2 is the addressing increment for c
  !     inc3 is the increment between input vectors a
  !     inc4 is the increment between output vectors c
  !     lot is the number of vectors
  !     n is the length of the vectors
  !     ifac is the current factor of n
  !     la = n/(product of factors used so far)
  !     ierr is an error indicator:
  !              0 - pass completed without error
  !              1 - lot greater than maxlot
  !              2 - ifac not catered for
  !              3 - ifac only catered for if la=n/ifac
  !-----------------------------------------------------------------------
  implicit none
  integer, intent(inout) :: ierr
  integer, intent(in) :: ifac
  integer, intent(in) :: inc1
  integer, intent(in) :: inc2
  integer, intent(in) :: inc3
  integer, intent(in) :: inc4
  integer, intent(in) :: la
  integer, intent(in) :: lot
  integer, intent(in) :: n
  integer, intent(in) :: ndim
  !
  real, intent(in), dimension(ndim) :: a !< Variable description\f$[units]\f$
  real, intent(in), dimension(ndim) :: b !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ndim) :: c !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ndim) :: d !< Variable description\f$[units]\f$
  real, intent(in), dimension(n) :: trigs !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  real :: a0
  real :: a1
  real :: a10
  real :: a11
  real :: a2
  real :: a20
  real :: a21
  real :: a3
  real :: a4
  real :: a5
  real :: a6
  real :: b0
  real :: b1
  real :: b10
  real :: b11
  real :: b2
  real :: b20
  real :: b21
  real :: b3
  real :: b4
  real :: b5
  real :: b6
  real :: c1
  real :: c2
  real :: c3
  real :: c4
  real :: c5
  integer :: i
  integer :: ia
  integer :: ib
  integer :: ibad
  integer :: ibase
  integer :: ic
  integer :: id
  integer :: ie
  integer :: if
  integer :: ig
  integer :: igo
  integer :: ih
  integer :: iink
  integer :: ijk
  integer :: ijump
  integer :: j
  integer :: ja
  integer :: jb
  integer :: jbase
  integer :: jc
  integer :: jd
  integer :: je
  integer :: jf
  integer :: jink
  integer :: k
  integer :: kb
  integer :: kc
  integer :: kd
  integer :: ke
  integer :: kf
  integer :: kstop
  integer :: l
  integer :: m
  integer :: maxlot
  real :: qrt5
  real :: s1
  real :: s2
  real :: s3
  real :: s4
  real :: s5
  real :: sin36
  real :: sin45
  real :: sin60
  real :: sin72
  real :: z
  real :: zqrt5
  real :: zsin36
  real :: zsin45
  real :: zsin60
  real :: zsin72
  !
  common /itrans/ maxlot
  !
  data sin36/0.587785252292473/,sin72/0.951056516295154/, &
     qrt5/0.559016994374947/,sin60/0.866025403784437/
  !========================================================================
  m=n/ifac
  iink=la*inc1
  jink=la*inc2
  ijump=(ifac-1)*iink
  kstop=(n-ifac)/(2*ifac)
  !
  ibad=1
  if (lot > maxlot) go to 910
  ibase=0
  jbase=0
  igo=ifac-1
  if (igo==7) igo=6
  ibad=2
  if (igo<1.or.igo>6) go to 910
  go to (200,300,400,500,600,800),igo
  !
  !     coding for factor 2
  !     -------------------
200 continue
  ia=1
  ib=ia+iink
  ja=1
  jb=ja+(2*m-la)*inc2
  !
  if (la==m) go to 290
  !
  do l=1,la
    i=ibase
    j=jbase
    ! vdir nodep
    do ijk=1,lot
      c(ja+j)=a(ia+i)+a(ib+i)
      c(jb+j)=a(ia+i)-a(ib+i)
      i=i+inc3
      j=j+inc4
    end do ! loop 210
    ibase=ibase+inc1
    jbase=jbase+inc2
  end do ! loop 220
  ja=ja+jink
  jink=2*jink
  jb=jb-jink
  ibase=ibase+ijump
  ijump=2*ijump+iink
  if (ja==jb) go to 260
  do k=la,kstop,la
    kb=k+k
    c1=trigs(kb+1)
    s1=trigs(kb+2)
    jbase=0
    do l=1,la
      i=ibase
      j=jbase
      ! vdir nodep
      do ijk=1,lot
        c(ja+j)=a(ia+i)+(c1*a(ib+i)+s1*b(ib+i))
        c(jb+j)=a(ia+i)-(c1*a(ib+i)+s1*b(ib+i))
        d(ja+j)=(c1*b(ib+i)-s1*a(ib+i))+b(ia+i)
        d(jb+j)=(c1*b(ib+i)-s1*a(ib+i))-b(ia+i)
        i=i+inc3
        j=j+inc4
      end do ! loop 230
      ibase=ibase+inc1
      jbase=jbase+inc2
    end do ! loop 240
    ibase=ibase+ijump
    ja=ja+jink
    jb=jb-jink
  end do ! loop 250
  if (ja>jb) go to 900
260 continue
  jbase=0
  do l=1,la
    i=ibase
    j=jbase
    ! vdir nodep
    do ijk=1,lot
      c(ja+j)=a(ia+i)
      d(ja+j)=-a(ib+i)
      i=i+inc3
      j=j+inc4
    end do ! loop 270
    ibase=ibase+inc1
    jbase=jbase+inc2
  end do ! loop 280
  go to 900
  !
290 continue
  z=1.0/float(n)
  do l=1,la
    i=ibase
    j=jbase
    ! vdir nodep
    do ijk=1,lot
      c(ja+j)=z*(a(ia+i)+a(ib+i))
      c(jb+j)=z*(a(ia+i)-a(ib+i))
      i=i+inc3
      j=j+inc4
    end do ! loop 292
    ibase=ibase+inc1
    jbase=jbase+inc2
  end do ! loop 294
  go to 900
  !
  !     coding for factor 3
  !     -------------------
300 continue
  ia=1
  ib=ia+iink
  ic=ib+iink
  ja=1
  jb=ja+(2*m-la)*inc2
  jc=jb
  !
  if (la==m) go to 390
  !
  do l=1,la
    i=ibase
    j=jbase
    ! vdir nodep
    do ijk=1,lot
      c(ja+j)=a(ia+i)+(a(ib+i)+a(ic+i))
      c(jb+j)=a(ia+i)-0.5*(a(ib+i)+a(ic+i))
      d(jb+j)=sin60*(a(ic+i)-a(ib+i))
      i=i+inc3
      j=j+inc4
    end do ! loop 310
    ibase=ibase+inc1
    jbase=jbase+inc2
  end do ! loop 320
  ja=ja+jink
  jink=2*jink
  jb=jb+jink
  jc=jc-jink
  ibase=ibase+ijump
  ijump=2*ijump+iink
  if (ja==jc) go to 360
  do k=la,kstop,la
    kb=k+k
    kc=kb+kb
    c1=trigs(kb+1)
    s1=trigs(kb+2)
    c2=trigs(kc+1)
    s2=trigs(kc+2)
    jbase=0
    do l=1,la
      i=ibase
      j=jbase
      ! vdir nodep
      do ijk=1,lot
        a1=(c1*a(ib+i)+s1*b(ib+i))+(c2*a(ic+i)+s2*b(ic+i))
        b1=(c1*b(ib+i)-s1*a(ib+i))+(c2*b(ic+i)-s2*a(ic+i))
        a2=a(ia+i)-0.5*a1
        b2=b(ia+i)-0.5*b1
        a3=sin60*((c1*a(ib+i)+s1*b(ib+i))-(c2*a(ic+i)+s2*b(ic+i)))
        b3=sin60*((c1*b(ib+i)-s1*a(ib+i))-(c2*b(ic+i)-s2*a(ic+i)))
        c(ja+j)=a(ia+i)+a1
        d(ja+j)=b(ia+i)+b1
        c(jb+j)=a2+b3
        d(jb+j)=b2-a3
        c(jc+j)=a2-b3
        d(jc+j)=-(b2+a3)
        i=i+inc3
        j=j+inc4
      end do ! loop 330
      ibase=ibase+inc1
      jbase=jbase+inc2
    end do ! loop 340
    ibase=ibase+ijump
    ja=ja+jink
    jb=jb+jink
    jc=jc-jink
  end do ! loop 350
  if (ja>jc) go to 900
360 continue
  jbase=0
  do l=1,la
    i=ibase
    j=jbase
    ! vdir nodep
    do ijk=1,lot
      c(ja+j)=a(ia+i)+0.5*(a(ib+i)-a(ic+i))
      d(ja+j)=-sin60*(a(ib+i)+a(ic+i))
      c(jb+j)=a(ia+i)-(a(ib+i)-a(ic+i))
      i=i+inc3
      j=j+inc4
    end do ! loop 370
    ibase=ibase+inc1
    jbase=jbase+inc2
  end do ! loop 380
  go to 900
  !
390 continue
  z=1.0/float(n)
  zsin60=z*sin60
  do l=1,la
    i=ibase
    j=jbase
    ! vdir nodep
    do ijk=1,lot
      c(ja+j)=z*(a(ia+i)+(a(ib+i)+a(ic+i)))
      c(jb+j)=z*(a(ia+i)-0.5*(a(ib+i)+a(ic+i)))
      d(jb+j)=zsin60*(a(ic+i)-a(ib+i))
      i=i+inc3
      j=j+inc4
    end do ! loop 392
    ibase=ibase+inc1
    jbase=jbase+inc2
  end do ! loop 394
  go to 900
  !
  !     coding for factor 4
  !     -------------------
400 continue
  ia=1
  ib=ia+iink
  ic=ib+iink
  id=ic+iink
  ja=1
  jb=ja+(2*m-la)*inc2
  jc=jb+2*m*inc2
  jd=jb
  !
  if (la==m) go to 490
  !
  do l=1,la
    i=ibase
    j=jbase
    ! vdir nodep
    do ijk=1,lot
      c(ja+j)=(a(ia+i)+a(ic+i))+(a(ib+i)+a(id+i))
      c(jc+j)=(a(ia+i)+a(ic+i))-(a(ib+i)+a(id+i))
      c(jb+j)=a(ia+i)-a(ic+i)
      d(jb+j)=a(id+i)-a(ib+i)
      i=i+inc3
      j=j+inc4
    end do ! loop 410
    ibase=ibase+inc1
    jbase=jbase+inc2
  end do ! loop 420
  ja=ja+jink
  jink=2*jink
  jb=jb+jink
  jc=jc-jink
  jd=jd-jink
  ibase=ibase+ijump
  ijump=2*ijump+iink
  if (jb==jc) go to 460
  do k=la,kstop,la
    kb=k+k
    kc=kb+kb
    kd=kc+kb
    c1=trigs(kb+1)
    s1=trigs(kb+2)
    c2=trigs(kc+1)
    s2=trigs(kc+2)
    c3=trigs(kd+1)
    s3=trigs(kd+2)
    jbase=0
    do l=1,la
      i=ibase
      j=jbase
      ! vdir nodep
      do ijk=1,lot
        a0=a(ia+i)+(c2*a(ic+i)+s2*b(ic+i))
        a2=a(ia+i)-(c2*a(ic+i)+s2*b(ic+i))
        a1=(c1*a(ib+i)+s1*b(ib+i))+(c3*a(id+i)+s3*b(id+i))
        a3=(c1*a(ib+i)+s1*b(ib+i))-(c3*a(id+i)+s3*b(id+i))
        b0=b(ia+i)+(c2*b(ic+i)-s2*a(ic+i))
        b2=b(ia+i)-(c2*b(ic+i)-s2*a(ic+i))
        b1=(c1*b(ib+i)-s1*a(ib+i))+(c3*b(id+i)-s3*a(id+i))
        b3=(c1*b(ib+i)-s1*a(ib+i))-(c3*b(id+i)-s3*a(id+i))
        c(ja+j)=a0+a1
        c(jc+j)=a0-a1
        d(ja+j)=b0+b1
        d(jc+j)=b1-b0
        c(jb+j)=a2+b3
        c(jd+j)=a2-b3
        d(jb+j)=b2-a3
        d(jd+j)=-(b2+a3)
        i=i+inc3
        j=j+inc4
      end do ! loop 430
      ibase=ibase+inc1
      jbase=jbase+inc2
    end do ! loop 440
    ibase=ibase+ijump
    ja=ja+jink
    jb=jb+jink
    jc=jc-jink
    jd=jd-jink
  end do ! loop 450
  if (jb>jc) go to 900
460 continue
  sin45=sqrt(0.5)
  jbase=0
  do l=1,la
    i=ibase
    j=jbase
    ! vdir nodep
    do ijk=1,lot
      c(ja+j)=a(ia+i)+sin45*(a(ib+i)-a(id+i))
      c(jb+j)=a(ia+i)-sin45*(a(ib+i)-a(id+i))
      d(ja+j)=-a(ic+i)-sin45*(a(ib+i)+a(id+i))
      d(jb+j)=a(ic+i)-sin45*(a(ib+i)+a(id+i))
      i=i+inc3
      j=j+inc4
    end do ! loop 470
    ibase=ibase+inc1
    jbase=jbase+inc2
  end do ! loop 480
  go to 900
  !
490 continue
  z=1.0/float(n)
  do l=1,la
    i=ibase
    j=jbase
    ! vdir nodep
    do ijk=1,lot
      c(ja+j)=z*((a(ia+i)+a(ic+i))+(a(ib+i)+a(id+i)))
      c(jc+j)=z*((a(ia+i)+a(ic+i))-(a(ib+i)+a(id+i)))
      c(jb+j)=z*(a(ia+i)-a(ic+i))
      d(jb+j)=z*(a(id+i)-a(ib+i))
      i=i+inc3
      j=j+inc4
    end do ! loop 492
    ibase=ibase+inc1
    jbase=jbase+inc2
  end do ! loop 494
  go to 900
  !
  !     coding for factor 5
  !     -------------------
500 continue
  ia=1
  ib=ia+iink
  ic=ib+iink
  id=ic+iink
  ie=id+iink
  ja=1
  jb=ja+(2*m-la)*inc2
  jc=jb+2*m*inc2
  jd=jc
  je=jb
  !
  if (la==m) go to 590
  !
  do l=1,la
    i=ibase
    j=jbase
    ! vdir nodep
    do ijk=1,lot
      a1=a(ib+i)+a(ie+i)
      a3=a(ib+i)-a(ie+i)
      a2=a(ic+i)+a(id+i)
      a4=a(ic+i)-a(id+i)
      a5=a(ia+i)-0.25*(a1+a2)
      a6=qrt5*(a1-a2)
      c(ja+j)=a(ia+i)+(a1+a2)
      c(jb+j)=a5+a6
      c(jc+j)=a5-a6
      d(jb+j)=-sin72*a3-sin36*a4
      d(jc+j)=-sin36*a3+sin72*a4
      i=i+inc3
      j=j+inc4
    end do ! loop 510
    ibase=ibase+inc1
    jbase=jbase+inc2
  end do ! loop 520
  ja=ja+jink
  jink=2*jink
  jb=jb+jink
  jc=jc+jink
  jd=jd-jink
  je=je-jink
  ibase=ibase+ijump
  ijump=2*ijump+iink
  if (jb==jd) go to 560
  do k=la,kstop,la
    kb=k+k
    kc=kb+kb
    kd=kc+kb
    ke=kd+kb
    c1=trigs(kb+1)
    s1=trigs(kb+2)
    c2=trigs(kc+1)
    s2=trigs(kc+2)
    c3=trigs(kd+1)
    s3=trigs(kd+2)
    c4=trigs(ke+1)
    s4=trigs(ke+2)
    jbase=0
    do l=1,la
      i=ibase
      j=jbase
      ! vdir nodep
      do ijk=1,lot
        a1=(c1*a(ib+i)+s1*b(ib+i))+(c4*a(ie+i)+s4*b(ie+i))
        a3=(c1*a(ib+i)+s1*b(ib+i))-(c4*a(ie+i)+s4*b(ie+i))
        a2=(c2*a(ic+i)+s2*b(ic+i))+(c3*a(id+i)+s3*b(id+i))
        a4=(c2*a(ic+i)+s2*b(ic+i))-(c3*a(id+i)+s3*b(id+i))
        b1=(c1*b(ib+i)-s1*a(ib+i))+(c4*b(ie+i)-s4*a(ie+i))
        b3=(c1*b(ib+i)-s1*a(ib+i))-(c4*b(ie+i)-s4*a(ie+i))
        b2=(c2*b(ic+i)-s2*a(ic+i))+(c3*b(id+i)-s3*a(id+i))
        b4=(c2*b(ic+i)-s2*a(ic+i))-(c3*b(id+i)-s3*a(id+i))
        a5=a(ia+i)-0.25*(a1+a2)
        a6=qrt5*(a1-a2)
        b5=b(ia+i)-0.25*(b1+b2)
        b6=qrt5*(b1-b2)
        a10=a5+a6
        a20=a5-a6
        b10=b5+b6
        b20=b5-b6
        a11=sin72*b3+sin36*b4
        a21=sin36*b3-sin72*b4
        b11=sin72*a3+sin36*a4
        b21=sin36*a3-sin72*a4
        c(ja+j)=a(ia+i)+(a1+a2)
        c(jb+j)=a10+a11
        c(je+j)=a10-a11
        c(jc+j)=a20+a21
        c(jd+j)=a20-a21
        d(ja+j)=b(ia+i)+(b1+b2)
        d(jb+j)=b10-b11
        d(je+j)=-(b10+b11)
        d(jc+j)=b20-b21
        d(jd+j)=-(b20+b21)
        i=i+inc3
        j=j+inc4
      end do ! loop 530
      ibase=ibase+inc1
      jbase=jbase+inc2
    end do ! loop 540
    ibase=ibase+ijump
    ja=ja+jink
    jb=jb+jink
    jc=jc+jink
    jd=jd-jink
    je=je-jink
  end do ! loop 550
  if (jb>jd) go to 900
560 continue
  jbase=0
  do l=1,la
    i=ibase
    j=jbase
    ! vdir nodep
    do ijk=1,lot
      a1=a(ib+i)+a(ie+i)
      a3=a(ib+i)-a(ie+i)
      a2=a(ic+i)+a(id+i)
      a4=a(ic+i)-a(id+i)
      a5=a(ia+i)+0.25*(a3-a4)
      a6=qrt5*(a3+a4)
      c(ja+j)=a5+a6
      c(jb+j)=a5-a6
      c(jc+j)=a(ia+i)-(a3-a4)
      d(ja+j)=-sin36*a1-sin72*a2
      d(jb+j)=-sin72*a1+sin36*a2
      i=i+inc3
      j=j+inc4
    end do ! loop 570
    ibase=ibase+inc1
    jbase=jbase+inc2
  end do ! loop 580
  go to 900
  !
590 continue
  z=1.0/float(n)
  zqrt5=z*qrt5
  zsin36=z*sin36
  zsin72=z*sin72
  do l=1,la
    i=ibase
    j=jbase
    ! vdir nodep
    do ijk=1,lot
      a1=a(ib+i)+a(ie+i)
      a3=a(ib+i)-a(ie+i)
      a2=a(ic+i)+a(id+i)
      a4=a(ic+i)-a(id+i)
      a5=z*(a(ia+i)-0.25*(a1+a2))
      a6=zqrt5*(a1-a2)
      c(ja+j)=z*(a(ia+i)+(a1+a2))
      c(jb+j)=a5+a6
      c(jc+j)=a5-a6
      d(jb+j)=-zsin72*a3-zsin36*a4
      d(jc+j)=-zsin36*a3+zsin72*a4
      i=i+inc3
      j=j+inc4
    end do ! loop 592
    ibase=ibase+inc1
    jbase=jbase+inc2
  end do ! loop 594
  go to 900
  !
  !     coding for factor 6
  !     -------------------
600 continue
  ia=1
  ib=ia+iink
  ic=ib+iink
  id=ic+iink
  ie=id+iink
  if=ie+iink
  ja=1
  jb=ja+(2*m-la)*inc2
  jc=jb+2*m*inc2
  jd=jc+2*m*inc2
  je=jc
  jf=jb
  !
  if (la==m) go to 690
  !
  do l=1,la
    i=ibase
    j=jbase
    ! vdir nodep
    do ijk=1,lot
      a11=(a(ic+i)+a(if+i))+(a(ib+i)+a(ie+i))
      c(ja+j)=(a(ia+i)+a(id+i))+a11
      c(jc+j)=(a(ia+i)+a(id+i)-0.5*a11)
      d(jc+j)=sin60*((a(ic+i)+a(if+i))-(a(ib+i)+a(ie+i)))
      a11=(a(ic+i)-a(if+i))+(a(ie+i)-a(ib+i))
      c(jb+j)=(a(ia+i)-a(id+i))-0.5*a11
      d(jb+j)=sin60*((a(ie+i)-a(ib+i))-(a(ic+i)-a(if+i)))
      c(jd+j)=(a(ia+i)-a(id+i))+a11
      i=i+inc3
      j=j+inc4
    end do ! loop 610
    ibase=ibase+inc1
    jbase=jbase+inc2
  end do ! loop 620
  ja=ja+jink
  jink=2*jink
  jb=jb+jink
  jc=jc+jink
  jd=jd-jink
  je=je-jink
  jf=jf-jink
  ibase=ibase+ijump
  ijump=2*ijump+iink
  if (jc==jd) go to 660
  do k=la,kstop,la
    kb=k+k
    kc=kb+kb
    kd=kc+kb
    ke=kd+kb
    kf=ke+kb
    c1=trigs(kb+1)
    s1=trigs(kb+2)
    c2=trigs(kc+1)
    s2=trigs(kc+2)
    c3=trigs(kd+1)
    s3=trigs(kd+2)
    c4=trigs(ke+1)
    s4=trigs(ke+2)
    c5=trigs(kf+1)
    s5=trigs(kf+2)
    jbase=0
    do l=1,la
      i=ibase
      j=jbase
      ! vdir nodep
      do ijk=1,lot
        a1=c1*a(ib+i)+s1*b(ib+i)
        b1=c1*b(ib+i)-s1*a(ib+i)
        a2=c2*a(ic+i)+s2*b(ic+i)
        b2=c2*b(ic+i)-s2*a(ic+i)
        a3=c3*a(id+i)+s3*b(id+i)
        b3=c3*b(id+i)-s3*a(id+i)
        a4=c4*a(ie+i)+s4*b(ie+i)
        b4=c4*b(ie+i)-s4*a(ie+i)
        a5=c5*a(if+i)+s5*b(if+i)
        b5=c5*b(if+i)-s5*a(if+i)
        a11=(a2+a5)+(a1+a4)
        a20=(a(ia+i)+a3)-0.5*a11
        a21=sin60*((a2+a5)-(a1+a4))
        b11=(b2+b5)+(b1+b4)
        b20=(b(ia+i)+b3)-0.5*b11
        b21=sin60*((b2+b5)-(b1+b4))
        c(ja+j)=(a(ia+i)+a3)+a11
        d(ja+j)=(b(ia+i)+b3)+b11
        c(jc+j)=a20-b21
        d(jc+j)=a21+b20
        c(je+j)=a20+b21
        d(je+j)=a21-b20
        a11=(a2-a5)+(a4-a1)
        a20=(a(ia+i)-a3)-0.5*a11
        a21=sin60*((a4-a1)-(a2-a5))
        b11=(b5-b2)-(b4-b1)
        b20=(b3-b(ia+i))-0.5*b11
        b21=sin60*((b5-b2)+(b4-b1))
        c(jb+j)=a20-b21
        d(jb+j)=a21-b20
        c(jd+j)=a11+(a(ia+i)-a3)
        d(jd+j)=b11+(b3-b(ia+i))
        c(jf+j)=a20+b21
        d(jf+j)=a21+b20
        i=i+inc3
        j=j+inc4
      end do ! loop 630
      ibase=ibase+inc1
      jbase=jbase+inc2
    end do ! loop 640
    ibase=ibase+ijump
    ja=ja+jink
    jb=jb+jink
    jc=jc+jink
    jd=jd-jink
    je=je-jink
    jf=jf-jink
  end do ! loop 650
  if (jc>jd) go to 900
660 continue
  jbase=0
  do l=1,la
    i=ibase
    j=jbase
    ! vdir nodep
    do ijk=1,lot
      c(ja+j)=(a(ia+i)+0.5*(a(ic+i)-a(ie+i)))+ sin60*(a(ib+i)-a(if+i))
      d(ja+j)=-(a(id+i)+0.5*(a(ib+i)+a(if+i)))-sin60*(a(ic+i)+a(ie+i))
      c(jb+j)=a(ia+i)-(a(ic+i)-a(ie+i))
      d(jb+j)=a(id+i)-(a(ib+i)+a(if+i))
      c(jc+j)=(a(ia+i)+0.5*(a(ic+i)-a(ie+i)))-sin60*(a(ib+i)-a(if+i))
      d(jc+j)=-(a(id+i)+0.5*(a(ib+i)+a(if+i)))+sin60*(a(ic+i)+a(ie+i))
      i=i+inc3
      j=j+inc4
    end do ! loop 670
    ibase=ibase+inc1
    jbase=jbase+inc2
  end do ! loop 680
  go to 900
  !
690 continue
  z=1.0/float(n)
  zsin60=z*sin60
  do l=1,la
    i=ibase
    j=jbase
    ! vdir nodep
    do ijk=1,lot
      a11=(a(ic+i)+a(if+i))+(a(ib+i)+a(ie+i))
      c(ja+j)=z*((a(ia+i)+a(id+i))+a11)
      c(jc+j)=z*((a(ia+i)+a(id+i))-0.5*a11)
      d(jc+j)=zsin60*((a(ic+i)+a(if+i))-(a(ib+i)+a(ie+i)))
      a11=(a(ic+i)-a(if+i))+(a(ie+i)-a(ib+i))
      c(jb+j)=z*((a(ia+i)-a(id+i))-0.5*a11)
      d(jb+j)=zsin60*((a(ie+i)-a(ib+i))-(a(ic+i)-a(if+i)))
      c(jd+j)=z*((a(ia+i)-a(id+i))+a11)
      i=i+inc3
      j=j+inc4
    end do ! loop 692
    ibase=ibase+inc1
    jbase=jbase+inc2
  end do ! loop 694
  go to 900
  !
  !     coding for factor 8
  !     -------------------
800 continue
  ibad=3
  if (la/=m) go to 910
  ia=1
  ib=ia+iink
  ic=ib+iink
  id=ic+iink
  ie=id+iink
  if=ie+iink
  ig=if+iink
  ih=ig+iink
  ja=1
  jb=ja+la*inc2
  jc=jb+2*m*inc2
  jd=jc+2*m*inc2
  je=jd+2*m*inc2
  z=1.0/float(n)
  zsin45=z*sqrt(0.5)
  !
  do l=1,la
    i=ibase
    j=jbase
    ! vdir nodep
    do ijk=1,lot
      c(ja+j)=z*(((a(ia+i)+a(ie+i))+(a(ic+i)+a(ig+i)))+ &
     ((a(id+i)+a(ih+i))+(a(ib+i)+a(if+i))))
      c(je+j)=z*(((a(ia+i)+a(ie+i))+(a(ic+i)+a(ig+i)))- &
     ((a(id+i)+a(ih+i))+(a(ib+i)+a(if+i))))
      c(jc+j)=z*((a(ia+i)+a(ie+i))-(a(ic+i)+a(ig+i)))
      d(jc+j)=z*((a(id+i)+a(ih+i))-(a(ib+i)+a(if+i)))
      c(jb+j)=z*(a(ia+i)-a(ie+i)) &
     +zsin45*((a(ih+i)-a(id+i))-(a(if+i)-a(ib+i)))
      c(jd+j)=z*(a(ia+i)-a(ie+i)) &
     -zsin45*((a(ih+i)-a(id+i))-(a(if+i)-a(ib+i)))
      d(jb+j)=zsin45*((a(ih+i)-a(id+i))+(a(if+i)-a(ib+i))) &
          +z*(a(ig+i)-a(ic+i))
      d(jd+j)=zsin45*((a(ih+i)-a(id+i))+(a(if+i)-a(ib+i))) &
          -z*(a(ig+i)-a(ic+i))
      i=i+inc3
      j=j+inc4
    end do ! loop 810
    ibase=ibase+inc1
    jbase=jbase+inc2
  end do ! loop 820
  !
  !     return
  !     ------
900 continue
  ibad=0
910 continue
  ierr=ibad

  return
end subroutine qpassf3
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
