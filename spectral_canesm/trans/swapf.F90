!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine swapf(spec1, spec2, ps, p, fvort, &
                la, ilev, levs, ntrac, hoskf)
  !
  !     * dec 10/2004 - j.scinocca. calls revised routine swaps2f.
  !     *                           uses hoskins filter.
  !     * nov 05/2003 - m.lazare.   previous version swap.
  !
  !     * swaps order of spectral fields in preparation for transforms.
  !     * this is done outside of main parallel loops since it operates
  !     * on spectral fields, thus avoiding memory bank conflicts from
  !     * parallel tasks accessing same memory locations.
  !
  !     * algorithm
  !     * ---------
  !     *
  !     * on entry to this routine, the complex :: fields to be transformed are
  !     * not organized for optimal performance.  the individual transforms
  !     * are organized in column-major format.  since vectorization in the
  !     * LEGENDRE AND FOURIER TRANSFORMS IS ACROSS PARALLEL TRANSFORMS, IT'S
  !     * desirable to transpose the "SPEC"  arrays so that the
  !     * transforms are in row major format and vectorization is across
  !     * independent levels, latitudes, and variables with unit-strides
  !     * between vector elements for both the legendre and fourier transforms.
  !     *
  !     * input fields
  !     * ------------
  !     * the complex :: spectral fields to be transformed are aligned as follows:
  !     *
  !     *     ps (la)
  !     *      p (la,ilev)
  !     *      c (la,ilev)
  !     *      t (la,ilev)
  !     *     es (la,levs)
  !     *   trac (la,ilev,ntrac)      if itrac /= 0
  !     *
  !     * the above fields are complex :: and contiguous in memory.  they
  !     * are therefore equivalent to a single real :: array dimensioned as
  !     *
  !     *   spec1( 2, la, 1+3*ilev+levs+ntrac*ilev)
  !     *
  !     * however, the spec1 array is not used in this form in the
  !     * transforms.  rather, it is referenced as a transposed array:
  !     *
  !     *   spec1( 2, 1+3*ilev+levs+ntrac*ilev, la)
  !     *
  !     * this storage convention allows unit-stride references to the "SPEC1"
  !     * array for better performance.
  !     *
  !     * the p and c arrays must also be copied into a separate array of
  !     * spectral coefficients called
  !     *
  !     *   spec2( 2, la, 2*ilev)
  !     *
  !     * as before, these arrays are actually referenced in these and lower
  !     * routines as the transposed array:
  !     *
  !     *   spec2( 2, 2*ilev, la)
  !     *
  !     * for vectorization reasons.  the "SPEC1" and "SPEC2" arrays are
  !     * therefore swapped into in their transposed formats in this routine.
  !
  !     * NOTE ALSO THE INCLUSION OF THE EARTH'S ROTATION TO CONVERT TO
  !     * absolute vorticity.
  !
  implicit none
  real, intent(in) :: fvort
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: la
  integer, intent(in) :: levs  !< Number of moisture levels in the vertical \f$[unitless]\f$
  integer, intent(in) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  !
  !     * output arrays:
  !
  complex, intent(out) :: spec1(1+3*ilev+levs+ntrac*ilev, la) !< Variable description\f$[units]\f$
  complex, intent(out) :: spec2(2*ilev,                   la) !< Variable description\f$[units]\f$
  !
  !     * input spectral arrays:
  !
  complex, intent(inout) :: p(la,ilev) !< Variable description\f$[units]\f$
  complex, intent(inout) :: ps(la) !< Variable description\f$[units]\f$
  complex, intent(inout) :: hoskf(la) !< Variable description\f$[units]\f$
  !
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  real :: fpsi
  integer :: lev
  integer :: nlev1
  integer :: nlev2
  !
  !     * node information.
  !
  integer*4 :: mynode !<
  common /mpinfo/ mynode
  !-----------------------------------------------------------------------
  nlev1                   = 1 + 3*ilev + levs + ntrac*ilev
  nlev2                   = 2*ilev
  call swaps2f(ps, spec1, la, nlev1, hoskf)
  call swaps2f(p,  spec2, la, nlev2, hoskf)
  !
  !     * include effect of earth's rotation in streamfunction.
  !     * since this is for the "true" (0,1) location in the triangle,
  !     * this must only be done for node 0 which always contains the first
  !     * m=0 row.
  !
  if (mynode==0) then
    fpsi = -.5 * fvort
    spec2(1:ilev, 2) = cmplx(real(spec2(1:ilev, 2)) + fpsi, &
                             aimag(spec2(1:ilev, 2)))
  end if
  !
  return
end subroutine swapf
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
