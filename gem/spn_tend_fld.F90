!---------------------------------- LICENCE BEGIN -------------------------------
! GEM - Library of kernel routines for the GEM numerical atmospheric model
! Copyright (C) 1990-2010 - Division de Recherche en Prevision Numerique
!                       Environnement Canada
! This library is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, version 2.1 of the License. This library is
! distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
! without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
! PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
! You should have received a copy of the GNU Lesser General Public License
! along with this library; if not, write to the Free Software Foundation, Inc.,
! 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
!---------------------------------- LICENCE END --------------------------------

!*s/r spn_tend_fld - 
!             - and applying nudging tendency directly

      subroutine spn_tend_fld ( F_Minx, F_Maxx, F_Miny, F_Maxy,  &
                           F_Nk,  Fld_S )
      use cstv
      use gem_options
      use spn_options
      use glb_ld
      use gmm_vt1
      use mem_tracers
      use spn_work_mod
      use step_options
      use tdpack
      use glb_pil
      use gmm_pw
      use phy_itf, only: phy_get
      use, intrinsic :: iso_fortran_env
      implicit none
#include <arch_specific.hf>

      integer  F_Minx, F_Maxx, F_Miny, F_Maxy
      integer  F_Nk
      character (len=1) Fld_S

!author
!     Minwei Qian (CCRD) & Bernard Dugas, Syed Husain  (MRB)  - summer 2015
!
!
!arguments
!  Name        I/O                 Description
!----------------------------------------------------------------
! F_Minx       I    - minimum index on X (ldnh_minx)
! F_Maxx       I    - maximum index on X (ldnh_maxx)
! F_Miny       I    - minimum index on Y (ldnh_miny)
! F_Maxy       I    - maximum index on Y (ldnh_maxy)
! F_Njl        I    - number of points on local PEy for J (ldnh_nj)
! F_Minz       I    - minimum index on local PEx for K (trp_12smin)
! F_Maxz       I    - maximum index on local PEx for K (trp_12smax)
! F_Nk         I    - G_nk points in Z direction globally
! F_Nkl        I    - number of points on local PEx for K (trp_12sn)
! F_Gni        I    - number of points in X direction globally (G_ni)
! F_Gnj        I    - number of points in Y direction globally (G_nj)
! F_Minij      I    - minimum index on local PEy for I (trp_22min)
! F_Maxij      I    - maximum index on local PEy for I (trp_22max)
! F_nij        I    - number of points on local PEy for I (trp_22n)
! F_nij0       I    - global offset of the first I element on PEy
! F_npex1      I    - number of processors in X
! F_npey1      I    - number of processors in Y
! Fld_S        I    - name of variable to treat (either of 't','u','v')


!      external rpn_comm_transpose

!      real(kind=REAL64) fdwfft(F_Miny:F_Maxy,F_Minz :F_Maxz ,F_Gni+2+F_npex1)
!      real(kind=REAL64)   fdg2(F_Minz:F_Maxz,F_Minij:F_Maxij,F_Gnj+2+F_npey1)
!      real(kind=REAL64)  pri

      real, dimension(:,:,:), pointer :: fld3d=>null()

      integer istat

      integer tmdt

!
!----------------------------------------------------------------------
!
      tmdt    = int(Cstv_dt_8)
!
      if (Fld_S == 't') then
!
         fld3d => tt1
         istat = phy_get(nest_spnt, 'SPNTTEND')
         fld3d (F_Minx:F_Maxx,F_Miny:F_Maxy,1:F_Nk)= &
              nest_spnt(F_Minx:F_Maxx,F_Miny:F_Maxy,1:F_Nk)*tmdt + &
                   fld3d(F_Minx:F_Maxx,F_Miny:F_Maxy,1:F_Nk)
      end if
!
      if (Fld_S == 'u') then
!
         fld3d => ut1
         istat = phy_get(nest_spnu, 'SPNUTEND')

         fld3d (F_Minx:F_Maxx,F_Miny:F_Maxy,1:F_Nk)= &
              nest_spnu(F_Minx:F_Maxx,F_Miny:F_Maxy,1:F_Nk)*tmdt + &
                   fld3d(F_Minx:F_Maxx,F_Miny:F_Maxy,1:F_Nk)
      end if
!
      if (Fld_S == 'v') then
!
         fld3d => vt1
         istat = phy_get(nest_spnv, 'SPNVTEND')

         fld3d (F_Minx:F_Maxx,F_Miny:F_Maxy,1:F_Nk)= &
              nest_spnv(F_Minx:F_Maxx,F_Miny:F_Maxy,1:F_Nk)*tmdt + &
                   fld3d(F_Minx:F_Maxx,F_Miny:F_Maxy,1:F_Nk)
      end if
!
      if (Fld_S == 'q') then

      istat = tr_get('HU:P',fld3d)
      istat = phy_get(nest_spnq, 'SPNQTEND')
      fld3d (F_Minx:F_Maxx,F_Miny:F_Maxy,1:F_Nk)= &
              nest_spnq(F_Minx:F_Maxx,F_Miny:F_Maxy,1:F_Nk)*tmdt + &
                   fld3d(F_Minx:F_Maxx,F_Miny:F_Maxy,1:F_Nk)
      end if
!
!
!----------------------------------------------------------------------
!
      return
      end subroutine spn_tend_fld

