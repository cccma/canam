
#define IF_BLOCK(OPTION) if ( OPTION ) then
#define IF_BLOCK_ELSE else
#define IF_BLOCK_END end if

#if !defined(KOUNT_OPTION)
#  define KOUNT_OPTION .true.
#endif

#if !defined(PACKING_OPTION)
#  define PACKING_OPTION .true.
#endif

#define PHYVAR2D13(NAME, DESC) call gesdict(ni, nk, NAME, # DESC)

#define PHYVAR2D12(NAME, DESC1, DIMS, DESC2) PHYVAR2D13(NAME, DESC1 ## DIMS ##  DESC2)

# define ILG_IZPBLM_DIM                        A*"//C_IZPBLM//"
# define ILG_IZPBLT_DIM                        A*"//C_IZPBLT//"
# define ILG_NLKLM_DIM                         A*"//C_NLKLM//"
# define ILG_LEVWF_DIM                         A*"//C_LEVWF//"
# define ILG_LEVOX_DIM                         A*"//C_LEVOX//"
# define ILG_LEVOZ_DIM                         A*"//C_LEVOZ//"
# define ILG_LEVOZC_DIM                        A*"//C_LEVOZC//"
# define ILG_LEVAIR_DIM                        A*"//C_LEVAIR//"
# define ILG_NBS_DIM                           A*"//C_NBS//"
# define ILG_NTRAC_DIM                         A*"//C_NTRAC//"
# define ILG_2_NTRAC_DIM                       A*"//C_2_NTRAC//"
# define ILG_ISDIAG_DIM                        A*"//C_ISDIAG//"
# define ILG_ISDUST_DIM                        A*"//C_ISDUST//"
# define ILG_ISDNUM_DIM                        A*"//C_ISDNUM//"
# define ILG_NRFP_DIM                          A*"//C_NRFP//"
# define ILG_ILEVP2_NRFP_DIM                   A*"//C_ILEVP2_NRFP//"
# define ILG_ILEVP2_DIM                        A*"//C_ILEVP2//"
# define ILG_LEVSA_DIM                         A*"//C_LEVSA//"
# define ILG_LEVSA_NBL_DIM                     A*"//C_LEVSA_NBL//"
# define ILG_LEVSA_NBS_DIM                     A*"//C_LEVSA_NBS//"
# define ILG_ICTEM_DIM                         A*"//C_ICTEM//"
# define ILG_IGND_DIM                          A*"//C_IGND//"
# define ILG_IM_NRFP_DIM                       A*"//C_IM_NRFP//"
# define ILG_IM_NBS_DIM                        A*"//C_IM_NBS//"
# define ILG_IM_DIM                            A*"//C_IM//"
# define ILG_NTLD_ICAN_IGND_DIM                A*"//C_NTLD_ICAN_IGND//"
# define ILG_NTLD_ICANP1_DIM                   A*"//C_NTLD_ICANP1//"
# define ILG_NTLD_ICAN_DIM                     A*"//C_NTLD_ICAN//"
# define ILG_NTLD_ICTEM_IGND_DIM               A*"//C_NTLD_ICTEM_IGND//"
# define ILG_NTLD_ICTEMP1_DIM                  A*"//C_NTLD_ICTEMP1//"
# define ILG_NTLD_ICTEM_DIM                    A*"//C_NTLD_ICTEM//"
# define ILG_NTLD_IGND_DIM                     A*"//C_NTLD_IGND//"
# define ILG_NTLD_4_DIM                        A*"//C_NTLD_4//"
# define ILG_NTLD_2_DIM                        A*"//C_NTLD_2//"
# define ILG_NTLD_DIM                          A*"//C_NTLD//"
# define ILG_LEVSSAD_DIM                       A*"//C_LEVSSAD//"
# define ILG_NWFLX_DIM                         A*"//C_NWFLX//"
# define ILG_NVDEP_DIM                         A*"//C_NVDEP//"
# define ILG_NSFCEM_DIM                        A*"//C_NSFCEM//"
# define ILG_NLVARC_DIM                        A*"//C_NLVARC//"
# define ILG_NLSO4_DIM                         A*"//C_NLSO4//"
# define ILG_4_DIM                             A*"//C_4//"
# define ILG_2_DIM                             A*"//C_2//"
# define ILG_ILEV_KEXTT_NRMFLD_DIM             T*"//C_KEXTT_NRMFLD//"
# define ILG_ILEV_KEXTT_DIM                    T*"//C_KEXTT//"
# define ILG_ILEV_NRMFLD_DIM                   T*"//C_NRMFLD//"
# define ILG_ILEV_ISDNUM_DIM                   T*"//C_ISDNUM//"
# define ILG_ILEV_NRFP_DIM                     T*"//C_NRFP//"
# define ILG_ILEV_NTRAC_DIM                    T*"//C_NTRAC//"
# define ILG_ILEV_NCHDG_DIM                    T*"//C_NCHDG//"
# define ILG_ILEV_2_DIM                        T*"//C_2//"
# define ILG_ILEV_IM_DIM                       T*"//C_IM//"
# define ILG_DIM                               A
# define ILG_ILEV_DIM                          T

#define PHYVAR2D1(NAME, DESC1, DIMS, DESC2) PHYVAR2D12(NAME, DESC1, DIMS, DESC2)

# define MKARR1DN(PNAME,PTAR,NI,ONAME,DECR,PBUS,INIT,INAME) PHYVAR2D1(n_ ## PNAME, VN=PNAME ;ON=ONAME ; VD=PTAR DECR ; VS=,A, ; VB=PBUS ## INIT;INAME)
# define MKARR2DN(PNAME,PTAR,NI,NK,ONAME,DECR,PBUS,INIT,INAME) PHYVAR2D1(n_ ## PNAME, VN=PNAME ;ON=ONAME ; VD=PTAR DECR ;VS=,NI ## _ ## NK ## _DIM, ;VB=PBUS ## INIT;INAME)
# define MKARR3DN(PNAME,PTAR,NI,NK,NN,ONAME,DECR,PBUS,INIT,INAME) PHYVAR2D1(n_ ## PNAME, VN=PNAME ;ON=ONAME ; VD=PTAR DECR ;VS=,NI ## _ ## NK ## _ ## NN ## _DIM, ;VB=PBUS ## INIT;INAME)
# define MKARR4DN(PNAME,PTAR,NI,NK,NN,NM,ONAME,DECR,PBUS,INIT,INAME) PHYVAR2D1(n_ ## PNAME, VN=PNAME ;ON=ONAME ; VD=PTAR DECR ;VS=,NI ## _ ## NK ## _ ## NN ## _ ## NM ## _DIM, ;VB=PBUS ## INIT;INAME)
# define MKSARR2DN(PNAME,PTAR,NI,NK,ONAME,DECR,PBUS,INIT,INAME) PHYVAR2D1(n_ ## PNAME, VN=PNAME ;ON=ONAME ; VD=PTAR DECR ;VS=,NI ## _ ## NK ## _DIM, ;VB=PBUS ## INIT;INAME)
# define MKSARR3DN(PNAME,PTAR,NI,NK,NN,ONAME,DECR,PBUS,INIT,INAME) PHYVAR2D1(n_ ## PNAME, VN=PNAME ;ON=ONAME ; VD=PTAR DECR ;VS=,NI ## _ ## NK ## _ ## NN ## _DIM, ;VB=PBUS ## INIT;INAME)

