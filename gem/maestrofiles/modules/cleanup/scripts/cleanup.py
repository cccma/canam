#!/usr/bin/python

## Pour lancer les unittest:
##      cleanup.py --test
##   sinon un appel classique est
###     cleanup.py --filter=${TASK_INPUT}/filter --pattern=${DEFAULT_ERASE_DATE} --path=${truepath}

verbose=False

import sys
import re
import os
import fnmatch
import optparse
import datetime
import unittest
import time

parser = optparse.OptionParser(prog="cleanup.py", description='Erase files according to filter rules')
parser.set_defaults(runtest=False)
parser.set_defaults(verbose=False)
parser.add_option('--filter', '-f', dest='filter', action='store', nargs=1, type="string",
                   help='Filter that needs to be interpreted')
parser.add_option('--date', '-d', dest='date', action='store', nargs=1, type="string",
                   help='A date to identify files that needs to be erased')
parser.add_option('--delay', dest='delay', action='store', nargs=1, type="int",
                   help='A delay by which all the files that do not correspond to a line in the filter are removed')
parser.add_option('--path', '-p', dest='path', action='store', nargs=1, type="string",
                   help='Output file')
parser.add_option('--delete', dest='delete', action='store', nargs=1, type="string",
                   help="If 'yes', then we do remove the files else just print a message if a file would have been removed")
parser.add_option('--test', '-t', dest='runtest', action='store_true', 
                   help='Run all the Unit Tests')
parser.add_option('--verbose', '-v', dest='verbose', action='store_true', 
                   help='Run in verbose mode with erasing anything')

class getPatternsError(Exception):
    def __init__(self,msg):
        self.msg=msg

    def __str__(self):
        return "%s" % (self.msg)

class Test_getPatterns(unittest.TestCase):
    def test1(self):
        patterns = getPatterns(['prog 0\n'])
        self.assertEqual([('prog',0)], patterns, "Pattern is not equal to [(prog,0)]")

    def test2(self):
        patterns = getPatterns(['prog 0\n', 'dynbcor 240\n'])
        self.assertEqual([('prog',0),('dynbcor',240)], patterns, "Pattern is not equal to [(prog,0),(dynbcor,240)]")

    def test3(self):
        self.assertRaises(getPatternsError,getPatterns,['prog\n'])

    def test4(self):
        self.assertRaises(getPatternsError,getPatterns,['prog'])

    def test5(self):
        patterns = getPatterns(['prog 0 3 4 5\n'])
        self.assertEqual([('prog',0)], patterns, "Pattern is not equal to [(prog,0)]")

    ## Ici, on verifie que les lignes vides sont ignorees
    def test6(self):
        patterns = getPatterns(['\n'])
        self.assertEqual([], patterns, "Pattern should be empty")
    def test7(self):
        patterns = getPatterns(['prog 0\n','\n'])
        self.assertEqual([('prog',0)], patterns, "Pattern is not equal to [(prog,0)]")
    def test8(self):
        patterns = getPatterns(['\n','prog 0\n'])
        self.assertEqual([('prog',0)], patterns, "Pattern is not equal to [(prog,0)]")
    def test9(self):
        patterns = getPatterns(['prog 0\n', '\n', 'dynbcor 240\n'])
        self.assertEqual([('prog',0),('dynbcor',240)], patterns, "Pattern is not equal to [(prog,0),(dynbcor,240)]")
    ## On s'assure que la valeur 'infinite' est geree correctement.
    def test10(self):
        patterns = getPatterns(['gridpt/anal/hyb/*_sfc infinite\n'])
        self.assertEqual([('gridpt/anal/hyb/*_sfc','infinite')], patterns, "Pattern is not equal to [(gridpt/anal/hyb/*_sfc,infinite)]")

def getPatterns(filterLines):
    n=1
    patterns = []
    for line in filterLines:
        if not line.strip(): continue
        sline = re.split('\s+',line.strip())
        if len(sline)>2:
            sys.stderr.write('Warning: the line "%s" (line number %d) contains more than 2 elements\n' % (line.strip(),n))
        try:
            pattern=sline[0]
            delay=sline[1]
            if delay == 'infinite':
                patterns.append((pattern,'infinite'))
            else:
                patterns.append((pattern,int(delay)))
        except ValueError, error:  ## La vraie facon d'ecrire cette ligne est 'except ValueError as error:' mais
                                   ## il semble que ca ne soit pas supporte avec python 2.4.4 sur la machine 'idl'
                                   ## alors on doit l'ecrire de cette facon.
            raise getPatternsError(error)
        except IndexError, error:  ## Meme commentaire que ci-haut
            raise getPatternsError(error)
        
        n += 1
    return patterns

class Test_filterFiles(unittest.TestCase):
    def test1(self):
        self.assertTrue(filterFile('prog/model/2011020100_024',[('prog',0)],'20110201000000',6))
    def test2(self):
        self.assertFalse(filterFile('prog/model/2011020112_024',[('prog',0)],'20110201000000',6))
    def test3(self):
        self.assertTrue(filterFile('trial/model/2011020106_360m',[('prog',0)],'20110201120000',6))
    def test4(self):
        self.assertFalse(filterFile('trial/model/2011020106_360m',[('prog',0)],'20110201060000',6))
    def test5(self):
        self.assertTrue(filterFile('dynbcor/coeffs/airs/2011020418_gbcif_airs_aqua_group07',[('dynbcor',240)],'20110214180000',6))
    def test6(self):
        ## Ce fichier doit s'effacer
        self.assertTrue(filterFile('gridpt/anal/hyb/2011021500_precon',[('gridpt/anal/hyb/*_precon',24)],'20110216000000',6))
    def test7(self):
        ## Ce fichier ne doit pas s'effacer
        self.assertFalse(filterFile('gridpt/anal/hyb/2011021500_precon',[('gridpt/anal/hyb/*_precon',24)],'20110216120000',6))
    def test8(self):
        ## Ce fichier ne doit pas s'effacer
        self.assertFalse(filterFile('gridpt/anal/hyb/2011021500_precon',[('gridpt/anal/hyb/*_precon','infinite')],'20110216120000',6))

def filterFile(thisfile,patterns,date,default_delay):
    ## Le code original etait ceci:
    ##        thisdate = datetime.datetime.strptime(date,"%Y%m%d%H%M%S")
    ## mais on a du le changer pour le code ici-bas parce que la version de Python
    ## sur la machine 'idl' ne supporte pas la fonction datetime.datetime.strptime
    thisdate = datetime.datetime(*(time.strptime(date,"%Y%m%d%H%M%S")[0:6]))
    delay = default_delay
    
    for pattern in patterns:
        if fnmatch.fnmatch(thisfile,'*'+pattern[0]+'*'):
            delay = pattern[1]
            break

    ## Si le delai est infini alors on n'efface pas le fichier
    if delay == 'infinite':
        return False

    date_delay = datetime.timedelta(hours=delay)

    newdate = thisdate - date_delay
    newdatestr = newdate.strftime("%Y%m%d%H")

    datefromfile = os.path.basename(thisfile).split('_')[0]  ## prend la premiere partie de '2011020418_gbcif_airs_aqua_group07'
    if verbose: print 'filterFile: %s %s %s %s %s %s' % (thisfile,pattern,date,delay,datefromfile,newdatestr)
    return datefromfile == newdatestr

def filterThoseFiles(path,patterns,date,delay,delete):
    for root, dirs, files in os.walk(path):
        for thisfile in files:
            fullfile = os.path.join(root,thisfile)
            do_remove = filterFile(fullfile,patterns,date,delay)
            if do_remove:
                if delete == 'yes':
                    os.remove(fullfile)
                    if verbose:
                        print "The file %s has been removed" % fullfile
                elif verbose:
                    print "The file %s would have been removed" % fullfile
            elif verbose:
                print "The file %s won't be removed" % fullfile
    
if __name__ == '__main__':
    (options, args) = parser.parse_args()

    if options.runtest:
        ## On doit effacer les arguments puisqu'ils sont traites par le module 'unittest'
        del sys.argv[1:]
        ## On ajoute l'option '--verbose' pour le module 'unittest'
        sys.argv.append('--verbose')
        unittest.main()
    else:
        filteropen = open(options.filter,'r')
        filterLines = filteropen.readlines()
        patterns = getPatterns(filterLines)

        verbose = options.verbose
        filterThoseFiles(options.path,patterns,options.date,options.delay,options.delete)
