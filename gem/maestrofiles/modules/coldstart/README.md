`coldstart` Maestro Module
==========================

Module to run at the beginning of the first time step of a cycling suite for suite initialization.

Documentation
-------------

Documention is available on the [wiki](https://wiki.cmc.ec.gc.ca/wiki/Assimilation/Maestro/Documentation/beta/Modules/coldstart).