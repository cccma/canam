#!/bin/bash

# clean_exp.sh
# Cleans files under logs, sequencing and listings/latest directories without asking for permission,
#        as well as listings, gridpt and work directories after asking for permission.
# Deletes files that are under a remote directory, both on frontend and backend machines defined in the experiment's resources.
# If ${SEQ_EXP_HOME} isn’t set, it is recommended to initiate this script from ${SEQ_EXP_HOME}.
# Author: Robert Meyer
# Modified: Verica Savic-Jovcic

if [ -z "${SEQ_EXP_HOME}" ]
then
    echo 'SEQ_EXP_HOME is unset.' >&2
    echo -n "Set \${SEQ_EXP_HOME} to pwd ($PWD)? [y/n] "
    read seqexphomeopt
    if [ "${seqexphomeopt}" = "y" ]
    then
        export SEQ_EXP_HOME=`pwd`
        echo "SEQ_EXP_HOME=${SEQ_EXP_HOME}"
    else
        echo 'Sorry, ${SEQ_EXP_HOME} has to be set.'
        exit 1
    fi
fi

machines=""
tmpmach=$(getdef resources BACKEND 2> /dev/null) && machines="$machines $tmpmach"
tmpmach=$(getdef resources FRONTEND 2> /dev/null) && machines="$machines $tmpmach"

# Ask to remove listings, gridpt and work
echo -n 'Remove listings? [y/n] '
read listingopt
echo -n 'Remove logs? [y/n] '
read logopt
echo -n 'Remove gridpt? [y/n] '
read gridopt
echo -n 'Remove work? [y/n] '
read workopt

# Remove logs
if [ -d "${SEQ_EXP_HOME}/logs" ] && [ "${logopt}" = "y" ]
then
    echo 'Removing logs/*...'
    rm -rf "${SEQ_EXP_HOME:-/dev/null}"/logs/*
fi

# Remove sequencing
if [ -d "${SEQ_EXP_HOME}/sequencing" ]
then
    echo 'Removing sequencing/*...'
    rm -rf "${SEQ_EXP_HOME:-/dev/null}"/sequencing/*
fi

# Remove listings/latest
if [ -d "${SEQ_EXP_HOME}/listings/latest" ]
then
    echo 'Removing listings/latest/*...'
    rm -rf "${SEQ_EXP_HOME:-/dev/null}"/listings/latest/*
fi

for mach in $machines
do
    # Dereference link
    realMachPath=$(readlink "${SEQ_EXP_HOME}"/hub/"${mach}")

    # If it is not a link, do not delete anything to do with it
    if [ -z "$realMachPath" ]
    then
        echo hub/$mach is not a valid link. Skipping...
    else

        # If the directory is valid on host, it is local
        if [ -d "$realMachPath" ]
        then
	    #Delete Locally

            if [ -d "$realMachPath"/work ] && [ "${workopt}" = "y" ]
            then
                echo Removing hub/${mach}/work/'*...'
                rm -rf "${realMachPath:-/dev/null}"/work/*
            fi

            if [ -d "$realMachPath"/listings ] && [ "${listingopt}" = "y" ]
            then
                echo Removing hub/${mach}/listings/'*...'
                rm -rf "${realMachPath:-/dev/null}"/listings/*
            fi

            if [ -d "$realMachPath"/gridpt ] && [ "${gridopt}" = "y" ]
            then
                echo Removing hub/${mach}/gridpt/'*...'
                rm -rf "${realMachPath:-/dev/null}"/gridpt/*
            fi

        else

            #Delete remotely

            if ssh "$mach" [ -d "${realMachPath}"/work ] && [ "${workopt}" = "y" ]
            then
                echo Removing hub/${mach}/work/'*...'
                ssh "$mach" rm -rf "${realMachPath:-/dev/null}"/work/*
            fi

            if ssh "$mach" [ -d "${realMachPath}"/listings ] && [ "${listingopt}" = "y" ]
            then
                echo Removing hub/${mach}/listings/'*...'
                ssh "$mach" rm -rf "${realMachPath:-/dev/null}"/listings/*
            fi

            if ssh "$mach" [ -d "$realMachPath"/gridpt ] && [ "${gridopt}" = "y" ]
            then
                echo Removing hub/${mach}/gridpt/'*...'
                ssh "$mach" rm -rf "${realMachPath:-/dev/null}"/gridpt/*
            fi

        fi # [ -d $realMachPath ]

    fi # [ -z $realMachPath ]

done

