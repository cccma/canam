!> \file vrtdf.F90
!> \brief CanAM interface to the vertical diffusion schemes.
!!
!! @author Routine author name(s)
!
 module vrtdf_mod

 implicit none

 public :: vrtdf

 contains
 subroutine vrtdf(pa, xrow, qrow, throw, urow, vrow, tfrow, tsgb, utg, vtg,        &
                  pressg, shj, shtj, sgj, sgbj, shtxkj, shxkj, dsgj, dshj, dpog,   &
                  utendcv, utendgw, vtendcv, vtendgw, zclf, wsub,                  &
                  zspd, qgrot, cdmrot, qfsrot, hfsrot, qgrow, cdm, qfsrol, hfsrol, &
                  iowat, iosic, saverad, savebeg, dtadv, il1, il2, ilg)
!
  use agcm_types_mod,   only: phys_arrays_type
  use agcm_types_mod,   only: phys_options
  use agcm_types_mod,   only: phys_diag
  use psizes_19,        only: ilev, ntrac, im
  use times_mod,        only: delt
  use phys_consts,      only: grav

  implicit none

  real,    intent(in) :: dtadv   !< Advective timestep for atmospheric model \f$[seconds]\f$
  real,    intent(in) :: saverad !<
  real,    intent(in) :: savebeg !<
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: iowat !< Open water surface tile index \f$[unitless]\f$
  integer, intent(in) :: iosic !< Sea-ice tile index \f$[unitless]\f$

  type(phys_arrays_type), intent(inout)      :: pa

  real, intent(inout), dimension(ilg,ilev,ntrac) :: xrow !< Tracer mixing ratio\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev)   :: qrow  !< Physics internal field for specific humidity \f$[kg kg^{-1}]\f$
  real, intent(inout), dimension(ilg,ilev)   :: urow !< u-wind (real).
  real, intent(inout), dimension(ilg,ilev)   :: vrow !< v-wind (real).
  real, intent(inout), dimension(ilg,ilev)   :: throw !< Physics internal field for temperature \f$[K]\f$
  real, intent(inout), dimension(ilg,ilev)   :: utg !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev)   :: vtg !< Variable description\f$[units]\f$
  real, intent(in),    dimension(ilg,ilev+1) :: tfrow  !< Temperature at interface. \f$[K]\f$
  real, intent(in),    dimension(ilg)        :: pressg !< Surface pressure \f$[Pa]\f$
  real, intent(in),    dimension(ilg,ilev)   :: sgj   !< Eta-level for mid-point of the momentum layer \f$[unitless]\f$
  real, intent(in),    dimension(ilg,ilev)   :: sgbj   !< Eta-level for base of momentum layers \f$[unitless]\f$
  real, intent(in),    dimension(ilg,ilev+1) :: shtj   !< Eta-level for top of thermodynamic layer, plus eta-level for surface (base of lowest layer) \f$[unitless]\f$
  real, intent(in),    dimension(ilg,ilev)   :: shj    !< Eta-level for mid-point of thermodynamic layer \f$[unitless]\f$
  real, intent(in),    dimension(ilg,ilev)   :: dshj   !< Thickness of thermodynamic layers in eta  coordinates \f$[unitless]\f$
  real, intent(in),    dimension(ilg,ilev)   :: dsgj   !< Thickness of momentum layers in eta coordinates \f$[unitless]\f$
  real, intent(in),    dimension(ilg,ilev)   :: dpog   !< Mass of air in grid cell area \f$[kg/m2]\f$
  real, intent(in),    dimension(ilg,ilev)   :: shtxkj !< sigma**(rgas/cpres) at layers tops \f$[unitless]\f$
  real, intent(in),    dimension(ilg,ilev)   :: shxkj !< sigma**(rgas/cpres) at layers centres \f$[unitless]\f$

  real, intent(out),   dimension(ilg,ilev)   :: wsub !< Variable description\f$[units]\f$
  real, intent(in),    dimension(ilg,ilev)   :: utendgw !< Variable description\f$[units]\f$
  real, intent(in),    dimension(ilg,ilev)   :: vtendgw !< Variable description\f$[units]\f$
  real, intent(in),    dimension(ilg,ilev)   :: utendcv !< Variable description\f$[units]\f$
  real, intent(in),    dimension(ilg,ilev)   :: vtendcv !< Variable description\f$[units]\f$
  real, intent(in),    dimension(ilg,ilev)   :: tsgb   !< Variable description\f$[units]\f$
  real, intent(in),    dimension(ilg,im)     :: qgrot  !< Variable description\f$[units]\f$
  real, intent(in),    dimension(ilg,im)     :: cdmrot !< Variable description\f$[units]\f$
  real, intent(in),    dimension(ilg,im)     :: qfsrot !< Variable description\f$[units]\f$
  real, intent(in),    dimension(ilg,im)     :: hfsrot !< Variable description\f$[units]\f$
  real, intent(in),    dimension(ilg,im)     :: zclf   !< Variable description\f$[units]\f$
  real, intent(in),    dimension(ilg)        :: qgrow  !< Variable description\f$[units]\f$
  real, intent(in),    dimension(ilg)        :: cdm    !< Variable description\f$[units]\f$
  real, intent(in),    dimension(ilg)        :: qfsrol !< Variable description\f$[units]\f$
  real, intent(in),    dimension(ilg)        :: hfsrol !< Variable description\f$[units]\f$
  real, intent(in),    dimension(ilg)        :: zspd   !< wind speed at lowest model level sqrt(u**2+v**2) \f$[m s-1]\f$


  real    :: dtodt
  real    :: dqodt
  real    :: duodt
  real    :: dvodt
  real    :: dxodt
  real    :: rcdt
  integer :: il
  integer :: jk
  integer :: nn
  real, dimension(ilg,ilev)       :: txrow !<
  real, dimension(ilg,ilev)       :: qxrow !<
  real, dimension(ilg,ilev)       :: uxrow !<
  real, dimension(ilg,ilev)       :: vxrow !<
  real, dimension(ilg,ilev,ntrac) :: xxrow !<

!
! * internal workspace for condensation and deposition contributions from vertical diffusion:
!   (needed for xtrals and canam_mach phy_options)
  real, dimension(ilg,ilev) :: cndrox !<
  real, dimension(ilg,ilev) :: deprox !<
!   Instataneous fields needed for canam_mach phy_options
  real, dimension(ilg) :: uerox !<
  real, dimension(ilg) :: ilmorox !<


  rcdt = 1.0 / dtadv
  !
  do jk = 1, ilev
    do il = il1, il2
       txrow(il,jk) = throw(il,jk)
       qxrow(il,jk) = qrow(il,jk)
    end do
  end do
  if (phys_diag%uprhsc) then
     uxrow(il1:il2,1:ilev) = urow(il1:il2,1:ilev)
  end if
  if (phys_diag%vprhsc) then
     vxrow(il1:il2,1:ilev) = vrow(il1:il2,1:ilev)
  end if
  !
  if (phys_diag%xprhsc) then
     xxrow(il1:il2,1:ilev,1:ntrac) = xrow(il1:il2,1:ilev,1:ntrac)
  end if
  !

  if (phys_options%use_tke) then
  !
     call vrtdftke(throw, qrow, urow, vrow, xrow, utg, vtg,        &
                   pa%ufsirol, pa%vfsirol, pa%ufsorol, pa%vfsorol, &
                   pa%ufsrol,  pa%vfsrol,  pa%ufsrow,  pa%vfsrow,  &
                   pa%ufsinstrol, pa%vfsinstrol,                   &
                   pa%pbltrow, uerox, ilmorox, cndrox,             &
                   deprox, wsub, pa%rkmrol, pa%rkhrol, pa%rkqrol,  &
                   pa%xlmrow, pa%tkemrow, pa%svarrow, pa%xlmrot,   &

      !-------- outputs or updated inputs are above this line,
      !-------- inputs are below.

                   pa%gtrot, qgrot, cdmrot, qfsrot, hfsrot, &
                   pa%gtrow, qgrow, cdm, qfsrol, hfsrol,    &
                   pa%farerot, pressg, zspd, pa%flndrow,    &
                   pa%cqfxrow, pa%chfxrow, tsgb, tfrow,     &
                   utendcv, vtendcv, utendgw, vtendgw,      &
                   sgj, sgbj, shtxkj, shxkj, shtj, shj,     &
                   dsgj, dshj, pa%cvsgrow, iowat, iosic,    &
                   dtadv, delt, ilg, il1, il2, im, ilev,    &
                   ilev+1, ntrac,                           &
                   phys_diag%xtrals, phys_diag%canam_mach, saverad, savebeg)
!
  else
!
     call vrtdf22(throw, qrow, urow, vrow, xrow, utg, vtg,               &
                  pa%ufsirol,pa%vfsirol,pa%ufsorol,pa%vfsorol,           &
                  pa%ufsrol, pa%vfsrol, pa%ufsrow, pa%vfsrow,            &
                  pa%ufsinstrol, pa%vfsinstrol,                          &
                  pa%almxrow, pa%almcrow, pa%pbltrow, uerox, ilmorox,    &
                  cndrox, deprox, wsub, pa%rkmrol, pa%rkhrol, pa%rkqrol, &


     !-------- outputs or updated inputs are above this line,
     !-------- inputs are below.

                  pa%gtrot, qgrot, cdmrot, qfsrot, hfsrot,            &
                  pa%gtrow, qgrow, cdm, qfsrol, hfsrol,               &
                  pa%farerot, pressg, zspd, pa%flndrow,               &
                  pa%cqfxrow, pa%chfxrow, tsgb, tfrow,                &
                  utendcv, vtendcv, utendgw, vtendgw,                 &
                  sgj, sgbj, shtxkj, shxkj, shtj, shj,                &
                  dsgj, dshj, pa%cvsgrow, zclf, iowat, iosic,         &
                  dtadv, ilg, il1, il2, im, ilev, ilev+1,             &
                  ntrac, phys_options%pla,                            &
                  phys_diag%xtrals, phys_diag%canam_mach, saverad, savebeg)
  end if
!
  do il=il1,il2
     pa%uerow  (il)=pa%uerow  (il)+uerox  (il) * saverad
     pa%ilmorow(il)=pa%ilmorow(il)+ilmorox(il) * saverad
  end do
!
  if (phys_diag%canam_mach) then
     do il = il1,il2
        pa%ilmorol(il) = ilmorox(il)
        pa%uerol(il)   = uerox(il)
     end do
  end if


  if (phys_diag%canam_mach) then
!
!     * sum up sampled condensation and deposition contributions from microphysics and vertical diffusion.
!
     do jk = 1, ilev
        do il = il1, il2
           pa%cndrol(il,jk) = pa%cndrol(il,jk) + cndrox(il,jk)
           pa%deprol(il,jk) = pa%deprol(il,jk) + deprox(il,jk)
        end do
     end do
  end if
!
  if (phys_diag%xtrals) then
!     * accumulate condensation and deposition from mixing into diagnostic output arrays.
!
     do jk = 1, ilev
        do il = il1, il2
           pa%cndrow (il,jk) = pa%cndrow(il,jk) + cndrox(il,jk) * saverad
           pa%deprow (il,jk) = pa%deprow(il,jk) + deprox(il,jk) * saverad
           pa%vcndrow(il) = pa%vcndrow(il) + dpog(il,jk) * cndrox(il,jk) * saverad
           pa%vdeprow(il) = pa%vdeprow(il) + dpog(il,jk) * deprox(il,jk) * saverad
        end do
     end do
  end if

  do jk = 1, ilev
    do il = il1, il2
      pa%rkmrow(il,jk)  = pa%rkmrow(il,jk) + pa%rkmrol(il,jk)*saverad
      pa%rkhrow(il,jk)  = pa%rkhrow(il,jk) + pa%rkhrol(il,jk)*saverad
      pa%rkqrow(il,jk)  = pa%rkqrow(il,jk) + pa%rkqrol(il,jk)*saverad
      dtodt         = (throw(il,jk) - txrow(il,jk)) * rcdt
      dqodt         = (qrow(il,jk) - qxrow(il,jk)) * rcdt
      if (phys_diag%tprhsc) then
             pa%ttpvrow(il,jk) = pa%ttpvrow(il,jk) + dtodt * saverad
      end if
      if (phys_diag%qprhsc) then
             pa%qtpvrow(il,jk) = pa%qtpvrow(il,jk) + dqodt * saverad
      end if
      if (phys_diag%uprhsc) then
             duodt         = (urow(il,jk) - uxrow(il,jk)) * rcdt
             pa%utpvrow(il,jk) = pa%utpvrow(il,jk) + duodt * saverad
      end if
      if (phys_diag%vprhsc) then
             dvodt         = (vrow(il,jk) - vxrow(il,jk)) * rcdt
             pa%vtpvrow(il,jk) = pa%vtpvrow(il,jk) + dvodt * saverad
      end if
      pa%ttpvrol(il,jk) = dtodt
      pa%qtpvrol(il,jk) = dqodt
    end do
  end do ! loop 520
  !
  if (phys_diag%xprhsc) then
     do nn = 1, ntrac
       do jk = 1, ilev
         do il = il1, il2
           dxodt = ( xrow(il,jk,nn) - xxrow(il,jk,nn))*rcdt
           pa%xtpvrow(il,jk,nn) = pa%xtpvrow(il,jk,nn) + dxodt*saverad
         end do
       end do
     end do ! loop 521
  end if
  !=======================================================================

  return
  end subroutine vrtdf

 end module vrtdf_mod
