!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine oceang (snogat,  gtgat,   angat,   rhongat, tngat, &
                         refgat,  bcsngat, gcgat, &
                         snowgat, tagat, &
                         ulgat,   vlgat,   cszgat,  presgat, &
                         fsggat,  flggat,  qagat,   thlgat, &
                         vmodgat, gustgat, &
                         zrfmgat, zrfhgat, zdmgat,  zdhgat, &
                         depbgat,  spcpgat,  rhsigat,  pregat, &
                         fsdgat,  fsfgat,  csdgat,  csfgat, &
                         wrkagat, wrkbgat, cldtgat, &
                         iwmos,   jwmos, &
                         nmw,nl,nm,ilg,nbs, &
                         snorot,  gtrot,   anrot,   rhonrot, tnrot, &
                         refrot,  bcsnrot, gcrot, &
                         snowrow, tarow, &
                         ulrow,   vlrow,   cszrow,  presrow, &
                         fsgrot,  flgrot,  qarow,   thlrow, &
                         vmodl,   gustrol, &
                         zrfmrow, zrfhrow, zdmrow,  zdhrow, &
                         depbrow,  spcprow,  rhsirow,  prerow, &
                         fsdrot,  fsfrot,  csdrot,  csfrot, &
                         wrkarol, wrkbrol, cldtrol)
  !
  !     * aug 07, 2017 - m.lazare. new git version.
  !     * mar 03, 2015 - m.lazare. new version called by sfcproc2
  !     *                          in new model version gcm18. this
  !     *                          is modelled on "classg" since
  !     *                          ocean is now tiled as well.
  !     *                          - zrfm,zrfh,zdm,zdh added.
  !     *                          - sgl,shl removed as no longer needed
  !     *                            in oiflux11.
  !     *                          - tisl,mask removed and
  !     *                            gt,gust added.
  !     *                          - wrka,wrkb,cldt added for ocean albedo
  !     *                            calculation.
  !     *                          - dsic,gta,gtp,sic,sicn removed.
  !     *                          - rain,res,ilsl,jl removed.
  !     * nov 15, 2013 - m.lazare. previous version "oceang" called by
  !     *                          "sfcproc" in previous model version gcm17.
  !     *                          this differs from the pre-nov 15/2013
  !     *                          version in that rofn has been removed.
  !     * note: this contains the following changes compared to the
  !     *       working temporary version used in conjunction with
  !     *       updates to gcm16 (ie not official):
  !     *         1) {TN,RHON,REF,BCSN,TISL} added for Maryam's
  !     *            contribution to the new snow albedo formulation.
  !     *         2) no more snorot,anrot (snorow,anrow still there)
  !     *            since these are for land only.
  !     *         3) {fso,fsf,csd,csf| now 2-d arrays since defined
  !     *            over 4 bands (hence nbs passed in).
  !--------------------------------------------------------------------
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in)  :: nmw !< Variable description\f$[units]\f$
  integer, intent(in)  :: nl !< Variable description\f$[units]\f$
  integer, intent(in)  :: nm !< Variable description\f$[units]\f$
  integer, intent(in)  :: ilg !< Variable description\f$[units]\f$
  integer  :: k !< Variable description\f$[units]\f$
  integer  :: l !< Variable description\f$[units]\f$
  integer  :: m !< Variable description\f$[units]\f$
  integer, intent(in)  :: nbs !< Variable description\f$[units]\f$
  integer  :: iocean !< Variable description\f$[units]\f$
  integer  :: ib !< Variable description\f$[units]\f$
  !
  !     * ocean prognostic variables.
  !
  real, intent(in), dimension(nl,nm) :: snorot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: gtrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: anrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: rhonrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: tnrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: refrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: bcsnrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: gcrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: fsgrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: flgrot !< Variable description\f$[units]\f$

  real, intent(out), dimension(ilg)   :: snogat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg)   :: gtgat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg)   :: angat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg)   :: rhongat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg)   :: tngat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg)   :: refgat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg)   :: bcsngat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg)   :: gcgat !< Variable description\f$[units]\f$
  !
  !     * atmospheric and grid-constant input variables.
  !
  real, intent(in), dimension(nl,nm,nbs) :: fsdrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm,nbs) :: fsfrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm,nbs) :: csdrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm,nbs) :: csfrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nbs)    :: wrkarol !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nbs)    :: wrkbrol !< Variable description\f$[units]\f$

  real, intent(in), dimension(nl) :: snowrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: tarow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: ulrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: vlrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: cszrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: cldtrol !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: presrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: qarow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: thlrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: vmodl !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: gustrol !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: zrfmrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: zrfhrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: zdmrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: zdhrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: depbrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: spcprow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: rhsirow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: prerow !< Variable description\f$[units]\f$

  real, intent(out), dimension(ilg,nbs) :: fsdgat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg,nbs) :: fsfgat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg,nbs) :: csdgat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg,nbs) :: csfgat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg,nbs) :: wrkagat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg,nbs) :: wrkbgat !< Variable description\f$[units]\f$

  real, intent(out), dimension(ilg) :: snowgat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: tagat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: ulgat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: vlgat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: cszgat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: cldtgat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: presgat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: fsggat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: flggat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: qagat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: thlgat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: vmodgat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: gustgat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: zrfmgat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: zrfhgat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: zdmgat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: zdhgat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: depbgat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: spcpgat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: rhsigat !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: pregat !< Variable description\f$[units]\f$
  !
  !     * gather-scatter index arrays.
  !
  integer, intent(in), dimension(ilg) :: iwmos !< Variable description\f$[units]\f$
  integer, intent(in), dimension(ilg) :: jwmos !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !--------------------------------------------------------------------
  do k=1,nmw
    snogat (k)=snorot (iwmos(k),jwmos(k))
    gtgat  (k)=gtrot  (iwmos(k),jwmos(k))
    angat  (k)=anrot  (iwmos(k),jwmos(k))
    rhongat(k)=rhonrot(iwmos(k),jwmos(k))
    tngat  (k)=tnrot  (iwmos(k),jwmos(k))
    refgat (k)=refrot (iwmos(k),jwmos(k))
    bcsngat(k)=bcsnrot(iwmos(k),jwmos(k))
    gcgat  (k)=gcrot  (iwmos(k),jwmos(k))
    fsggat (k)=fsgrot (iwmos(k),jwmos(k))
    flggat (k)=flgrot (iwmos(k),jwmos(k))
  end do ! loop 100
  !
  do k=1,nmw
    snowgat  (k)=snowrow  (iwmos(k))
    tagat    (k)=tarow    (iwmos(k))
    ulgat    (k)=ulrow    (iwmos(k))
    vlgat    (k)=vlrow    (iwmos(k))
    cszgat   (k)=cszrow   (iwmos(k))
    cldtgat  (k)=cldtrol  (iwmos(k))
    presgat  (k)=presrow  (iwmos(k))
    qagat    (k)=qarow    (iwmos(k))
    thlgat   (k)=thlrow   (iwmos(k))
    vmodgat  (k)=vmodl    (iwmos(k))
    gustgat  (k)=gustrol  (iwmos(k))
    zrfmgat  (k)=zrfmrow  (iwmos(k))
    zrfhgat  (k)=zrfhrow  (iwmos(k))
    zdmgat   (k)=zdmrow   (iwmos(k))
    zdhgat   (k)=zdhrow   (iwmos(k))
    depbgat  (k)=depbrow  (iwmos(k))
    spcpgat  (k)=spcprow  (iwmos(k))
    rhsigat  (k)=rhsirow  (iwmos(k))
    pregat   (k)=prerow   (iwmos(k))
  end do ! loop 200
  !
  do ib=1,nbs
    do k=1,nmw
      fsdgat (k,ib) = fsdrot (iwmos(k),jwmos(k),ib)
      fsfgat (k,ib) = fsfrot (iwmos(k),jwmos(k),ib)
      csdgat (k,ib) = csdrot (iwmos(k),jwmos(k),ib)
      csfgat (k,ib) = csfrot (iwmos(k),jwmos(k),ib)
      wrkagat(k,ib) = wrkarol(iwmos(k),ib)
      wrkbgat(k,ib) = wrkbrol(iwmos(k),ib)
    end do
  end do ! loop 300
  !--------------------------------------------------------------------
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
