!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine ocnalb(salb, csal, & ! output
                        wrka, wrkb, & ! input
                        zspd, cldt, csz, & ! input
                        gc,   wcap, gt, & ! input
                        fsd,  fsf,  csd,  csf, & ! input
                        ilg, il1, il2, nbs, isnoalb, ksalb)        ! control
  !
  !     * apr 18/2018 - j. cole    adjusted call to lookup table to only
  !     *                          use the visible optical thickness (wrka,wrkb)
  !     *                          instead of values for each band.
  !     * dec 02/2014 - m.lazare.  new version for gcm18+:
  !     *                          - band-specific whitecap albedoes
  !     *                            introduced.
  !     *                          - combination of sfcalb and
  !     *                            gc=0 section from oifprp9.
  !     *                          - whitecap effect limited
  !     *                            to where sicn<sicn_crt
  !     *                            (wcap passed in and used).
  !
  !     * initialize surface albedo parameters
  !     * ksalb = 0...surface albedo is calculated in from old class_ formulation
  !     * ksalb = 1...surface albedo is calculated from a lookup table
  !     * ksalb = 2...surface albedo is calculated from a formula
  !
  implicit none
  !
  !     * output data.
  !
  real, intent(out) , dimension(ilg,nbs) :: salb !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ilg,nbs) :: csal !< Variable description\f$[units]\f$
  !
  !     * input data.
  !
  real, intent(inout), dimension(ilg,nbs) :: wrka !< OUTPUT FROM SW_CLD_PROPS3\f$[units]\f$
  real, intent(inout), dimension(ilg,nbs) :: wrkb !< OUTPUT FROM SW_CLD_PROPS3\f$[units]\f$

  real, intent(in) , dimension(ilg) :: zspd !< Variable description\f$[units]\f$
  real, intent(in) , dimension(ilg) :: cldt !< Variable description\f$[units]\f$
  real, intent(in) , dimension(ilg) :: csz !< Variable description\f$[units]\f$
  real, intent(in) , dimension(ilg) :: gc !< Variable description\f$[units]\f$
  real, intent(in) , dimension(ilg) :: wcap !< Variable description\f$[units]\f$
  real, intent(in) , dimension(ilg) :: gt !< Variable description\f$[units]\f$

  integer, intent(in) :: ksalb !< Variable description\f$[units]\f$
  integer, intent(in) :: il1 !< Variable description\f$[units]\f$
  integer, intent(in) :: il2 !< Variable description\f$[units]\f$
  integer, intent(in) :: ilg !< Variable description\f$[units]\f$
  integer, intent(in) :: nbs !< Variable description\f$[units]\f$
  integer, intent(in) :: isnoalb !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================


  integer, parameter :: nsalb = 4 !<
  !
  !     * local data.
  !
  real, intent(in), dimension(ilg,nbs) :: fsd !<
  real, intent(in), dimension(ilg,nbs) :: fsf !<
  real, intent(in), dimension(ilg,nbs) :: csd !<
  real, intent(in), dimension(ilg,nbs) :: csf !<
  real, dimension(ilg,nbs) :: wdircta !<
  real, dimension(ilg,nbs) :: wdirctc !<
  real, dimension(ilg,nbs) :: wdiffua !<
  real, dimension(ilg,nbs) :: wdiffuc !<
  !
  real, dimension(ilg) :: fsd_bb !<
  real, dimension(ilg) :: fsf_bb !<
  real, dimension(ilg) :: csd_bb !<
  real, dimension(ilg) :: csf_bb !<
  real, dimension(ilg) :: wdircta_bb !<
  real, dimension(ilg) :: wdirctc_bb !<
  real, dimension(ilg) :: wdiffua_bb !<
  real, dimension(ilg) :: wdiffuc_bb !<
  real, dimension(ilg) :: fwcap !<
  real, dimension(ilg) :: chl !<
  real, dimension(ilg) :: wrkc !<
  real, dimension(ilg) :: wrkd !<
  real, dimension(ilg) :: vmod !<
  !
  real, parameter :: alwcapb(nsalb) = (/0.216, 0.134, 0.044, 0.005/) ! band-specific whitecap albedoes
  !
  real  :: zspdmax !<
  real  :: temp !<
  real  :: alball !<
  real  :: alwcap !<
  real  :: x !<
  real  :: acoef !<
  real  :: bcoef !<
  real  :: arg !<
  !
  real :: ws !<
  real :: fs !<
  real :: f1 !<
  real :: f12 !<
  real :: f13 !<
  real :: f14 !<
  real :: f15 !<
  real :: f16 !<
  real :: fi !<
  real :: fi2 !<
  real :: fi3 !<
  real :: wo !<
  real :: wtst !<
  real :: enh !<
  real :: wo3 !<
  real :: wttt !<
  real :: fct !<
  real :: wss !<
  real :: albdif !<
  real :: albdir !<
  real :: alba !<
  real :: albc !<
  real :: ala !<
  real :: alc !<

  integer :: i !<
  integer :: ib !<
  !
  !     * statement functions for whitecaps parameterizations.
  !
  acoef(arg)=6.779e-03-1.83e-03*arg+1.917e-04*arg**2 &
           -3.778e-06*arg**3
  bcoef(arg)=0.7566+6.096e-02*arg-6.547e-03*arg**2+1.276e-04*arg**3
  !======================================================================
  if (ksalb==1) then
    !
    !       * when albedo is to be determined from a table set the max
    !       * wind speed to the largest wind speed nodal value in that
    !       * table, to avoid extrapolation.
    !
    zspdmax=18.
  else
    zspdmax=30.
  end if
  !
  !     * calculate whitecap fraction.
  !     * observational studies suggest that a large fetch of
  !     * open water is required for whitecaps to develop.
  !     * this is likely not possible if there is a sufficient
  !     * amount of seaice in the grid square.
  !     * therefore we limit this effect to occur only where
  !     * sicn<sicn_crt (defined by wcap=1. defined in sfcproc).
  !
  do i = il1,il2
    fwcap(i)=0.
    vmod(i) =  max(min(zspd(i), zspdmax), 0.)
    if (gc(i)==0. .and. wcap(i)==1.) then
      temp=gt(i)-273.16
      fwcap(i)=min(acoef(temp)*vmod(i)**bcoef(temp),1.)
    end if
  end do
  !
  if (ksalb==1) then
    !--- determine open water albedoes from nasa lookup table
    chl(il1:il2)=0.3 ! no variability due to chlorophyl
    do ib=1,nbs
      !--- limit optical depths to 25 which is the largest nodal
      !--- value in the lookup table used by albval
      where (wrka(il1:il2,ib)>25.0) wrka(il1:il2,ib)=25.0
      where (wrkb(il1:il2,ib)>25.0) wrkb(il1:il2,ib)=25.0
      if (any(wrka(il1:il2,ib)<0.0))  call xit('OCNALB',-1)
      !--- on return from albval wrkc will contain clear sky albedos
      !--- note for now only passing in the band 1 (visible) optical thickness
      call albval2(wrkc,ib,wrka(1,1),csz,vmod,chl,il1,il2,ilg)
      if (any(wrkb(il1:il2,ib)<0.0))  call xit('OCNALB',-2)
      !--- on return from albval wrkd will contain all sky albedos
      !--- note for now only passing in the visible (550 nm) optical thickness
      call albval2(wrkd,ib,wrkb(1,1),csz,vmod,chl,il1,il2,ilg)
      !--- whitecap albedo
      alwcap=alwcapb(ib)
      do i = il1,il2
        if (gc(i)==0.) then
          csal(i,ib) = (1. - fwcap(i)) * wrkc(i) + fwcap(i) * alwcap
          csal(i,ib) = min( max(csal(i,ib),0.), 1.)
          alball  = cldt(i) * (wrkd(i) - wrkc(i)) + wrkc(i)
          salb(i,ib) = (1. - fwcap(i)) * alball  + fwcap(i) * alwcap
          salb(i,ib) = min( max(salb(i,ib),0.), 1.)
        end if
      end do
    end do
  else if (ksalb==2) then
    !--- determine open water albedoes using hadley center formula
    do ib=1,nbs
      !--- whitecap albedo
      alwcap=alwcapb(ib)
      do i = il1,il2
        if (gc(i)==0.) then
          x         = 0.037/(1.1*(csz(i)**1.4) + 0.15)
          salb(i,ib) = (1. - fwcap(i)) * x + fwcap(i) * alwcap
          salb(i,ib) = min( max(salb(i,ib),0.), 1.)
          csal(i,ib) = salb(i,ib)
        end if
      end do
    end do
  else
    !--- albedo values are calculated as in old class_  formula
    !
    !       * compute fractions of direct and diffuse beams for both
    !       * each band and over all bands (both all-sky and clear-sky).
    !
    fsd_bb(:) = 0.0
    fsf_bb(:) = 0.0
    csd_bb(:) = 0.0
    csf_bb(:) = 0.0

    do ib = 1, nbs
      do i = il1,il2
        wdircta(i,ib) = fsd(i,ib)/(fsd(i,ib)+fsf(i,ib)+1.e-10)
        wdirctc(i,ib) = csd(i,ib)/(csd(i,ib)+csf(i,ib)+1.e-10)
        wdiffua(i,ib) = 1.0 - wdircta(i,ib)
        wdiffuc(i,ib) = 1.0 - wdirctc(i,ib)
        fsd_bb(i)     = fsd_bb(i) + fsd(i,ib)
        fsf_bb(i)     = fsf_bb(i) + fsf(i,ib)
        csd_bb(i)     = csd_bb(i) + csd(i,ib)
        csf_bb(i)     = csf_bb(i) + csf(i,ib)

        if (wdircta(i,ib) > 1.0 .or. wdircta(i,ib) < 0.0) then
          write(6,*) i,ib,wdircta(i,ib),fsd(i,ib),fsf(i,ib)
          call xit('OCNALB',-3)
        end if

        if (wdirctc(i,ib) > 1.0 .or. wdirctc(i,ib) < 0.0) then
          write(6,*) i,ib,wdirctc(i,ib),csd(i,ib),csf(i,ib)
          call xit('OCNALB',-4)
        end if
      end do
    end do
    !
    if (isnoalb == 1) then ! compute from band means
      do i = il1,il2
        wdircta_bb(i) = fsd_bb(i)/(fsd_bb(i)+fsf_bb(i)+1.e-10)
        wdiffua_bb(i) = 1.0 - wdircta_bb(i)
        wdirctc_bb(i) = csd_bb(i)/(csd_bb(i)+csf_bb(i)+1.e-10)
        wdiffuc_bb(i) = 1.0 - wdirctc_bb(i)
      end do
    else if (isnoalb == 0) then ! have only total radiation.  should have same value in
      ! each band so just use first one.
      do i = il1,il2
        wdircta_bb(i) = fsd(i,1)/(fsd(i,1)+fsf(i,1)+1.e-10)
        wdiffua_bb(i) = 1.0 - wdircta_bb(i)
        wdirctc_bb(i) = csd(i,1)/(csd(i,1)+csf(i,1)+1.e-10)
        wdiffuc_bb(i) = 1.0 - wdirctc_bb(i)
      end do
    end if
    !
    !--- whitecap albedo
    alwcap=0.3     ! old constant value.
    do i = il1,il2
      if (gc(i)==0.) then
        if (csz(i)>=0.001) then
          !
          !             * over open water, the albedoes are a complicated function of
          !             * cosine of solar zenith angle and wind speed (for the direct
          !             * beam) and wind speed for the diffuse beam, plus the result
          !             * is weighted by the fraction and albedo of white caps which
          !             * may be present. when the sun is on or below the horizon,
          !             * an equal weight of direct and diffuse beam is assumed, since
          !             * there is no radiation information to determine the proper
          !             * weights and there is little solar input.
          !
          !             * note that for now, there is no wavelength dependence on the
          !             * direct or diffuse beams.
          !
          f1   =  1. - csz(i)
          f12  =  f1 * f1
          f13  =  f12 * f1
          f14  =  f12 * f12
          f15  =  f14 * f1
          ws=zspd(i)
          f16 = f15*f1
          fi=csz(i)
          fi2= fi*fi
          fi3= fi2*fi
          fs=1.-fi2
          wo=180.*fi3*fs

          wtst=ws-wo
          if (wtst<0.) then
            enh=5.4*fi2*fs*fs
            wo3=wo*wo*wo
            wttt=ws-1.1*wo
            fct=1.+enh*ws*wttt*wttt/wo3
          else
            fct=1.
          end if
          wss=max(0.,wtst)
          albdir=fct*(0.021 + 0.0421*f12 + 0.128*f13 - 0.04*f16 + &
                (4./(5.68+wss)+(0.074*f1)/(1.+3.*wss))*f16)
          albdif=.067*(0.55*exp(-(ws/7.)**2)+1.45*exp(-(ws/40.)**2) &
               +1.)/3.
          alba=albdir*wdircta_bb(i) + albdif*wdiffua_bb(i)
          albc=albdir*wdirctc_bb(i) + albdif*wdiffuc_bb(i)
          !
          ala=(1.-fwcap(i))*alba+fwcap(i)*alwcap
          alc=(1.-fwcap(i))*albc+fwcap(i)*alwcap
          !
          salb(i,1)=ala
          salb(i,2)=ala
          salb(i,3)=ala
          salb(i,4)=ala
          csal(i,1)=alc
          csal(i,2)=alc
          csal(i,3)=alc
          csal(i,4)=alc
        else
          salb(i,1)=0.066
          salb(i,2)=0.066
          salb(i,3)=0.066
          salb(i,4)=0.066
          csal(i,1)=0.066
          csal(i,2)=0.066
          csal(i,3)=0.066
          csal(i,4)=0.066
        end if
      end if
    end do
  end if ! ksalb
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
