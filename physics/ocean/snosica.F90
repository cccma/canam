!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine snosica(salb,csal, &
                   fsd,fsf,csd,csf, &
                   sno,ts,albs,gc,gta,sic,csz, &
                   rhon,gt,refsn,bcsn, &
                   ilg,iis,iif,nbs,isnoalb)
  !
  !     * feb 13/21 - m.lazare. removed special case where rhon=0.
  !     *                       for sno>0., since fixed in oifprp10.
  !     * aug 06/19 - s.kharin/ modify definiton of snofac to handle
  !     *             m.lazare. rhon=0. cosmetic fix for cmip6; will
  !     *                       be looked at to avoid rhon=0 in next
  !     *                       development cycle.
  !     * apr 22/18 - m.lazare. replace fractional snow coverage
  !     *                       by that from classa (ie linear based
  !     *                       on depth, instead of sqrt based on swe).
  !     * apr 30/18 - m.lazare. remove unused {acoef,bcoef}. calculation
  !     *                       now in ocnalb.
  !     * dec 01/14 - m.lazare. new version for gcm18+:
  !     *                       - based on old oifprp9 but without
  !     *                         simpler calculations moved to new
  !     *                         oifprp10.
  !     *                       - leads fraction effect removed
  !     *                         since now applies to ice-covered
  !     *                         portion only.
  !     *                       - unused {ul,vl,vmod} removed.
  !     *                       - initialize c_flag to zero (bugfix).
  !
  !     * performs albedo calculations over sea-ice.
  !
  use phys_consts, only : gtfsw, deni
  implicit none
  !
  !     * output or i/o fields:
  !
  real, intent(out), dimension(ilg,nbs) :: salb !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg,nbs) :: csal !< Variable description\f$[units]\f$
  !
  !     * input fields:
  !
  real, intent(in), dimension(ilg,nbs) :: fsd !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,nbs) :: fsf !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,nbs) :: csd !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,nbs) :: csf !< Variable description\f$[units]\f$
  !
  real, intent(in), dimension(ilg) :: sno !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: ts !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: albs !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: gc !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: gta !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: sic !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: csz !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: rhon !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: gt !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: refsn !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: bcsn !< Variable description\f$[units]\f$

  integer, intent(in) :: ilg !< Variable description\f$[units]\f$
  integer, intent(in) :: iis  !< Variable description\f$[units]\f$
  integer, intent(in) :: iif  !< Variable description\f$[units]\f$
  integer, intent(in) :: nbs !< Variable description\f$[units]\f$
  integer, intent(in) :: isnoalb !< Variable description\f$[units]\f$

  !==================================================================
  ! physical (adjustable) parameters
  !
  !     * melt pond albedoes for 4 spectral bands (based on
  !     * ebert and curry (1993), assuming melt pond depth of 0.2m
  !
  real, parameter :: albp1 = 0.27
  real, parameter :: albp2 = 0.05
  real, parameter :: albp3 = 0.05
  real, parameter :: albp4 = 0.03
  real, parameter :: puddle_max = 0.25
  !
  !==================================================================

  !
  !     * local fields ("A"=all-sky, "C"=clear-sky):
  !
  real, dimension(ilg,nbs) :: albbi !<
  real, dimension(ilg,nbs) :: alsnodif !<
  real, dimension(ilg,nbs) :: alsnodir !<
  !
  real, dimension(ilg,nbs) :: alsnoa !<
  real, dimension(ilg,nbs) :: alsnoc !<
  real, dimension(ilg,nbs) :: wdircta !<
  real, dimension(ilg,nbs) :: wdirctc !<
  real, dimension(ilg,nbs) :: wdiffua !<
  real, dimension(ilg,nbs) :: wdiffuc !<
  !
  real, dimension(ilg) :: wdircta_bb !<
  real, dimension(ilg) :: wdirctc_bb !<
  real, dimension(ilg) :: wdiffua_bb !<
  real, dimension(ilg) :: wdiffuc_bb !<
  real, dimension(ilg) :: fsd_bb !<
  real, dimension(ilg) :: fsf_bb !<
  real, dimension(ilg) :: csd_bb !<
  real, dimension(ilg) :: csf_bb !<
  !
  real, dimension(ilg) :: snofac !<
  real, dimension(ilg) :: refsno !<
  real, dimension(ilg) :: bcsno !<
  integer, dimension(ilg) :: c_flag !<
  integer :: sum_c_flag !<

  integer :: ib, i
  real :: zsnolim, zsnow, hice, x, timfac, puddle, dpuddle
  real :: alice1, alice2, alice3, alice4, dsnofac
  !-----------------------------------------------------------------------
  !     * check that the assumed band structure, currently 4 bands,
  !     * matches that of the external radiative model.

  if (nbs /= 4) then
    call xit("SNOSICA",-1)
  end if

  sum_c_flag = 0
  !
  !     * compute fractions of direct and diffuse beams for both
  !     * each band and over all bands (both all-sky and clear-sky).
  !
  fsd_bb(:) = 0.0
  fsf_bb(:) = 0.0
  csd_bb(:) = 0.0
  csf_bb(:) = 0.0

  do ib = 1, nbs
    do i = iis, iif
      wdircta(i,ib) = fsd(i,ib)/(fsd(i,ib)+fsf(i,ib)+1.e-10)
      wdirctc(i,ib) = csd(i,ib)/(csd(i,ib)+csf(i,ib)+1.e-10)
      wdiffua(i,ib) = 1.0 - wdircta(i,ib)
      wdiffuc(i,ib) = 1.0 - wdirctc(i,ib)
      fsd_bb(i)     = fsd_bb(i) + fsd(i,ib)
      fsf_bb(i)     = fsf_bb(i) + fsf(i,ib)
      csd_bb(i)     = csd_bb(i) + csd(i,ib)
      csf_bb(i)     = csf_bb(i) + csf(i,ib)

      if (wdircta(i,ib) > 1.0 .or. wdircta(i,ib) < 0.0) then
        write(6,*) i,ib,wdircta(i,ib),fsd(i,ib),fsf(i,ib)
        call xit("SNOSICA",-2)
      end if

      if (wdirctc(i,ib) > 1.0 .or. wdirctc(i,ib) < 0.0) then
        write(6,*) i,ib,wdirctc(i,ib),csd(i,ib),csf(i,ib)
        call xit("SNOSICA",-3)
      end if
    end do
  end do
  !
  if (isnoalb == 1) then ! compute from band means
    do i = iis, iif
      wdircta_bb(i) = fsd_bb(i)/(fsd_bb(i)+fsf_bb(i)+1.e-10)
      wdiffua_bb(i) = 1.0 - wdircta_bb(i)
      wdirctc_bb(i) = csd_bb(i)/(csd_bb(i)+csf_bb(i)+1.e-10)
      wdiffuc_bb(i) = 1.0 - wdirctc_bb(i)
    end do
  else if (isnoalb == 0) then ! have only total radiation.  should have same value in
    ! each band so just use first one.
    do i = iis, iif
      wdircta_bb(i) = fsd(i,1)/(fsd(i,1)+fsf(i,1)+1.e-10)
      wdiffua_bb(i) = 1.0 - wdircta_bb(i)
      wdirctc_bb(i) = csd(i,1)/(csd(i,1)+csf(i,1)+1.e-10)
      wdiffuc_bb(i) = 1.0 - wdirctc_bb(i)
    end do
  end if

  do i=iis,iif

    ! compute the sea-ice albedos, snow water equivalent and run-off in this loop. needed
    ! to perform this loop first so that appropriate fields can be passed to snow albedo
    ! look up tables.

    !
    !       * initialize variables
    !
    alsnoa(i,:) = 0.0
    alsnoc(i,:) = 0.0
    c_flag(i)   = 0
    !
    !       * fractional snow coverage.
    !
    zsnolim=0.10
    if (sno(i)>0.0) then
      zsnow=sno(i)/rhon(i)
      if(zsnow >= (zsnolim-0.00001)) then
        snofac(i)=1.0
      else
        snofac(i)=zsnow/zsnolim
      end if
    else
      snofac(i)=0.0
    end if

    if (gc(i)==1.) then
      !
      !         * pack (bare ice) albedoes have values in each spectral interval
      !         * which follow from ebert and curry).
      !         * these are in effect until "PUDDLING" occurs, which
      !         * is included to reduce albedo in each spectral interval,
      !         * only if gta lies between gtfsw and gtmsi. the effect
      !         * of snow is then superimposed, based on the above value, the
      !         * effective snow coverage (snofac) and the above-calculated deep
      !         * snow albedoes.
      !
      hice=sic(i)/deni
      if (hice >= 2.) then
        albbi(i,1)    =  0.778
        albbi(i,2)    =  0.443
      else if (hice < 2. .and. hice >= 1.) then
        albbi(i,1)    =  0.760 + 0.018 * (hice - 1.0)
        albbi(i,2)    =  0.247 + 0.196 * (hice - 1.0)
      else
        x = log(max(hice,0.01) + 1.e-10)
        albbi(i,1)    =  0.760 + 0.140 * x
        albbi(i,2)    =  0.247 + 0.029 * x
      end if
      albbi(i,3)      =  0.055
      albbi(i,4)      =  0.036

      if (isnoalb == 1) then
        if (sno(i) > 0.0) then
          c_flag(i) = 1
        else
          c_flag(i) = 0
        end if
        sum_c_flag = sum_c_flag + c_flag(i)
      else if (isnoalb == 0) then
        !
        !         * albedoes over snow are aged as in "CLASS".
        !         * for now, until a 4-band snow albedo model is developed,
        !         * it is assumed that the albedo for the last 3 bands
        !         * (near-ir) are the same.
        !
        if (albs(i)>0.70) then
          timfac=(albs(i)-0.70)/0.14
          alsnoa(i,1)=0.135*timfac+0.84
          alsnoa(i,2)=0.272*timfac+0.56
        else
          timfac=(albs(i)-0.50)/0.34
          alsnoa(i,1)=0.365*timfac+0.61
          alsnoa(i,2)=0.452*timfac+0.38
        end if
        alsnoa(i,3)=0.25
        alsnoa(i,4)=0.025
        !
        alsnoc(i,1)=alsnoa(i,1)
        alsnoc(i,2)=alsnoa(i,2)
        alsnoc(i,3)=alsnoa(i,3)
        alsnoc(i,4)=alsnoa(i,4)

      end if
    end if
  end do ! loop 400

  if (isnoalb == 1 .and. sum_c_flag > 0) then
    ! convert the units of the snow grain size and bc mixing ratio
    ! snow grain size from meters to microns and bc from kg bc/kg snow to ng bc/kg snow
    do i = iis,iif
      refsno(i) = refsn(i)*1.0e6
      if (rhon(i) > 0.0) then
        bcsno(i)  = (bcsn(i)/rhon(i))*1.0e12
      else
        bcsno(i)  = bcsn(i)
      end if
    end do ! i

    call snow_albval(alsnodif, & ! output
                     alsnodir, &
                     csz, & ! input
                     albbi, &
                     bcsno, &
                     refsno, &
                     sno, &
                     c_flag, &
                     iis, &
                     iif, &
                     ilg, &
                     nbs)
    do ib = 1, nbs
      do i=iis,iif
        if (c_flag(i) == 1) then
          alsnoa(i,ib) = alsnodif (i,ib)*wdiffua(i,ib) &
                         + alsnodir(i,ib)*wdircta(i,ib)
          alsnoc(i,ib) = alsnodif (i,ib)*wdiffuc(i,ib) &
                         + alsnodir(i,ib)*wdirctc(i,ib)
        else
          alsnoa(i,ib) = 0.0
          alsnoc(i,ib) = 0.0
        end if
      end do ! loop 450
    end do ! loop 420
  end if
  !
  do i=iis,iif
    !
    !       * albedo calculations (4 bands).
    !
    if (gc(i)==1.) then
      !
      !         * ESTIMATE MELT POND FRACTION ('PUDDLE') FROM SURFACE AIR TEMP.
      !         * (note maximum pond fraction is 0.25, as in ebert and curry, 1993)
      !
      puddle = max(0., (min((gta(i)-gtfsw)/4.,puddle_max)))
      !
      !         * now take total ice albedo to be mix of bare ice and pond
      !         * albedo weighted by melt pond fraction.
      !
      dpuddle = 1.-puddle
      alice1  = dpuddle*albbi(i,1) + puddle*albp1
      alice2  = dpuddle*albbi(i,2) + puddle*albp2
      alice3  = dpuddle*albbi(i,3) + puddle*albp3
      alice4  = dpuddle*albbi(i,4) + puddle*albp4
      !
      !         * now obtain an effective ice-snow albedo based on "SNOFAC"
      !         * as a fractional coverage (for both all-sky and clear-sky).
      !
      dsnofac   = 1.-snofac(i)
      salb(i,1) = alice1*dsnofac + alsnoa(i,1)*snofac(i)
      salb(i,2) = alice2*dsnofac + alsnoa(i,2)*snofac(i)
      salb(i,3) = alice3*dsnofac + alsnoa(i,3)*snofac(i)
      salb(i,4) = alice4*dsnofac + alsnoa(i,4)*snofac(i)
      csal(i,1) = alice1*dsnofac + alsnoc(i,1)*snofac(i)
      csal(i,2) = alice2*dsnofac + alsnoc(i,2)*snofac(i)
      csal(i,3) = alice3*dsnofac + alsnoc(i,3)*snofac(i)
      csal(i,4) = alice4*dsnofac + alsnoc(i,4)*snofac(i)
    end if
  end do ! loop 500
  !
  return
end subroutine snosica
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
