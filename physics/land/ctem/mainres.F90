subroutine mainres (fcan,      fct,     stemmass,   rootmass, &
                          icc,       ig,          ilg,        il1, &
                          il2,     tcan,         tbar,   rmatctem, &
                          sort, nol2pfts,           ic,      isand, &
  !    -------------- inputs above this line, outputs below ----------
                          rmsveg, rmrveg,     roottemp)
  !
  !
  !               canadian terrestrial ecosystem model (ctem) v1.1
  !                    maintenance respiration subtoutine
  !
  !     20  sep. 2001 - this subroutine calculates maintenance respiration,
  !     v. arora        over a given sub-area, for stem and root components.
  !                     leaf respiration is estimated within the phtsyn
  !                     subroutine.

  !     change history:

  !     j. melton 20 sep 2012 - made it so does not do calcs for pfts with
  !                             fcan = 0.
  !     j. melton 23 aug 2012 - change sand to isand, converting sand to
  !                             int was missing some gridcells assigned
  !                             to bedrock in classb. isand is now passed
  !                             in.
  !
  !     inputs
  !
  !     FCAN      - FRACTIONAL COVERAGE OF CTEM's 9 PFTs OVER THE GIVEN
  !                 sub-area
  !     fct       - sum of all fcan
  !                 fcan & fct are not used at this time but could
  !                 be used at some later stage
  !     stemmass  - stem biomass for the 9 pfts in kg c/m2
  !     rootmass  - root biomass for the 9 pfts in kg c/m2
  !     icc       - no. of ctem pfts (currently 9)
  !     ig        - no. of soil layers (currently 3)
  !     ilg       - no. of grid cells in latitude circle
  !     il1,il2   - il1=1, il2=ilg
  !     tcan      - canopy temperature, k
  !     tbar      - soil temperature, k
  !     rmatctem  - fraction of roots in each layer for each pft
  !     sort      - index for correspondence between 9 pfts and 12 values
  !                 in the parameter vectors
  !     nol2pfts  - number of level 2 ctem pfts
  !     ic        - number of class pfts, currently 4
  !     isand      - flag for bedrock or ice in a soil layer
  !
  !     outputs
  !
  !     rmsveg    - maintenance respiration for stem for the 9 pfts
  !     rmrveg    - maintenance respiration for root for the 9 pfts
  !                 both in u mol co2/m2. sec
  !     roottemp  - root temperature (k)
  !
  implicit none
  !
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: icc !<
  integer, intent(in) :: ig !<
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer :: i !<
  integer :: j !<
  integer :: k !<
  integer, intent(in) :: sort(icc) !<
  integer :: kk !<
  integer, intent(in) :: nol2pfts(ic) !<
  integer :: k1 !<
  integer :: k2 !<
  integer, intent(in) :: ic !<
  integer :: m !<
  integer, intent(in) :: isand(ilg,ig) !<
  !
  parameter(kk=12) ! product of class pfts and l2max (4 x 3 = 12)
  !
  real, intent(in)  :: fcan(ilg,icc) !<
  real, intent(in)  :: fct(ilg) !<
  real, intent(in)  :: stemmass(ilg,icc) !<
  real, intent(in)  :: tcan(ilg) !<
  real, intent(in)  :: tbar(ilg,ig) !<
  real, intent(in)  :: rootmass(ilg,icc) !<
  real, intent(inout)  :: rmsveg(ilg,icc) !<
  real, intent(inout)  :: rmrveg(ilg,icc) !<
  real, intent(in)  :: rmatctem(ilg,icc,ig) !<
  !
  real  :: bsrtstem(kk) !<
  real  :: bsrtroot(kk) !<
  real  :: tempq10r(ilg,icc) !<
  real  :: tempq10s(ilg) !<
  real, intent(inout)  :: roottemp(ilg,icc) !<
  real  :: q10 !<
  real  :: q10func !<
  real  :: zero !<
  real  :: livstmfr(ilg,icc) !<
  real  :: livrotfr(ilg,icc) !<
  real  :: minlvfr !<
  !
  logical :: consq10 !<
  !
  !     constants and parameters
  !
  !     note the structure of vectors which clearly shows the class
  !     pfts (along rows) and ctem sub-pfts (along columns)
  !
  !     needle leaf |  evg       dcd       ---
  !     broad leaf  |  evg   dcd-cld   dcd-dry
  !     crops       |   c3        c4       ---
  !     grasses     |   c3        c4       ---
  !
  !     ---------------------------------------------------
  !
  !     base respiration rates for stem and root for ctem pfts in
  !     kg c/kg c.year at 15 degrees celcius. note that maintenance
  !     respiration rates of root are higher because they contain
  !     both wood (coarse roots) and fine roots.

  !    new parameter values to produce carbon use efficiencies more in
  !    line with literature (zhang et al. 2009, luyssaert et al. gcb 2007)
  !    values changed for bsrtstem and bsrtroot. jm 06.2012

  data bsrtstem/0.0900, 0.0550, 0.0000, &
               0.0600, 0.0335, 0.0300, &
               0.0365, 0.0365, 0.0000, &
               0.0000, 0.0000, 0.0000/ ! no stem component for grasses

  data bsrtroot/0.5000, 0.2850, 0.0000, &
               0.6500, 0.2250, 0.0550, &
               0.1600, 0.1600, 0.0000, &
               0.1000, 0.1000, 0.0000/
  !
  !     set the following switch to .true. for using constant temperature
  !     indepedent q10 specified below
  data consq10 /.false./
  !
  !     q10 - if using a constant temperature independent value, i.e.
  !     if consq10 is set to true
  data q10/2.00/
  !
  !     minimum live wood fraction
  data minlvfr/0.05/
  !
  !     ---------------------------------------------------
  !
  !     initialize required arrays to zero
  !
  do j = 1, icc
    do i = il1, il2
      roottemp(i,j) = 0.0        ! root temperature
      rmsveg(i,j) = 0.0          ! stem maintenance respiration
      rmrveg(i,j) = 0.0          ! root maintenance respiration
      livstmfr(i,j)= 0.0         ! live stem fraction
      livrotfr(i,j)= 0.0         ! live root fraction
    end do ! loop 110
  end do ! loop 100
  !
  !     initialization ends
  !
  !     based on root and stem biomass, find fraction which is live.
  !     for stem this would be the sapwood to total wood ratio.
  !
  k1=0
  do j = 1, ic
    if (j==1) then
      k1 = k1 + 1
    else
      k1 = k1 + nol2pfts(j-1)
    end if
    k2 = k1 + nol2pfts(j) - 1
    do m = k1, k2
      do i = il1, il2
        if (j<=2) then     ! trees
          livstmfr(i,m) = exp(-0.2835*stemmass(i,m))  ! following century model
          livstmfr(i,m) = max(minlvfr,min(livstmfr(i,m),1.0))
          livrotfr(i,m) = exp(-0.2835*rootmass(i,m))
          livrotfr(i,m) = max(minlvfr,min(livrotfr(i,m),1.0))
        else                 ! crop and grass are all live
          livstmfr(i,m) = 1.0
          livrotfr(i,m) = 1.0
        end if
      end do ! loop 130
    end do ! loop 125
  end do ! loop 120
  !
  !     fraction of roots for each vegetation type, for each soil layer,
  !     in each grid cell is given by rmatctem (grid cell, veg type, soil layer)
  !     which bio2str subroutine calculates. rmatctem can thus be used
  !     to find average root temperature for each plant functional type
  !
  do j = 1, icc
    do i = il1, il2
      if (fcan(i,j) > 0.) then
        roottemp(i,j)=tbar(i,1)*rmatctem(i,j,1) + &
        tbar(i,2)*rmatctem(i,j,2) + &
        tbar(i,3)*rmatctem(i,j,3)
        roottemp(i,j)=roottemp(i,j) / &
        (rmatctem(i,j,1)+rmatctem(i,j,2)+rmatctem(i,j,3))

        !
        !        make sure that i do not use temperatures from 2nd and 3rd layers
        !        if they are bed rock
        !
        if (isand(i,3)==-3) then ! third layer bed rock
          roottemp(i,j)=tbar(i,1)*rmatctem(i,j,1) + &
         tbar(i,2)*rmatctem(i,j,2)
          roottemp(i,j)=roottemp(i,j) / &
         (rmatctem(i,j,1)+rmatctem(i,j,2))
        end if
        if (isand(i,2)==-3) then ! second layer bed rock
          roottemp(i,j)=tbar(i,1)
        end if
      end if ! fcan check.
    end do ! loop 190
  end do ! loop 180
  !
  !     we assume that stem temperature is same as canopy temperature tcan.
  !     using stem and root temperatures we can find their maintenance
  !     respirations rates
  !
  do i = il1, il2
    !
    !         first find the q10 response function to scale base respiration
    !         rate from 15 c to current temperature, we do the stem first.
    !
    if (.not.consq10) then
      !           when finding temperature dependent q10, use temperature which
      !           is close to average of actual temperature and the temperature
      !           at which base rate is specified
      tempq10s(i)=(15.0+273.16+tcan(i))/1.9
      q10 = 3.22 - 0.046*(tempq10s(i)-273.16)
      q10 = min(4.0, max(1.5, q10))
    end if
    !
    q10func = q10**(0.1*(tcan(i)-288.16))
    !
    do j = 1, icc
      if (fcan(i,j) > 0.) then

        !         this q10 value is then used with the base rate of respiration
        !         (commonly taken at some reference temperature (15 deg c), see tjoelker et
        !         al. 2009 new phytologist or atkin et al. 2000 new phyto for
        !         an example.). long-term acclimation to temperature could be occuring
        !         see king et al. 2006 nature som for a possible approach. jm.

        rmsveg(i,j)=stemmass(i,j)* livstmfr(i,j)* q10func* &
      (bsrtstem(sort(j))/365.0)
        !
        !         convert kg c/m2.day -> u mol co2/m2.sec
        rmsveg(i,j)= rmsveg(i,j) * 963.62
        !
        !         root respiration
        !
        if (.not.consq10) then
          tempq10r(i,j)=(15.0+273.16+roottemp(i,j))/1.9
          q10 = 3.22 - 0.046*(tempq10r(i,j)-273.16)
          q10 = min(4.0, max(1.5, q10))
        end if
        !
        q10func = q10**(0.1*(roottemp(i,j)-288.16))
        rmrveg(i,j)=rootmass(i,j)* livrotfr(i,j)* q10func* &
      (bsrtroot(sort(j))/365.0)
        !
        !         convert kg c/m2.day -> u mol co2/m2.sec
        rmrveg(i,j)= rmrveg(i,j) * 963.62
        !
      end if ! fcan check.
    end do ! loop 210
  end do ! loop 200
  !
  return
end


