subroutine allcar(lfstatus,    thliq,    ailcg, &
                        icc,       ig,      ilg,       il1, &
                        il2,     sand,     clay,  rmatctem, &
                        gleafmas, stemmass, rootmass,      sort, &
                        l2max, nol2pfts,       ic,   fcancmx, &
  !    5 ------------------ inputs above this line ----------------------
                        afrleaf,  afrstem,  afrroot,  wiltsm, &
                        fieldsm, wtstatus, ltstatus)
  !    8 ------------------outputs  above this line ---------------------
  !
  !               canadian terrestrial ecosystem model (ctem) v1.1
  !                            allocation subroutine
  !
  !     sk: may  2018 - revise tolerances for 32-bit version.
  !     22  nov 2012  - calling this version 1.1 since a fair bit of ctem
  !     v. arora        subroutines were changed for compatibility with class
  !                     version 3.6 including the capability to run ctem in
  !                     mosaic/tile version along with class.
  !
  !     24  sep 2012  - add in checks to prevent calculation of non-present
  !     j. melton       pfts
  !
  !     05  may 2003  - this subroutine calculates the allocation fractions
  !     V. ARORA        FOR LEAF, STEM, AND ROOT COMPONENTS FOR CTEM's PFTs
  !
  !     inputs
  !
  !     lfstatus  - leaf status. an integer :: indicating if leaves are
  !                 in "MAX. GROWTH", "NORMAL GROWTH", "FALL/HARVEST",
  !                 or "NO LEAVES" mode. see phenolgy subroutine for
  !                 more details.
  !     thliq     - liquid soil moisture content in 3 soil layers
  !     ailcg     - green or live leaf area index
  !     icc       - no. of ctem plant function types, currently 9
  !     ig        - no. of soil layers (currently 3)
  !     ilg       - no. of grid cells in latitude circle
  !     il1,il2   - il1=1, il2=ilg
  !     sand      - percentage sand
  !     clay      - percentage clay
  !     rmatctem  - fraction of roots in each soil layer for each pft
  !     gleafmas  - green or live leaf mass in kg c/m2, for the 9 pfts
  !     stemmass  - stem mass for each of the 9 ctem pfts, kg c/m2
  !     rootmass  - root mass for each of the 9 ctem pfts, kg c/m2
  !     sort      - index for correspondence between 9 pfts and the
  !                 12 values in parameters vectors
  !     l2max     - maximum number of level 2 ctem pfts
  !     nol2pfts  - number of level 2 ctem pfts
  !     ic        - number of class pfts
  !     FCANCMX   - MAX. FRACTIONAL COVERAGE OF CTEM's 9 PFTs, BUT THIS CAN BE
  !                modified by land-use change, and competition between pfts
  !
  !     outputs
  !
  !     afrleaf   - allocation fraction for leaves
  !     afrstem   - allocation fraction for stem
  !     afrroot   - allocation fraction for root
  !     wiltsm    - wilting point soil moisture content
  !     fieldsm   - field capacity soil moisture content
  !     wtstatus  - soil water status (0 dry -> 1 wet)
  !     ltstatus  - light status
  !
  implicit none
  !
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: icc !<
  integer, intent(in) :: ig !<
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer :: i !<
  integer :: j !<
  integer :: k !<
  integer, intent(in) :: lfstatus(ilg,icc) !<
  integer :: kk !<
  integer, intent(in) :: ic !<
  integer :: n !<
  integer :: k1 !<
  integer :: k2 !<
  integer :: m !<
  !
  parameter (kk=12) ! product of class pfts and l2max (4 x 3 = 12)
  !
  logical :: consallo !<
  !
  integer, intent(in)       :: sort(icc) !<
  integer, intent(in)       :: l2max !<
  integer, intent(in)       :: nol2pfts(ic) !<
  !
  real, intent(in)     :: ailcg(ilg,icc) !<
  real, intent(in)     :: thliq(ilg,ig) !<
  real, intent(inout)     :: wiltsm(ilg,ig) !<
  real, intent(inout)     :: fieldsm(ilg,ig) !<
  real, intent(in)     :: rootmass(ilg,icc) !<
  real, intent(in)     :: rmatctem(ilg,icc,ig) !<
  real, intent(in)     :: gleafmas(ilg,icc) !<
  real, intent(in)     :: stemmass(ilg,icc) !<
  real, intent(in)     :: sand(ilg,ig) !<
  real, intent(in)     :: clay(ilg,ig) !<
  real     :: thpor(ilg,ig) !<
  real     :: psisat(ilg,ig) !<
  real     :: b(ilg,ig) !<
  real     :: grksat(ilg,ig) !<
  !
  real, intent(inout)   :: afrleaf(ilg,icc) !<
  real, intent(inout)   :: afrstem(ilg,icc) !<
  real, intent(inout)   :: afrroot(ilg,icc) !<
  real, intent(in)   :: fcancmx(ilg,icc) !<
  !
  real          :: omega(kk) !<
  real          :: epsilonl(kk) !<
  real          :: epsilons(kk) !<
  real          :: epsilonr(kk) !<
  real          :: kn(kk) !<
  real          :: zero !<
  real          :: eta(kk) !<
  real          :: kappa(kk) !<
  real          :: caleaf(kk) !<
  real          :: castem(kk) !<
  real          :: caroot(kk) !<
  real          :: rtsrmin(kk) !<
  real          :: aldrlfon(kk) !<
  real          :: sum1 !<
  !
  real  :: avwiltsm(ilg,icc) !<
  real  :: afieldsm(ilg,icc) !<
  real  :: avthliq(ilg,icc) !<
  real, intent(inout)  :: wtstatus(ilg,icc) !<
  real, intent(inout)  :: ltstatus(ilg,icc) !<
  real  :: nstatus(ilg,icc) !<
  real  :: wnstatus(ilg,icc) !<
  real  :: denom !<
  real  :: mnstrtms(ilg,icc) !<
  real  :: diff !<
  real  :: term1 !<
  real  :: term2 !<
  real  :: aleaf(ilg,icc) !<
  real  :: astem(ilg,icc) !<
  real  :: aroot(ilg,icc) !<
  !
  !
  common /ctem1/ eta, kappa, kn
  !     ------------------------------------------------------------------
  !                     constants and parameters
  !
  !     note the structure of vectors which clearly shows the class
  !     pfts (along rows) and ctem sub-pfts (along columns)
  !
  !     needle leaf |  evg       dcd       ---
  !     broad leaf  |  evg   dcd-cld   dcd-dry
  !     crops       |   c3        c4       ---
  !     grasses     |   c3        c4       ---
  !
  !     ------------------------------------------------------------------
  !
  !     omega, parameter used in allocation formulae
  data  omega/0.80, 0.50, 0.00, &
             0.80, 0.80, 0.80, &
             0.05, 0.05, 0.00, &
             1.00, 1.00, 0.00/
  !
  !     epsilon leaf, parameter used in allocation formulae
  data epsilonl/0.20, 0.06, 0.00, &
               0.35, 0.35, 0.25, &
               0.80, 0.80, 0.00, &
               0.01, 0.01, 0.00/
  !
  !     epsilon stem, parameter used in allocation formulae
  data epsilons/0.15, 0.05, 0.00, &
               0.05, 0.10, 0.10, &
               0.15, 0.15, 0.00, &
               0.00, 0.00, 0.00/
  !
  !     epsilon root, parameter used in allocation formulae
  data epsilonr/0.65, 0.89, 0.00, &
               0.60, 0.55, 0.65, &
               0.05, 0.05, 0.00, &
               0.99, 0.99, 0.00/
  !
  !     constant allocation fractions if not using dynamic allocation.
  !     THE FOLLOWING VALUES HAVEN'T BEEN THOROUGHLY TESTED, AND USING
  !     dynamic allocation is preferable.
  data caleaf/0.275, 0.300, 0.000, &
             0.200, 0.250, 0.250, &
             0.400, 0.400, 0.000, &
             0.450, 0.450, 0.000/
  !
  data castem/0.475, 0.450, 0.000, &
             0.370, 0.400, 0.400, &
             0.150, 0.150, 0.000, &
             0.000, 0.000, 0.000/
  !
  data caroot/0.250, 0.250, 0.000, &
             0.430, 0.350, 0.350, &
             0.450, 0.450, 0.000, &
             0.550, 0.550, 0.000/
  !
  !     logical :: switch for using constant allocation factors
  data consallo /.false./     ! default value is false
  !
  !     minimum root:shoot ratio mostly for support and stability
  data rtsrmin /0.16, 0.16, 0.00, &
               0.16, 0.16, 0.32, &
               0.16, 0.16, 0.00, &
               0.50, 0.50, 0.00/
  !
  !     allocation to leaves during leaf onset
  data aldrlfon/1.00, 1.00, 0.00, &
               1.00, 1.00, 0.50, &
               1.00, 1.00, 0.00, &
               1.00, 1.00, 0.00/
  !
  !     zero
  !     data zero/1.0e-12/        ! 64-bit
  data zero/1.0e-06/        ! 32-bit
  !
  !     ---------------------------------------------------------------
  !
  if (icc/=9)                            call xit('ALLCAR',-1)
  !
  !     initialize required arrays to zero
  !
  do j = 1,icc
    do i = il1, il2
      afrleaf(i,j)=0.0    ! allocation fraction for leaves
      afrstem(i,j)=0.0    ! allocation fraction for stem
      afrroot(i,j)=0.0    ! allocation fraction for root
      !
      aleaf(i,j)=0.0    ! temporary variable
      astem(i,j)=0.0    ! temporary variable
      aroot(i,j)=0.0    ! temporary variable
      !
      !                             ! averaged over the root zone
      avwiltsm(i,j)=0.0   ! wilting point soil moisture
      afieldsm(i,j)=0.0   ! field capacity soil moisture
      avthliq(i,j)=0.0   ! liquid soil moisture content
      !
      wtstatus(i,j)=0.0   ! water status
      ltstatus(i,j)=0.0   ! light status
      nstatus(i,j)=0.0   ! nitrogen status, if and when we
      !                             ! will have n cycle in the model
      wnstatus(i,j)=0.0   ! min. of water & n status
      !
      mnstrtms(i,j)=0.0   ! min. (stem+root) biomass needed to
      !                             ! support leaves
    end do ! loop 150
  end do ! loop 140
  !
  !     initialization ends
  !
  !     ------------------------------------------------------------------
  !     estimate field capacity and wilting point soil moisture contents
  !
  !     wilting point corresponds to matric potential of 150 m
  !     field capacity corresponds to hydarulic conductivity of
  !     0.10 mm/day -> 1.157x1e-09 m/s
  !
  do j = 1, ig
    do i = il1, il2
      !
      psisat(i,j)= (10.0**(-0.0131*sand(i,j)+1.88))/100.0
      grksat(i,j)= (10.0**(0.0153*sand(i,j)-0.884))*7.0556e-6
      thpor(i,j) = (-0.126*sand(i,j)+48.9)/100.0
      b(i,j)     = 0.159*clay(i,j)+2.91
      !
      wiltsm(i,j) = (150./psisat(i,j))**(-1.0/b(i,j))
      wiltsm(i,j) = thpor(i,j) * wiltsm(i,j)
      !
      fieldsm(i,j) = (1.157e-09/grksat(i,j))** &
                         (1./(2.*b(i,j)+3.))
      fieldsm(i,j) = thpor(i,j) *  fieldsm(i,j)
      !
    end do ! loop 170
  end do ! loop 160
  !
  !
  !     calculate liquid soil moisture content, and wilting and field capacity
  !     soil moisture contents averaged over the root zone. note that while
  !     the soil moisture content is same under the entire gcm grid cell,
  !     soil moisture averaged over the rooting depth is different for each
  !     pft because of different fraction of roots present in each soil layer.
  !
  do j = 1, icc
    do i = il1, il2
      if (fcancmx(i,j)>0.0) then
        avwiltsm(i,j) =  wiltsm(i,1)*rmatctem(i,j,1) + &
                         wiltsm(i,2)*rmatctem(i,j,2) + &
                         wiltsm(i,3)*rmatctem(i,j,3)
        avwiltsm(i,j) = avwiltsm(i,j) / &
                         (rmatctem(i,j,1)+rmatctem(i,j,2)+rmatctem(i,j,3))
        !
        afieldsm(i,j) =  fieldsm(i,1)*rmatctem(i,j,1) + &
                         fieldsm(i,2)*rmatctem(i,j,2) + &
                         fieldsm(i,3)*rmatctem(i,j,3)
        afieldsm(i,j) = afieldsm(i,j) / &
                         (rmatctem(i,j,1)+rmatctem(i,j,2)+rmatctem(i,j,3))
        !
        avthliq(i,j)  =  thliq(i,1)*rmatctem(i,j,1) + &
                         thliq(i,2)*rmatctem(i,j,2) + &
                         thliq(i,3)*rmatctem(i,j,3)
        avthliq(i,j)  = avthliq(i,j) / &
                         (rmatctem(i,j,1)+rmatctem(i,j,2)+rmatctem(i,j,3))
      end if
    end do ! loop 210
  end do ! loop 200
  !
  !     using liquid soil moisture content together with wilting and field
  !     capacity soil moisture contents averaged over the root zone, find
  !     soil water status.
  !
  do j = 1, icc
    do i = il1, il2
      if (fcancmx(i,j)>0.0) then
        if (avthliq(i,j)<=avwiltsm(i,j)) then
          wtstatus(i,j)=0.0
        else if (avthliq(i,j)>avwiltsm(i,j).and. &
     avthliq(i,j)<afieldsm(i,j)) then
          wtstatus(i,j)=(avthliq(i,j)-avwiltsm(i,j))/ &
       (afieldsm(i,j)-avwiltsm(i,j))
        else
          wtstatus(i,j)=1.0
        end if
      end if
    end do ! loop 240
  end do ! loop 230
  !
  !     calculate light status as a function of lai and light extinction
  !     parameter. for now set nitrogen status equal to 1, which means
  !     nitrogen is non-limiting.
  !
  k1=0
  do j = 1, ic
    if (j==1) then
      k1 = k1 + 1
    else
      k1 = k1 + nol2pfts(j-1)
    end if
    k2 = k1 + nol2pfts(j) - 1
    do m = k1, k2
      do i = il1, il2
        if (j==4) then  ! grasses
          ltstatus(i,m)=max(0.0, (1.0-(ailcg(i,m)/4.0)) )
        else             ! trees and crops
          ltstatus(i,m)=exp(-kn(sort(m))*ailcg(i,m))
        end if
        nstatus(i,m) =1.0
      end do ! loop 260
    end do ! loop 255
  end do ! loop 250
  !
  !     allocation to roots is determined by min. of water and nitrogen
  !     status
  !
  do j = 1,icc
    do i = il1, il2
      if (fcancmx(i,j)>0.0) then
        wnstatus(i,j)=min(nstatus(i,j),wtstatus(i,j))
      end if
    end do ! loop 390
  end do ! loop 380
  !
  !     now that we know water, light, and nitrogen status we can find
  !     allocation fractions for leaves, stem, and root components. note
  !     that allocation formulae for grasses are different from those
  !     for trees and crops, since there is no stem component in grasses.
  !
  k1=0
  do j = 1, ic
    if (j==1) then
      k1 = k1 + 1
    else
      k1 = k1 + nol2pfts(j-1)
    end if
    k2 = k1 + nol2pfts(j) - 1
    do m = k1, k2
      do i = il1, il2
        n = sort(m)
        if (j<=3) then           ! trees and crops
          denom = 1.0 + (omega(n)*( 2.0-ltstatus(i,m)-wnstatus(i,m) ))
          afrstem(i,m)=( epsilons(n)+omega(n)*(1.0-ltstatus(i,m)) )/ &
                      denom
          afrroot(i,m)=( epsilonr(n)+omega(n)*(1.0-wnstatus(i,m)) )/ &
                      denom
          afrleaf(i,m)=  epsilonl(n)/denom
        else if (j==4) then     ! grasses
          denom = 1.0 + (omega(n)*( 1.0+ltstatus(i,m)-wnstatus(i,m) ))
          afrleaf(i,m)=( epsilonl(n) + omega(n)*ltstatus(i,m) ) /denom
          afrroot(i,m)=( epsilonr(n)+omega(n)*(1.0-wnstatus(i,m)) )/ &
                      denom
          afrstem(i,m)= 0.0
        end if
      end do ! loop 410
    end do ! loop 405
  end do ! loop 400
  !
  !     if using constant allocation factors then replace the dynamically
  !     calculated allocation fractions.
  !
  if (consallo) then
    do j = 1, icc
      do i = il1, il2
        if (fcancmx(i,j)>0.0) then
          afrleaf(i,j)=caleaf(sort(j))
          afrstem(i,j)=castem(sort(j))
          afrroot(i,j)=caroot(sort(j))
        end if
      end do ! loop 421
    end do ! loop 420
  end if
  !
  !     make sure allocation fractions add to one
  !
  do j = 1, icc
    do i = il1, il2
      if (fcancmx(i,j)>0.0) then
        sum1=afrstem(i,j)+afrroot(i,j)+afrleaf(i,j)
        if (abs(sum1-1.0)>zero) then
          write(6,2000) i,j,(afrstem(i,j)+afrroot(i,j)+afrleaf(i,j))
2000      format(' AT (I) = (',i3,'), PFT=',i2,'  ALLOCATION FRACTIONS &
 not adding to one. sum  = ',F12.7)
          call xit('ALLCAR',-2)
        end if
      end if
    end do ! loop 440
  end do ! loop 430
  !
  !     the allocation fractions calculated above are overridden by two
  !     rules.
  !
  !     rule 1 which states that at the time of leaf onset which corresponds
  !     to leaf status equal to 1, more c is allocated to leaves so
  !     that they can grow asap. in addition when leaf status is
  !     "FALL/HARVEST" then nothing is allocated to leaves.
  !
  k1=0
  do j = 1, ic
    if (j==1) then
      k1 = k1 + 1
    else
      k1 = k1 + nol2pfts(j-1)
    end if
    k2 = k1 + nol2pfts(j) - 1
    do m = k1, k2
      do i = il1, il2
        if (fcancmx(i,m)>0.0) then
          if (lfstatus(i,m)==1) then
            aleaf(i,m)=aldrlfon(sort(m))
            !
            !           for grasses we use the usual allocation even at leaf onset
            !
            if (j==4) then
              aleaf(i,m)=afrleaf(i,m)
            end if
            !
            diff  = afrleaf(i,m)-aleaf(i,m)
            if ((afrstem(i,m)+afrroot(i,m))>zero) then
              term1 = afrstem(i,m)/(afrstem(i,m)+afrroot(i,m))
              term2 = afrroot(i,m)/(afrstem(i,m)+afrroot(i,m))
            else
              term1 = 0.0
              term2 = 0.0
            end if
            astem(i,m) = afrstem(i,m) + diff*term1
            aroot(i,m) = afrroot(i,m) + diff*term2
            afrleaf(i,m)=aleaf(i,m)
            afrstem(i,m)=max(0.0,astem(i,m))
            afrroot(i,m)=max(0.0,aroot(i,m))
          else if (lfstatus(i,m)==3) then
            aleaf(i,m)=0.0
            diff  = afrleaf(i,m)-aleaf(i,m)
            if ((afrstem(i,m)+afrroot(i,m))>zero) then
              term1 = afrstem(i,m)/(afrstem(i,m)+afrroot(i,m))
              term2 = afrroot(i,m)/(afrstem(i,m)+afrroot(i,m))
            else
              term1 = 0.0
              term2 = 0.0
            end if
            astem(i,m) = afrstem(i,m) + diff*term1
            aroot(i,m) = afrroot(i,m) + diff*term2
            afrleaf(i,m)=aleaf(i,m)
            afrstem(i,m)=astem(i,m)
            afrroot(i,m)=aroot(i,m)
          end if
        end if
      end do ! loop 510
    end do ! loop 505
  end do ! loop 500
  !
  !
  !     rule 2 overrides rule 1 above and makes sure that we do not allow the
  !     amount of leaves on trees and crops (i.e. pfts 1 to 7) to exceed
  !     an amount such that the remaining woody biomass cannot support.
  !     if this happens, allocation to leaves is reduced and most npp
  !     is allocated to stem and roots, in a proportion based on calculated
  !     afrstem and afrroot. for grasses this rule essentially constrains
  !     THE ROOT:SHOOT RATIO, MEANING THAT THE MODEL GRASSES CAN'T HAVE
  !     lots of leaves without having a reasonable amount of roots.
  !
  do j = 1, icc
    n=sort(j)
    do i = il1, il2
      if (fcancmx(i,j)>0.0) then
        !         find min. stem+root biomass needed to support the green leaf
        !         biomass.
        mnstrtms(i,j)=eta(n)*(gleafmas(i,j)**kappa(n))
        !
        if ( (stemmass(i,j)+rootmass(i,j))<mnstrtms(i,j)) then
          if ( (afrstem(i,j)+afrroot(i,j))>zero) then
            aleaf(i,j)=min(0.05,afrleaf(i,j))
            diff  = afrleaf(i,j)-aleaf(i,j)
            term1 = afrstem(i,j)/(afrstem(i,j)+afrroot(i,j))
            term2 = afrroot(i,j)/(afrstem(i,j)+afrroot(i,j))
            astem(i,j) = afrstem(i,j) + diff*term1
            aroot(i,j) = afrroot(i,j) + diff*term2
            afrleaf(i,j)=aleaf(i,j)
            afrstem(i,j)=astem(i,j)
            afrroot(i,j)=aroot(i,j)
          else
            aleaf(i,j)=min(0.05,afrleaf(i,j))
            diff  = afrleaf(i,j)-aleaf(i,j)
            afrleaf(i,j)=aleaf(i,j)
            afrstem(i,j)=diff*0.5 + afrstem(i,j)
            afrroot(i,j)=diff*0.5 + afrroot(i,j)
          end if
        end if
      end if
    end do ! loop 540
  end do ! loop 530
  !
  !     make sure that root:shoot ratio is at least equal to rtsrmin. if not
  !     allocate more to root and decrease allocation to stem.
  !
  do j = 1, icc
    n=sort(j)
    do i = il1, il2
      if (fcancmx(i,j)>0.0) then
        if ( (stemmass(i,j)+gleafmas(i,j))>0.05) then
          if ( (rootmass(i,j)/(stemmass(i,j)+gleafmas(i,j))) &
              .lt. rtsrmin(n) ) then
            astem(i,j)=min(0.05,afrstem(i,j))
            diff = afrstem(i,j)-astem(i,j)
            afrstem(i,j)=afrstem(i,j)-diff
            afrroot(i,j)=afrroot(i,j)+diff
          end if
        end if
      end if
    end do ! loop 542
  end do ! loop 541
  !
  !     finally check if all allocation fractions are positive and check
  !     again they all add to one.
  !
  do j = 1, icc
    do i = il1, il2
      if (fcancmx(i,j)>0.0) then
        if ( (afrleaf(i,j)<0.0).or.(afrstem(i,j)<0.0).or. &
            (afrroot(i,j)<0.0)) then
          write(6,2200) i,j
2200      format(' AT (I) = (',i3,'), PFT=',i2,'  ALLOCATION FRACTIONS &
  negative')
          write(6,2100)afrleaf(i,j),afrstem(i,j),afrroot(i,j)
2100      format(' ALEAF = ',f12.9,' ASTEM = ',f12.9,' AROOT = ',f12.9)
          call xit('ALLOCATE',-3)
        end if
      end if
    end do ! loop 560
  end do ! loop 550
  !
  do j = 1, icc
    do i = il1, il2
      if (fcancmx(i,j)>0.0) then
        sum1=afrstem(i,j)+afrroot(i,j)+afrleaf(i,j)
        if (abs(sum1-1.0)>zero) then
          write(6,2300) i,j,(afrstem(i,j)+afrroot(i,j)+afrleaf(i,j))
2300      format(' AT (I) = (',i3,'), PFT=',i2,'  ALLOCATION FRACTIONS &
 not adding to one. sum  = ',F12.7)
          call xit('ALLOCATE',-4)
        end if
      end if
    end do ! loop 590
  end do ! loop 580
  !
  return
end

