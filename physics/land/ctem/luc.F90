subroutine    luc(icc,      ilg,      il1,       il2, &
                        ic, nol2pfts,    l2max, &
                        grclarea, pfcancmx, nfcancmx,      iday, &
                        todfrac,  yesfrac, interpol, &
  !    ----------------------- inputs above this line -------------
                        gleafmas, bleafmas, stemmass, rootmass, &
                        litrmass, soilcmas, vgbiomas, gavgltms, &
                        gavgscms,  fcancmx,   fcanmx, &
  !    ----------- updates above this line, outputs below ---------
                        lucemcom, lucltrin, lucsocin)
  !
  !     ----------------------------------------------------------------
  !
  !           canadian terrestrial ecosystem model (ctem) v1.1
  !                       land use change subroutine
  !
  !     sk: may  2018 - revise tolerances for 32-bit version.
  !     03  feb. 2017 - abort condition in loop 100 changed from zero to
  !                     1.e-5 like other cases.
  !     02  jan. 2004 - this subroutine deals with the changes in the land
  !     v. arora        cover and estimates land use change (luc)
  !                     related carbon emissions. based on whether there
  !                     is conversion of forests/grassland to crop area,
  !                     or croplands abandonment, this subroutine
  !                     reallocates carbon from live vegetation to litter
  !                     and soil c components. the decomposition from the
  !                     litter and soil c pools thus implicitly models luc
  !                     related carbon emissions. set of rules are
  !                     followed to determine the fate of carbon that
  !                     results from deforestation or replacement of
  !                     grasslands by crops.
  !
  !     ----------------------------------------------------------------
  !     inputs
  !
  !     icc       - no of pfts for use by ctem, currently 9
  !     ic        - no of pfts for use by class, currently 4
  !     ilg       - no. of grid cells in latitude circle
  !     il1, il2  - il1=1, il2=ilg
  !     nol2pfts  - number of level 2 pfts
  !     l2max     - maximum number of level 2 pfts
  !     FCANCMX   - MAX. FRACTIONAL COVERAGES OF CTEM's 9 PFTs.
  !     grclarea  - gcm grid cell area, km2
  !     PFCANCMX  - PREVIOUS MAX. FRACTIONAL COVERAGES OF CTEM's 9 PFTs.
  !     NFCANCMX  - NEXT MAX. FRACTIONAL COVERAGES OF CTEM's 9 PFTs.
  !     iday      - day of year
  !     TODFRAC   - TODAY'S FRACTIONAL COVERAGE OF ALL PFTs
  !     YESFRAC   - YESTERDAY'S FRACTIONAL COVERAGE OF ALL PFTs
  !     interpol  - if todfrac & yesfrac are provided then interpol must
  !                 BE SET TO FALSE SO THAT THIS SUBROUTINE DOESN'T DO ITS
  !                 own interpolation using pfcancmx and nfcancmx which
  !                 are year end values
  !     updates
  !
  !     gleafmas  - green or live leaf mass in kg c/m2, for the 9 pfts
  !     bleafmas  - brown or dead leaf mass in kg c/m2, for the 9 pfts
  !     stemmass  - stem biomass in kg c/m2, for the 9 pfts
  !     rootmass  - root biomass in kg c/m2, for the 9 pfts
  !     litrmass  - litter mass in kg c/m2, for the 9 pfts + bare
  !     soilcmas  - soil c mass in kg c/m2, for the 9 pfts + bare
  !     vgbiomas  - grid averaged vegetation biomass, kg c/m2
  !     gavgltms  - grid averaged litter mass, kg c/m2
  !     gavgscms  - grid averaged soil c mass, kg c/m2
  !
  !     outputs
  !
  !     fcanmx   - fractional coverages of class 4 pfts (these are found based
  !                on new fcancmxs)
  !     lucemcom - luc related carbon emission losses from combustion
  !                u-mol co2/m2.sec
  !     lucltrin - luc related input to litter pool, u-mol co2/m2.sec
  !     lucsocin - luc related input to soil carbon pool, u-mol co2/m2.sec
  !
  !     ----------------------------------------------------------------
  !
  implicit none

  integer, intent(in) :: icc !<
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ic !<
  integer :: i !<
  integer :: j !<
  integer :: k !<
  integer :: m !<
  integer :: n !<
  integer :: kk !<
  integer :: k1 !<
  integer :: k2 !<
  integer, intent(in) :: iday   !< Julian calendar day of year \f$[day]\f$
  integer, intent(in)    :: nol2pfts(ic) !<
  integer, intent(in)    :: l2max !<
  integer    :: luctkplc(ilg) !<
  integer    :: fraciord(ilg,icc) !<
  integer    :: treatind(ilg,icc) !<
  integer    :: bareiord(ilg) !<

  logical, intent(in)  :: interpol !<
  !
  real, intent(inout)  :: gleafmas(ilg,icc) !<
  real, intent(inout)  :: bleafmas(ilg,icc) !<
  real, intent(inout)  :: stemmass(ilg,icc) !<
  real, intent(inout)  :: rootmass(ilg,icc) !<
  real, intent(inout)  :: fcancmx(ilg,icc) !<
  real, intent(inout)  :: pfcancmx(ilg,icc) !<
  real, intent(inout)  :: vgbiomas(ilg) !<
  real  :: zero !<
  real, intent(inout)  :: soilcmas(ilg,icc+1) !<
  real, intent(inout)  :: litrmass(ilg,icc+1) !<
  real, intent(inout)  :: gavgltms(ilg) !<
  real, intent(inout)  :: gavgscms(ilg) !<
  real, intent(in)  :: nfcancmx(ilg,icc) !<
  real  :: fcancmy(ilg,icc) !<
  real, intent(in)  :: todfrac(ilg,icc) !<
  real, intent(in)  :: yesfrac(ilg,icc) !<
  !
  real, intent(inout)     :: fcanmx(ilg,ic) !<
  real     :: delfrac(ilg,icc) !<
  real     :: combust(3) !<
  real     :: paper(3) !<
  real     :: furniture(3) !<
  real     :: abvgmass(ilg,icc) !<
  real     :: bmasthrs(2) !<
  real, intent(in)     :: grclarea(ilg) !<
  real     :: combustc(ilg,icc) !<
  real     :: paperc(ilg,icc) !<
  real     :: furnturc(ilg,icc) !<
  real     :: incrlitr(ilg,icc) !<
  real     :: incrsolc(ilg,icc) !<
  real     :: tolrnce1 !<
  real     :: tolrnce2 !<
  real     :: chopedbm(ilg) !<
  real     :: km2tom2 !<
  real     :: diff !<
  real     :: sum1 !<
  real     :: sum2 !<
  real     :: rdif !<
  !
  real          :: redubmas1 !<
  real          :: term !<
  real          :: barefrac(ilg) !<
  real          :: grsumcom(ilg) !<
  real          :: grsumpap(ilg) !<
  real          :: grsumfur(ilg) !<
  real          :: grsumlit(ilg) !<
  real          :: grsumsoc(ilg) !<
  real          :: pbarefra(ilg) !<
  real          :: grdencom(ilg) !<
  real          :: grdenpap(ilg) !<
  real          :: grdenfur(ilg) !<
  real          :: grdenlit(ilg) !<
  real          :: grdensoc(ilg) !<
  real          :: totcmass(ilg) !<
  real          :: totlmass(ilg) !<
  real          :: totdmas1(ilg) !<
  real          :: ntotcmas(ilg) !<
  real          :: ntotlmas(ilg) !<
  real          :: ntotdms1(ilg) !<
  real, intent(inout)          :: lucemcom(ilg) !<
  real          :: pvgbioms(ilg) !<
  real          :: pgavltms(ilg) !<
  real          :: pgavscms(ilg) !<
  real          :: redubmas2 !<
  real, intent(inout)          :: lucltrin(ilg) !<
  real, intent(inout)          :: lucsocin(ilg) !<
  real          :: totdmas2(ilg) !<
  real          :: ntotdms2(ilg) !<

  !
  !     how much deforested/chopped off biomass is combusted
  data combust/0.15, 0.30, 0.45/
  !
  !     how much deforested/chopped off biomass goes into short term
  !     storage such as paper
  data paper/0.70, 0.70, 0.55/
  !
  !     how much deforested/chopped off biomass goes into long term
  !     storage such as furniture
  data furniture/0.15, 0.0, 0.0/
  !
  !     biomass thresholds for determining if deforested area is a forest,
  !     a shrubland, or a bush kg c/m2
  data bmasthrs/4.0, 1.0/
  !
  data zero/1.0e-20/
  !
  data tolrnce1/1.0e-06/    ! 32-bit fraction sum tolerance
  data tolrnce2/1.0e-03/    ! 32-bit relative tolerance of c balance
  !
  data km2tom2/1.0e+06/ ! changes from km2 to m2
  !     ---------------------------------------------------------------
  !
  if (icc/=9)                               call xit('LUC',-1)
  if (ic/=4)                                call xit('LUC',-2)
  !
  !     ------------------------------------------------------------------
  !
  !     check if fractions of deforested biomass that is combusted, and
  !     turned into paper and furnitire add to 1.0
  !
  do k = 1, 3
    sum1=combust(k)+paper(k)+furniture(k)
    if (abs(sum1-1.0)>tolrnce1) then
      write(6,*)'COMBUST + PAPER + FURNITURE DO NOT ADD TO 1.0'
      write(6,*)'COMBUST(',k,')=',combust(k)
      write(6,*)'PAPER(',k,')=',paper(k)
      write(6,*)'FURNITURE(',k,')=',furniture(k)
      write(6,*)'SUM(',k,')=',sum1
      write(6,*)'DIFF(',k,')=',abs(sum1-1.0)
      call xit('LUC',-3)
    end if
  end do ! loop 100
  !
  !     FIND/USE PROVIDED CURRENT AND PREVIOUS DAY'S FRACTIONAL COVERAGE
  !
  if (interpol) then ! perform interpolation
    do j = 1, icc
      do i = il1, il2
        delfrac(i,j)=nfcancmx(i,j)-pfcancmx(i,j) ! change in fraction
        delfrac(i,j)=delfrac(i,j)/365.0
        fcancmx(i,j)=pfcancmx(i,j)+(real(iday)*delfrac(i,j)) !  current day
        fcancmy(i,j)=pfcancmx(i,j)+(real(iday-1)*delfrac(i,j)) ! previous day
        !
        if (fcancmx(i,j)<0.0) then
          if (fcancmx(i,j)>-tolrnce1) then
            !             assume this is round-off
            fcancmx(i,j)=0.0
          else
            write(6,*)'FCANCMX(',i,',',j,')=',fcancmx(i,j)
            write(6,*)'FRACTIONAL COVERAGE CANNOT BE NEGATIVE'
            call xit('LUC',-4)
          end if
        end if
        !
        if (fcancmy(i,j)<0.0) then
          if (fcancmy(i,j)>-tolrnce1) then
            !             assume this is round-off
            fcancmy(i,j)=0.0
          else
            write(6,*)'FCANCMY(',i,',',j,')=',fcancmy(i,j)
            write(6,*)'FRACTIONAL COVERAGE CANNOT BE NEGATIVE'
            call xit('LUC',-5)
          end if
        end if

      end do ! loop 111
    end do ! loop 110
  else if (.not. interpol) then ! use provided values but still check
    !                                   they are not -ve
    do j = 1, icc
      do i = il1, il2
        fcancmx(i,j) = todfrac(i,j)
        fcancmy(i,j) = yesfrac(i,j)

        if (fcancmx(i,j)<0.0) then
          if (fcancmx(i,j)>-tolrnce2) then
            !             assume this is round-off
            fcancmx(i,j)=0.0
          else
            write(6,*)'FCANCMX(',i,',',j,')=',fcancmx(i,j)
            write(6,*)'FRACTIONAL COVERAGE CANNOT BE NEGATIVE'
            call xit('LUC',-6)
          end if
        end if
        !
        if (fcancmy(i,j)<0.0) then
          if (fcancmy(i,j)>-tolrnce2) then
            !             assume this is round-off
            fcancmy(i,j)=0.0
          else
            write(6,*)'FCANCMY(',i,',',j,')=',fcancmy(i,j)
            write(6,*)'FRACTIONAL COVERAGE CANNOT BE NEGATIVE'
            call xit('LUC',-7)
          end if
        end if

      end do ! loop 116
    end do ! loop 115
  else
    write(6,*)'INTERPOL MUST BE 0 OR 1'
    write(6,*)'INTERPOL  = ',interpol
    call xit('LUC',-8)
  end if
  !
  !     CHECK IF THIS YEAR'S FRACTIONAL COVERAGES HAVE CHANGED OR NOT
  !     for any pft
  !
  do i = il1, il2
    luctkplc(i)=0  ! did land use change take place for any pft
  end do ! loop 150
  !
  do j = 1, icc
    do i = il1, il2
      if ( (abs(fcancmx(i,j)-fcancmy(i,j)))>zero) then
        luctkplc(i)=1 ! yes, luc did take place in this grid cell
      end if
    end do ! loop 250
  end do ! loop 200
  !
  !     -------------------------------------------------------------------
  !
  !     initialization
  !
  do j = 1, ic
    do i = il1, il2
      if (luctkplc(i)==1) then
        fcanmx(i,j)=0.0 ! FRACTIONAL COVERAGE OF CLASS' PFTs
      end if
    end do ! loop 261
  end do ! loop 260

  !
  do j = 1, icc
    do i = il1, il2
      fraciord(i,j)=0   ! fractional coverage increase or decrease
      !                           ! increase +1, decrease -1
      abvgmass(i,j)=0.0 ! above-ground biomass
      treatind(i,j)=0   ! treatment index for combust, paper, & furniture
      combustc(i,j)=0.0 ! total carbon from deforestation- combustion
      paperc(i,j)=0.0   ! total carbon from deforestation- paper
      furnturc(i,j)=0.0 ! total carbon from deforestation- furniture
    end do ! loop 271
  end do ! loop 270
  !
  do i = il1, il2
    pvgbioms(i)=vgbiomas(i)  ! store grid average quantities in
    pgavltms(i)=gavgltms(i)  ! temporary arrays
    pgavscms(i)=gavgscms(i)
    !
    vgbiomas(i)=0.0
    gavgltms(i)=0.0
    gavgscms(i)=0.0
    !
    barefrac(i)=1.0          ! initialize bare fraction to 1.0
    pbarefra(i)=1.0          ! INITIALIZE PREVIOUS YEARS'S BARE FRACTION TO 1.0
    !
    grsumcom(i)=0.0          ! grid sum of combustion carbon for all
    ! pfts that are chopped
    grsumpap(i)=0.0          ! similarly for paper,
    grsumfur(i)=0.0          ! furniture,
    grsumlit(i)=0.0          ! litter, and
    grsumsoc(i)=0.0          ! soil c
    !
    grdencom(i)=0.0          ! grid averaged densities for combustion carbon,
    grdenpap(i)=0.0          ! paper,
    grdenfur(i)=0.0          ! furniture,
    grdenlit(i)=0.0          ! litter, and
    grdensoc(i)=0.0          ! soil c
    !
    totcmass(i)=0.0          ! total c mass (live+dead)
    totlmass(i)=0.0          ! total c mass (live)
    totdmas1(i)=0.0          ! total c mass (dead) litter
    totdmas2(i)=0.0          ! total c mass (dead) soil c
    !
    !       and the same 3 quantities as above after luc treatment
    !
    ntotcmas(i)=0.0          ! total c mass (live+dead)
    ntotlmas(i)=0.0          ! total c mass (live)
    ntotdms1(i)=0.0          ! total c mass (dead) litter
    ntotdms2(i)=0.0          ! total c mass (dead) soil c
    !
    lucemcom(i)=0.0          ! luc related combustion emission losses
    lucltrin(i)=0.0          ! luc related inputs to litter pool
    lucsocin(i)=0.0          ! luc related inputs to soil c pool
    !
    bareiord(i)=0            ! bare fraction increases or decreases
    chopedbm(i)=0.0          ! chopped off biomass
  end do ! loop 280
  !
  !     initialization ends
  !
  !     -------------------------------------------------------------------
  !
  !     if land use change has taken place then get fcanmxs for use by
  !     class based on the new fcancmxs
  !
  k1=0
  do j = 1, ic
    if (j==1) then
      k1 = k1 + 1
    else
      k1 = k1 + nol2pfts(j-1)
    end if
    k2 = k1 + nol2pfts(j) - 1
    do m = k1, k2
      do i = il1, il2
        if (luctkplc(i)==1) then
          fcanmx(i,j)=fcanmx(i,j)+fcancmx(i,m)
        end if
        barefrac(i)=barefrac(i)-fcancmx(i,m)
      end do ! loop 302
    end do ! loop 301
  end do ! loop 300
  !
  !     FIND PREVIOUS DAY'S BARE FRACTION USING PREVIOUS DAY'S FCANCMXs
  !
  do j = 1, icc
    do i = il1, il2
      pbarefra(i)=pbarefra(i)-fcancmy(i,j)
    end do ! loop 311
  end do ! loop 310
  !
  !     based on sizes of 3 live pools and 2 dead pools we estimate the
  !     total amount of c in each grid cell.
  !
  do j = 1, icc
    do i = il1, il2
      totlmass(i)=totlmass(i)+ &
                 (fcancmy(i,j)*(gleafmas(i,j)+bleafmas(i,j)+ &
                  stemmass(i,j)+rootmass(i,j))*grclarea(i)*km2tom2)
    end do ! loop 321
  end do ! loop 320
  !
  do j = 1, icc+1
    do i = il1, il2
      if (j<icc+1) then
        term = fcancmy(i,j)
      else if (j==icc+1) then
        term = pbarefra(i)
      end if
      totdmas1(i)=totdmas1(i)+ &
                 (term*litrmass(i,j)*grclarea(i)*km2tom2)
      totdmas2(i)=totdmas2(i)+ &
                 (term*soilcmas(i,j)*grclarea(i)*km2tom2)
    end do ! loop 341
  end do ! loop 340
  !
  do i = il1, il2
    totcmass(i)=totlmass(i)+totdmas1(i)+totdmas2(i)
  end do ! loop 350
  !
  !     bare fractions cannot be negative
  !
  do i = il1, il2
    if (pbarefra(i)<0.0) then
      if (pbarefra(i)>-tolrnce1) then
        !           assume this is round-off
        pbarefra(i)=0.0
      else
        write(6,*)'BARE FRACTIONS CANNOT BE NEGATIVE'
        write(6,*)'PREV. BARE FRACTION(',i,')  =',pbarefra(i)
        call xit('LUC',-9)
      end if
    end if
    !
    if (barefrac(i)<0.0) then
      if (barefrac(i)>-tolrnce1) then
        !           assume this is round-off
        barefrac(i)=0.0
      else
        write(6,*)'BARE FRACTIONS CANNOT BE NEGATIVE'
        write(6,*)'BARE FRACTION(',i,')  =',barefrac(i)
        call xit('LUC',-10)
      end if
    end if
  end do ! loop 440
  !
  !     find above ground biomass and treatment index for combust, paper,
  !     and furniture
  !
  k1=0
  do j = 1, ic
    if (j==1) then
      k1 = k1 + 1
    else
      k1 = k1 + nol2pfts(j-1)
    end if
    k2 = k1 + nol2pfts(j) - 1
    do m = k1, k2
      do i = il1, il2
        abvgmass(i,m)=gleafmas(i,m)+bleafmas(i,m)+stemmass(i,m)
        if (j==1.or.j==2) then  ! trees
          if (abvgmass(i,m)>=bmasthrs(1)) then ! forest
            treatind(i,m)=1
          else if (abvgmass(i,m)<=bmasthrs(2)) then ! bush
            treatind(i,m)=3
          else  ! shrubland
            treatind(i,m)=2
          end if
        else                       ! crops and grasses
          treatind(i,m)=3
        end if
      end do ! loop 520
    end do ! loop 510
  end do ! loop 500
  !
  !     if treatment index is zero then something is wrong - we exit
  !
  do j = 1, icc
    do i = il1, il2
      if (treatind(i,j)==0) then
        write(6,*)'TREATMENT INDEX ZERO FOR GRID CELL ',i,' AND PFT &
 ',J
        call xit('LUC',-11)
      end if
    end do ! loop 531
  end do ! loop 530
  !
  !     CHECK IF A PFT's FRACTIONAL COVER IS INCREASING OR DECREASING
  !
  do j = 1, icc
    do i = il1, il2
      if ( (fcancmx(i,j)>fcancmy(i,j)) .and. &
          (abs(fcancmy(i,j)-fcancmx(i,j))>zero) ) then
        fraciord(i,j)=1  ! increasing
      else if ( (fcancmx(i,j)<fcancmy(i,j)) .and. &
              (abs(fcancmy(i,j)-fcancmx(i,j))>zero) ) then
        fraciord(i,j)=-1 ! decreasing
      end if
    end do ! loop 551
  end do ! loop 550
  !
  !     check if bare fraction increases of decreases
  !
  do i = il1, il2
    if ( (barefrac(i)>pbarefra(i)) .and. &
        (abs(pbarefra(i)-barefrac(i))>zero) ) then
      bareiord(i)=1  ! increasing
    else if ( (barefrac(i)<pbarefra(i)) .and. &
             (abs(pbarefra(i)-barefrac(i))>zero) ) then
      bareiord(i)=-1 ! decreasing
    end if
  end do ! loop 560
  !
  !     if the fractional coverage of pfts increases then spread their
  !     live & dead biomass uniformly over the new fraction. this
  !     effectively reduces their per m2 c density.
  !
  do j = 1, icc
    do i = il1, il2
      if (fraciord(i,j)==1) then
        term = fcancmy(i,j)/fcancmx(i,j)
        gleafmas(i,j)=gleafmas(i,j)*term
        bleafmas(i,j)=bleafmas(i,j)*term
        stemmass(i,j)=stemmass(i,j)*term
        rootmass(i,j)=rootmass(i,j)*term
        litrmass(i,j)=litrmass(i,j)*term
        soilcmas(i,j)=soilcmas(i,j)*term
      end if
    end do ! loop 571
  end do ! loop 570
  !
  !     if bare fraction increases then spread its litter and soil c
  !     uniformly over the increased fraction
  !
  do i = il1, il2
    if (bareiord(i)==1) then
      term = pbarefra(i)/barefrac(i)
      litrmass(i,icc+1)=litrmass(i,icc+1)*term
      soilcmas(i,icc+1)=soilcmas(i,icc+1)*term
    end if
  end do ! loop 580
  !
  !     if any of the pfts fractional coverage decreases, then we chop the
  !     aboveground biomass and treat it according to our rules (burn it,
  !     and convert it into paper and furniture). the below ground live
  !     biomass and litter of this pfts gets assimilated into litter of
  !     all pfts (uniformly spread over the whole grid cell), and soil c
  !     from the chopped off fraction of this pft, gets assimilated into
  !     soil c of all existing pfts as well.
  !
  k1=0
  do j = 1, ic
    if (j==1) then
      k1 = k1 + 1
    else
      k1 = k1 + nol2pfts(j-1)
    end if
    k2 = k1 + nol2pfts(j) - 1
    do m = k1, k2
      do i = il1, il2
        !
        if (fraciord(i,m)==-1) then
          !             chop off above ground biomass
          redubmas1=(fcancmy(i,m)-fcancmx(i,m))*grclarea(i) &
                   *abvgmass(i,m)*km2tom2
          !
          if (redubmas1<0.0) then
            write(6,*)'REDUBMAS1 LESS THAN ZERO'
            write(6,*)'FCANCMY-FCANCMX = ', &
                      fcancmy(i,m)-fcancmx(i,m)
            write(6,*)'GRID CELL = ',i,' PFT = ',m
            call xit('LUC',-12)
          end if
          !
          !             rootmass needs to be chopped as well and all of it goes to
          !             the litter/paper pool
          !
          redubmas2=(fcancmy(i,m)-fcancmx(i,m))*grclarea(i) &
                   *rootmass(i,m)*km2tom2
          !
          !
          !             keep adding chopped off biomass for each pft to get the total
          !             for a grid cell for diagnostics
          !
          chopedbm(i)=chopedbm(i) + redubmas1 + redubmas2
          !
          !             FIND WHAT'S BURNT, AND WHAT'S CONVERTED TO PAPER &
          !             furniture
          combustc(i,m)=combust(treatind(i,m))*redubmas1
          paperc(i,m)=paper(treatind(i,m))*redubmas1 + redubmas2
          furnturc(i,m)=furniture(treatind(i,m))*redubmas1
          !
          !             keep adding all this for a given grid cell
          grsumcom(i)=grsumcom(i)+combustc(i,m)
          grsumpap(i)=grsumpap(i)+paperc(i,m)
          grsumfur(i)=grsumfur(i)+furnturc(i,m)
          !
          !             litter from the chopped off fraction of the chopped
          !             off pft needs to be assimilated, and so does soil c from
          !             the chopped off fraction of the chopped pft
          !
          redubmas1=(fcancmy(i,m)-fcancmx(i,m))*grclarea(i) &
                   *litrmass(i,m)*km2tom2
          incrlitr(i,m)=redubmas1
          !
          redubmas1=(fcancmy(i,m)-fcancmx(i,m))*grclarea(i) &
                   *soilcmas(i,m)*km2tom2
          incrsolc(i,m)=redubmas1
          !
          grsumlit(i)=grsumlit(i)+incrlitr(i,m)
          grsumsoc(i)=grsumsoc(i)+incrsolc(i,m)
        end if
        !
      end do ! loop 620
    end do ! loop 610
  end do ! loop 600
  !
  !     if bare fraction decreases then chop off the litter and soil c
  !     from the decreased fraction and add it to grsumlit & grsumsoc
  !     for spreading over the whole grid cell
  !
  do i = il1, il2
    if (bareiord(i)==-1) then

      redubmas1=(pbarefra(i)-barefrac(i))*grclarea(i) &
               *litrmass(i,icc+1)*km2tom2
      !
      redubmas2=(pbarefra(i)-barefrac(i))*grclarea(i) &
               *soilcmas(i,icc+1)*km2tom2
      !
      grsumlit(i)=grsumlit(i)+redubmas1
      grsumsoc(i)=grsumsoc(i)+redubmas2
    end if
  end do ! loop 630
  !
  !     calculate if the chopped off biomass equals the sum of grsumcom(i),
  !     grsumpap(i) & grsumfur(i)
  !
  do i = il1, il2
    sum1=grsumcom(i)+grsumpap(i)+grsumfur(i)
    diff=abs(chopedbm(i)-sum1)
    if (chopedbm(i)==0.) then
      if (diff==0.) then
        rdif=0.              ! relative accuracy
      else
        rdif=1.              ! relative accuracy
      end if
    else
      rdif=diff/chopedbm(i)  ! relative accuracy
    end if
    if (rdif > tolrnce2) then
      write(6,*)'AT GRID CELL = ',i
      write(6,*)'CHOPPED BIOMASS DOES NOT EQUALS SUM OF TOTAL'
      write(6,*)'LUC RELATED EMISSIONS'
      write(6,*)'CHOPEDBM(I) = ',chopedbm(i)
      write(6,*)'GRSUMCOM(I) = ',grsumcom(i)
      write(6,*)'GRSUMPAP(I) = ',grsumpap(i)
      write(6,*)'GRSUMFUR(I) = ',grsumfur(i)
      write(6,*)'SUM OF GRSUMCOM, GRSUMPAP, GRSUMFUR(I) = ', &
        sum1
      write(6,*)'ABS. DIFF. = ',diff
      write(6,*)'REL. DIFF. = ',rdif
      call xit('LUC',-13)
    end if
  end do ! loop 632
  !
  !     spread chopped off stuff uniformly over the litter and soil c
  !     pools of all existing pfts, including the bare fraction.
  !
  !     convert the available c into density
  !
  do i = il1, il2
    grdencom(i)=grsumcom(i)/(grclarea(i)*km2tom2)
    grdenpap(i)=grsumpap(i)/(grclarea(i)*km2tom2)
    grdenfur(i)=grsumfur(i)/(grclarea(i)*km2tom2)
    grdenlit(i)=grsumlit(i)/(grclarea(i)*km2tom2)
    grdensoc(i)=grsumsoc(i)/(grclarea(i)*km2tom2)
  end do ! loop 640
  !
  do j = 1, icc
    do i = il1, il2
      if (fcancmx(i,j)>zero) then
        litrmass(i,j)=litrmass(i,j)+grdenpap(i)+grdenlit(i)
        soilcmas(i,j)=soilcmas(i,j)+grdenfur(i)+grdensoc(i)
      else
        gleafmas(i,j)=0.0
        bleafmas(i,j)=0.0
        stemmass(i,j)=0.0
        rootmass(i,j)=0.0
        litrmass(i,j)=0.0
        soilcmas(i,j)=0.0
      end if
    end do ! loop 651
  end do ! loop 650
  !
  do i = il1, il2
    if (barefrac(i)>zero) then
      litrmass(i,icc+1)=litrmass(i,icc+1)+grdenpap(i)+grdenlit(i)
      soilcmas(i,icc+1)=soilcmas(i,icc+1)+grdenfur(i)+grdensoc(i)
    else
      litrmass(i,icc+1)=0.0
      soilcmas(i,icc+1)=0.0
    end if
  end do ! loop 660
  !
  !     the combusted c is used to find the c flux that we can release
  !     into the atmosphere.
  !
  do i = il1, il2
    lucemcom(i)=grdencom(i)     ! this is flux in kg c/m2.day that
    !                                   ! will be emitted
    !       lucltrin(i)=grdenpap(i)+grdenlit(i) ! flux in kg c/m2.day
    !       lucsocin(i)=grdenfur(i)+grdensoc(i) ! flux in kg c/m2.day
    !
    lucltrin(i)=grdenpap(i) ! flux in kg c/m2.day
    lucsocin(i)=grdenfur(i) ! flux in kg c/m2.day
    !
    !       convert all land use change fluxes to u-mol co2-c/m2.sec
    lucemcom(i)=lucemcom(i)*963.62
    lucltrin(i)=lucltrin(i)*963.62
    lucsocin(i)=lucsocin(i)*963.62
  end do ! loop 670
  !
  !     and finally we see if the total amount of carbon is conserved
  !
  do j = 1, icc
    do i = il1, il2
      ntotlmas(i)=ntotlmas(i)+ &
                 (fcancmx(i,j)*(gleafmas(i,j)+bleafmas(i,j)+ &
                 stemmass(i,j)+rootmass(i,j))*grclarea(i)*km2tom2)
    end do ! loop 701
  end do ! loop 700
  !
  do j = 1, icc+1
    do i = il1, il2
      if (j<icc+1) then
        term = fcancmx(i,j)
      else if (j==icc+1) then
        term = barefrac(i)
      end if
      ntotdms1(i)=ntotdms1(i)+ &
                 (term*litrmass(i,j)*grclarea(i)*km2tom2)
      ntotdms2(i)=ntotdms2(i)+ &
                 (term*soilcmas(i,j)*grclarea(i)*km2tom2)
    end do ! loop 711
  end do ! loop 710
  !
  do i = il1, il2
    ntotcmas(i)=ntotlmas(i)+ntotdms1(i)+ntotdms2(i)
  end do ! loop 720
  !
  !     total litter mass (before + input from chopped off biomass)
  !     and after must be same
  !
  do i = il1, il2
    sum1=totdmas1(i)+grsumpap(i)
    diff=abs(sum1-ntotdms1(i))
    if (ntotdms1(i)==0.) then
      if (diff==0.) then
        rdif=0.             ! relative accuracy
      else
        rdif=1.             ! relative accuracy
      end if
    else
      rdif=diff/ntotdms1(i) ! relative accuracy
    end if
    if (rdif>tolrnce2) then
      write(6,*)'AT GRID CELL = ',i
      write(6,*)'TOTAL LITTER CARBON DOES NOT BALANCE AFTER LUC'
      write(6,*)'TOTDMAS1(I) = ',totdmas1(i)
      write(6,*)'GRSUMPAP(I) = ',grsumpap(i)
      write(6,*)'TOTDMAS1(I) + GRSUMPAP(I) = ',sum1
      write(6,*)'NTOTDMS1(I) = ',ntotdms1(i)
      write(6,*)'ABS. DIFF. = ',diff
      write(6,*)'REL. DIFF. = ',rdif
      write(6,*)'TOLRNCE2 = ',tolrnce2
      call xit('LUC',-14)
    end if
  end do ! loop 722
  !
  !     for conservation totcmass(i) must be equal to ntotcmas(i) plus
  !     combustion carbon losses
  !
  do i = il1, il2
    sum1=ntotcmas(i)+grsumcom(i)
    diff=abs(totcmass(i)-sum1)
    if (totcmass(i)==0.) then
      if (diff==0.) then
        rdif=0.             ! relative accuracy
      else
        rdif=1.             ! relative accuracy
      end if
    else
      rdif=diff/totcmass(i) ! relative accuracy
    end if
    if (rdif>tolrnce2) then
      write(6,*)'AT GRID CELL = ',i
      write(6,*)'TOTAL CARBON DOES NOT BALANCE AFTER LUC'
      write(6,*)'TOTCMASS(I) = ',totcmass(i)
      write(6,*)'NTOTCMAS(I) = ',ntotcmas(i)
      write(6,*)'GRSUMCOM(I) = ',grsumcom(i)
      write(6,*)'NTOTCMAS(I) + GRSUMCOM(I) = ',sum1
      write(6,*)'ABS. DIFF. = ',diff
      write(6,*)'REL. DIFF. = ',rdif
      write(6,*)'TOLRNCE2 = ',tolrnce2
      call xit('LUC',-15)
    end if
  end do ! loop 730
  !
  !     update grid averaged vegetation biomass, and litter and soil c
  !     densities
  !
  do j = 1, icc
    do i = il1, il2
      vgbiomas(i)=vgbiomas(i)+fcancmx(i,j)*(gleafmas(i,j)+ &
                 bleafmas(i,j)+stemmass(i,j)+rootmass(i,j))
      gavgltms(i)=gavgltms(i)+fcancmx(i,j)*litrmass(i,j)
      gavgscms(i)=gavgscms(i)+fcancmx(i,j)*soilcmas(i,j)
    end do ! loop 751
  end do ! loop 750
  !
  do i = il1, il2
    gavgltms(i)=gavgltms(i)+( barefrac(i)*litrmass(i,icc+1) )
    gavgscms(i)=gavgscms(i)+( barefrac(i)*soilcmas(i,icc+1) )
  end do ! loop 760
  !
  !     just like total amount of carbon must balance, the grid averagred
  !     densities must also balance
  !
  do i = il1, il2
    sum1=pvgbioms(i)+pgavltms(i)+pgavscms(i)
    sum2=vgbiomas(i)+gavgltms(i)+gavgscms(i)+grdencom(i)
    diff=abs(sum1-sum2)
    if (sum1==0.) then
      if (diff==0.) then
        rdif=0.              ! relative accuracy
      else
        rdif=1.              ! relative accuracy
      end if
    else
      rdif=diff/sum1         ! relative accuracy
    end if
    if (rdif >tolrnce2) then
      write(6,*)'IDAY = ',iday
      write(6,*)'AT GRID CELL = ',i
      write(6,*)'PBAREFRA(I) = ',pbarefra(i)
      write(6,*)'BAREFRAC(I) = ',barefrac(i)
      write(6,*)'PFCANCMX(I,J) = ',(pfcancmx(i,j),j=1,icc)
      write(6,*)'NFCANCMX(I,J) = ',(nfcancmx(i,j),j=1,icc)
      write(6,*)'TOTAL CARBON DENSITY DOES NOT BALANCE AFTER LUC'
      write(6,*)'PVGBIOMS(I) = ',pvgbioms(i)
      write(6,*)'PGAVLTMS(I) = ',pgavltms(i)
      write(6,*)'PGAVSCMS(I) = ',pgavscms(i)
      write(6,*)'VGBIOMAS(I) = ',vgbiomas(i)
      write(6,*)'GAVGLTMS(I) = ',gavgltms(i)
      write(6,*)'GAVGSCMS(I) = ',gavgscms(i)
      write(6,*)'GRDENCOM(I) = ',grdencom(i)
      write(6,*)'PVGBIOMS + PGAVLTMS + PGAVSCMS = ',sum1
      write(6,*)'VGBIOMAS + GAVGLTMS + GAVGSCMS + GRDENCOM = ',sum2
      write(6,*)'ABS. DIFF. = ',diff
      write(6,*)'REL. DIFF. = ',rdif
      write(6,*)'TOLRNCE2 = ',tolrnce2
      call xit('LUC',-16)
    end if
  end do ! loop 780
  !
  do j = 1, icc
    do i = il1, il2
      if (iday==365) then
        pfcancmx(i,j)=nfcancmx(i,j)
      end if
    end do ! loop 810
  end do ! loop 800
  !
  return
end

