subroutine disturb (stemmass, rootmass, gleafmas, bleafmas, &
                          thliq,   wiltsm,  fieldsm,     vmod, &
                          lightng,  fcancmx, litrmass, &
                          rmatctem, pftareab, &
                          il1,      il2,       ig,      icc, &
                          ilg,     sort, nol2pfts,       ic, &
                          grclarea,    thice,   popdin, lucemcom, &
                          dofire, &
  !    8 ------------------ inputs above this line ----------------------
                          stemltdt, rootltdt, glfltrdt, blfltrdt, &
                          pftareaa, glcaemls, rtcaemls, stcaemls, &
                          blcaemls, ltrcemls, burnfrac, probfire, &
                          emit_co2, emit_co,  emit_ch4, emit_nmhc, &
                          emit_h2,  emit_nox, emit_n2o, emit_pm25, &
                          emit_tpm, emit_tc,  emit_oc,  emit_bc)

  !    b ------------------outputs above this line ----------------------
  !
  !               canadian terrestrial ecosystem model (ctem) v1.1
  !                           disturbance subroutine
  !
  !     31  oct 2015  - input {uwind,vwind} replaced by vmod in conjunction
  !                     with other changes to use proper wind magnitude
  !                     and not stresses. see also routine "CTEM".
  !     24  sep 2012  - add in checks to prevent calculation of non-present
  !     j. melton       pfts
  !
  !     09  may 2012  - addition of emission factors and revising of the
  !     j. melton       fire scheme
  !
  !     15  may 2003  - this subroutine calculates the litter generated
  !     v. arora        and c emissions from leaves, stem, and root
  !                     components due to fire. c emissions from burned
  !                     litter are also estimated. at present no other
  !                     form of disturbance is modelled.
  !     inputs
  !
  !     stemmass  - stem mass for each of the 9 ctem pfts, kg c/m2
  !     rootmass  - root mass for each of the 9 ctem pfts, kg c/m2
  !     gleafmas  - green leaf mass for each of the 9 ctem pfts, kg c/m2
  !     bleafmas  - brown leaf mass
  !     thliq     - liquid soil moisture content
  !     wiltsm    - wilting point soil moisture content
  !     fieldsm   - field capacity soil moisture content
  !     vmod      - wind speed, m/s
  !     lightng   - total lightning, flashes/(km^2.year)
  !                 it is assumed that cloud to ground lightning is
  !                 some fixed fraction of total lightning.
  !     FCANCMX   - FRACTIONAL COVERAGES OF CTEM's 9 PFTs
  !     litrmass  - litter mass for each of the 9 pfts
  !     prbfrhuc  - probability of fire due to human causes
  !     rmatctem  - fraction of roots in each soil layer for each pft
  !     extnprob  - fire extinguishing probability
  !     pftareab  - areas of different pfts in a grid cell, before fire, km^2
  !     icc       - no. of ctem plant function types, currently 9
  !     ig        - no. of soil layers (currently 3)
  !     ilg       - no. of grid cells in latitude circle
  !     il1,il2   - il1=1, il2=ilg
  !     sort      - index for correspondence between 9 pfts and size 12 of
  !                 parameters vectors
  !     nol2pfts  - number of level 2 ctem pfts
  !     ic        - number of class pfts
  !     grclarea  - gcm grid cell area, km^2
  !     thice     - frozen soil moisture content over canopy fraction
  !     popdin    - population density (people / km^2)
  !     lucemcom  - land use change (luc) related combustion emission losses,
  !                 u-mol co2/m2.sec
  !     dofire    - boolean, if true allow fire, if false no fire.
  !
  !     outputs
  !
  !     stemltdt  - stem litter generated due to disturbance (kg c/m2)
  !     rootltdt  - root litter generated due to disturbance (kg c/m2)
  !     glfltrdt  - green leaf litter generated due to disturbance (kg c/m2)
  !     blfltrdt  - brown leaf litter generated due to disturbance (kg c/m2)
  !     burnarea  - total area burned, km^2
  !     burnfrac  - total areal :: fraction burned, (%)
  !     probfire  - probability of fire
  !     pftareaa  - areas of different pfts in a grid cell, after fire, km^2
  !
  !     note the following c burned will be converted to a trace gas
  !     emission or aerosol on the basis of emission factors.
  !
  !     glcaemls  - green leaf carbon emission losses, kg c/m2
  !     blcaemls  - brown leaf carbon emission losses, kg c/m2
  !     rtcaemls  - root carbon emission losses, kg c/m2
  !     stcaemls  - stem carbon emission losses, kg c/m2
  !     ltrcemls  - litter carbon emission losses, kg c/m2

  !     emission factors for trace gases and aerosols. units are
  !     g of compound emitted per kg of dry organic matter.
  !     values are taken from li et al. 2012 biogeosci
  !     emif_co2  - carbon dioxide
  !     emif_co   - carbon monoxide
  !     emif_ch4  - methane
  !     emif_nmhc - non-methane hydrocarbons
  !     emif_h2   - hydrogen gas
  !     emif_nox  - nitrogen oxides
  !     emif_n2o  - nitrous oxide
  !     emif_pm25 - particulate matter less than 2.5 um in diameter
  !     emif_tpm  - total particulate matter
  !     emif_tc   - total carbon
  !     emif_oc   - organic carbon
  !     emif_bc   - black carbon

  !     emitted compounds from biomass burning in g of compound
  !     emit_co2  - carbon dioxide
  !     emit_co   - carbon monoxide
  !     emit_ch4  - methane
  !     emit_nmhc - non-methane hydrocarbons
  !     emit_h2   - hydrogen gas
  !     emit_nox  - nitrogen oxides
  !     emit_n2o  - nitrous oxide
  !     emit_pm25 - particulate matter less than 2.5 um in diameter
  !     emit_tpm  - total particulate matter
  !     emit_tc   - total carbon
  !     emit_oc   - organic carbon
  !     emit_bc   - black carbon

  !     tot_emit  - sum of all pools to be converted to emissions/aerosols (g c/m2)
  !     tot_emit_dom - tot_emit converted to kg dom / m2

  !     hb_interm - interm calculation
  !     hbratio   - head to back ratio of ellipse


  !
  implicit none
  !
  integer, intent(in)    :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in)    :: icc !<
  integer, intent(in)    :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in)    :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer    :: i !<
  integer    :: j !<
  integer    :: k !<
  integer, intent(in)    :: ig !<
  integer    :: iseed !<
  integer    :: fire(ilg) !<
  integer    :: kk !<
  integer    :: m !<
  integer    :: k1 !<
  integer    :: k2 !<
  integer    :: n !<
  integer, intent(in)    :: ic !<
  !
  parameter (kk=12)  ! product of class pfts and l2max
  !
  integer, intent(in)       :: sort(icc) !<
  integer, intent(in)       :: nol2pfts(ic) !<

  logical, intent(in) :: dofire !<
  !
  real, intent(in)  :: stemmass(ilg,icc) !<
  real, intent(in)  :: rootmass(ilg,icc) !<
  real, intent(in)  :: gleafmas(ilg,icc) !<
  real, intent(in)  :: bleafmas(ilg,icc) !<
  real, intent(in)  :: thliq(ilg,ig) !<
  real, intent(in)  :: wiltsm(ilg,ig) !<
  real, intent(in)  :: fieldsm(ilg,ig) !<
  real, intent(in)  :: vmod(ilg) !<
  real, intent(in)  :: fcancmx(ilg,icc) !<
  real, intent(in)  :: lightng(ilg) !<
  real, intent(in)  :: litrmass(ilg,icc+1) !<
  real  :: prbfrhuc(ilg) !<
  real  :: extnprob(ilg) !<
  real, intent(in)  :: pftareab(ilg,icc) !<
  real, intent(in)  :: rmatctem(ilg,icc,ig) !<
  real, intent(in)  :: thice(ilg,ig) !<
  real, intent(in)  :: popdin(ilg) !<
  real, intent(in)  :: lucemcom(ilg) !<
  !
  real, intent(inout)  :: stemltdt(ilg,icc) !<
  real, intent(inout)  :: rootltdt(ilg,icc) !<
  real, intent(inout)  :: glfltrdt(ilg,icc) !<
  real  :: burnarea(ilg) !<
  real, intent(inout)  :: pftareaa(ilg,icc) !<
  real, intent(inout)  :: glcaemls(ilg,icc) !<
  real, intent(inout)  :: rtcaemls(ilg,icc) !<
  real, intent(inout)  :: stcaemls(ilg,icc) !<
  real, intent(inout)  :: ltrcemls(ilg,icc) !<
  real, intent(inout)  :: blfltrdt(ilg,icc) !<
  real, intent(inout)  :: blcaemls(ilg,icc) !<
  real, intent(inout)  :: burnfrac(ilg) !<
  real, intent(inout)  :: emit_co2(ilg) !<
  real, intent(inout)  :: emit_co(ilg) !<
  real, intent(inout)  :: emit_ch4(ilg) !<
  real, intent(inout)  :: emit_nmhc(ilg) !<
  real, intent(inout)  :: emit_h2(ilg) !<
  real, intent(inout)  :: emit_nox(ilg) !<
  real, intent(inout)  :: emit_n2o(ilg) !<
  real, intent(inout)  :: emit_pm25(ilg) !<
  real, intent(inout)  :: emit_tpm(ilg) !<
  real, intent(inout)  :: emit_tc(ilg) !<
  real, intent(inout)  :: emit_oc(ilg) !<
  real, intent(inout)  :: emit_bc(ilg) !<
  !
  real        :: bmasthrs(2) !<
  real        :: zero !<
  real        :: extnmois !<
  real        :: lwrlthrs !<
  real        :: hgrlthrs !<
  real        :: parmlght !<
  real        :: parblght !<
  real        :: alpha !<
  real        :: f0 !<
  real        :: maxsprd(kk) !<
  real        :: frco2lf(kk) !<
  real        :: frltrlf(kk) !<
  real        :: frco2stm(kk) !<
  real        :: frltrstm(kk) !<
  real        :: frco2rt(kk) !<
  real        :: frltrrt(kk) !<
  real        :: frltrbrn(kk) !<
  real        :: c2dom !<
  real        :: emif_co2(kk) !<
  real        :: emif_co(kk) !<
  real        :: emif_ch4(kk) !<
  real        :: emif_nmhc(kk) !<
  real        :: emif_h2(kk) !<
  real        :: emif_nox(kk) !<
  real        :: emif_n2o(kk) !<
  real        :: emif_pm25(kk) !<
  real        :: emif_tpm(kk) !<
  real        :: emif_tc(kk) !<
  real        :: emif_oc(kk) !<
  real        :: emif_bc(kk) !<
  !
  real   :: biomass(ilg,icc) !<
  real   :: bterm(ilg) !<
  real   :: drgtstrs(ilg,icc) !<
  real   :: betadrgt(ilg,ig) !<
  real   :: avgdryns(ilg) !<
  real   :: fcsum(ilg) !<
  real   :: avgbmass(ilg) !<
  real   :: mterm(ilg) !<
  real   :: c2glgtng(ilg) !<
  real   :: betalght(ilg) !<
  real   :: y(ilg) !<
  real   :: ymin !<
  real   :: ymax !<
  real   :: slope !<
  real   :: lterm(ilg) !<
  real, intent(inout)   :: probfire(ilg) !<
  real   :: ctime !<
  real   :: random !<
  real   :: temp !<
  real   :: betmsprd(ilg) !<
  real   :: smfunc(ilg) !<
  real   :: wind(ilg) !<
  real   :: wndfunc(ilg) !<
  real   :: sprdrate(ilg) !<
  real   :: lbratio(ilg) !<
  real   :: arbn1day(ilg) !<
  real   :: areamult(ilg) !<
  real   :: burnveg(ilg,icc) !<
  real   :: vegarea(ilg) !<
  real, intent(in)   :: grclarea(ilg) !<
  real   :: reparea !<
  real   :: tot_emit !<
  real   :: tot_emit_dom !<

  real          :: hb_interm !<
  real          :: hbratio(ilg) !<
  real          :: popdthrshld !<
  real          :: fden_m !<
  !
  !     ------------------------------------------------------------------
  !                     constants used in the model
  !     note the structure of vectors which clearly shows the class
  !     pfts (along rows) and ctem sub-pfts (along columns)
  !
  !     needle leaf |  evg       dcd       ---
  !     broad leaf  |  evg   dcd-cld   dcd-dry
  !     crops       |   c3        c4       ---
  !     grasses     |   c3        c4       ---
  !
  !     min. and max. vegetation biomass thresholds to initiate fire, kg c/m^2
  !     data bmasthrs/0.2, 1.0/
  data bmasthrs/0.25, 1.0/
  !
  real, parameter :: pi=3.1415926535898 !<
  !
  !     extinction moisture content for estimating fire likeliness due
  !     to soil moisture
  !     data extnmois/0.35/
  !     flag testing:
  data extnmois/0.21/
  !
  !     lower cloud-to-ground lightning threshold for fire likelihood
  !     flashes/km^2.year
  data lwrlthrs/0.25/
  !
  !     higher cloud-to-ground lightning threshold for fire likelihood
  !     flashes/km^2.year
  data hgrlthrs/10.0/
  !
  !     parameter m (mean) and b of logistic distribution used for
  !     estimating fire likelihood due to lightning
  data parmlght/0.4/
  data parblght/0.1/
  !
  !     parameter alpha and f0 used for estimating wind function for
  !     fire spread rate
  data alpha/8.16326e-04/

  !     flag testing:
  !     the fire spread rate in the absence of wind, now a derived
  !     quantity from the formulation of the wind speed fire spread
  !     rate scalar
  !     old:
  !     data f0/0.1/
  !     new:
  data f0/0.05/

  !
  !     max. fire spread rate, km/hr
  !     flag try having a pft-dependent max spread rate
  !     old:
  !     data maxsprd/0.45/
  !     li et al. values:
  !     data maxsprd/0.54, 0.54, 0.00,
  !     &             0.40, 0.40, 0.40,
  !     &             0.00, 0.00, 0.00,
  !     &             0.72, 0.72, 0.00/
  !     flag testing:
  data maxsprd/0.32, 0.32, 0.00, &
              0.22, 0.22, 0.22, &
              0.00, 0.00, 0.00, &
              0.40, 0.40, 0.00/

  !     fraction of leaf biomass converted to gases due to combustion
  data frco2lf/0.70, 0.70, 0.00, &
              0.70, 0.70, 0.70, &
              0.00, 0.00, 0.00, &
              0.80, 0.80, 0.00/
  !
  !     fraction of leaf biomass becoming litter after combustion
  data frltrlf/0.20, 0.20, 0.00, &
              0.20, 0.20, 0.20, &
              0.00, 0.00, 0.00, &
              0.10, 0.10, 0.00/
  !
  !     fraction of stem biomass converted to co2 due to combustion
  data frco2stm/0.20, 0.20, 0.00, &
               0.20, 0.10, 0.10, &
               0.00, 0.00, 0.00, &
               0.00, 0.00, 0.00/
  !
  !     fraction of stem biomass becoming litter after combustion
  data frltrstm/0.60, 0.60, 0.00, &
               0.60, 0.40, 0.40, &
               0.00, 0.00, 0.00, &
               0.00, 0.00, 0.00/
  !
  !     fraction of root biomass converted to co2 due to combustion
  data frco2rt/0.0, 0.0, 0.0, &
              0.0, 0.0, 0.0, &
              0.0, 0.0, 0.0, &
              0.0, 0.0, 0.0/
  !
  !     fraction of root biomass becoming litter after combustion
  data frltrrt/0.10, 0.10, 0.00, &
              0.10, 0.10, 0.10, &
              0.00, 0.00, 0.00, &
              0.25, 0.25, 0.00/
  !
  !     fraction of litter burned during fire and emitted as co2
  data frltrbrn/0.50, 0.50, 0.00, &
               0.60, 0.60, 0.60, &
               0.00, 0.00, 0.00, &
               0.70, 0.70, 0.00/
  !
  !     ========================

  !     emissions factors by chemical species
  !
  !     values are from andreae 2011 as described in li et al. 2012
  !     biogeosci.

  !     pft-specific emission factors for co2
  data emif_co2/1576.0, 1576.0,   0.00, &
               1604.0, 1576.0, 1654.0, &
               1576.0, 1654.0,   0.00, &
               1576.0, 1654.0,   0.00/

  !     pft-specific emission factors for co
  data emif_co /106.0, 106.0, 0.00, &
               103.0, 106.0, 64.0, &
               106.0,  64.0, 0.00, &
               106.0,  64.0, 0.00/

  !     pft-specific emission factors for ch4
  data emif_ch4/ 4.8, 4.8, 0.0, &
                5.8, 4.8, 2.4, &
                4.8, 2.4, 0.0, &
                4.8, 2.4, 0.0/

  !     pft-specific emission factors for nmhc
  data emif_nmhc/ 5.7, 5.7, 0.0, &
                 6.4, 5.7, 3.7, &
                 5.7, 3.7, 0.0, &
                 5.7, 3.7, 0.0/

  !     pft-specific emission factors for h2
  data emif_h2/ 1.80, 1.80, 0.00, &
               2.54, 1.80, 0.98, &
               1.80, 0.98, 0.00, &
               1.80, 0.98, 0.00/

  !     pft-specific emission factors for nox
  data emif_nox/3.24, 3.24, 0.00, &
               2.90, 3.24, 2.49, &
               3.24, 2.49, 0.00, &
               3.24, 2.49, 0.00/

  !     pft-specific emission factors for n2o
  data emif_n2o/0.26, 0.26, 0.00, &
               0.23, 0.26, 0.20, &
               0.26, 0.20, 0.00, &
               0.26, 0.20, 0.00/

  !     emission factors for aerosols

  !     pft-specific emission factors for pm2.5
  !     (particles less than 2.5 micrometers in
  !     diameter)
  data emif_pm25/12.7, 12.7, 0.0, &
                10.5, 12.7, 5.2, &
                12.7,  5.2, 0.0, &
                12.7,  5.2, 0.0/

  !     pft-specific emission factors for tpm
  !     (total particulate matter)
  data emif_tpm/17.6, 17.6, 0.0, &
               14.7, 17.6, 8.5, &
               17.6,  8.5, 0.0, &
               17.6,  8.5, 0.0/

  !     pft-specific emission factors for tc
  !     (total carbon)
  data emif_tc/ 8.3, 8.3, 0.0, &
               7.2, 8.3, 3.4, &
               8.3, 3.4, 0.0, &
               8.3, 3.4, 0.0/

  !     pft-specific emission factors for oc
  !     (organic carbon)
  data emif_oc/ 9.1, 9.1, 0.0, &
               6.7, 9.1, 3.2, &
               9.1, 3.2, 0.0, &
               9.1, 3.2, 0.0/

  !     pft-specific emission factors for bc
  !     (black carbon)
  data emif_bc/ 0.56, 0.56, 0.00, &
               0.56, 0.56, 0.47, &
               0.56, 0.47, 0.00, &
               0.56, 0.47, 0.00/

  !     conversion factor from carbon to dry organic matter
  !     value is from li et al. 2012 biogeosci
  data c2dom/450.0/ ! gc / kg dry organic matter

  !     ========================

  !     TYPICAL AREA REPRESENTING CTEM's FIRE PARAMETERIZATION
  data reparea/1000.0/ ! km^2
  !
  !     threshold of population density (people/km2) [kloster et al., biogeosci. 2010]
  data popdthrshld/300./

  !     zero
  data zero/1e-20/
  !
  !     ---------------------------------------------------------------
  !
  if (icc/=9)                            call xit('DISTURB',-1)
  !
  !     initialize required arrays to zero
  !
  do j = 1,icc
    do i = il1, il2
      stemltdt(i,j)=0.0     ! stem litter due to disturbance
      rootltdt(i,j)=0.0     ! root litter due to disturbance
      glfltrdt(i,j)=0.0     ! green leaf litter due to disturbance
      blfltrdt(i,j)=0.0     ! brown leaf litter due to disturbance
      biomass(i,j)=0.0      ! total biomass for fire purposes
      drgtstrs(i,j)=0.0     ! soil dryness factor for pfts
      burnveg(i,j)=0.0      ! burn area for each pft
      pftareaa(i,j)=0.0     ! pft area after fire
      glcaemls(i,j)=0.0     ! green leaf carbon fire emissions
      blcaemls(i,j)=0.0     ! brown leaf carbon fire emissions
      stcaemls(i,j)=0.0     ! stem carbon fire emissions
      rtcaemls(i,j)=0.0     ! root carbon fire emissions
      ltrcemls(i,j)=0.0     ! litter carbon fire emissions
    end do ! loop 150
  end do ! loop 140
  !
  do k = 1,ig
    do i = il1, il2
      betadrgt(i,k)=0.0     ! dryness term for soil layers
    end do ! loop 170
  end do ! loop 160
  !
  do i = il1, il2
    avgbmass(i)=0.0         ! avg. veg. biomass over the veg. fraction
    !                               ! of grid cell
    avgdryns(i)=0.0         ! avg. dryness over the vegetated fraction
    fcsum(i)=0.0            ! total vegetated fraction
    bterm(i)=0.0            ! biomass fire probability term
    mterm(i)=0.0            ! moisture fire probability term
    c2glgtng(i)=0.0         ! cloud-to-ground lightning
    betalght(i)=0.0         ! 0-1 lightning term
    y(i)=0.0                ! logistic dist. for fire prob. due to lightning
    lterm(i)=0.0            ! lightning fire probability term
    probfire(i)=0.0         ! probability of fire
    fire(i)=0               ! fire, 1 means yes, 0 means no
    burnarea(i)=0.0         ! total area burned due to fire
    burnfrac(i)=0.0         ! total areal :: fraction burned due to fire
    betmsprd(i)=0.0         ! beta moisture for calculating fire spread rate
    smfunc(i)=0.0           ! soil moisture function used for fire spread rate
    wind(i)=0.0             ! wind speed in km/hr
    wndfunc(i)=0.0          ! wind function for fire spread rate
    sprdrate(i)=0.0         ! fire spread rate
    lbratio(i)=0.0          ! length to breadth ratio of fire
    arbn1day(i)=0.0         ! area burned in 1 day, km^2
    areamult(i)=0.0         ! multiplier to find area burned
    vegarea(i)=0.0          ! total vegetated area in a grid cell

    emit_co2(i) = 0.0
    emit_co(i) = 0.0
    emit_ch4(i) = 0.0
    emit_nmhc(i) = 0.0
    emit_h2(i) = 0.0
    emit_nox(i) = 0.0
    emit_n2o(i) = 0.0
    emit_pm25(i) = 0.0
    emit_tpm(i) = 0.0
    emit_tc(i) = 0.0
    emit_oc(i) = 0.0
    emit_bc(i) = 0.0

  end do ! loop 180

  !     if not simulating fire, leave the subroutine now.
  if (dofire) then

    !
    !     initialization ends
    !
    !     ------------------------------------------------------------------
    !
    !     find the probability of fire as a product of three functions
    !     with dependence on total biomass, soil moisture, and lightning
    !
    !     1. dependence on total biomass
    !
    do j = 1, icc
      do i = il1, il2
        if (fcancmx(i,j)>0.0) then
          !         root biomass is not used to initiate fire. for example if
          !         the last fire burned all grass leaves, and some of the roots
          !         were left, its unlikely these roots could catch fire.
          biomass(i,j)=gleafmas(i,j)+bleafmas(i,j)+stemmass(i,j)+ &
                    litrmass(i,j)
        end if
      end do ! loop 210
    end do ! loop 200
    !
    !     find average biomass over the vegetated fraction
    !
    do j = 1, icc
      do i = il1, il2
        if (fcancmx(i,j)>0.0) then
          avgbmass(i) = avgbmass(i)+biomass(i,j)*fcancmx(i,j)
        end if
      end do ! loop 230
    end do ! loop 220
    !
    do i = il1, il2
      fcsum(i)=fcancmx(i,1)+fcancmx(i,2)+fcancmx(i,3)+fcancmx(i,4)+ &
              fcancmx(i,5)+fcancmx(i,6)+fcancmx(i,7)+fcancmx(i,8)+ &
              fcancmx(i,9)

      if (fcsum(i)>zero) then
        avgbmass(i)=avgbmass(i)/fcsum(i)
      else
        avgbmass(i)=0.0
      end if
      !
      if (avgbmass(i)>=bmasthrs(2)) then
        bterm(i)=1.0
      else if (avgbmass(i)<bmasthrs(2).and. &
      avgbmass(i)>bmasthrs(1)) then
        bterm(i)=(avgbmass(i)-bmasthrs(1))/(bmasthrs(2)-bmasthrs(1))
      else if (avgbmass(i)<=bmasthrs(1)) then
        bterm(i)=0.0  ! no fire if biomass below the lower threshold
      end if
      bterm(i)=max(0.0, min(bterm(i),1.0))
    end do ! loop 250
    !
    !     2. dependence on soil moisture
    !
    !     this is calculated in a way such that more dry the root zone
    !     of a pft type is, and more fractional area is covered with that
    !     pft, the more likely it is that fire will get started. that is
    !     the dryness factor is weighted by fraction of roots in soil
    !     layers, as well as according to the fractional coverage of
    !     different pfts. the assumption here is that if there is less
    !     moisture in root zone, then it is more likely the vegetation
    !     will be dry and thus the likeliness of fire is more.
    !
    !     first find the dryness factor for each soil layer.
    !
    do j = 1, ig
      do i = il1, il2
        !
        if ((thliq(i,j)+thice(i,j))<=wiltsm(i,j)) then
          betadrgt(i,j)=0.0
        else if ((thliq(i,j)+thice(i,j))>wiltsm(i,j).and. &
        (thliq(i,j)+thice(i,j))<fieldsm(i,j)) then
          betadrgt(i,j)=(thliq(i,j)+thice(i,j)-wiltsm(i,j))
          betadrgt(i,j)=betadrgt(i,j)/(fieldsm(i,j)-wiltsm(i,j))
        else
          betadrgt(i,j)=1.0
        end if
        betadrgt(i,j)=max(0.0, min(betadrgt(i,j),1.0))
        !
      end do ! loop 310
    end do ! loop 300
    !
    !     now find weighted value of this dryness factor averaged over
    !     the rooting depth, for each pft
    !
    do j = 1, icc
      do i = il1, il2
        if (fcancmx(i,j)>0.0) then
          drgtstrs(i,j) =  (betadrgt(i,1))*rmatctem(i,j,1) + &
                            (betadrgt(i,2))*rmatctem(i,j,2) + &
                            (betadrgt(i,3))*rmatctem(i,j,3)
          drgtstrs(i,j) = drgtstrs(i,j) / &
                            (rmatctem(i,j,1)+rmatctem(i,j,2)+rmatctem(i,j,3))
          drgtstrs(i,j)=max(0.0, min(drgtstrs(i,j),1.0))
        end if
      end do ! loop 330
    end do ! loop 320
    !
    !     next find this dryness factor averaged over the vegetated fraction
    !
    do j = 1, icc
      do i = il1, il2
        if (fcancmx(i,j)>0.0) then
          avgdryns(i) = avgdryns(i)+drgtstrs(i,j)*fcancmx(i,j)
        end if
      end do ! loop 360
    end do ! loop 350
    !
    do i = il1, il2
      if (fcsum(i)>zero) then
        avgdryns(i)=avgdryns(i)/fcsum(i)
      else
        avgdryns(i)=0.0
      end if
    end do ! loop 370
    !
    !     use average root zone vegetation dryness to find likelihood of
    !     fire due to moisture.
    !
    do i = il1, il2
      if (fcsum(i)>zero) then
        !         mterm(i)=exp( -1.0*pi*(avgdryns(i)/extnmois)**2)
        mterm(i)=1.0-tanh((1.75*avgdryns(i)/extnmois)**2)
      else
        mterm(i)=0.0   ! no fire likelihood due to moisture if no vegetation
      end if
      mterm(i)=max(0.0, min(mterm(i),1.0))
    end do ! loop 380
    !
    !     3. dependence on lightning
    !
    !     dependence on lightning is modelled in a simple way which implies that
    !     a large no. of lightning flashes are more likely to cause fire than
    !     few lightning flashes.
    !
    do i = il1, il2
      c2glgtng(i)=0.25*lightng(i)
      if (c2glgtng(i)<=lwrlthrs) then
        betalght(i)=0.0
      else if (c2glgtng(i)>lwrlthrs.and. &
    c2glgtng(i)<hgrlthrs) then
        betalght(i)=(c2glgtng(i)-lwrlthrs)/(hgrlthrs-lwrlthrs)
      else if (c2glgtng(i)>=hgrlthrs) then
        betalght(i)=1.0
      end if
      y(i)=1.0/( 1.0+exp((parmlght-betalght(i))/parblght) )
      ymin=1.0/( 1.0+exp((parmlght-0.0)/parblght) )
      ymax=1.0/( 1.0+exp((parmlght-1.0)/parblght) )
      slope=abs(0.0-ymin)+abs(1.0-ymax)
      temp=y(i)+(0.0-ymin)+betalght(i)*slope

      !     flag testing:
      !     determine the probability of fire due to human causes
      !     this is based upon the population density from the .popd
      !     read-in file
      prbfrhuc(i)=min(1.0,(popdin(i)/popdthrshld)**0.43)

      lterm(i)=temp+(1.0-temp)*prbfrhuc(i)
      lterm(i)=max(0.0, min(lterm(i),1.0))

    end do ! loop 400
    !
    !     multiply the bterm, mterm, and the lterm to find probability of
    !     fire. also generate a random number to see if we are going to
    !     start a fire or not.
    !
    do i = il1, il2
      probfire(i)=bterm(i)*mterm(i)*lterm(i)
      !       DON'T NEED THE RANDOM THING BECAUSE PROB. FIRE DETERMINES BURN
      !       area anyway.
      fire(i)=1
    end do ! loop 420
    !
    !     if fire is to be started then estimate burn area and litter generated
    !     by the fire, else do nothing.

    do i = il1, il2
      if (fire(i)==1) then
        !
        !         find spread rate as a function of wind speed and soil moisture in the
        !         root zone (as found above) which we use as a surrogate for moisture
        !         content of vegetation.
        if (avgdryns(i)>extnmois) then
          betmsprd(i)= 1.0
        else
          betmsprd(i)= avgdryns(i)/extnmois
        end if
        smfunc(i)=(1.0-betmsprd(i))**2.0
        wind(i)=vmod(i)*3.60     ! change m/s to km/hr

        !         length to breadth ratio of fire
        !         flag testing: note, li et al. use a value of -0.06
        !         orig a &b paper value:
        lbratio(i)=1.0+10.0*(1.0-exp(-0.017*wind(i)))
        !         li et al. value:
        !          lbratio(i)=1.0+10.0*(1.0-exp(-0.06*wind(i)))

        !         flag testing:
        !         calculate the head to back ratio of the fire
        hb_interm = (lbratio(i)**2 - 1.0)**0.5
        hbratio(i) = (lbratio(i) + hb_interm)/(lbratio(i) - hb_interm)

        !         flag testing:
        !         following li et al. 2012 this function has been derived
        !         from the fire rate spread perpendicular to the wind
        !         direction, in the downwind direction, the head to back
        !         ratio and the length to breadth ratio. f0 is also now
        !         a derived quantity (0.05)

        !         old:
        !          wndfunc(i)=1.0 - ( (1.0-f0)*exp(-1.0*alpha*wind(i)**2) )

        !         new:
        wndfunc(i)= (2.0 * lbratio(i)) / (1.0 + 1.0 &
                        / hbratio(i)) * f0

        !         flag: try a pft-dependent spread rate
        !         old:
        !          sprdrate(i)=maxsprd* smfunc(i)* wndfunc(i)
        !         new:
        do j = 1, icc
          n = sort(j)
          sprdrate(i)=sprdrate(i) + maxsprd(n) * smfunc(i) &
                            * wndfunc(i) * fcancmx(i,j)
        end do ! loop 435

        !
        !         area burned in 1 day, km^2
        !         the assumed ratio of the head to back ratio was 5.0 in
        !         arora and boer 2005 jgr, this value can be calculated
        !         as was pointed out in li et al. 2012 biogeosci. we
        !         adopt the calculated version below
        !         flag testing:

        !         old:
        !          arbn1day(i)=(pi*0.36*24*24*sprdrate(i)**2)/lbratio(i)

        !         new:
        arbn1day(i)=(pi*24.0*24.0*sprdrate(i)**2)/(4.0 * lbratio(i)) &
                    *(1.0 + 1.0 / hbratio(i))**2

        !
        !         based on fire extinguishing probability we estimate the number
        !         which needs to be multiplied with arbn1day to estimate average
        !         area burned

        !         flag testing:
        !         fire extinction is based upon population density
        extnprob(i)=max(0.0,0.9-exp(-0.025*popdin(i)))
        extnprob(i)=0.5+extnprob(i)/2.0

        areamult(i)=((1.0-extnprob(i))*(2.0-extnprob(i)))/ &
        extnprob(i)**2.0
        !
        !         area burned, km^2
        burnarea(i)=arbn1day(i)*areamult(i)
        !
        !         flag testing:
        !         some regions can have multiple fires in our representative
        !         area, to accomodate this we have a fire density multiplier
        !         in most regions this is unity. in regions with high fire
        !         density this becomes dependent upon the amount of grass in
        !         the gridcell, the logic is that grasslands more rapidly
        !         respond to drying conditions, rapidly refresh the c stocks
        !         and do not have long lasting, smouldering fires
        !         should this be per mosiac or per gridcell?? i think per
        !         gridcell but this would make the mosiacs all higher...
        !         problem? -nothing implimented yet.
        !
        burnarea(i)=burnarea(i)*(grclarea(i)/reparea)*probfire(i)
        burnfrac(i)=100.*burnarea(i)/grclarea(i)
        !
      end if
    end do ! loop 430
    !
    !     make sure area burned is not greater than the vegetated area.
    !     distribute burned area equally amongst pfts present in the grid cell.
    !
    do i = il1, il2
      vegarea(i)=pftareab(i,1)+pftareab(i,2)+pftareab(i,3)+ &
                pftareab(i,4)+pftareab(i,5)+pftareab(i,6)+ &
                pftareab(i,7)+pftareab(i,8)+pftareab(i,9)
      if (burnarea(i)>vegarea(i)) then
        burnarea(i)=vegarea(i)
        burnfrac(i)=100.*burnarea(i)/grclarea(i)
      end if
    end do ! loop 460
    !
    k1=0
    do j = 1, ic
      if (j==1) then
        k1 = k1 + 1
      else
        k1 = k1 + nol2pfts(j-1)
      end if
      k2 = k1 + nol2pfts(j) - 1
      do m = k1, k2
        do i = il1, il2
          if (fcancmx(i,m)>0.0) then
            if (vegarea(i)>zero) then
              burnveg(i,m)= (burnarea(i)*pftareab(i,m)/vegarea(i))
              if (j==3) then  ! crops not allowed to burn
                burnveg(i,m)= 0.0
              end if
            else
              burnveg(i,m)= 0.0
            end if
            pftareaa(i,m)=pftareab(i,m)-burnveg(i,m)
          end if
        end do ! loop 480
      end do ! loop 475
    end do ! loop 470
    !
    do i = il1, il2
      burnarea(i)=burnveg(i,1)+burnveg(i,2)+burnveg(i,3)+burnveg(i,4)+ &
                burnveg(i,5)+burnveg(i,6)+burnveg(i,7)+burnveg(i,8)+ &
                burnveg(i,9)
      burnfrac(i)=100.*burnarea(i)/grclarea(i)
    end do ! loop 490
    !
    !     check that the sum of fraction of leaves, stem, and root
    !     that needs to be burned and converted into co2, and fraction that
    !     NEEDS TO BECOME LITTER DOESN'T EXCEED ONE.
    !
    do j = 1, icc
      n = sort(j)
      if ( (frco2lf(n)+frltrlf(n))>1.0)    call xit('DISTURB',-3)
      if ( (frco2stm(n)+frltrstm(n))>1.0)  call xit('DISTURB',-4)
      if ( (frco2rt(n)+frltrrt(n))>1.0)    call xit('DISTURB',-5)
      if (frltrbrn(n)>1.0)               call xit('DISTURB',-6)
    end do ! loop 500
    !
    !     and finally estimate amount of litter generated from each pft, and
    !     each vegetation component (leaves, stem, and root) based on their
    !     resistance to combustion. we also estimate co2 emissions from each
    !     of these components. note that the litter which is generated due
    !     to disturbance is uniformly distributed over the entire area of
    !     a given pft, and this essentially thins the vegetation biomass.
    !     at this stage we do not make the burn area bare, and therefore
    !     FIRE DOESN'T CHANGE THE FRACTIONAL COVERAGE OF PFTs.
    !
    do j = 1, icc
      n = sort(j)
      do i = il1, il2
        if (fcancmx(i,j)>0.0) then
          !
          if (pftareab(i,j)>zero) then
            glfltrdt(i,j)=frltrlf(n) *gleafmas(i,j)* &
          (burnveg(i,j)/pftareab(i,j))
            !
            blfltrdt(i,j)=frltrlf(n) *bleafmas(i,j)* &
          (burnveg(i,j)/pftareab(i,j))
            !
            stemltdt(i,j)=frltrstm(n)*stemmass(i,j)* &
          (burnveg(i,j)/pftareab(i,j))
            !
            rootltdt(i,j)=frltrrt(n) *rootmass(i,j)* &
          (burnveg(i,j)/pftareab(i,j))
            !
            glcaemls(i,j)=frco2lf(n) *gleafmas(i,j)* &
          (burnveg(i,j)/pftareab(i,j))
            !
            blcaemls(i,j)=frco2lf(n) *bleafmas(i,j)* &
          (burnveg(i,j)/pftareab(i,j))
            !
            stcaemls(i,j)=frco2stm(n)*stemmass(i,j)* &
          (burnveg(i,j)/pftareab(i,j))
            !
            rtcaemls(i,j)=frco2rt(n) *rootmass(i,j)* &
          (burnveg(i,j)/pftareab(i,j))
            !
            ltrcemls(i,j)=frltrbrn(n)*litrmass(i,j)* &
          (burnveg(i,j)/pftareab(i,j))

            !          calculate the emissions of trace gases and aerosols based upon how
            !          much plant matter was burnt

            !          sum all pools that will be converted to emissions/aerosols (g c/m2)
            tot_emit = (glcaemls(i,j) + blcaemls(i,j) + rtcaemls(i,j) &
                        + stcaemls(i,j) + ltrcemls(i,j)) * 1000.0

            !          add in the emissions due to luc fires (deforestation)
            !          the luc emissions are converted from umol co2 m-2 s-1
            !          to g c m-2 (day-1) before adding to tot_emit
            tot_emit = tot_emit + (lucemcom(i) / 963.62 * 1000.0)

            !          convert burnt plant matter from carbon to dry organic matter using
            !          a conversion factor, assume all parts of the plant has the same
            !          ratio of carbon to dry organic matter. units: kg dom / m2
            tot_emit_dom = tot_emit / c2dom

            !          convert the dom to emissions/aerosols using emissions factors
            !          units: g compound / m2

            emit_co2(i) = emit_co2(i) + emif_co2(j) * &
                          tot_emit_dom * fcancmx(i,j)
            emit_co(i) = emit_co(i) + emif_co(j) * &
                          tot_emit_dom * fcancmx(i,j)
            emit_ch4(i) = emit_ch4(i) + emif_ch4(j) * &
                          tot_emit_dom * fcancmx(i,j)
            emit_nmhc(i) = emit_nmhc(i) + emif_nmhc(j) * &
                            tot_emit_dom * fcancmx(i,j)
            emit_h2(i) = emit_h2(i) + emif_h2(j) * &
                          tot_emit_dom * fcancmx(i,j)
            emit_nox(i) = emit_nox(i) + emif_nox(j) * &
                          tot_emit_dom * fcancmx(i,j)
            emit_n2o(i) = emit_n2o(i) + emif_n2o(j) * &
                          tot_emit_dom * fcancmx(i,j)
            emit_pm25(i) = emit_pm25(i) + emif_pm25(j) * &
                            tot_emit_dom * fcancmx(i,j)
            emit_tpm(i) = emit_tpm(i) + emif_tpm(j) * &
                          tot_emit_dom * fcancmx(i,j)
            emit_tc(i) = emit_tc(i) + emif_tc(j) * &
                          tot_emit_dom * fcancmx(i,j)
            emit_oc(i) = emit_oc(i) + emif_oc(j) * &
                          tot_emit_dom * fcancmx(i,j)
            emit_bc(i) = emit_bc(i) + emif_bc(j) * &
                          tot_emit_dom * fcancmx(i,j)
          end if
          !
        end if
      end do ! loop 530
    end do ! loop 520
  end if ! dofire
  return
end
