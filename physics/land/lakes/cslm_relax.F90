!> \file
!> \brief Routine to selectively nudge, or bias correct, cslm lake temperature and lake ice
!!
!! @author John Scinocca
!
subroutine cslm_relax(t_ref_in, icon_ref, amsk, flx_skin, &
                      t0, lkiceh, dmix_in, ilg, il1, il2)
  use hydcon, only : tfrez, delskin, hcpw, hcpice, rhoice, clhmlt
  use relax_cslm_param_defs, only : tau_lake_sref, ithresh

  implicit none

  real, intent(in),dimension(ilg) :: t_ref_in !< reference lake surface water temperature \f$[K]\f$
  real, intent(in),dimension(ilg) :: icon_ref !< reference lake ice concentration [0,100] \f$[%]\f$
  real, intent(in),dimension(ilg) :: amsk !< weight applied to lake nudging [0,1] \f$[n/a]\f$
  real, intent(out),dimension(ilg) :: flx_skin !< heating/cooling flux applied to top of skin-layer \f$[W/m^2 = J/s]\f$
  real, intent(in),dimension(ilg) :: t0 !< skin layer temperature \f$[K]\f$
  real, intent(in),dimension(ilg) :: lkiceh !< average lake ice thickness over whole lake \f$[m]\f$
  real, intent(in),dimension(ilg) :: dmix_in !< depth of mixed layer depth \f$[m]\f$

  integer, intent(in) :: ilg !< dimension of row arrays \f$[n/a]\f$
  integer, intent(in) :: il1,il2 !< start end indices of gathered arrays \f$[n/a]\f$


! internal arrays
  real, dimension(ilg) :: ifrac_ref !< reference lake ice cover fraction [0,1]
  real :: t_ref !< adjusted t_ref_in
  integer :: il
  real :: hinc_skin ! top of skin-layer heating/cooling increment [J / m^2 ]
  real :: tau_ss !< timescale of lake skin layer relaxation \f$[s]\f$
  real :: dmix ! local value of mixed-layer depth
  real :: w1,w2 ! t_ref adjustment


  ifrac_ref=icon_ref/100. ! change % to fraction
  tau_ss=3600.*tau_lake_sref


! loop over gathered longitudes
  do il=il1,il2
     dmix=max(delskin,dmix_in(il)) ! dim no smaller than ds
     hinc_skin=0. ! initialize ice heat increment to zero
     if (amsk(il) > 0. .and. t_ref_in(il) > 0.) then ! if the lakes represented by the current GCM grid cell have
        ! a non-zero portion of ARC-lakes with reference nudging data
        t_ref=t_ref_in(il)
        if (ifrac_ref(il) >= ithresh) then ! set t_ref to freezing when ice is in reference data
             t_ref=tfrez
        endif
! consider all cases
        if (ifrac_ref(il) < ithresh) then ! zero ice considered present in the reference data
           w1=ifrac_ref(il)/ithresh
           w2=(ithresh-ifrac_ref(il))/ithresh
           t_ref=w2*t_ref+w1*tfrez ! adjust t_ref smoothly from tfrez to t_refinput below threshold
           if (lkiceh(il) > 0.) then ! non-zero ice is present in cslm
                 hinc_skin = lkiceh(il) * hcpice * (tfrez - t0(il)) ! heat required to raise temperature of ice to tfrez
                 hinc_skin = hinc_skin + lkiceh(il) * rhoice * clhmlt ! heat required to melt ice
!!                 hinc_skin = hinc_skin + dmix * hcpw * (t_ref-tfrez)! warm the water from tfrez to t_ref
           else ! no ice in model and ref so just nudge the temperature
                 hinc_skin = dmix * hcpw * (t_ref-t0(il))! warm/cool the water in skin layer based on mixed-layer depth this will get mixed down
           endif
        elseif(lkiceh(il) == 0.) then ! zero ice is present in cslm and ice present in the reference
           hinc_skin = dmix * hcpw * (tfrez-t0(il)) ! mix layer cooling to tfrez to encourage ice growth
        endif
     endif
! construct increment
     flx_skin(il)= amsk(il)*hinc_skin/tau_ss
  enddo

  return
end subroutine cslm_relax
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
