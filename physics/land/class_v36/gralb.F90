subroutine gralb(alvsg,alirg,alvsgc,alirgc, &
                       algwv,algwn,algdv,algdn, &
                       thliq,fsnow,alvsu,aliru,fcmxu, &
                       agvdat,agidat,fg,isand, &
                       ilg,ig,il1,il2,jl,ialg)
  !
  !     * feb 09/15 - d.verseghy. new version for gcm18 and class 3.6:
  !     *                         - wet and dry albedoes for each of
  !     *                           visible and near-ir are passed in
  !     *                           instead of algwet and algdry. these
  !     *                           are used to calculate alisg and alirg.
  !     * nov 16/13 - m.lazare.   final version for gcm17:
  !     *                         - remove unnecessary lower bound
  !     *                           of 1.e-5 on "FURB".
  !     * apr 13/06 - d.verseghy. separate albedos for open and
  !     *                         canopy-covered ground.
  !     * nov 03/04 - d.verseghy. add "IMPLICIT NONE" command.
  !     * sep 04/03 - d.verseghy. rationalize calculation of urban
  !     *                         albedo.
  !     * mar 18/02 - d.verseghy. updates to allow assignment of user-
  !     *                         specified values to ground albedo.
  !     *                         pass in ice and organic albedos
  !     *                         via new common block "CLASS8".
  !     * feb 07/02 - d.verseghy. revisions to bare soil albedo
  !     *                         calculations; removal of soil
  !     *                         moisture extrapolation to surface.
  !     * jun 05/97 - d.verseghy. class - version 2.7.
  !     *                         calculate soil albedo from percent
  !     *                         sand content rather than from colour
  !     *                         index.
  !     * sep 27/96 - d.verseghy. class - version 2.5.
  !     *                         fix bug to calculate ground albedo
  !     *                         under canopies as well as over bare
  !     *                         soil.
  !     * nov 29/94 - m.lazare.   class - version 2.3.
  !     *                         "CALL ABORT" changed to "CALL XIT",
  !     *                         TO ENABLE RUNNING RUN ON PC'S.
  !     * feb 12/93 - d.verseghy/m.lazare. increase dry soil albedo to
  !     *                                  0.45 from 0.35.
  !     * mar 13/92 - d.verseghy/m.lazare. revised and vectorized code
  !     *                                  for model version gcm7.
  !     * aug 12/91 - d.verseghy. class - version 2.0.
  !     *                         code for model version gcm7u (with
  !     *                         canopy).
  !     * apr 11/89 - d.verseghy. calculate visible and near-ir soil
  !     *                         albedos based on texture and surface
  !     *                         wetness. (set to ice albedos over
  !     *                         continental ice sheets.)
  !
  use hydcon, only : alvsi, aliri, alvso, aliro
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in)  :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in)  :: ig !<
  integer, intent(in)  :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in)  :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in)  :: jl !<
  integer, intent(in)  :: ialg !<
  !
  !     * output arrays.
  !
  real, intent(inout) :: alvsg (ilg) !<
  real, intent(inout) :: alirg (ilg) !<
  real, intent(inout) :: alvsgc (ilg) !<
  real, intent(inout) :: alirgc (ilg) !<
  !
  !     * input arrays.
  !
  real, intent(in), dimension(ilg,ig) :: thliq !<
  real, intent(in) :: algwv (ilg) !<
  real, intent(in) :: algwn(ilg) !<
  real, intent(in) :: algdv(ilg) !<
  real, intent(in) :: algdn(ilg) !<
  real, intent(in) :: fsnow (ilg) !<
  real, intent(in) :: alvsu (ilg) !<
  real, intent(in) :: aliru (ilg) !<
  real, intent(in) :: fcmxu (ilg) !<
  real, intent(in) :: agvdat(ilg) !<
  real, intent(in) :: agidat(ilg) !<
  real, intent(in) :: fg    (ilg) !<
  !
  integer, intent(in), dimension(ilg,ig) :: isand !<
  !
  !     * temporary variables.
  !
  integer  :: iptbad !<
  integer  :: i !<
  real :: furb !<
  real :: albsol !<
  !---------------------------------------------------------------------
  iptbad=0
  do i=il1,il2
    if (ialg==0) then
      if (isand(i,1)>=0) then
        furb=fcmxu(i)*(1.0-fsnow(i))
        if (thliq(i,1)>=0.26) then
          alvsg(i)=algwv(i)
          alirg(i)=algwn(i)
        else if (thliq(i,1)<=0.22) then
          alvsg(i)=algdv(i)
          alirg(i)=algdn(i)
        else
          alvsg(i)=thliq(i,1)*(algwv(i)-algdv(i))/0.04+ &
                     algdv(i)-5.50*(algwv(i)-algdv(i))
          alirg(i)=thliq(i,1)*(algwn(i)-algdn(i))/0.04+ &
                     algdn(i)-5.50*(algwn(i)-algdn(i))
        end if
        !
        if (fg(i)>0.001) then
          alvsg(i)=((fg(i)-furb)*alvsg(i)+furb*alvsu(i))/fg(i)
          alirg(i)=((fg(i)-furb)*alirg(i)+furb*aliru(i))/fg(i)
        end if
        if (alvsg(i)>1.0.or.alvsg(i)<0.0) iptbad=i
        if (alirg(i)>1.0.or.alirg(i)<0.0) iptbad=i
      else if (isand(i,1)==-4) then
        alvsg(i)=alvsi
        alirg(i)=aliri
      else if (isand(i,1)==-3) then
        alvsg(i)=algdv(i)
        alirg(i)=algdn(i)
      else if (isand(i,1)==-2) then
        alvsg(i)=alvso
        alirg(i)=aliro
      end if
    else if (ialg==1) then
      alvsg(i)=agvdat(i)
      alirg(i)=agidat(i)
    end if
    alvsgc(i)=alvsg(i)
    alirgc(i)=alirg(i)
  end do ! loop 100
  !
  if (iptbad/=0) then
    write(6,6100) iptbad,jl,alvsg(iptbad),alirg(iptbad)
    6100    format('0AT (I,J)= (',i3,',',i3,'), ALVSG,ALIRG = ',2f10.5)
    call xit('GRALB',-1)
  end if

  return
end subroutine gralb
