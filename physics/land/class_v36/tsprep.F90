subroutine tsprep(gcoeffs,gconsts,cphchg,iwater, &
                        fi,zsnow,tsnow,tcsnow, &
                        ilg,il1,il2,jl)
  !
  !     * aug 16/06 - d.verseghy. major revision to implement thermal
  !     *                         separation of snow and soil.
  !     * may 24/06 - d.verseghy. limit delz3 to <= 4.1 m.
  !     * oct 04/05 - d.verseghy. use three-layer tbar,tctop,tcbot.
  !     * nov 04/04 - d.verseghy. add "IMPLICIT NONE" command.
  !     * aug 06/02 - d.verseghy. shortened class3 common block.
  !     * jun 17/02 - d.verseghy. use new lumped soil and ponded water
  !     *                         temperature for first layer; shortened
  !     *                         class4 common block.
  !     * jun 20/97 - d.verseghy. class - version 2.7.
  !     *                         incorporate explicitly calculated
  !     *                         thermal conductivities at tops and
  !     *                         bottoms of soil layers.
  !     * aug 24/95 - d.verseghy. class - version 2.4.
  !     *                         revisions to allow for inhomogeneity
  !     *                         between soil layers and fractional
  !     *                         organic matter content.
  !     * nov 28/94 - m. lazare.  class - version 2.3.
  !     *                         tcsatw,tcsati declared real(16).
  !     * apr 10/92 - m. lazare.  class - version 2.1.
  !     *                         divide previous subroutine "T4LAYR"
  !     *                         into "TSPREP" and "TSPOST" and
  !     *                         vectorize.
  !     * apr 11/89 - d.verseghy. calculate coefficients for ground heat
  !     *                         flux, expressed as a linear function of
  !     *                         surface temperature. coefficients are
  !     *                         calculated from layer temperatures,
  !     *                         thicknesses and thermal conductivities,
  !     *                         assuming a quadratic variation of
  !     *                         temperature with depth within each
  !     *                         soil/snow layer. set the surface
  !     *                         latent heat of vaporization of water
  !     *                         and the starting temperature for the
  !     *                         iteration in "TSOLVC"/"TSOLVE".
  !
  use hydcon, only : clhmlt, clhvap
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: jl !<
  !
  !     * output arrays.
  !
  real, intent(inout) :: gcoeffs(ilg) !<
  real, intent(inout) :: gconsts(ilg) !<
  real, intent(inout) :: cphchg(ilg) !<
  !
  integer, intent(inout), dimension(ilg) :: iwater !<
  !
  !     * input arrays.
  !
  real, intent(in) :: fi    (ilg) !<
  real, intent(in) :: zsnow (ilg) !<
  real, intent(in) :: tsnow (ilg) !<
  real, intent(in) :: tcsnow(ilg) !<
  !
  integer :: i !<
  integer :: j !<
  !-----------------------------------------------------------------------
  !     * calculate coefficients.
  !
  do i=il1,il2
    if (fi(i)>0.) then
      gcoeffs(i)=3.0*tcsnow(i)/zsnow(i)
      gconsts(i)=-3.0*tcsnow(i)*tsnow(i)/zsnow(i)
      iwater(i)=2
      cphchg(i)=clhvap+clhmlt
    end if
  end do ! loop 100
  !
  return
end subroutine tsprep
