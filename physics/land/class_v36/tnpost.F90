subroutine tnpost(tbarpr,g12,g23,tpond,gzero,qfrezg,gconst, &
                        gcoeff,tbar,tctop,tcbot,hcp,zpond,tsurf, &
                        tbase,tbar1p,a1,a2,b1,b2,c2,fi,iwater, &
                        isand,delz,delzw,ilg,il1,il2,jl,ig)
  !
  !     * nov 01/06 - d.verseghy. allow ponding on ice sheets.
  !     * oct 04/05 - d.verseghy. modify 300 loop for cases where ig>3.
  !     * nov 04/04 - d.verseghy. add "IMPLICIT NONE" command.
  !     * jun 17/02 - d.verseghy. reset ponded water temperature
  !     *                         using calculated ground heat flux
  !     *                         shortened class4 common block.
  !     * jun 20/97 - d.verseghy. class - version 2.7.
  !     *                         incorporate explicitly calculated
  !     *                         thermal conductivities at tops and
  !     *                         bottoms of soil layers, and
  !     *                         modifications to allow for variable
  !     *                         soil permeable depth.
  !     * sep 27/96 - d.verseghy. class - version 2.5.
  !     *                         fix bug in calculation of fluxes
  !     *                         between soil layers (present since
  !     *                         first release of version 2.5).
  !     * dec 22/94 - d.verseghy. class - version 2.3.
  !     *                         revise calculation of tbarpr(i,1).
  !     * apr 10/92 - m.lazare.   class - version 2.2.
  !     *                         divide previous subroutine "T3LAYR" into
  !     *                         "TNPREP" and "TNPOST" and vectorize.
  !     * apr 11/89 - d.verseghy. calculate heat fluxes between soil
  !     *                         layers; disaggregate first soil layer
  !     *                         temperature into ponded water and
  !     *                         soil temperatures; consistency check
  !     *                         on calculated surface latent heat of
  !     *                         melting/freezing; convert soil layer
  !     *                         temperatures to degrees c.
  !
  use hydcon, only : tfrez, hcpw, hcpsnd
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: jl !<
  integer, intent(in) :: ig !<
  !
  !     * output arrays.
  !
  real, intent(inout), dimension(ilg,ig) :: tbarpr !<
  !
  real, intent(inout) :: g12   (ilg) !<
  real, intent(inout) :: g23   (ilg) !<
  real, intent(inout) :: tpond (ilg) !<
  !
  !     * input/output arrays.
  !
  real, intent(inout) :: gzero (ilg) !<
  real, intent(inout) :: qfrezg(ilg) !<
  !
  !     * input arrays.
  !
  real, intent(in) :: tbar  (ilg,ig) !<
  real, intent(in) :: tctop (ilg,ig) !<
  real, intent(in) :: tcbot (ilg,ig) !<
  real, intent(in) :: hcp   (ilg,ig) !<
  real, intent(in) :: delzw (ilg,ig) !<
  !
  real, intent(in) :: zpond (ilg) !<
  real, intent(in) :: tsurf (ilg) !<
  real, intent(in) :: tbase (ilg) !<
  real, intent(in) :: tbar1p(ilg) !<
  real, intent(in) :: a1    (ilg) !<
  real, intent(in) :: a2    (ilg) !<
  real, intent(in) :: b1    (ilg) !<
  real, intent(in) :: b2    (ilg) !<
  real, intent(in) :: c2    (ilg) !<
  real, intent(in) :: fi    (ilg) !<
  real, intent(in) :: gconst(ilg) !<
  real, intent(in) :: gcoeff(ilg) !<
  !
  integer, intent(in)              :: iwater(ilg) !<
  integer, intent(in)              :: isand (ilg,ig) !<
  !
  real, intent(in), dimension(ig) :: delz !<
  !
  !     * temporary variables.
  !
  integer :: i !<
  integer :: j !<
  real :: gzrold !<
  real :: delz1 !<
  !-----------------------------------------------------------------------
  !
  do i=il1,il2
    if (fi(i)>0.) then
      gzrold=gcoeff(i)*tsurf(i)+gconst(i)
      g12(i)=(tsurf(i)-tbar1p(i)-a1(i)*gzrold)/b1(i)
      g23(i)=(tsurf(i)-tbar(i,2)-a2(i)*gzrold-b2(i)*g12(i))/ &
                c2(i)
      if (zpond(i)>0.) then
        delz1=delz(1)+zpond(i)
        tpond(i)=(gzrold/tctop(i,1)-g12(i)/tcbot(i,1))* &
                      (zpond(i)*zpond(i)-delz1*delz1)/(6.0*delz1)- &
                      gzrold*(zpond(i)-delz1)/(2.0*tctop(i,1))+ &
                      tbar1p(i)-tfrez
        tbarpr(i,1)=((hcp(i,1)*delzw(i,1)+hcpsnd*(delz(1)- &
                         delzw(i,1))+hcpw*zpond(i))*tbar1p(i)- &
                         hcpw*zpond(i)*(tpond(i)+tfrez))/ &
                         (hcp(i,1)*delzw(i,1)+hcpsnd*(delz(1)- &
                         delzw(i,1)))-tfrez
      else
        tpond(i)=0.
        tbarpr(i,1)=tbar(i,1)-tfrez
      end if
      !
      if ((iwater(i)==1 .and. qfrezg(i)>0.) .or. &
          (iwater(i)==2 .and. qfrezg(i)<0.) .or. &
          iwater(i)==0) then
        gzero(i)=gzero(i)+qfrezg(i)
        qfrezg(i)=0.
      end if
    end if
  end do ! loop 100
  !
  do i=il1,il2
    if (fi(i)>0.) then
      tbarpr(i,2)=tbar(i,2)-tfrez
      if (delzw(i,3)>0.0 .and. delzw(i,3)<delz(3) &
          .and. ig==3) then
        tbarpr(i,3)=(tbar(i,3)*(hcp(i,3)*delzw(i,3)+ &
                          hcpsnd*(delz(3)-delzw(i,3)))-tbase(i)* &
                          hcpsnd*(delz(3)-delzw(i,3)))/(hcp(i,3)* &
                          delzw(i,3))-tfrez
      else
        do j=3,ig
          tbarpr(i,j)=tbar(i,j)-tfrez
        end do ! loop 150
      end if
    end if
  end do ! loop 200
  !
  return
end subroutine tnpost
