!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
function lenstr(input_string)

  !     * purpose: returns the number of characters in the input string.
  !
  !     * modification:
  !
  !     * 10/15/98 by scott tinis
  !     * file created.

  implicit none

  integer :: i
  character, intent(in) :: input_string * ( * )   !< INPUT STRING FOR PARSING\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  integer   :: lenstr ! function


  do i = len( input_string), 1, - 1
    if (input_string(i:i) /= ' ') then
      lenstr = i
      go to 999
    end if
  end do

  lenstr = 0

999 return

end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
