!> \file pam_driver.F90
!> \brief CanAM interface to the PLA/PAM aerosol scheme.
!!
!! @author Routine author name(s)
!
 module pam_driver_mod

 implicit none

 public :: pam_driver

 contains
 subroutine pam_driver(pa, xrow, throw, qrow, hrow, pressg, rhc,           &
                       cdml, cdmnl, fc, fg, fcs, fgs, bsfrac, smfrac,      &
                       fwatrow, fcanrow, fnrol, gtrowbs, ustarbs,          &
                       zspd, zspdso, zspdsbs, zh, zf, ph, pf, dp,          &
                       qtn, hmn, depbrol, wg, wsub, zmratep, zmlwc,        &
                       zfsnow, zfrain, zclf, clrfr, clrfs, zfevap, zfsubl, &
                       atau, sgpp, ogpp, docem1, docem2, docem3, docem4,   &
                       dbcem1, dbcem2, dbcem3, dbcem4, dsuem1, dsuem2,     &
                       dsuem3, dsuem4, o3phs, hno3phs, nh3phs, nh4phs,     &
                       il1, il2, ilg, kount, dtadv, saverad, sicn_crt)
!
  use agcm_types_mod,   only: phys_arrays_type
  use agcm_types_mod,   only: phys_options
  use agcm_types_mod,   only: phys_diag
  use psizes_19,        only: ilev, ntrac, ican, im
  use phys_consts,      only: grav, rgas, sbc
  use tracers_info_mod, only: modl_tracers, iso2, ihpo, igs6, igsp, ntracp, iaindt
  use radcons_mod,      only: co2_ppm
  use compar,           only: isdiag, isdust, isdnum

  use pamdiag_mod,      only: pamdiag
  use pamdriv_mod,      only: pamdriv
  implicit none

  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: kount  !< Current model timestep \f$[unitless]\f$
  real,    intent(in) :: dtadv   !< Advective timestep for atmospheric model \f$[seconds]\f$
  real,    intent(in) :: saverad !<
  real,    intent(in) :: sicn_crt !<

  type(phys_arrays_type), intent(inout), target    :: pa

  real, intent(inout),   dimension(ilg,ilev,ntrac) :: xrow !< Tracer mixing ratio\f$[units]\f$
  real, intent(in),    dimension(ilg,ilev) :: qrow  !< Physics internal field for specific humidity \f$[kg kg^{-1}]\f$
  real, intent(in),    dimension(ilg,ilev) :: throw !< Physics internal field for temperature \f$[K]\f$
  real, intent(in),    dimension(ilg)      :: pressg !< Surface pressure \f$[Pa]\f$
  !
  real, intent(in),    dimension(ilg)      :: cdml   !< Variable description\f$[units]\f$
  real, intent(in),    dimension(ilg)      :: cdmnl  !< Variable description\f$[units]\f$

  real, intent(in),   dimension(ilg,ican+1) :: fcanrow !<
  real, intent(in),   dimension(ilg)        :: fc !<
  real, intent(in),   dimension(ilg)        :: fg !<
  real, intent(in),   dimension(ilg)        :: fcs !<
  real, intent(in),   dimension(ilg)        :: fgs !<
  real, intent(in),   dimension(ilg)        :: fwatrow !<
  real, intent(in),   dimension(ilg)        :: bsfrac !<
  real, intent(in),   dimension(ilg)        :: smfrac !<
  real, intent(in),   dimension(ilg)        :: gtrowbs !<
  real, intent(in),   dimension(ilg)        :: ustarbs !<
  real, intent(in),   dimension(ilg)        :: fnrol !<
  real, intent(in),   dimension(ilg)        :: zspdso !<
  real, intent(in),   dimension(ilg)        :: zspdsbs !<
  real, intent(in),   dimension(ilg)        :: zspd   !< Variable description\f$[units]\f$

  real, intent(in),   dimension(ilg,ilev)   :: pf !<
  real, intent(in),   dimension(ilg,ilev)   :: zf !<
  real, intent(in),   dimension(ilg,ilev)   :: ph !<
  real, intent(in),   dimension(ilg,ilev)   :: zh !<
  real, intent(in),   dimension(ilg,ilev)   :: dp !<
  real, intent(in),   dimension(ilg,ilev)   :: qtn !<
  real, intent(in),   dimension(ilg,ilev)   :: hmn !<
  real, intent(in),   dimension(ilg,ilev)   :: rhc !<
  real, intent(in),   dimension(ilg,ilev)   :: clrfr !<
  real, intent(in),   dimension(ilg,ilev)   :: clrfs !<
  real, intent(in),   dimension(ilg,ilev)   :: zfrain !<
  real, intent(in),   dimension(ilg,ilev)   :: zfsubl !<
  real, intent(in),   dimension(ilg,ilev)   :: zfevap !<
  real, intent(in),   dimension(ilg,ilev)   :: zclf !<
  real, intent(in),   dimension(ilg,ilev)   :: zmratep !<
  real, intent(in),   dimension(ilg,ilev)   :: zfsnow !<
  real, intent(in),   dimension(ilg,ilev)   :: zmlwc !<
  real, intent(inout),dimension(ilg)        :: depbrol !<
  real, intent(in),   dimension(ilg,ilev)   :: wg !< Variable description\f$[units]\f$
  real, intent(in),   dimension(ilg,ilev)   :: wsub !< Variable description\f$[units]\f$
  real, intent(in),   dimension(ilg,ilev)   :: hrow !<
  real, intent(in),   dimension(ilg)        :: atau !< Aging time scale of hydrophobic black carbon \f$[s]\f$
  real, intent(in),   dimension(ilg,ilev)   :: ogpp !<
  real, intent(in),   dimension(ilg,ilev)   :: sgpp !< sulphuric acid (gas) chemical production or emission rate \f$[kg of sulphur/kg/sec]\f$
  real, intent(in),   dimension(ilg,ilev)   :: docem1 !< Organic aerosol emission rate for open vegetation and biofuel burning \f$[kg/kg/sec]\f$
  real, intent(in),   dimension(ilg,ilev)   :: docem2 !< Organic aerosol emission rate for fossil fuel burning \f$[kg/kg/sec]\f$
  real, intent(in),   dimension(ilg,ilev)   :: docem3 !< Organic aerosol emission rate for aircraft \f$[kg/kg/sec]\f$
  real, intent(in),   dimension(ilg,ilev)   :: docem4 !< Organic aerosol emission rate for shipping \f$[kg/kg/sec]\f$
  real, intent(in),   dimension(ilg,ilev)   :: dbcem1 !< Black carbon emission rate for open vegetation and biofuel burning \f$[kg/kg/sec]\f$
  real, intent(in),   dimension(ilg,ilev)   :: dbcem2 !< Black carbon emission rate for fossil fuel burning \f$[kg/kg/sec]\f$
  real, intent(in),   dimension(ilg,ilev)   :: dbcem3 !< Black carbon emission rate for aircraft \f$[kg/kg/sec]\f$
  real, intent(in),   dimension(ilg,ilev)   :: dbcem4 !< Black carbon emission rate for shipping \f$[kg/kg/sec]\f$
  real, intent(in),   dimension(ilg,ilev)   :: dsuem1 !< Ammonium sulphate emission rate for open vegetation and biofuel burning \f$[kg/kg/sec]\f$
  real, intent(in),   dimension(ilg,ilev)   :: dsuem2 !<
  real, intent(in),   dimension(ilg,ilev)   :: dsuem3 !< Ammonium git sulphate emission rate for aircraft \f$[kg/kg/sec]\f$
  real, intent(in),   dimension(ilg,ilev)   :: dsuem4 !< Ammonium sulphate emission rate for shipping \f$[kg/kg/sec]\f$
  real, intent(in),   dimension(ilg,ilev)   :: o3phs
  real, intent(in),   dimension(ilg,ilev)   :: hno3phs
  real, intent(in),   dimension(ilg,ilev)   :: nh3phs
  real, intent(in),   dimension(ilg,ilev)   :: nh4phs

  logical :: rtstep
  logical :: radfrc

  integer :: ilga
  integer :: leva
  !     * processor ID
  integer*4 :: mynode

  real,   dimension(ilg)   :: zfsh !< height of surface (bottom of atmosphere) (m)
  real,   dimension(ntrac) :: tmin !< Minimum tracers concentration \f$[s]\f$
  !
  common /mpinfo/ mynode
  !
  ! pamdriv extrals
  real, dimension(ilg,ilev) :: cornrox
  real, dimension(ilg,ilev) :: cormrox
  real, dimension(ilg,ilev) :: rsn1rox
  real, dimension(ilg,ilev) :: rsm1rox
  real, dimension(ilg,ilev) :: rsn2rox
  real, dimension(ilg,ilev) :: rsm2rox
  real, dimension(ilg,ilev) :: rsn3rox
  real, dimension(ilg,ilev) :: rsm3rox
  real, dimension(ilg)      :: vrn1rox
  real, dimension(ilg)      :: vrm1rox
  real, dimension(ilg)      :: vrn2rox
  real, dimension(ilg)      :: vrm2rox
  real, dimension(ilg)      :: vrn3rox
  real, dimension(ilg)      :: vrm3rox
  real, dimension(ilg)      :: vncnrox
  real, dimension(ilg)      :: vasnrox
  real, dimension(ilg)      :: vscdrox
  real, dimension(ilg)      :: vgsprox
  real, dimension(ilg,ilev) :: sncnrox
  real, dimension(ilg,ilev) :: ssunrox
  real, dimension(ilg,ilev) :: scndrox
  real, dimension(ilg,ilev) :: sgsprox
  real, dimension(ilg,ilev) :: ccnrox
  real, dimension(ilg,ilev) :: ccnerox
  real, dimension(ilg,ilev) :: cc02rox
  real, dimension(ilg,ilev) :: cc04rox
  real, dimension(ilg,ilev) :: cc08rox
  real, dimension(ilg,ilev) :: cc16rox
  real, dimension(ilg,ilev) :: rcrirox
  real, dimension(ilg,ilev) :: supsrox
  real, dimension(ilg,ilev) :: henrrox
  real, dimension(ilg,ilev) :: o3frox
  real, dimension(ilg,ilev) :: wparrox
  real, dimension(ilg,ilev) :: ntrox
  real, dimension(ilg,ilev) :: n20rox
  real, dimension(ilg,ilev) :: n50rox
  real, dimension(ilg,ilev) :: n100rox
  real, dimension(ilg,ilev) :: n200rox
  real, dimension(ilg,ilev) :: wtrox
  real, dimension(ilg,ilev) :: w20rox
  real, dimension(ilg,ilev) :: w50rox
  real, dimension(ilg,ilev) :: w100rox
  real, dimension(ilg,ilev) :: w200rox
  real, dimension(ilg,ilev) :: acasrox
  real, dimension(ilg,ilev) :: acoarox
  real, dimension(ilg,ilev) :: acbcrox
  real, dimension(ilg,ilev) :: acssrox
  real, dimension(ilg,ilev) :: acmdrox
  real, dimension(ilg,ilev) :: h2o2frox
  real, dimension(ilg,isdiag) :: defxrox
  real, dimension(ilg,isdiag) :: defnrox
  real, dimension(ilg)        :: voaerox
  real, dimension(ilg)        :: vbcerox
  real, dimension(ilg)        :: vaserox
  real, dimension(ilg)        :: vmderox
  real, dimension(ilg)        :: vsserox
  real, dimension(ilg)        :: voawrox
  real, dimension(ilg)        :: vbcwrox
  real, dimension(ilg)        :: vaswrox
  real, dimension(ilg)        :: vmdwrox
  real, dimension(ilg)        :: vsswrox
  real, dimension(ilg)        :: voadrox
  real, dimension(ilg)        :: vbcdrox
  real, dimension(ilg)        :: vasdrox
  real, dimension(ilg)        :: vmddrox
  real, dimension(ilg)        :: vssdrox
  real, dimension(ilg)        :: voagrox
  real, dimension(ilg)        :: vbcgrox
  real, dimension(ilg)        :: vasgrox
  real, dimension(ilg)        :: vmdgrox
  real, dimension(ilg)        :: vssgrox
  real, dimension(ilg)        :: vasirox
  real, dimension(ilg)        :: vas1rox
  real, dimension(ilg)        :: vas2rox
  real, dimension(ilg)        :: vas3rox
  real, dimension(ilg)        :: vccnrox
  real, dimension(ilg)        :: vcnerox
  real, dimension(ilg,isdust) :: dmacrox
  real, dimension(ilg,isdust) :: dmcorox
  real, dimension(ilg,isdust) :: dnacrox
  real, dimension(ilg,isdust) :: dncorox
  real, dimension(ilg)        :: duwdrox
  real, dimension(ilg)        :: dustrox
  real, dimension(ilg)        :: duthrox
  real, dimension(ilg)        :: fallrox
  real, dimension(ilg)        :: fa10rox
  real, dimension(ilg)        :: fa2rox
  real, dimension(ilg)        :: fa1rox
  real, dimension(ilg)        :: usmkrox
  real, dimension(ilg)        :: defarox
  real, dimension(ilg)        :: defcrox
  !
  ! pamdiag extrals
  real, dimension(ilg,isdiag) :: sdvlrox
  real, dimension(ilg,isdnum) :: snssrox
  real, dimension(ilg,isdnum) :: snmdrox
  real, dimension(ilg,isdnum) :: sniarox
  real, dimension(ilg,isdnum) :: smssrox
  real, dimension(ilg,isdnum) :: smmdrox
  real, dimension(ilg,isdnum) :: smocrox
  real, dimension(ilg,isdnum) :: smbcrox
  real, dimension(ilg,isdnum) :: smsprox
  real, dimension(ilg,isdnum) :: siwhrox
  real, dimension(ilg,isdnum) :: sewhrox
  real, dimension(ilg,ilev,isdnum) :: sdnurox
  real, dimension(ilg,ilev,isdnum) :: sdmarox
  real, dimension(ilg,ilev,isdnum) :: sdacrox
  real, dimension(ilg,ilev,isdnum) :: sdcorox
  real, dimension(ilg,ilev,isdnum) :: sssnrox
  real, dimension(ilg,ilev,isdnum) :: smdnrox
  real, dimension(ilg,ilev,isdnum) :: sianrox
  real, dimension(ilg,ilev,isdnum) :: sssmrox
  real, dimension(ilg,ilev,isdnum) :: smdmrox
  real, dimension(ilg,ilev,isdnum) :: sewmrox
  real, dimension(ilg,ilev,isdnum) :: ssumrox
  real, dimension(ilg,ilev,isdnum) :: socmrox
  real, dimension(ilg,ilev,isdnum) :: sbcmrox
  real, dimension(ilg,ilev,isdnum) :: siwmrox
  real, dimension(ilg)             :: tnssrox
  real, dimension(ilg)             :: tnmdrox
  real, dimension(ilg)             :: tniarox
  real, dimension(ilg)             :: tmssrox
  real, dimension(ilg)             :: tmmdrox
  real, dimension(ilg)             :: tmocrox
  real, dimension(ilg)             :: tmbcrox
  real, dimension(ilg)             :: tmsprox
  real, dimension(ilg,ilev)        :: pm25rox
  real, dimension(ilg,ilev)        :: pm10rox
  real, dimension(ilg,ilev)        :: dm25rox
  real, dimension(ilg,ilev)        :: dm10rox

  zfsh(il1:il2) = 0.
  leva = 22
  ilga = il2 - il1 + 1
  tmin(:) = modl_tracers(:) % tmin

  call pamdriv(pa%oednrow, pa%oercrow, pa%oidnrow, &
               pa%oircrow, pa%svvbrow, &
               pa%psvvrow, pa%svmbrow, pa%svcbrow, &
               pa%devbrow, pa%pdevrow, pa%dembrow, pa%decbrow, &
               pa%divbrow, pa%pdivrow, pa%dimbrow, &
               pa%dicbrow, pa%revbrow, &
               pa%prevrow, pa%rembrow, pa%recbrow, &
               pa%rivbrow, pa%privrow, pa%rimbrow, pa%ricbrow, &
               xrow, pa%sfrcrol, iaindt, ilg, ntrac, &
               il1, il2, iso2, ihpo, igs6, igsp, ntracp, &
               ilev, ican, zh, zf, ph, pf, dp, throw, qrow, &
               qtn, hmn, rhc, hrow, wg, wsub, zmratep, zmlwc, zfsnow, &
               zfrain, zclf, clrfr, clrfs, zfevap, zfsubl, &
               pa%pbltrow, smfrac, fcs, fgs, fc, fg, &
               zspd, zspdso, zspdsbs, cdml, cdmnl, ustarbs, &
               pa%flndrow, fwatrow, pa%sicnrow, gtrowbs, fnrol, fcanrow, &
               pa%spotrow, pa%st02row, pa%st03row, pa%st04row, &
               pa%st06row, pa%st13row, pa%st14row, pa%st15row, &
               pa%st16row, pa%st17row, pa%suz0row, bsfrac, &
               pa%pdsfrow, atau, zfsh, pressg, &
               sgpp, ogpp, docem1, docem2, docem3, docem4, dbcem1, dbcem2, dbcem3, dbcem4, &
               dsuem1, dsuem2, dsuem3, dsuem4, o3phs, hno3phs, nh3phs, nh4phs, dtadv, &
               co2_ppm, kount, tmin, pa%zcdnrow, pa%bcicrow, depbrol, &
               cornrox, cormrox, rsn1rox, rsm1rox, vrn1rox, vrm1rox, rsn2rox, rsm2rox, vrn2rox, &
               vrm2rox, rsn3rox, rsm3rox, vrn3rox, vrm3rox, vncnrox, vasnrox, vscdrox, vgsprox, &
               sncnrox, ssunrox, scndrox, sgsprox, defxrox, defnrox, acasrox, acoarox, acbcrox, &
               acssrox, acmdrox, ntrox, n20rox, n50rox, n100rox, n200rox, &
               wtrox, w20rox, w50rox, w100rox, w200rox, &
               ccnrox, ccnerox, cc02rox, cc04rox, cc08rox, cc16rox, rcrirox, supsrox, &
               voaerox, vbcerox, vaserox, vmderox, vsserox, voawrox, vbcwrox, vaswrox, &
               vmdwrox, vsswrox, voadrox, vbcdrox, vasdrox, vmddrox, vssdrox, voagrox, &
               vbcgrox, vasgrox, vmdgrox, vssgrox, vasirox, vas1rox, vas2rox, vas3rox, &
               henrrox, o3frox, h2o2frox, wparrox, vccnrox, vcnerox, dmacrox, dmcorox, &
               dnacrox, dncorox, duwdrox, dustrox, duthrox, fallrox, fa10rox, fa2rox,  &
               fa1rox ,usmkrox, defarox, defcrox, &
               sicn_crt, phys_diag%xtradust, leva, ilga, mynode)

  if (phys_diag%xtrapla1) then
     pa%vncnrow(il1:il2)    = pa%vncnrow(il1:il2)   +   vncnrox(il1:il2) * saverad
     pa%vasnrow(il1:il2)    = pa%vasnrow(il1:il2)   +   vasnrox(il1:il2) * saverad
     pa%vscdrow(il1:il2)    = pa%vscdrow(il1:il2)   +   vscdrox(il1:il2) * saverad
     pa%vgsprow(il1:il2)    = pa%vgsprow(il1:il2)   +   vgsprox(il1:il2) * saverad
     pa%sncnrow(il1:il2, :) = sncnrox(il1:il2, :)
     pa%ssunrow(il1:il2, :) = pa%ssunrow(il1:il2, :)   +   ssunrox(il1:il2, :) * saverad
     pa%scndrow(il1:il2, :) = scndrox(il1:il2, :)
     pa%sgsprow(il1:il2, :) = sgsprox(il1:il2, :)
     pa%acasrow(il1:il2, :) = pa%acasrow(il1:il2, :)   +   acasrox(il1:il2, :) * saverad
     pa%acoarow(il1:il2, :) = pa%acoarow(il1:il2, :)   +   acoarox(il1:il2, :) * saverad
     pa%acbcrow(il1:il2, :) = pa%acbcrow(il1:il2, :)   +   acbcrox(il1:il2, :) * saverad
     pa%acssrow(il1:il2, :) = pa%acssrow(il1:il2, :)   +   acssrox(il1:il2, :) * saverad
     pa%acmdrow(il1:il2, :) = pa%acmdrow(il1:il2, :)   +   acmdrox(il1:il2, :) * saverad
     pa%ntrow  (il1:il2, :) = pa%ntrow  (il1:il2, :)   +     ntrox(il1:il2, :) * saverad
     pa%n20row (il1:il2, :) = pa%n20row (il1:il2, :)   +    n20rox(il1:il2, :) * saverad
     pa%n50row (il1:il2, :) = pa%n50row (il1:il2, :)   +    n50rox(il1:il2, :) * saverad
     pa%n100row(il1:il2, :) = pa%n100row(il1:il2, :)   +   n100rox(il1:il2, :) * saverad
     pa%n200row(il1:il2, :) = pa%n200row(il1:il2, :)   +   n200rox(il1:il2, :) * saverad
     pa%wtrow  (il1:il2, :) = pa%wtrow  (il1:il2, :)   +     wtrox(il1:il2, :) * saverad
     pa%w20row (il1:il2, :) = pa%w20row (il1:il2, :)   +    w20rox(il1:il2, :) * saverad
     pa%w50row (il1:il2, :) = pa%w50row (il1:il2, :)   +    w50rox(il1:il2, :) * saverad
     pa%w100row(il1:il2, :) = pa%w100row(il1:il2, :)   +   w100rox(il1:il2, :) * saverad
     pa%w200row(il1:il2, :) = pa%w200row(il1:il2, :)   +   w200rox(il1:il2, :) * saverad
     pa%ccnrow (il1:il2, :) = ccnrox    (il1:il2, :)
     pa%ccnerow(il1:il2, :) = ccnerox   (il1:il2, :)
     pa%cc02row(il1:il2, :) = pa%cc02row(il1:il2, :)   +   cc02rox(il1:il2, :) * saverad
     pa%cc04row(il1:il2, :) = pa%cc04row(il1:il2, :)   +   cc04rox(il1:il2, :) * saverad
     pa%cc08row(il1:il2, :) = pa%cc08row(il1:il2, :)   +   cc08rox(il1:il2, :) * saverad
     pa%cc16row(il1:il2, :) = pa%cc16row(il1:il2, :)   +   cc16rox(il1:il2, :) * saverad
     pa%rcrirow(il1:il2, :) = rcrirox   (il1:il2, :)
     pa%supsrow(il1:il2, :) = supsrox   (il1:il2, :)
     pa%voaerow(il1:il2)    = pa%voaerow(il1:il2)   +   voaerox(il1:il2) * saverad
     pa%vbcerow(il1:il2)    = pa%vbcerow(il1:il2)   +   vbcerox(il1:il2) * saverad
     pa%vaserow(il1:il2)    = pa%vaserow(il1:il2)   +   vaserox(il1:il2) * saverad
     pa%vmderow(il1:il2)    = pa%vmderow(il1:il2)   +   vmderox(il1:il2) * saverad
     pa%vsserow(il1:il2)    = pa%vsserow(il1:il2)   +   vsserox(il1:il2) * saverad
     pa%voawrow(il1:il2)    = pa%voawrow(il1:il2)   +   voawrox(il1:il2) * saverad
     pa%vbcwrow(il1:il2)    = pa%vbcwrow(il1:il2)   +   vbcwrox(il1:il2) * saverad
     pa%vaswrow(il1:il2)    = pa%vaswrow(il1:il2)   +   vaswrox(il1:il2) * saverad
     pa%vmdwrow(il1:il2)    = pa%vmdwrow(il1:il2)   +   vmdwrox(il1:il2) * saverad
     pa%vsswrow(il1:il2)    = pa%vsswrow(il1:il2)   +   vsswrox(il1:il2) * saverad
     pa%voadrow(il1:il2)    = pa%voadrow(il1:il2)   +   voadrox(il1:il2) * saverad
     pa%vbcdrow(il1:il2)    = pa%vbcdrow(il1:il2)   +   vbcdrox(il1:il2) * saverad
     pa%vasdrow(il1:il2)    = pa%vasdrow(il1:il2)   +   vasdrox(il1:il2) * saverad
     pa%vmddrow(il1:il2)    = pa%vmddrow(il1:il2)   +   vmddrox(il1:il2) * saverad
     pa%vssdrow(il1:il2)    = pa%vssdrow(il1:il2)   +   vssdrox(il1:il2) * saverad
     pa%voagrow(il1:il2)    = pa%voagrow(il1:il2)   +   voagrox(il1:il2) * saverad
     pa%vbcgrow(il1:il2)    = pa%vbcgrow(il1:il2)   +   vbcgrox(il1:il2) * saverad
     pa%vasgrow(il1:il2)    = pa%vasgrow(il1:il2)   +   vasgrox(il1:il2) * saverad
     pa%vmdgrow(il1:il2)    = pa%vmdgrow(il1:il2)   +   vmdgrox(il1:il2) * saverad
     pa%vssgrow(il1:il2)    = pa%vssgrow(il1:il2)   +   vssgrox(il1:il2) * saverad
     pa%vasirow(il1:il2)    = pa%vasirow(il1:il2)   +   vasirox(il1:il2) * saverad
     pa%vas1row(il1:il2)    = pa%vas1row(il1:il2)   +   vas1rox(il1:il2) * saverad
     pa%vas2row(il1:il2)    = pa%vas2row(il1:il2)   +   vas2rox(il1:il2) * saverad
     pa%vas3row(il1:il2)    = pa%vas3row(il1:il2)   +   vas3rox(il1:il2) * saverad
     pa%henrrow(il1:il2, :) = henrrox(il1:il2, :)
     pa%o3frrow(il1:il2, :) = o3frox(il1:il2, :)
     pa%h2o2frrow(il1:il2, :)  = h2o2frox(il1:il2, :)
     pa%wparrow(il1:il2, :) = wparrox(il1:il2, :)
     pa%vccnrow(il1:il2)    = vccnrox(il1:il2)
     pa%vcnerow(il1:il2)    = vcnerox(il1:il2)
  end if
  if (phys_diag%xtrapla2) then
     pa%cornrow(il1:il2, :) = pa%cornrow(il1:il2, :) + cornrox(il1:il2, :) * saverad
     pa%cormrow(il1:il2, :) = pa%cormrow(il1:il2, :) + cormrox(il1:il2, :) * saverad
     pa%rsn1row(il1:il2, :) = pa%rsn1row(il1:il2, :) + rsn1rox(il1:il2, :) * saverad
     pa%rsm1row(il1:il2, :) = pa%rsm1row(il1:il2, :) + rsm1rox(il1:il2, :) * saverad
     pa%vrn1row(il1:il2)    = pa%vrn1row(il1:il2)    + vrn1rox(il1:il2) * saverad
     pa%vrm1row(il1:il2)    = pa%vrm1row(il1:il2)    + vrm1rox(il1:il2) * saverad
     pa%rsn2row(il1:il2, :) = pa%rsn2row(il1:il2, :) + rsn2rox(il1:il2, :) * saverad
     pa%rsm2row(il1:il2, :) = pa%rsm2row(il1:il2, :) + rsm2rox(il1:il2, :) * saverad
     pa%vrn2row(il1:il2)    = pa%vrn2row(il1:il2)    + vrn2rox(il1:il2) * saverad
     pa%vrm2row(il1:il2)    = pa%vrm2row(il1:il2)    + vrm2rox(il1:il2) * saverad
     pa%rsn3row(il1:il2, :) = pa%rsn3row(il1:il2, :) + rsn3rox(il1:il2, :) * saverad
     pa%rsm3row(il1:il2, :) = pa%rsm3row(il1:il2, :) + rsm3rox(il1:il2, :) * saverad
     pa%vrn3row(il1:il2)    = pa%vrn3row(il1:il2)    + vrn3rox(il1:il2) * saverad
     pa%vrm3row(il1:il2)    = pa%vrm3row(il1:il2)    + vrm3rox(il1:il2) * saverad
  end if

  if (phys_diag%xtradust) then
     pa%dmacrow(il1:il2, :) = pa%dmacrow(il1:il2, :) + dmacrox(il1:il2, :) * saverad
     pa%dmcorow(il1:il2, :) = pa%dmcorow(il1:il2, :) + dmcorox(il1:il2, :) * saverad
     pa%dnacrow(il1:il2, :) = pa%dnacrow(il1:il2, :) + dnacrox(il1:il2, :) * saverad
     pa%dncorow(il1:il2, :) = pa%dncorow(il1:il2, :) + dncorox(il1:il2, :) * saverad
     pa%defxrow(il1:il2, :) = pa%defxrow(il1:il2, :) + defxrox(il1:il2, :) * saverad
     pa%defnrow(il1:il2, :) = pa%defnrow(il1:il2, :) + defnrox(il1:il2, :) * saverad

     pa%duwdrow(il1:il2)    = pa%duwdrow(il1:il2) + duwdrox(il1:il2) * saverad
     pa%dustrow(il1:il2)    = pa%dustrow(il1:il2) + dustrox(il1:il2) * saverad
     pa%duthrow(il1:il2)    = pa%duthrow(il1:il2) + duthrox(il1:il2) * saverad
     pa%fallrow(il1:il2)    = pa%fallrow(il1:il2) + fallrox(il1:il2) * saverad
     pa%fa10row(il1:il2)    = pa%fa10row(il1:il2) + fa10rox(il1:il2) * saverad
     pa%fa2row (il1:il2)    = pa%fa2row (il1:il2) +  fa2rox(il1:il2) * saverad
     pa%fa1row (il1:il2)    = pa%fa1row (il1:il2) +  fa1rox(il1:il2) * saverad
     pa%usmkrow(il1:il2)    = pa%usmkrow(il1:il2) + usmkrox(il1:il2) * saverad
     pa%defarow(il1:il2)    = pa%defarow(il1:il2) + defarox(il1:il2) * saverad
     pa%defcrow(il1:il2)    = pa%defcrow(il1:il2) + defcrox(il1:il2) * saverad
  end if
!
!     * diagnostic calculations for aerosol optical and other
!     * properties.
!
  rtstep = .true. !(.not. (lcsw .or. lclw))
  radfrc = (phys_diag%radforce .and. rtstep)

  ! * pamdiag calculations for full vertical domain.
  leva = ilev
  call pamdiag(xrow, ilg, ntrac, iaindt, il1, il2, ilev, leva, ilga, dp, throw, rhc, ph, &
               pa%cldtrol, pa%cszrol, &
               pa%ssldrow, pa%ressrow, pa%vessrow, pa%dsldrow, &
               pa%redsrow, pa%vedsrow, pa%bcldrow, pa%rebcrow, &
               pa%vebcrow, pa%ocldrow, pa%reocrow, pa%veocrow, &
               pa%amldrow, pa%reamrow, pa%veamrow, pa%fr1row,  &
               pa%fr2row,  pa%sulirow, pa%rsuirow, pa%vsuirow, &
               pa%f1surow, pa%f2surow, pa%bclirow, pa%rbcirow, &
               pa%vbcirow, pa%f1bcrow, pa%f2bcrow, pa%oclirow, &
               pa%rocirow, pa%vocirow, pa%f1ocrow, pa%f2ocrow, &
               sdvlrox, sdnurox, sdmarox, sdacrox, sdcorox, sssnrox, smdnrox, sianrox, &
               sssmrox, smdmrox, sewmrox, ssumrox, socmrox, sbcmrox, siwmrox, tnssrox, &
               tnmdrox, tniarox, tmssrox, tmmdrox, tmocrox, tmbcrox, tmsprox, snssrox, &
               snmdrox, sniarox, smssrox, smmdrox, smocrox, smbcrox, smsprox, siwhrox, &
               sewhrox, pm25rox, pm10rox, dm25rox, dm10rox, &
               rtstep, radfrc)

  if (phys_diag%xtrapla1) then
     pa%pm25row(il1:il2, :) = pa%pm25row(il1:il2, :) + pm25rox(il1:il2, :) * saverad
     pa%pm10row(il1:il2, :) = pa%pm10row(il1:il2, :) + pm10rox(il1:il2, :) * saverad
     pa%dm25row(il1:il2, :) = pa%dm25row(il1:il2, :) + dm25rox(il1:il2, :) * saverad
     pa%dm10row(il1:il2, :) = pa%dm10row(il1:il2, :) + dm10rox(il1:il2, :) * saverad
  end if

  if (phys_diag%xtrapla2) then
     pa%sdnurow(il1:il2, :, :) = pa%sdnurow(il1:il2, :, :) + sdnurox(il1:il2, :, :) * saverad
     pa%sdmarow(il1:il2, :, :) = pa%sdmarow(il1:il2, :, :) + sdmarox(il1:il2, :, :) * saverad
     pa%sdacrow(il1:il2, :, :) = pa%sdacrow(il1:il2, :, :) + sdacrox(il1:il2, :, :) * saverad
     pa%sdcorow(il1:il2, :, :) = pa%sdcorow(il1:il2, :, :) + sdcorox(il1:il2, :, :) * saverad
     pa%sssnrow(il1:il2, :, :) = pa%sssnrow(il1:il2, :, :) + sssnrox(il1:il2, :, :) * saverad
     pa%smdnrow(il1:il2, :, :) = pa%smdnrow(il1:il2, :, :) + smdnrox(il1:il2, :, :) * saverad
     pa%sianrow(il1:il2, :, :) = pa%sianrow(il1:il2, :, :) + sianrox(il1:il2, :, :) * saverad
     pa%sssmrow(il1:il2, :, :) = pa%sssmrow(il1:il2, :, :) + sssmrox(il1:il2, :, :) * saverad
     pa%smdmrow(il1:il2, :, :) = pa%smdmrow(il1:il2, :, :) + smdmrox(il1:il2, :, :) * saverad
     pa%sewmrow(il1:il2, :, :) = pa%sewmrow(il1:il2, :, :) + sewmrox(il1:il2, :, :) * saverad
     pa%ssumrow(il1:il2, :, :) = pa%ssumrow(il1:il2, :, :) + ssumrox(il1:il2, :, :) * saverad
     pa%socmrow(il1:il2, :, :) = pa%socmrow(il1:il2, :, :) + socmrox(il1:il2, :, :) * saverad
     pa%sbcmrow(il1:il2, :, :) = pa%sbcmrow(il1:il2, :, :) + sbcmrox(il1:il2, :, :) * saverad
     pa%siwmrow(il1:il2, :, :) = pa%siwmrow(il1:il2, :, :) + siwmrox(il1:il2, :, :) * saverad

     pa%sdvlrow(il1:il2, :) = sdvlrox(il1:il2, :)

     pa%tnssrow(il1:il2)    = pa%tnssrow(il1:il2) + tnssrox(il1:il2) * saverad
     pa%tnmdrow(il1:il2)    = pa%tnmdrow(il1:il2) + tnmdrox(il1:il2) * saverad
     pa%tniarow(il1:il2)    = pa%tniarow(il1:il2) + tniarox(il1:il2) * saverad
     pa%tmssrow(il1:il2)    = pa%tmssrow(il1:il2) + tmssrox(il1:il2) * saverad
     pa%tmmdrow(il1:il2)    = pa%tmmdrow(il1:il2) + tmmdrox(il1:il2) * saverad
     pa%tmocrow(il1:il2)    = pa%tmocrow(il1:il2) + tmocrox(il1:il2) * saverad
     pa%tmbcrow(il1:il2)    = pa%tmbcrow(il1:il2) + tmbcrox(il1:il2) * saverad
     pa%tmsprow(il1:il2)    = pa%tmsprow(il1:il2) + tmsprox(il1:il2) * saverad
     pa%snssrow(il1:il2, :) = pa%snssrow(il1:il2, :) + snssrox(il1:il2, :) * saverad
     pa%snmdrow(il1:il2, :) = pa%snmdrow(il1:il2, :) + snmdrox(il1:il2, :) * saverad
     pa%sniarow(il1:il2, :) = pa%sniarow(il1:il2, :) + sniarox(il1:il2, :) * saverad
     pa%smssrow(il1:il2, :) = pa%smssrow(il1:il2, :) + smssrox(il1:il2, :) * saverad
     pa%smmdrow(il1:il2, :) = pa%smmdrow(il1:il2, :) + smmdrox(il1:il2, :) * saverad
     pa%smocrow(il1:il2, :) = pa%smocrow(il1:il2, :) + smocrox(il1:il2, :) * saverad
     pa%smbcrow(il1:il2, :) = pa%smbcrow(il1:il2, :) + smbcrox(il1:il2, :) * saverad
     pa%smsprow(il1:il2, :) = pa%smsprow(il1:il2, :) + smsprox(il1:il2, :) * saverad
     pa%siwhrow(il1:il2, :) = pa%siwhrow(il1:il2, :) + siwhrox(il1:il2, :) * saverad
     pa%sewhrow(il1:il2, :) = pa%sewhrow(il1:il2, :) + sewhrox(il1:il2, :) * saverad
  end if

  return
  end subroutine pam_driver

 end module pam_driver_mod
