!>\file condensation.F90
!>\brief Cloud microphysics
!!
!! @author
!
 module condensation_mod

 implicit none

 public :: condensation

 contains
 subroutine condensation(pa, throw, qrow, xrow, pressg, tfrow, hrow, zclf,             &
                         qlwc, qiwc, qc, rhc, dshj, shj, shtj, dpog, snow, rainls,     &
                         qcwvar, zfevap, zmratep, zfsnow, zmlwc, zfrain, zfsubl, zcdn, &
                         clrfr, clrfs, rmixrol, smixrol, rrefrol, srefrol, qtn, hmn,   &
                         il1, il2, lev, dtadv, saverad)
  use agcm_types_mod, only : phys_arrays_type
  use agcm_types_mod, only : phys_options
  use agcm_types_mod, only : phys_diag
  use psizes_19,      only : ilg, ilev, ntrac

  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: lev  !< Number of vertical levels plus 1 \f$[unitless]\f$
  real, intent(in) :: dtadv  !< Timestep for atmospheric model \f$[seconds]\f$
  real, intent(in) :: saverad  !< Reciprocal of timestep interval to save radiative transfer output \f$[unitless]\f$

  type(phys_arrays_type), intent(inout)          :: pa

  real, intent(inout), dimension(ilg,ilev,ntrac) :: xrow    !< Tracers concentration
  real, intent(in)   , dimension(ilg)            :: pressg  !< Surface pressure \f$[Pa]\f$
  real, intent(inout), dimension(ilg,ilev)       :: throw !< Temperature at mid-layer.
  real, intent(inout), dimension(ilg,ilev)       :: qrow !< Specific humidity
  real, intent(out)  , dimension(ilg,ilev)       :: hrow !<
  real, intent(in)   , dimension(ilg,ilev+1)     :: tfrow !< Temperature at interface.
  real, intent(out)  , dimension(ilg,ilev)       :: qiwc !<
  real, intent(out)  , dimension(ilg,ilev)       :: qlwc !<
  real, intent(out)  , dimension(ilg,ilev)       :: zclf !<
  real, intent(out)  , dimension(ilg,ilev)       :: zcdn !<
  real, intent(out)  , dimension(ilg)            :: snow !<
  real, intent(out)  , dimension(ilg)            :: rainls !<
  real, intent(out)  , dimension(ilg,ilev)       :: qc !<
  real, intent(out)  , dimension(ilg,ilev)       :: rhc !<
  real, intent(out)  , dimension(ilg,ilev)       :: clrfr !<
  real, intent(out)  , dimension(ilg,ilev)       :: clrfs !<
  real, intent(out)  , dimension(ilg,ilev)       :: qcwvar !<
  real, intent(out)  , dimension(ilg,ilev)       :: zfevap !<
  real, intent(out)  , dimension(ilg,ilev)       :: zfrain !<
  real, intent(out)  , dimension(ilg,ilev)       :: zfsubl !<
  real, intent(out)  , dimension(ilg,ilev)       :: zfsnow !<
  real, intent(out)  , dimension(ilg,ilev)       :: zmlwc !<
  real, intent(out)  , dimension(ilg,ilev)       :: zmratep !<
  real, intent(out)  , dimension(ilg,ilev)       :: rmixrol !<
  real, intent(out)  , dimension(ilg,ilev)       :: smixrol !<
  real, intent(out)  , dimension(ilg,ilev)       :: rrefrol !<
  real, intent(out)  , dimension(ilg,ilev)       :: srefrol !<
  real, intent(out)  , dimension(ilg,ilev)       :: qtn !<
  real, intent(out)  , dimension(ilg,ilev)       :: hmn !<
  !
  !     * vertical levelling information.
  !
  real, intent(in),    dimension(ilg,ilev+1)     :: shtj !< Eta-level for top of thermodynamic layer, plus eta-level for surface (base of lowest layer) \f$[unitless]\f$
  real, intent(in),    dimension(ilg,ilev)       :: shj  !< Eta-level for mid-point of thermodynamic layer \f$[unitless]\f$
  real, intent(in),    dimension(ilg,ilev)       :: dshj !< Thickness of thermodynamic layers in eta coordinates \f$[unitless]\f$
  real, intent(in),    dimension(ilg,ilev)       :: dpog !< Mass of air in grid cell area \f$[kg/m2]\f$

  real,   dimension(ilg,ilev)         :: dqldt !<
!
!     * internal workspace for optional cloud microphysics output:
!       (xtrals CPP definition)
!
  real,   dimension(ilg,ilev)         :: aggrox !<
  real,   dimension(ilg,ilev)         :: autrox !<
  real,   dimension(ilg,ilev)         :: evprox !<
  real,   dimension(ilg,ilev)         :: frhrox !<
  real,   dimension(ilg,ilev)         :: frkrox !<
  real,   dimension(ilg,ilev)         :: frsrox !<
  real,   dimension(ilg,ilev)         :: mltirox !<
  real,   dimension(ilg,ilev)         :: mltsrox !<
  real,   dimension(ilg,ilev)         :: raclrox !<
  real,   dimension(ilg,ilev)         :: rainrox !<
  real,   dimension(ilg,ilev)         :: sacirox !<
  real,   dimension(ilg,ilev)         :: saclrox !<
  real,   dimension(ilg,ilev)         :: snowrox !<
  real,   dimension(ilg,ilev)         :: subrox !<
  real,   dimension(ilg,ilev)         :: sedirox !<
  real,   dimension(ilg,ilev)         :: cndrox !<
  real,   dimension(ilg,ilev)         :: deprox !<
  !
  real,   dimension(ilg,ilev)         :: tprow !<
  real,   dimension(ilg,ilev)         :: qprow !<
  !
  real,   dimension(ilg,ilev)         :: xlmrox !<
!
  integer :: il
  integer :: jk
  integer :: lev1
  real    :: dtodt
  real    :: dqodt
  real    :: rcdt
  !
  !     * there must be no cloud within first "LEV1" layers for radiation
  !     * to work properly. this is defined in the "TOPLW" subroutine
  !     * called at the beginning of the model.
  !
  common /itoplw/ lev1
!
! Initilize to zero "right-hand side" terms that are sampled instantaneously.
! It is possible that they will not be computed unless the correct tendencies options
! are set which will cause problems for accumulation of these variables.
  if (phys_diag%tprhsc) then
      tprow(il1:il2, 1:ilev) = throw(il1:il2, 1:ilev)
  else
      tprow(il1:il2, 1:ilev) = 0.0
  end if
!
  if (phys_diag%qprhsc) then
      qprow(il1:il2, 1:ilev) =  qrow(il1:il2, 1:ilev)
  else
      qprow(il1:il2, 1:ilev) = 0.0
  end if
!
  if (phys_diag%switch_cosp_diag) then
! zero work arrays
      do jk=1,ilev
         do il=il1,il2
            rmixrol(il,jk) = 0.0
            smixrol(il,jk) = 0.0
            rrefrol(il,jk) = 0.0
            srefrol(il,jk) = 0.0
         end do ! il
      end do  ! l
  end if

! TKE scheme's mixing length
  if (phys_options%use_tke) xlmrox = pa%xlmrow
!
  if (phys_options%pla) then
!
!     * overwrite emiprically-based cdnc by results from pla calculations.
!
     if (phys_diag%pfrc .and. phys_diag%cdncfrc) then
        zcdn(il1:il2, 1:ilev)=max(pa%zcdnfrow(il1:il2, 1:ilev),1.e+06)
     else
        zcdn(il1:il2, 1:ilev)=max(pa%zcdnrow(il1:il2, 1:ilev),1.e+06)
     end if
  end if
  !
  !     * restore dqldt from zdetrow prior to use in cond.
  !
  dqldt(il1:il2, 1:ilev) = pa%zdetrow(il1:il2, 1:ilev) / dtadv
  !
  !     * updating tfrow to be consistent with modified throw.
  !
  call cond11(xrow, throw, qrow, hrow, zclf, zcdn,                 &
              dshj, shj, shtj, rainls, snow, pressg, qlwc,         &
              qiwc, tfrow, dqldt, zfevap, zmratep, zfsnow, zmlwc,  &
              clrfr, zfrain, pa%cvarrow, pa%cvmcrow, pa%cvsgrow,   &
              pa%pbltrow, qc, rhc, clrfs, zfsubl, qcwvar,          &
              xlmrox, pa%almcrow, pa%almxrow,                      &
              dtadv, ntrac, ilg, il1, il2, ilev, lev,              &
              rmixrol, smixrol, rrefrol, srefrol,                  &
              aggrox, autrox, cndrox, deprox,                      &
              evprox, frhrox, frkrox, frsrox,                      &
              mltirox, mltsrox, raclrox, rainrox,                  &
              sacirox, saclrox, snowrox, subrox, sedirox, qtn, hmn,&
              phys_options%use_tke, phys_options%pla, phys_diag%switch_cosp_diag)

  if (phys_diag%canam_mach .or. phys_diag%xtrals) then
     do jk=1,ilev
        do il=il1,il2
           pa%rainrol(il,jk) = rainrox(il,jk)
           pa%snowrol(il,jk) = snowrox(il,jk)
        enddo
     enddo
  end if

  if (phys_diag%canam_mach) then
     do jk = 1, ilev
        do il = il1, il2
           pa%cndrol(il,jk) = cndrox(il,jk)
           pa%deprol(il,jk) = deprox(il,jk)
        end do
     end do
  end if
!
  if (phys_diag%xtrals) then
!
!     * accumulate into diagnostic output arrays.
!
      do jk=1,ilev
      do il=il1,il2
        pa%aggrow (il,jk) = pa%aggrow (il,jk) + aggrox (il,jk)*saverad
        pa%autrow (il,jk) = pa%autrow (il,jk) + autrox (il,jk)*saverad
        pa%cndrow (il,jk) = pa%cndrow (il,jk) + cndrox (il,jk)*saverad
        pa%deprow (il,jk) = pa%deprow (il,jk) + deprox (il,jk)*saverad
        pa%evprow (il,jk) = pa%evprow (il,jk) + evprox (il,jk)*saverad
        pa%frhrow (il,jk) = pa%frhrow (il,jk) + frhrox (il,jk)*saverad
        pa%frkrow (il,jk) = pa%frkrow (il,jk) + frkrox (il,jk)*saverad
        pa%frsrow (il,jk) = pa%frsrow (il,jk) + frsrox (il,jk)*saverad
        pa%mltirow(il,jk) = pa%mltirow(il,jk) + mltirox(il,jk)*saverad
        pa%mltsrow(il,jk) = pa%mltsrow(il,jk) + mltsrox(il,jk)*saverad
        pa%raclrow(il,jk) = pa%raclrow(il,jk) + raclrox(il,jk)*saverad
        pa%rainrow(il,jk) = pa%rainrow(il,jk) + rainrox(il,jk)*saverad
        pa%sacirow(il,jk) = pa%sacirow(il,jk) + sacirox(il,jk)*saverad
        pa%saclrow(il,jk) = pa%saclrow(il,jk) + saclrox(il,jk)*saverad
        pa%snowrow(il,jk) = pa%snowrow(il,jk) + snowrox(il,jk)*saverad
        pa%subrow (il,jk) = pa%subrow (il,jk) + subrox (il,jk)*saverad
        pa%sedirow(il,jk) = pa%sedirow(il,jk) + sedirox(il,jk)*saverad
!
        pa%vaggrow(il) = pa%vaggrow(il) + dpog(il,jk)*aggrox (il,jk)*saverad
        pa%vautrow(il) = pa%vautrow(il) + dpog(il,jk)*autrox (il,jk)*saverad
        pa%vcndrow(il) = pa%vcndrow(il) + dpog(il,jk)*cndrox (il,jk)*saverad
        pa%vdeprow(il) = pa%vdeprow(il) + dpog(il,jk)*deprox (il,jk)*saverad
        pa%vevprow(il) = pa%vevprow(il) + dpog(il,jk)*evprox (il,jk)*saverad
        pa%vfrhrow(il) = pa%vfrhrow(il) + dpog(il,jk)*frhrox (il,jk)*saverad
        pa%vfrkrow(il) = pa%vfrkrow(il) + dpog(il,jk)*frkrox (il,jk)*saverad
        pa%vfrsrow(il) = pa%vfrsrow(il) + dpog(il,jk)*frsrox (il,jk)*saverad
        pa%vmlirow(il) = pa%vmlirow(il) + dpog(il,jk)*mltirox(il,jk)*saverad
        pa%vmlsrow(il) = pa%vmlsrow(il) + dpog(il,jk)*mltsrox(il,jk)*saverad
        pa%vrclrow(il) = pa%vrclrow(il) + dpog(il,jk)*raclrox(il,jk)*saverad
        pa%vscirow(il) = pa%vscirow(il) + dpog(il,jk)*sacirox(il,jk)*saverad
        pa%vsclrow(il) = pa%vsclrow(il) + dpog(il,jk)*saclrox(il,jk)*saverad
        pa%vsubrow(il) = pa%vsubrow(il) + dpog(il,jk)*subrox (il,jk)*saverad
        pa%vsedirow(il) = pa%vsedirow(il) + dpog(il,jk)*sedirox (il,jk)*saverad
      end do
      end do
  end if
  !
  rcdt  = 1./dtadv
  do jk = 1, ilev
    do il = il1, il2
      dtodt = (throw(il,jk)-tprow(il,jk))*rcdt
      dqodt = ( qrow(il,jk)-qprow(il,jk))*rcdt
      if (phys_diag%tprhsc) then
         pa%ttpprow(il,jk) = pa%ttpprow(il,jk) + dtodt*saverad
      end if
      if (phys_diag%qprhsc) then
          pa%qtpprow(il,jk) = pa%qtpprow(il,jk) + dqodt*saverad
      end if
      pa%ttpprol(il,jk) = dtodt
      pa%qtpprol(il,jk) = dqodt
    end do
  end do
  !
  !     * zero out liquid/ice and cloud amount in upper stratosphere.
  !
  qlwc(il1:il2, 1:lev1) = 0.0
  qiwc(il1:il2, 1:lev1) = 0.0
!
  return

  end subroutine condensation
 end module condensation_mod
