!> \file gwd.F90
!> \brief CanAM interface to the gravity wave drag scheme.
!!
!! @author Routine author name(s)
!
 module gwd_mod

 implicit none

 public :: gwd

 contains
 subroutine gwd(pa, throw, urow, vrow, tfrow, tsgb,       &
                pressg, shj, sgj, sgbj, shxkj, dsgj,      &
                radj, utendcv, utendgw, vtendcv, vtendgw, &
                gspong, switch_scheme_nogwd, ncdim, nadim, nazmth, nhar, &
                kount, saverad, dtadv, il1, il2, ilg)
!
  use agcm_types_mod, only: phys_arrays_type
  use agcm_types_mod, only: phys_options
  use agcm_types_mod, only: phys_diag
  use psizes_19,      only: ilev
  use times_mod,      only: delt
  use phys_consts,    only: grav, cpres, rgas, rgocp
  use phys_parm_defs, only: pp_mam_damp_gw_time

  implicit none

  real,    intent(in) :: dtadv   !< Advective timestep for atmospheric model \f$[seconds]\f$
  real,    intent(in) :: saverad !<
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: kount !< Current model timestep \f$[unitless]\f$
  integer, intent(in) :: nadim !< No. of azimuthal directions
  integer, intent(in) :: nazmth !< No. of azimuths for Hines non-oro GWD scheme
  integer, intent(in) :: ncdim !< No. phase speeds used to discretize spectrum
  integer, intent(in) :: nhar !< No. harmonics for Medvedev-Klassen non-oro GWD
  logical, intent(in) :: gspong !< Variable description\f$[units]\f$

  character(len=*), intent(in) :: switch_scheme_nogwd !< Choice of non-oro gwd scheme

  type(phys_arrays_type), intent(inout)      :: pa

  real, intent(inout), dimension(ilg,ilev)   :: urow !< u-wind (real).
  real, intent(inout), dimension(ilg,ilev)   :: vrow !< v-wind (real).
  real, intent(in),    dimension(ilg,ilev)   :: throw !< Physics internal field for temperature \f$[K]\f$
  real, intent(in),    dimension(ilg,ilev+1) :: tfrow  !< Temperature at interface. \f$[K]\f$
  real, intent(in),    dimension(ilg)        :: pressg !< Surface pressure \f$[Pa]\f$
  real, intent(in),    dimension(ilg,ilev)   :: sgj   !< Eta-level for mid-point of the momentum layer \f$[unitless]\f$
  real, intent(in),    dimension(ilg,ilev)   :: sgbj   !< Eta-level for base of momentum layers \f$[unitless]\f$
  real, intent(in),    dimension(ilg,ilev)   :: shj    !< Eta-level for mid-point of thermodynamic layer \f$[unitless]\f$
  real, intent(in),    dimension(ilg,ilev)   :: dsgj   !< Thickness of momentum layers in eta coordinates \f$[unitless]\f$
  real, intent(in),    dimension(ilg,ilev)   :: shxkj !< sigma**(rgas/cpres) at layers centres \f$[unitless]\f$

  real, intent(in),    dimension(ilg)        :: radj   !< Latitude \f$[radians]\f$

  real, intent(out),   dimension(ilg,ilev)   :: tsgb    !< Variable description\f$[units]\f$
  real, intent(out),   dimension(ilg,ilev)   :: utendgw !< Variable description\f$[units]\f$
  real, intent(out),   dimension(ilg,ilev)   :: vtendgw !< Variable description\f$[units]\f$
  real, intent(out),   dimension(ilg,ilev)   :: utendcv !< Variable description\f$[units]\f$
  real, intent(out),   dimension(ilg,ilev)   :: vtendcv !< Variable description\f$[units]\f$

  integer :: il
  integer :: jk
  integer :: idamp
  integer :: ifizgwd
  real :: duodtg
  real :: duodtn
  real :: duodts
  real :: dvodtg
  real :: dvodtn
  real :: dvodts
  real :: eplaunch
  real :: rcdt
  !
  real, dimension(ilg,ilev)  :: rmsgw !<
  real, dimension(ilg,ilev)  :: diffgw !<
  real, dimension(ilg,ilev)  :: heatgw !<
  real, dimension(ilg,ilev)  :: tsg    !<
  real, dimension(ilg,ilev)  :: unotnd !<
  real, dimension(ilg,ilev)  :: vnotnd !<
  real, dimension(ilg,ilev)  :: utenddw !<
  real, dimension(ilg,ilev)  :: vtenddw !<
  real, dimension(ilg,ilev)  :: utendlb !<
  real, dimension(ilg,ilev)  :: vtendlb !<
  real, dimension(ilg,ilev)  :: uxrow !<
  real, dimension(ilg,ilev)  :: vxrow !<
  real, dimension(ilg,ilev)  :: usrow !<
  real, dimension(ilg,ilev)  :: ugrow !<
  real, dimension(ilg,ilev)  :: ufrow !<
  real, dimension(ilg,ilev)  :: vsrow !<
  real, dimension(ilg,ilev)  :: vgrow !<
  real, dimension(ilg,ilev)  :: vfrow !<

  !=======================================================================
  if (phys_options%mam) then
!
!   * Set flag to put strong overhead absorber on in first two weeks
!   * of MAM run with initsp=on.
!
    if (kount * delt < pp_mam_damp_gw_time) then
      idamp = 1
    else
      idamp = 0
    end if
  else
    idamp = 0
  end if
  !
  !     * and initialize momentum tendency fields.
  !
  utendgw(il1:il2, 1:ilev) = 0.
  vtendgw(il1:il2, 1:ilev) = 0.
  utendcv(il1:il2, 1:ilev) = 0.
  vtendcv(il1:il2, 1:ilev) = 0.

  !     * set switch to control whether calculting component tendencies
  !     * in gwd routine.
  !
  ifizgwd = 0
  !
  if (phys_diag%uprhsc .or. phys_diag%vprhsc) then
     ifizgwd = 1
     uxrow(il1:il2, 1:ilev) = urow(il1:il2, 1:ilev)
     vxrow(il1:il2, 1:ilev) = vrow(il1:il2, 1:ilev)
  end if

  call tgcal2 (tsg, tsgb, throw, sgj, sgbj, shj, ilev, ilg, il1, il2)
  !
  !     * gravity-wave drag.
  !
  !     * zero unotnd,vnotnd since these are used to aquire tendency in
  !     * non-orographic schemes

  unotnd(il1:il2, 1:ilev) = 0.0
  vnotnd(il1:il2, 1:ilev) = 0.0

  rcdt = 1.0 / dtadv
  !
  if (phys_options%mam) then
!
!    * Non-orographic gravity-wave drag.
!
     if (trim(switch_scheme_nogwd) == "SCINOCCA") then   ! Use Scinocca scheme
!
!       * eplaunch - total e-p flux in (n,s,e,w) direction.
!
       eplaunch = 3.e-04*1.4142
       call gwdsat3(urow, vrow, tsg, tsgb, sgj, sgbj, pressg, radj,    &
                    rgas, rgocp, ilev, il1, il2, ilg, phys_options%ilaun, eplaunch, &
                    pa%ftnerow, pa%ftnwrow, pa%ftnyrow, &
                    pa%tdnxrow, pa%tdnyrow, pa%rhogrow, &
                    nadim, ncdim, unotnd, vnotnd, rcdt)

     end if
     if (trim(switch_scheme_nogwd) == "MEDKLA") then
!
!       * use medvedev-klaassen scheme.
!
        call mkgwintr2(urow, vrow, tfrow, throw, shxkj, shj, tsg, rgocp, &
                       sgj, sgbj, unotnd, vnotnd, rmsgw, &
                       pressg, rgas, cpres, ilev, ilg, il1, il2, nhar)

     end if
!
     if (trim(switch_scheme_nogwd) == "HINES") then
!
!       * USE HINES' DOPPLER SPREAD SCHEME.
!
        call hinesgw3 (urow, vrow, throw, tsg, pressg, sgj, shj, shxkj,   &
                       dsgj, radj, unotnd, vnotnd, heatgw, diffgw, rmsgw, &
                       rgas, rgocp, ilev, il1, il2, ilg, nazmth)

     end if
  end if
  !
  !     * orographic gravity-wave drag.
  !
  call gwdfl17(urow, vrow, tsg, tsgb, pa%envrow, pa%gamrow, pa%psirow, &
               pa%alphrow, pa%deltrow, pa%sigxrow, pa%phierow,         &
               kount, idamp, sgj, sgbj, dsgj, pressg,                  &
               utendgw, vtendgw, utenddw, vtenddw, utendlb, vtendlb,   &
               pa%ftoxrow, pa%ftoyrow, pa%tdoxrow, pa%tdoyrow,         &
               rgas,  rgocp,  dtadv,   ilev,    il1,    il2,   ilg,    &
               gspong, phys_options%envelop, ifizgwd, ugrow, vgrow,    &
               usrow, vsrow, ufrow, vfrow, unotnd, vnotnd, phys_options%mam)
  !
  if (phys_diag%uprhsc) then
     do jk = 1, ilev
        do il = il1, il2
          duodtg        = (ugrow(il,jk)-uxrow(il,jk))*rcdt
          duodtn        = (usrow(il,jk)-ugrow(il,jk))*rcdt
          duodts        = (ufrow(il, jk)-usrow(il,jk))*rcdt
          pa%utpgrow(il,jk) = pa%utpgrow(il,jk) + duodtg*saverad
          pa%utpnrow(il,jk) = pa%utpnrow(il,jk) + duodtn*saverad
          pa%utpsrow(il,jk) = pa%utpsrow(il,jk) + duodts*saverad
        end do
     end do
  end if
  if (phys_diag%vprhsc) then
     do jk = 1, ilev
        do il = il1, il2
          dvodtg        = (vgrow(il,jk)-vxrow(il,jk))*rcdt
          dvodtn        = (vsrow(il,jk)-vgrow(il,jk))*rcdt
          dvodts        = (vfrow(il,jk)-vsrow(il,jk))*rcdt
          pa%vtpgrow(il,jk) = pa%vtpgrow(il,jk) + dvodtg*saverad
          pa%vtpnrow(il,jk) = pa%vtpnrow(il,jk) + dvodtn*saverad
          pa%vtpsrow(il,jk) = pa%vtpsrow(il,jk) + dvodts*saverad
        end do
     end do
  end if
  !=======================================================================

  return
  end subroutine gwd

 end module gwd_mod
