!>\file
!>\brief
!!
!! @author David Plummer
!
module chem_photo_mod
  use chem_params_mod, only : rc

  implicit none ; private

  public :: photolysis2, interp2, jayno, cldcorr

contains

! -----------------------------------------------------------------------------

  subroutine photolysis2 (arj, zpos, apm, ozcrow, cszchem, csalrol, il1,il2, nbs)

  ! ---------------------------------------------------------------------------
  ! This subroutine finds appropriate values of photolysis rates in the
  ! Js library and interpolates them for use in the chemical module.
  !
  ! Called by chem3.f.
  ! Calls interp2.f.
  ! ---------------------------------------------------------------------------

  use msizes, only          : ilg, ilev
  use chem_params_mod, only : jpntjv, jpntpl, jpntsza, qpref, qzenang, vppi
  use sdet_mod, only        : rrsq

  implicit none

  integer, intent(in) :: il1, il2, nbs

  real(kind=rc), dimension(ilg,ilev),intent(in) :: apm
  real(kind=rc), dimension(ilg,ilev),intent(in) :: ozcrow
  real,          dimension(ilg,nbs), intent(in) :: csalrol
  real,          dimension(ilg),     intent(in) :: cszchem

  real(kind=rc), dimension(ilg,ilev),intent(out) :: zpos
  real(kind=rc), dimension(jpntjv,ilg,ilev), intent(out), target :: arj

  ! Locally arrays and variables
  real(kind=rc), dimension(400) :: zs

  integer :: i, ii, ik, n, impt
  real(kind=rc) :: asza, sralb, rjmin, rjmax
  real(kind=rc), dimension(:), pointer :: arj_ptr

  ! Log-pressure scale height (m)
  real(kind=rc), parameter :: qsh=7500.0_rc

  ! ---------------------------------------------------------------------------

  if (jpntpl > 400) call xit ('photolysis: jpntpl error', -1)

  arj(:,:,:) = 0.0_rc

  ! Compute fixed standard-profile log-p height (once).
  ! ZS (alternative coordinate) is -H.ln(p/po), where H and po are arbitary.
  zs(1) = 0.0_rc
  do i = 2, jpntpl
    zs(i) = -qsh * real(log( qpref(i)/qpref(1) ), kind=rc)
  enddo

  ! Divide the SZA search in two to avoid always searching through the large
  ! number of entries at high SZAs  [SZA = solar zenith angle]
  impt = 0
  do i = 1, jpntsza
    if (qzenang(i) <= 80.0) impt = i
  enddo

  ! Use the clear-sky albedo in the visible band for photolysis rate look-up.
  do ii = il1, il2
    if (cszchem(ii) < 1.74532) then ! ~100.0 degrees
      ! Get solar zenith angle from cosine array (units=degrees)
      asza = real(acos(cszchem(ii)) * 180.0 / vppi, kind=rc)
      sralb = real(csalrol(ii,1), kind=rc)
      do ik = 1, ilev

        arj_ptr => arj(1:jpntjv,ii,ik)
        call interp2 (arj_ptr, zpos(ii,ik), ozcrow(ii,ik), asza,  &
                      apm(ii,ik), sralb, zs, qsh, impt)
      enddo
    endif
  enddo

  ! Scale by orbital distance variation
  arj(:,:,:) = rrsq * arj(:,:,:)

  ! Set J-value limits
  rjmin = 1.0e-20_rc
  rjmax = 1.0_rc

  do ik = 1, ilev
    do ii = il1, il2
      do n = 1, jpntjv
        arj(n,ii,ik) = max(arj(n,ii,ik), rjmin)
        arj(n,ii,ik) = min(arj(n,ii,ik), rjmax)
      enddo
    enddo
  enddo
  !
  return
  end subroutine photolysis2

! -----------------------------------------------------------------------------

  subroutine interp2 (arj, zpos, pto3, pang, pr, sralb, zs, qsh, impt)

  ! --------------------------------------------------------------------------
  use chem_params_mod, only : jpntjv, jpnjt1, jpnjt2, jpntpl, jpntsza, &
                              jpntoz, jpnalb, &
                              jpidx1, jpidx2, qralb, qo3srat, qo3c, qpref, &
                              qzenang, rjvtab1, rjvtab2
  implicit none

  integer, intent(in) :: impt

  real(kind=rc), intent(in) :: pto3     !< ozone column
  real(kind=rc), intent(in) :: pang     !< solar zenith angle
  real(kind=rc), intent(in) :: pr       !< pressure of mid point of layer (Pa)
  real(kind=rc), intent(in) :: sralb    !< surface albedo
  real(kind=rc), intent(in) :: qsh
  real(kind=rc), dimension(400), intent(in) :: zs

  real(kind=rc), intent(out) :: zpos  !< position of current point in the height index of the J-value look-up table
  real(kind=rc), dimension(jpntjv), intent(out) :: arj  !< photolysis rates

  ! Local work
  integer :: i, j, k, nn
  integer :: ia, ia1, ix, iz, iz1, i3, i31, is, is1, i1, j1, k1
  real(kind=rc) :: qfak, p0, zstr, qdzr, qo3std, qo3rat, dalb
  real(kind=rc) :: qsum, qsum1, qsum2
  real(kind=rc), dimension(3) :: delta
  real(kind=rc), dimension(2,2,2) :: weight
! ------------------------------------------------------------------

  if (jpntsza > 100) call xit ('interp2: jpntsza error', -1)
  if (jpntoz  > 100) call xit ('interp2: jpntoz error', -2)

  qfak = log(10.0_rc)
  p0 = qpref(1)

  ! Find the position of the current solar zenith angle
  if (pang < qzenang(impt)) then
    ia = 1
    do i = 2, impt-1
      if (pang >= qzenang(i)) ia = i
    enddo
  else
    ia = impt
    do i = impt+1, jpntsza-1
      if (pang >= qzenang(i)) ia = i
    enddo
  endif

  ! Find the position of the current pressure
  !
  ! Compute Z* from reference P and local pressure.
  ! Interpolate table for local Z* relative to ref Z*.
  zstr = -qsh * log(pr/p0)
  if (zstr < 0.0_rc) zstr = 0.0_rc

  ! Slow search [sic]. The denominator of 575.0 is chosen to be close to, but
  ! below, the actual point.
  ix = int(zstr / 575.0_rc)
  if (ix > jpntpl-1) ix = jpntpl - 1
  if (ix < 1) ix = 1

  do k = ix, jpntpl-1
    if (zs(k) > zstr) then
      iz = k-1
      exit
    endif
  enddo

  if (iz == 0) iz = 1
  qdzr = (zstr-zs(iz)) / (zs(iz+1)-zs(iz))
  zpos = float(iz) + qdzr

  ! Find the position of the current overhead ozone column
  qo3std = qo3c(iz)  + (qo3c(iz+1)-qo3c(iz))*qdzr
  qo3rat = pto3/qo3std

  qo3rat = max(qo3rat, qo3srat(1)+0.00001_rc)
  qo3rat = min(qo3rat, qo3srat(jpntoz))
  i3 = int(qo3rat*4.0_rc)

  ! Find the position of the current albedo
  is = 1
  do i = 2, jpnalb-1
    if (sralb > qralb(i)) is = i
  enddo

  dalb = (sralb-qralb(is)) / (qralb(is+1)-qralb(is))
  dalb = max(dalb, 0.0_rc)
  dalb = min(dalb, 1.0_rc)

  delta(1) =  qdzr
  delta(2) = (pang - qzenang(ia))/(qzenang(ia+1)-qzenang(ia))

  if (i3 < jpntoz) then
    delta(3) = (qo3rat - qo3srat(i3))/(qo3srat(i3+1)-qo3srat(i3))
    i31 = i3 + 1
  else
    delta(3) = 0.0_rc
    i31 = i3
  endif

  ia1 = ia + 1
  iz1 = iz + 1
  is1 = is + 1

  do i = 1,2
    i1 = mod(i,2)
    do j = 1,2
      j1 = mod(j,2)
      do k = 1,2
        k1 = mod(k,2)

        weight(i,j,k) = (2-i-delta(1)) * (2-j-delta(2)) * (2-k-delta(3))
        if (mod(i1+j1+k1,2) == 1) weight(i,j,k) = -weight(i,j,k)
      enddo
    enddo
  enddo

  do nn = 1, jpnjt1
    qsum = 0.0_rc
    qsum = weight(1,1,1)*rjvtab1(nn,iz,ia,i3)   &
         + weight(1,1,2)*rjvtab1(nn,iz,ia,i31)  &
         + weight(1,2,1)*rjvtab1(nn,iz,ia1,i3)  &
         + weight(1,2,2)*rjvtab1(nn,iz,ia1,i31) &
         + weight(2,1,1)*rjvtab1(nn,iz1,ia,i3)  &
         + weight(2,1,2)*rjvtab1(nn,iz1,ia,i31) &
         + weight(2,2,1)*rjvtab1(nn,iz1,ia1,i3) &
         + weight(2,2,2)*rjvtab1(nn,iz1,ia1,i31)

    if (qsum <= 0.0_rc) qsum=0.0_rc
    arj(jpidx1(nn)) = exp(-qfak*qsum)
  enddo

  do nn = 1, jpnjt2
    qsum1 = 0.0_rc
    qsum2 = 0.0_rc
    qsum1 = weight(1,1,1)*rjvtab2(nn,iz,ia,i3,is)   &
          + weight(1,1,2)*rjvtab2(nn,iz,ia,i31,is)  &
          + weight(1,2,1)*rjvtab2(nn,iz,ia1,i3,is)  &
          + weight(1,2,2)*rjvtab2(nn,iz,ia1,i31,is) &
          + weight(2,1,1)*rjvtab2(nn,iz1,ia,i3,is)  &
          + weight(2,1,2)*rjvtab2(nn,iz1,ia,i31,is) &
          + weight(2,2,1)*rjvtab2(nn,iz1,ia1,i3,is) &
          + weight(2,2,2)*rjvtab2(nn,iz1,ia1,i31,is)

    qsum2 = weight(1,1,1)*rjvtab2(nn,iz,ia,i3,is1)   &
          + weight(1,1,2)*rjvtab2(nn,iz,ia,i31,is1)  &
          + weight(1,2,1)*rjvtab2(nn,iz,ia1,i3,is1)  &
          + weight(1,2,2)*rjvtab2(nn,iz,ia1,i31,is1) &
          + weight(2,1,1)*rjvtab2(nn,iz1,ia,i3,is1)  &
          + weight(2,1,2)*rjvtab2(nn,iz1,ia,i31,is1) &
          + weight(2,2,1)*rjvtab2(nn,iz1,ia1,i3,is1) &
          + weight(2,2,2)*rjvtab2(nn,iz1,ia1,i31,is1)

    if (qsum1 <= 0.0_rc) qsum1 = 0.0_rc
    if (qsum2 <= 0.0_rc) qsum2 = 0.0_rc

    arj(jpidx2(nn)) = exp( -qfak*((1.0_rc-dalb)*qsum1 + dalb*qsum2) )
  enddo
  !
  return
  end subroutine interp2

! -----------------------------------------------------------------------------

  subroutine jayno (arj, apm, airrow, cszchem, ozcrow, nocrow, qconst, il1,il2)

  ! ----------------------------------------------------------------------------
  ! This subroutine calculates the photolysis rate for NO, which overwrites
  ! the rate taken from the original photolysis rate look-up table.
  ! See Minschwaner and Siskind, JGR, 98, 20401-20412, 1993, for the algorithm.
  !
  ! D. Plummer, July 2004
  !
  ! Subroutine currently targeting photolysis reaction no. 36
  ! Called by chem3.f.
  ! ---------------------------------------------------------------------------

  use msizes, only          : ilg, ilev
  use chem_params_mod, only : jpntjv

  implicit none

  integer, intent(in) :: il1, il2

  real(kind=rc), intent(in) :: qconst !< scalar constant to convert pressure in Pa to a column of air in (molec. cm-2)
  real,          dimension(ilg), intent(in) :: cszchem     !< cosine of solar zenith angle
  real(kind=rc), dimension(ilg,ilev), intent(in) :: apm    !< pressure on model levels (Pa)
  real(kind=rc), dimension(ilg,ilev), intent(in) :: airrow !< background number density (molecules cm-3)
  real(kind=rc), dimension(ilg,ilev), intent(in) :: ozcrow !< vertical O3 column above each model level (molec. cm-2)
  real(kind=rc), dimension(ilg,ilev), intent(in) :: nocrow !< vertical NO column above each model level (molec. cm-2)

  real(kind=rc), dimension(jpntjv,ilg,ilev), intent(inout) :: arj !< photolysis rates on model levels (s-1)

  ! Local variables
  integer :: i, j, jz, k, l, m
  real(kind=rc) :: o2tra, prx, scolo2, tauno, csza

  real(kind=rc) :: cwork(3)

  ! Constants for the NO photolysis parameterization
  ! Parameterization sums contribution to NO photolysis from three different
  ! bands. The contribution from each of these three bands is calculated using
  ! a quadrature based on six sub-bands.
  real(kind=rc), dimension(3), parameter :: inot    = (/ 9.154e+11_rc, 3.315e+11_rc, 3.450e+11_rc /)
  real(kind=rc), dimension(3), parameter :: cso3(3) = (/  4.80e-19_rc,  6.88e-19_rc,  7.29e-19_rc /)
  real(kind=rc), dimension(3,6)   :: cso2
  real(kind=rc), dimension(3,6,2) :: wght, csno

  ! 1)  the O2 (5-0) band, NO d(0-0) band, 190.2 - 192.5nm
  data cso2(1,:) /1.117e-23_rc,2.447e-23_rc,7.188e-23_rc, 3.042e-22_rc,1.748e-21_rc,1.112e-20_rc/
  data wght(1,:,1) /0.00e+00_rc, 5.12e-02_rc, 1.36e-01_rc, 1.65e-01_rc, 1.41e-01_rc, 4.50e-02_rc/
  data wght(1,:,2) /0.00e+00_rc, 5.68e-03_rc, 1.52e-02_rc, 1.83e-02_rc, 1.57e-02_rc, 5.00e-03_rc/
  data csno(1,:,1) /0.00e+00_rc, 1.32e-18_rc, 6.35e-19_rc, 7.09e-19_rc, 2.18e-19_rc, 4.67e-19_rc/
  data csno(1,:,2) /0.00e+00_rc, 4.41e-17_rc, 4.45e-17_rc, 4.50e-17_rc, 2.94e-17_rc, 4.35e-17_rc/

  ! 2) the O2 (9-0) band, NO d(1-0) band, 183.1 - 184.6nm
  data cso2(2,:) /1.350e-22_rc,2.991e-22_rc,7.334e-22_rc, 3.074e-21_rc,1.689e-20_rc,1.658e-19_rc/
  data wght(2,:,1) /0.00e+00_rc, 0.00e+00_rc, 1.93e-03_rc, 9.73e-02_rc, 9.75e-02_rc, 3.48e-02_rc/
  data wght(2,:,2) /0.00e+00_rc, 0.00e+00_rc, 2.14e-04_rc, 1.08e-02_rc, 1.08e-02_rc, 3.86e-03_rc/
  data csno(2,:,1) /0.00e+00_rc, 0.00e+00_rc, 3.05e-21_rc, 5.76e-19_rc, 2.29e-18_rc, 2.21e-18_rc/
  data csno(2,:,2) /0.00e+00_rc, 0.00e+00_rc, 3.20e-21_rc, 5.71e-17_rc, 9.09e-17_rc, 6.00e-17_rc/

  ! 3) the O2 (10-0) band, NO d(1-0) band, 181.6 - 183.1nm
  data cso2(3,:) /2.968e-22_rc,5.831e-22_rc,2.053e-21_rc, 8.192e-21_rc,4.802e-20_rc,2.655e-19_rc/
  data wght(3,:,1) /4.50e-02_rc, 1.80e-01_rc, 2.25e-01_rc, 2.25e-01_rc, 1.80e-01_rc, 4.50e-02_rc/
  data wght(3,:,2) /5.00e-03_rc, 2.00e-02_rc, 2.50e-02_rc, 2.50e-02_rc, 2.00e-02_rc, 5.00e-03_rc/
  data csno(3,:,1) /1.80e-18_rc, 1.50e-18_rc, 5.01e-19_rc, 7.20e-20_rc, 6.72e-20_rc, 1.49e-21_rc/
  data csno(3,:,2) /1.40e-16_rc, 1.52e-16_rc, 7.00e-17_rc, 2.83e-17_rc, 2.73e-17_rc, 6.57e-18_rc/

  ! ---------------------------------------------------------------------------

  do j = 1, ilev
    do i = il1, il2
      arj(36,i,j) = 1.0e-15_rc   ! Initialization
      if (cszchem(i) > 0.0) then
        csza = real(cszchem(i), kind=rc)
        scolo2 = 0.21_rc * apm(i,j) * qconst / csza
        if (scolo2 < 1.0e24_rc) then

        do k = 1, 3
          cwork(k) = 0.0_rc
          do l = 1, 6
            o2tra = exp(-1.0_rc*scolo2*cso2(k,l))
            do m = 1, 2
              tauno = csno(k,l,m) * nocrow(i,j) / csza
              if (tauno < 50.0_rc) then
                cwork(k) = cwork(k)   &
                         + o2tra * csno(k,l,m) * wght(k,l,m) * exp(-1.0_rc*tauno)
              endif
            enddo
          enddo
        enddo

        prx = 1.65e+09_rc / (5.1e+07_rc + 1.65e+09_rc + 1.5e-09_rc*0.79_rc*airrow(i,j))
        arj(36,i,j) = prx*inot(1)*cwork(1) * exp( -1.0_rc*cso3(1)*ozcrow(i,j) / csza )   &
                    + inot(2)*cwork(2)*exp(-1.0_rc*cso3(2)*ozcrow(i,j)/csza) &
                    + inot(3)*cwork(3)*exp(-1.0_rc*cso3(3)*ozcrow(i,j)/csza)
        arj(36,i,j) = max(arj(36,i,j),1.0e-15_rc)

        end if
      endif
    enddo  ! loop 150 over i=il1,il2
  enddo  ! loop 100 over j=1,ilev
  !
  return
  end subroutine jayno

! -----------------------------------------------------------------------------

  subroutine cldcorr (arj, alswrol, cszchem, cmtx, tac, cg, omega, zpos,  &
                      shj, shtj, vppi, lon, il1, il2, ilg, ilev, &
                      lev, ilevcs, ilevcf, ilevc, nintsw)

  ! ---------------------------------------------------------------------------
  ! Purpose:
  ! Calculate a correction to the clear-sky photolysis rates to account for
  ! the effects of clouds. Based on approach of Chang et al. (1987).
  !
  ! Modified to include:
  ! 1- linear variation of in-cloud correction between cloud base and cloud top
  ! 2- height-varying correction above cloud
  !
  ! NB: this subroutine is as yet (August 2021) only aspirational. The call
  !     from chem3.F90 is blocked. "A work in progress", sez Dave.
  ! ---------------------------------------------------------------------------

  use chem_params_mod, only : jpntjv, jpnjt2, jpnalb, rjvtab2
  implicit none

  integer, intent(in) :: il1,il2, ilev,ilevc,ilevcf,ilevcs, ilg,lon,lev, nintsw

  real, intent(in) :: vppi
  real, dimension(ilg), intent(in) :: alswrol, cszchem
  real, dimension(ilg,ilev), intent(in) :: shj
  real, dimension(ilg, lev), intent(in) :: shtj
  real, dimension(ilg,lev+1,lev+1), intent(in) :: cmtx
  real, dimension(ilg,nintsw, lev), intent(in) :: cg, omega, tac

  real(kind=rc), dimension(ilg,ilev), intent(in) :: zpos
  real(kind=rc), dimension(jpntjv,lon,ilevc), intent(inout) :: arj

  ! Locally defined work arrays
  integer, dimension(ilg) :: ibot, isun, itop

  real, dimension(ilg) :: sec, tauclw, taucum
  real, dimension(ilg,  lev) :: taulst, tauold
  real, dimension(ilg,lev+1) :: c1i
  real, dimension(ilg,lev+1,lev+1) :: cmtxg

  ! Number of photolysis rates for which a cloud correction is applied
  integer, parameter :: ncorr = 22

  ! Position in the J-value array (not the look-up tables, the array holding the
  ! interpolated values) for those reactions that require a cloud correction.
  ! This is all the reactions in the 'visible' look-up table.
  integer, dimension(ncorr), parameter :: jindx = &
          [  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12,   &
               13, 14, 15, 16, 17, 19, 22, 23, 30, 34, 42 ]

  ! Reaction-specific alpha parameter to tune the cloud correction for the
  ! different photolysis reactions. In general, alpha < 1.0 for reactions
  ! driven in the UV and alpha > 1.0 for reactions largely from light further
  ! out into the visible.
  real, dimension(ncorr), parameter :: alpha = &
             [ 0.85, 1.15, 0.90, 1.00, 1.20, 1.20, 0.90, 0.90,  &
               0.90, 1.00, 0.95, 0.95, 1.00, 0.95, 1.00, 1.10,  &
               1.00, 0.90, 0.90, 0.90, 1.00, 0.85 ]

  ! Reaction specific maximum solar zenith angle
  real, dimension(ncorr), parameter :: szamax = &
              [ 60.0, 75.0, 65.0, 70.0, 75.0, 75.0, 65.0, 65.0,  &
                65.0, 65.0, 65.0, 65.0, 65.0, 65.0, 65.0, 65.0,  &
                70.0, 60.0, 60.0, 60.0, 65.0, 60.0 ]

  ! Local work arrays
  integer :: i, ipt, is, itc, j, jno, jyes, k, km1, kp1, kpct,kplc, lengath, n

  real :: alb, cneb, corr, crat, cut, epsi, facor, qfak, sza, szab, taux, zfac
  real, dimension(ncorr) :: alph2, alph3

  real, dimension(ncorr) ::  acfac, bcfac, crat0, tcfac

  real, dimension(100) :: jwork

  ! ---------------------------------------------------------------------------
  ! Threshold for fractional cloudiness in a single layer
  cut = 0.001

  ! Position of 1.0 standard ozone column in the J-table
  itc = 4

  ! Modified alpha coefficients used in the correction
  do i = 1, ncorr
    alph2(i) = alpha(i)
    if (alpha(i) > 1.0) alph2(i) = 0.5*(1.0 + alpha(i))
    alph3(i) = min(alpha(i), 1.0)
  enddo

  ! Determine which columns contain some cloud and are in daylight
  do i = il1, il2
    ibot(i) = 0
  enddo

  do k = 2, lev
    kp1 = k + 1
    do i = il1, il2
      if (cmtx(i,k,kp1) > cut) ibot(i) = 1
    enddo
  enddo

  jyes = 0
  jno  = il2 - il1 + 2
  do i = il1, il2
    if (ibot(i) == 1 .and. cszchem(i) > 0.02) then
      jyes = jyes + 1
      isun(jyes) = i
    else
      jno = jno - 1
      isun(jno) = i
    endif
  enddo
  lengath = jyes

  ! Only CMTX and the solar zenith angle are gathered as the other arrays
  ! are only referenced once or twice per pass
  do k = 1, lev+1
    do j = 1, lev+1
      do i = 1, lengath
        cmtxg(i,j,k) = cmtx(isun(i),j,k)
      enddo
    enddo
  enddo

  ! Modified solar zenith angle as used in SHORTW8
  do i = 1, lengath
    sec(i) = 35.0 / sqrt( 1224.0*cszchem(isun(i))*cszchem(isun(i)) + 1.0 )
  enddo

  ! Perform a correction to the random overlap cloud; the same calculation as
  ! is performed in SWCLDF2, called from SHORTW8.
  ! Note that here all arrays run from the top(k=1) to bottom (k=ilev)
  !
  ! Initialize  arrays
  do k = 1, lev
    do i = 1, lengath
      taulst(i,k) = 0.0
    enddo
  enddo

  do i = 1, lengath
    c1i(i,1)  = 0.0
    taucum(i) = 0.0
    tauclw(i) = 0.0
  enddo

  do k = 1, lev
    kp1 = k + 1
    do i = 1, lengath
      tauold(i,k) = cmtxg(i,k,kp1)
    enddo
  enddo

  do k = 1, lev
    kp1 = k + 1
    do i = 1, lengath
      if (cmtxg(i,k,kp1) <= cut) then
        taulst(i,k) = 0.0
        tauold(i,k) = 0.0
      else
        if (k == 1) then
          taulst(i,k) = -log(1.0 - cmtxg(i,k,kp1) + cut)
        else
          km1 = k - 1
          tauold(i,k) = max(tauold(i,k), tauold(i,km1))
          taulst(i,k) = taulst(i,km1) - log(1.0 - cmtxg(i,k,kp1) + cut)
        endif
      endif
    enddo
  enddo

  do k = lev-1, 1, -1
    kp1 = k + 1
    do i = 1, lengath
      if (cmtxg(i,k,kp1) > cut) then
        taulst(i,k) = max(taulst(i,k), taulst(i,kp1))
        tauold(i,k) = max(tauold(i,k), tauold(i,kp1))
      endif
    enddo
  enddo

  do k = 1, lev
    kp1 = k + 1
    do i = 1, lengath
      if (cmtxg(i,k,kp1) > cut) then
        epsi = -log(1.0 - tauold(i,k) + cut) / (taulst(i,k) + cut)
        cneb = 1.0 - (1.0 - cmtxg(i,k,kp1))**epsi
      else
        cneb = 0.0
      endif
      taux = tac(isun(i),1,k)
      taucum(i) = taucum(i) + taux
      facor = 1.0 - omega(isun(i),1,k)*cg(isun(i),1,k)*cg(isun(i),1,k)
      corr  = 1.0 - exp(-facor*taucum(i)*sec(i))
      c1i(i,kp1) = cmtxg(i,1,kp1)*corr
      tauclw(i) = tauclw(i) + taux*cneb*corr
    enddo
  enddo

  do i = 1, lengath
    if (c1i(i,lev+1) > 0.0) then
      tauclw(i) = tauclw(i) / c1i(i,lev+1)
    else
      tauclw(i) = 0.0
    endif
  enddo

  ! Find the top and bottom of the cloudy layer
  do i = 1, lengath
    itop(i) = lev
    ibot(i) = 2
  enddo

  do k = 2, lev
    kp1 = k + 1
    do i = 1, lengath
      if (cmtxg(i,k,kp1) > cut .and. k < itop(i)) itop(i) = k
    enddo
  enddo

  do k = lev, 2, -1
    kp1 = k + 1
    do i = 1, lengath
      if (cmtxg(i,k,kp1) > cut .and. k > ibot(i)) ibot(i) = k
    enddo
  enddo

  ! Modify itop and ibot to account for fact CMTX runs from 2,lev
  do i = 1, lengath
    itop(i) = itop(i) - 1
    ibot(i) = ibot(i) - 1
  enddo

  ! Calculate the cloud transmission
  do i = 1, lengath
    taucum(i) = (5.0 - exp(-0.2*tauclw(i))) / (4.0 + 3.0*0.14*tauclw(i))
  enddo

  ! Correct the clear-sky photolysis rates for clouds
  ! Note that clear-sky rates are assumed for clouds with an optical depth of
  ! less than 5.0. Corrected J-values are an average of the clear-sky and
  ! cloud-corrected rates weighted by cloudy sky fraction.
  ! J  = (fc*(Cr-1) + 1)Jo
  ! Cr = correction factor
  ! fc = cloud fraction
  ! Jo = clear-sky photolysis rate

  qfak = log(10.0)
  do i = 1, lengath
    if (tauclw(i) >= 5.0) then

      ipt = isun(i)
      sza = acos(cszchem(ipt)) * 180.0 / vppi
      alb = min(alswrol(ipt), 0.80)

      do k = ilevcs, ilevcf
        jwork(k) = arj(2,ipt,k)
      enddo

      ! Calculate the below-cloud, top-of-cloud and above-cloud correction
      ! factors for each reaction
      do n = 1, ncorr

        szab = min(sza, szamax(n)) * vppi / 180.0
        bcfac(n) = 0.65*(1.0 + alpha(n))*(taucum(i)+0.05)*cos(alph2(n)*szab) &
                 + 0.2 *(1.0 - taucum(i))*alb/(1.6/alpha(n) - alb)
        tcfac(n) = 1.0 + (1.8*alpha(n)*(0.85 - taucum(i))*cos(1.5*szab)      &
                 + 0.2*alpha(n)*taucum(i))*(1.1 - alb)
        acfac(n) = alpha(n) * (0.15 + (0.85 - taucum(i)) * cos(szab))*1.33   &
                 * (0.85 - alph3(n)*alb)
      enddo

      ! Apply below-cloud correction
      do k = ibot(i)+1, ilevcf
        kp1 = k - ilevcs + 1
        do n = 1, ncorr
          arj(jindx(n),ipt,kp1) = arj(jindx(n),ipt,kp1)  &
                                * (c1i(i,lev+1)*(bcfac(n)-1.0) + 1.0)
        enddo
      enddo

      ! Apply in-cloud correction, linearly varying over the depth of the cloud
      ! from the below-cloud factor to the top-of-cloud factor
      do k = itop(i), ibot(i)
        kp1 = k - ilevcs + 1
        zfac = (shj(ipt,k) - shtj(ipt,itop(i)))/(shtj(ipt,ibot(i)+1)  &
             - shtj(ipt,itop(i)))
        do n = 1, ncorr
          arj(jindx(n),ipt,kp1) = arj(jindx(n),ipt,kp1)*(c1i(i,lev+1) &
                                * (zfac*bcfac(n)                      &
                                + (1.0 - zfac)*tcfac(n) - 1.0) + 1.0)
        enddo
      enddo

      ! Apply above-cloud correction
      ! Above-cloud enhancement must be attenuated as the distance above
      ! cloud-top increases. Attenuation follows the change in the photolysis
      ! rates in the look-up table to a change in surface albedo.
      kpct = int(zpos(ipt,itop(i)))
      zfac = zpos(ipt,itop(i)) - float(kpct)
      is   = int((sza+5.0)/10.0) + 1

      do n = 1, ncorr
        crat0(n) = exp( -qfak*((1.0 - zfac)*rjvtab2(n,kpct,is,itc,jpnalb)  &
                 + zfac*rjvtab2(n,kpct+1,is,itc,jpnalb)) )  &
                 / exp( -qfak*((1.0 - zfac)*rjvtab2(n,kpct,is,itc,1)       &
                 + zfac*rjvtab2(n,kpct+1,is,itc,1)) ) - 1.0
      enddo

      do k=1,itop(i)-1
        kp1 = k - ilevcs + 1
        kplc = int(zpos(ipt,k))

        do n = 1, ncorr
          crat = exp( -qfak*((1.0 - zfac)*rjvtab2(n,kplc,is,itc,jpnalb) &
               + zfac*rjvtab2(n,kplc+1,is,itc,jpnalb)) )                &
               / exp( -qfak*((1.0 - zfac)*rjvtab2(n,kplc,is,itc,1)      &
               + zfac*rjvtab2(n,kplc+1,is,itc,1)) ) - 1.0
          arj(jindx(n),ipt,kp1) = arj(jindx(n),ipt,kp1)    &
                                * (c1i(i,lev+1)*acfac(n)*crat/crat0(n) + 1.0)
        enddo
      enddo

      do k = ilevcs, ilevcf
        jwork(k) = arj(2,ipt,k) / jwork(k)
      enddo

      do k = ilevcs, ilevcf
        if (jwork(k) < 1.0e-3) then
          write(6,*)"cldcorr: large corrections",i,lengath,ipt,k,ibot(i),itop(i)
          write(6,*) "cldcorr: tauc, sza, J", tauclw(i), sza, arj(2,ipt,k)
          write(6,*) "cldcorr: cfac", jwork(k)
          write(6,'(1p6e12.4)') (acfac(n),n=1,ncorr)
          write(6,'(1p6e12.4)') (tcfac(n),n=1,ncorr)
          write(6,'(1p6e12.4)') (bcfac(n),n=1,ncorr)
        endif
      enddo

    endif
  enddo
  !
  end subroutine cldcorr

! -----------------------------------------------------------------------------

end module chem_photo_mod
