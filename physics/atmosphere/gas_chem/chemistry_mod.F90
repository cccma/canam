!>\file
!>\brief Purpose:
!!
!! @author David Plummer
!
module chemistry_mod

  implicit none ; private

  public :: hetchem, chemheat

contains

  ! --------------------------------------------------------------------------
  subroutine hetchem (an3, temp, qmair, csa, h2ocpy, csadwrk, dt, &
                      il1, il2, ilg, ilev, jphcsta, jphcsto)

! Called by mamchmf2.f.
! Calls     ternary.f, hetrates.f, het_newton.f, all contained in het_chem_mod.
! ------------------------------------------------------------------------------

  use chem_params_mod, only : jppsc2, jpsrxn, rc
  use het_chem_mod
  implicit none

  integer, intent(in) :: il1, il2, ilg, ilev, jphcsta, jphcsto

  real(kind=rc), intent(in) :: dt
  real(kind=rc), dimension(ilg,ilev,12), intent(inout) :: an3  !< heterogeneous chemistry species array
  real(kind=rc), dimension(ilg,ilev),    intent(in)    :: temp, qmair, csa, h2ocpy

  real(kind=rc), dimension(2,ilg,ilev),  intent(out)   :: csadwrk

  ! In the following, leading G denotes gas phase and S denotes solid phase.
  ! They are the elements of the AN3 array in the calling routine mamchmf2.
  ! The values are built up with every new call to this routine, hence "inout".
  real(kind=rc), dimension(ilg,ilev) :: gclono2, ghocl, gn2o5
  real(kind=rc), dimension(ilg,ilev) :: gcl2, gbrono2, ghobr
  real(kind=rc), dimension(ilg,ilev) :: gbrcl, gh2o, ghno3, ghcl
  real(kind=rc), dimension(ilg,ilev) :: shno3, sh2o

  ! * Locally defined variables
  real(kind=rc), dimension(ilg,ilev) :: vlm, ws, hhocl, xfs, zmcl
  real(kind=rc), dimension(jpsrxn,ilg,ilev) :: ksulf
  real(kind=rc), dimension(jppsc2,ilg,ilev) :: kpsc2

  integer :: i, k

  ! ---------------------------------------------------------------------------
  ! A) The call to subroutine TERNARY
  !
  ! The purpose of ternary is to determine the condensed and gas-phase
  ! concentrations of HCl and HNO3 given temperature, water vapour pressure,
  ! and aerosol H2SO4 concentration.  Partitioning of gas species into the
  ! condensed phase is assumed not to occur unless temperature is 210K or below.
  ! Aerosol density and volume are also calculated.  Finally, the Henry's Law
  !coefficient for HOCl is calculated to determine the rate of HOCL + HCL(a)
  ! heterogeneous reaction. Heterogeneous chemistry is done only between model
  ! levels 25 (10 hPa) to 37 (269 hPa).
  !
  ! temp  : temperature (K)
  ! gh2o  : gaseous H2O (num. density)
  ! shno3 : condensed HNO3 (num. density)
  ! ghno3 : gaseous HNO3 (num. density)
  ! ghcl  : gaseous HCl (num. density)
  ! ws	  : weight fraction of H2SO4 in the aerosol. (needed in hetrates)
  ! hhocl : solubility of HOCL (needed in hetrates)
  ! vlm   : aerosol volume (cm^3/cm^3)
  ! tice  : ice frost point (K, a function of ambient T and water pressure)
  ! xfs	  : mole fraction of H2SO4 in aerosol (needed in clono2g)
  ! qmair : background number density
  ! csa	  : climatology surface area of aerosols (sulphate)
  !
  do k = 1, ilev
    do i = il1, il2
      gclono2(i,k) = an3(i,k,1)
      ghocl(i,k)   = an3(i,k,2)
      gn2o5(i,k)   = an3(i,k,3)
      gcl2(i,k)    = an3(i,k,4)
      gbrono2(i,k) = an3(i,k,5)
      ghobr(i,k)   = an3(i,k,6)
      gbrcl(i,k)   = an3(i,k,7)
      gh2o(i,k)    = an3(i,k,8)
      ghno3(i,k)   = an3(i,k,9)
      ghcl(i,k)    = an3(i,k,10)
      shno3(i,k)   = an3(i,k,11)
      sh2o(i,k)    = an3(i,k,12)
    enddo
  enddo

  !
  ! The explicit calculation of HCl(s) has been removed and SHCL is no longer
  ! carried as a separate quantity.  The amount of HCl in the aerosol phase is
  ! explicitly calculated, using an effective Henry's Law constant and the
  ! ambient concentration of HCl, and is included as a factor in the reaction
  ! probabilities of reactions involving HCl in the aerosol phase.
  ! Therefore it is not necessary to explicitly calculate HCl(s) or to use the
  ! concentration of HCl(s) in the heterogeneous chemistry mechanism. Doing so
  ! could even have unintended consequences if HCl(s) is small and depletion of
  ! HCl(s) during a chemical timestep artificially limits the rate of reaction.

  call ternary (temp, sh2o, gh2o, shno3, ghno3, ghcl, csa, ws, vlm, &
                xfs, hhocl, zmcl, il1, il2, jphcsta, jphcsto)

  ! B) The call to subroutine HETRATES
  !
  ! The purpose of hetrates is to calculate the rates of the heterogeneous
  ! reactions. The actual rate constants are passed in arrays kpsc2 (for water
  ! ice reactions) and ksulf (for STS and binary sulfate aerosols).

  call hetrates (kpsc2, ksulf, temp, ghcl, gh2o, sh2o, ws, vlm,   &
                 xfs, hhocl, zmcl, csadwrk, il1, il2, jphcsta, jphcsto)

  ! C) The call to subroutine HET_NEWTON
  !
  ! Routine het_newton calls the Newton solver to calculate the densities of
  ! the new species. The densities of all gas-phase and condensed species will
  ! be changed upon exit. This is a mass-conserving solver.

  call het_newton (gclono2, ghocl, gcl2, gn2o5, gbrono2, ghobr, gbrcl,  &
                   gh2o, ghno3, ghcl, qmair, h2ocpy, kpsc2, ksulf, dt,  &
                   il1, il2, jphcsta, jphcsto)

  ! Harmonize the diagnostic STS and ice surface areas with the condition on
  ! where heterogeneous chemistry is performed, based on the local water vapour
  ! mixing ratio
  do k = jphcsta, jphcsto
    do i = il1, il2
      if (h2ocpy(i,k) > 1.0e-05_rc) then
        csadwrk(1,i,k) = 0.0_rc
        csadwrk(2,i,k) = 0.0_rc
      endif
    enddo
  enddo
  !
  do k = 1, ilev
    do i = il1, il2
      an3(i,k,1)  = gclono2(i,k)
      an3(i,k,2)  = ghocl(i,k)
      an3(i,k,3)  = gn2o5(i,k)
      an3(i,k,4)  = gcl2(i,k)
      an3(i,k,5)  = gbrono2(i,k)
      an3(i,k,6)  = ghobr(i,k)
      an3(i,k,7)  = gbrcl(i,k)
      an3(i,k,8)  = gh2o(i,k)
      an3(i,k,9)  = ghno3(i,k)
      an3(i,k,10) = ghcl(i,k)
      an3(i,k,11) = shno3(i,k)
      an3(i,k,12) = sh2o(i,k)
    enddo
  enddo

  return
  end subroutine hetchem

! -----------------------------------------------------------------------------

  subroutine chemheat (heatch, xrow, ark, il1, il2, ntrac )

  ! ---------------------------------------------------------------------------
  ! PURPOSE
  ! -------
  ! Calculate heating rates due to some exothermic reactions associated with
  ! the odd oxygen and odd hydrogen families.
  !
  ! Called by chem3.f.
  !
  ! Odd oxygen:
  !  R1 :  O + O2 + M  --->  O3 + M     (1)
  !  R2 :  O + O3      --->  2*O2       (2)
  !  R77:  O + O + M   --->  O2 + M     (3)
  !
  ! Odd hydrogen:
  !  R7 :  H + O3      --->  OH + O2    (4)
  !  R8 :  O + OH      --->  H + O2     (5)
  !  R9 :  O + HO2     --->  OH + O2    (6)
  !  R18:  H + O2 + M  --->  HO2 + M    (7)
  !
  ! AUTHORS: C. Fu, J.de Grandpre, J.C.McConnell, V. Fomichev (Feb-Aug 98)
  ! modified:  C.F. (Oct 99)
  !
  ! METHOD: statistics using the data of model chemistry
  !
  ! Input fields:
  ! AIRROW: density of air [molecules/cm3]
  ! ARK   : all gas reaction coefficients, the concentrations of all species
  !
  ! Output fields:
  ! HEATCH: net chemical heating rate [K/s] on chemical grid (top -> surface)
  !         The HEATCH array is to be passed to MAMRAD
  ! --------------------------------------------------------------------------

  use msizes, only: ilg, ilev
  use chem_params_mod, only : jpnrks, rc
  use tracers_info_mod, only: itoo, ito3, ith0, ith1, ith2

  implicit none

  integer, intent(in) :: il1, il2, ntrac
  real, dimension(ilg,ilev,ntrac), intent(in) :: xrow
  real(kind=rc), dimension(jpnrks,ilg,ilev), intent(in) :: ark

  real, dimension(ilg,ilev), intent(out) :: heatch

  integer :: j, k
  real(kind=rc) :: conv1, qo2
  real(kind=rc)  :: qh, qho2, qoh, qo3, qo3p

  ! EFF: efficiency of thermalization for each reaction according to
  !      Mlynczack and Solomon: JGR, v 98, p 10517, 1993
  real, dimension(7), parameter :: eff = &
            [ 1.0, 1.0, 1.0, 0.6, 1.0, 1.0, 1.0 ]

  ! ENTH: enthalpy (heat) of each reaction [kcal/mole]
  real, dimension(7), parameter :: enth = &
            [ 25.47, 93.65, 119.40, 76.90, 16.77, 53.27, 49.10 ]

  ! --------------------------------------------------------------------------
  ! Initialization:
  heatch(:,:) = 0.0

  ! Conversion coefficient, to calculate chemical heating in K/s:
  conv1 = 4180.0_rc / (28.97_rc * 1.005_rc)

  qo2 = 0.209_rc
  do k = 1, ilev
    do j = il1, il2

      qo3p = real(xrow(j,k,itoo), kind=rc)
      qo3  = real(xrow(j,k,ito3), kind=rc)
       qh  = real(xrow(j,k,ith0), kind=rc)
      qoh  = real(xrow(j,k,ith1), kind=rc)
      qho2 = real(xrow(j,k,ith2), kind=rc)
      heatch(j,k) = real(conv1 *  &
                 (  ark(1,j,k) * qo3p * qo2  * enth(1) * eff(1) &
                  + ark(2,j,k) * qo3p * qo3  * enth(2) * eff(2) &
                  + ark(77,j,k)* qo3p * qo3p * enth(3) * eff(3) &
                  + ark(7,j,k) * qh   * qo3  * enth(4) * eff(4) &
                  + ark(8,j,k) * qo3p * qoh  * enth(5) * eff(5) &
                  + ark(9,j,k) * qo3p * qho2 * enth(6) * eff(6) &
                  + ark(18,j,k)* qh   * qo2  * enth(7) * eff(7) ))
    enddo
  enddo

  return
  end subroutine chemheat

! -----------------------------------------------------------------------------

end module chemistry_mod
