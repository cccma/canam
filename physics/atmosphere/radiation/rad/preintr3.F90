!> \file
!>\brief Determine the parameters needed to interpolate in H2O/CO2 ratio for gas optical properties
!!
!! @author Jiangnan Li
subroutine preintr3 (inpr, dir, q, co2, rhc, il1, il2, ilg, lay)
  !
  !     * feb 09,2009 - j.li.     new version for gcm15h:
  !     *                         - co2 passed directly, thus no need
  !     *                           for "trace" common block.
  !     * apr 21,2008 - l.solheim. previous version printr2 for gcm15g:
  !     *                          - cosmetic change to add threadprivate
  !     *                            for common block "trace", in support
  !     *                            of "radforce" model option.
  !     * apr 25,2003 - j.li.  previous version preintr for gcm15e/gcm15f.
  !----------------------------------------------------------------------c
  !     this subroutine determines the interpretion points for the ratio c
  !     of h2o and co2.                                                  c
  !                                                                      c
  !     inpr:  number of the ratio level for the standard 5 ratios       c
  !     dir:   interpretation factor for mass ratio of h2o / co2         c
  !            between two neighboring standard input ratios             c
  !     q:     water vapor mass mixing ratio                             c
  !     co2:   co2 mass mixing ratio                                     c
  !     rhc:   the ratio of the h2o mass mixing to co2 mass mixing       c
  !----------------------------------------------------------------------c
  implicit none
  integer :: i
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: j
  integer :: jendr
  integer :: k
  integer :: l
  integer, intent(in) :: lay  !< Number of vertical layers \f$[unitless]\f$
  integer :: lp1
  !
  real, intent(inout), dimension(ilg,lay) :: dir !< Interpretation factor for H2O/CO2 ratio \f$[1]\f$
  real, intent(in), dimension(ilg,lay) :: q !< H2O mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: co2 !< CO2 mixing ratio \f$[gram/gram]\f$
  real, intent(inout), dimension(ilg,lay) :: rhc !< Ratio of H2O / CO2 \f$[1]\f$
  real, dimension(5) :: standr
  integer, intent(inout), dimension(ilg,lay) :: inpr !< Index of standard input H2O/CO2 ratio for each model layer \f$[1]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  data standr / .06,  .24,  1., 4., 16. /
  !=======================================================================
  jendr  =  5
  do k = 1, lay
    !
    do i = il1, il2
      inpr(i,k)   =  0
      rhc(i,k)    =  q(i,k) / max(co2(i,k), 1.0e-10)
    end do ! loop 100
    !
    do j = 1, jendr
      do i = il1, il2
        if (rhc(i,k) > standr(j)) then
          inpr(i,k) =  inpr(i,k) + 1
        end if
      end do
    end do ! loop 200
    !
    do i = il1, il2
      l   =  inpr(i,k)
      lp1 =  l + 1
      if (l >= 1 .and. l < 5) then
        dir(i,k)  = (rhc(i,k) - standr(l)) / &
                    (standr(lp1) - standr(l))
      else
        !
        !----------------------------------------------------------------------c
        !     dir is not used with values of {0,5} in tlinehc, but we          c
        !     initialize here to avoid problems with nan when used             c
        !     in multitasking mode.                                            c
        !----------------------------------------------------------------------c
        !
        dir(i,k)  =  0.0
      end if
    end do ! loop 300
    !
  end do ! loop 400
  !
  return
end
!> \file
!> Determines the interpretion points for the ratio of H2O and CO2.  
!! STAND shows 5 standard values of the ratios of H2O/CO2, the 
!! absorption coeffients of the comined H2O and CO2 are
!! pre-calculated for the 5 ratios, general results are the
!! interpolations based on the 5 values.