!> \file
!>\brief Determine the parameters needed to interpolate in pressure for gas optical properties
!!
!! @author Jiangnan Li
!
subroutine preintp2(inpt, inptm, dip, dip0, p, il1, il2, ilg, lay)
  !
  !     * may 05,2006 - m.lazare. new version for gcm15e:
  !     *                         - implement rpn fix for inpt/inptm and
  !     *                           cosmetic reorganization.
  !     * original version preintp by jiangnan li.
  !----------------------------------------------------------------------c
  !     this subroutine determines the pressure interpretation points    c
  !                                                                      c
  !     inpt:  number of the level for the standard input data pressures c
  !            (for 28 interpretation levels)                            c
  !     inptm: number of the level for the standard input data pressures c
  !            (for 18 interpretation levels below 1 mb)                 c
  !     p:     pressure at middle of each layer                          c
  !     dip:   interpretation factor for pressure between two            c
  !            neighboring standard input data pressure levels           c
  !     dip0:  interpretation factor for pressure above model top level  c
  !----------------------------------------------------------------------c
  implicit none
  integer :: i
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: inp1
  integer :: inpdif
  integer :: j
  integer :: jends
  integer :: k
  integer, intent(in) :: lay  !< Number of vertical layers \f$[unitless]\f$
  integer :: m
  integer :: n
  real :: p0
  real :: pm
  real :: x
  !
  real, intent(inout), dimension(ilg,lay) :: dip !< Interpolation facter between two neighboring 
                                                 !! standard input pressure levels \f$[1]\f$
  real, intent(inout), dimension(ilg) :: dip0 !< As for DIP but between model top and top of atmosphere (0.005 hPa) \f$[1]\f$
  real, intent(in), dimension(ilg,lay) :: p !< Pressure at layer mid-point \f$[hPa]\f$
  real, dimension(28) :: standp
  integer, intent(inout), dimension(ilg,lay) :: inpt !< Index of standard input pressures for each model layer \f$[1]\f$
  integer, intent(inout), dimension(ilg,lay) :: inptm !< Index of standard input pressures for each model layer with pressure
                                                      !! less than 1 hPa \f$[1]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  data standp / 5.0000e-04, 1.4604e-03, 2.9621e-03, 6.0080e-03, &
               1.2186e-02, 2.4717e-02, 5.0134e-02, 1.0169e-01, &
               2.0625e-01, 4.1834e-01, &
               1.2180, 1.8075, 2.6824, 3.9806, 5.9072, 8.7662, &
               13.0091, 19.3054, 28.6491, 42.5151, 63.0922, &
               93.6284, 138.9440, 206.1920, 305.9876, 454.0837, &
               673.8573, 1000.0000 /
  !
  jends = 27
  do k = 1, lay
    do i = il1, il2
      inpt(i,k)   =  0
    end do ! loop 100
    !
    do j = 1, jends
      do i = il1, il2
        if (p(i,k) > standp(j)) then
          inpt(i,k) =  inpt(i,k) + 1
        end if
      end do
    end do ! loop 200
    !
    !----------------------------------------------------------------------c
    !     calculate arrays dip and dit required later for gasopt routines. c
    !     also, set values of inpt for a given level to be negative if all c
    !     longitude values are the same. this is also used in the gasopt   c
    !     routines to improve performance by eliminating the unnecessary   c
    !     indirect-addressing if inpl is negative for a given level.       c
    !     note that for inpt=0, it is assumed that levels are more or      c
    !     less horizontal in pressure, so scaling by -1 still preserves    c
    !     the value of zero and no indirect-addressing is done in the      c
    !     gasopt routines.                                                 c
    !----------------------------------------------------------------------c
    !
    inpdif =  0
    inp1   =  inpt(1,k)
    do i = il1, il2
      if (inpt(i,k) /= inp1)  inpdif = 1
      m  =  inpt(i,k)
      n  =  m + 1
      if (m > 0) then
        dip(i,k)  = (p(i,k) - standp(m)) / (standp(n) - standp(m))
      else
        dip(i,k)  =  p(i,k) / standp(1)
      end if
    end do ! loop 300
    !
    if (inpdif == 0) then
      do i = il1, il2
        inpt(i,k) =  inpt(i,k) + 1000
      end do ! loop 400
    end if
    !
    do i = il1, il2
      inptm(i,k)  =  inpt(i,k) - 10
    end do ! loop 500
  end do ! loop 600
  !
  !----------------------------------------------------------------------c
  !     interpretation factor for lattenu and sattenu (attenuation above c
  !     model top                                                        c
  !----------------------------------------------------------------------c
  !
  pm =  p(1,1)
  do i = il1, il2
    pm          =  min (pm, p(i,1))
  end do ! loop 700
  !
  if (pm <= 0.0005) then
    do i = il1, il2
      dip0(i)   =  0.0
    end do ! loop 800
  else
    do i = il1, il2
      p0        =  p(i,1) * p(i,1) / p(i,2)
      x         =  sqrt (p0 * p(i,1))
      dip0(i)   = (x - p(i,1)) / (p0 - p(i,1))
    end do ! loop 900
  end if
  !
  return
end
!> \file
!> Determines nearest pressure interpretation points for each layer.
!! INPT for 28 pressure level up to 0.001 mb which used in GH case
!! INPTM for 18 pressure level up to 1 mb which used in non GH case 