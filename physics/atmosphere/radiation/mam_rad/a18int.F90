!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine a18int(x1,y1,x2,y2,n1,n2)
  !****************************************************************************
  !*                                                                          *
  !*                    subroutine a18int                                     *
  !*                                                                          *
  !****************************************************************************
  !
  !         third order spline interpolation
  ! input argument and function:  x1(1:n1),y1(1:n1)
  ! output argument and function: x2(1:n2)x2(1:n2),y2(1:n2)
  ! the necessary conditionts are: x1(i) < x1(i+1), and the same for x2 array.
  !
  ! called by cco2gr
  ! calls nothing


  implicit none
  real :: f1
  real :: f2
  real :: f3
  real :: g
  real :: h1
  real :: h2
  integer :: i
  integer :: k
  integer :: kr
  integer :: l
  integer, intent(in) :: n1
  integer, intent(in) :: n2
  integer :: nvs
  real, intent(in), dimension(n1) :: x1 !< Variable description\f$[units]\f$
  real, intent(in), dimension(n2) :: x2 !< Variable description\f$[units]\f$
  real, intent(in), dimension(n1) :: y1 !< Variable description\f$[units]\f$
  real, intent(inout)   :: y2(n2) !< Variable description\f$[units]\f$
  real   :: a(150) !<
  real   :: e(150) !<
  real   :: f(150) !<
  real   :: h(150) !<
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  h2=x1(1)
  nvs=n1-1
  do k=1,nvs
    h1=h2
    h2=x1(k+1)
    h(k)=h2-h1
  end do ! loop 1
  a(1)=0.
  a(n1)=0.
  e(n1)=0.
  f(n1)=0.
  h1=h(n1-1)
  f1=y1(n1-1)
  f2=y1(n1)
  do kr=2,nvs
    k=nvs+2-kr
    h2=h1
    h1=h(k-1)
    f3=f2
    f2=f1
    f1=y1(k-1)
    g=1/(h2*e(k+1)+2.*(h1+h2))
    e(k)=-h1*g
    f(k)=(3.*((f3-f2)/h2-(f2-f1)/h1)-h2*f(k+1))*g
  end do ! loop 2
  g=0.
  do k=2,nvs
    g=e(k)*g+f(k)
    a(k)=g
  end do ! loop 3
  l=1
  do i=1,n2
    g=x2(i)
    do k=l,nvs
      if (g<=x1(k+1) .or. k==nvs) then
        l=k
        exit
      end if
    end do ! loop 6
    g=g-x1(l)
    h2=h(l)
    f2=y1(l)
    f1=h2**2
    f3=g**2
    y2(i)=f2+g/h2*(y1(l+1)-f2-(a(l+1)*(f1-f3)+ &
                a(l)*(2.*f1-3.*g*h2+f3))/3.)
  end do ! loop 4
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
