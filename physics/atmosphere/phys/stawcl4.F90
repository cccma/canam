!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine stawcl4 (stawsij, shbj,shbj1,shj, &
                    nkm1,nk,nkp1,il1,il2,ilg,rgocp)

  !     * sep 06/07 - m.lazare.  new version for gcm15f onward:
  !     *                        arrays real(8) :: instead of real :: to
  !     *                        support running 64-bit physics in
  !     *                        32-bit driver. note that stawcl3
  !     *                        was in "COMM" and this version must
  !     *                        be accessible in "MODEL".
  !     * dec 05/88 - m.lazare - previous version stawcl2 up to gcm15e.
  !
  !     * define a column of matrix staws(nk-1,6) used in
  !     * moist convective adjustment.
  !     * i   = this longitude
  !     * ilg = dimension in longitude
  !     * lon = number of distinct longitudes.

  implicit none
  real :: delk
  real :: delkp1
  integer :: i
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: k
  integer, intent(in) :: nk
  integer, intent(in) :: nkm1
  integer, intent(in) :: nkp1
  real, intent(in) :: rgocp  !< Ideal gas constant divided by heat capacity for dry air \f$[unitless]\f$
  real :: sfk
  real :: sfkp1
  real :: x
  !
  real, intent(in), dimension(ilg,nk) :: shbj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,nk) :: shj   !< Eta-level for mid-point of thermodynamic layer \f$[unitless]\f$
  real, intent(in), dimension(ilg) :: shbj1 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,nkm1,6) :: stawsij !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !-----------------------------------------------------------------------
  if (nkm1 /= nk - 1 .or. nkp1 /= nk + 1) call xit('STAWCL4', - 1)

  do k = 2,nk - 1
    do i = il1,il2
      sfk        = log(shbj(i  ,k)/shbj(i,k - 1))
      sfkp1      = log(shbj(i,k + 1)/shbj(i  ,k))
      delk       =     shbj(i  ,k) - shbj(i,k - 1)
      delkp1     =     shbj(i,k + 1) - shbj(i  ,k)
      x          = 1.e0/(sfk  + sfkp1)
      stawsij(i,k,1) = x * (sfkp1 + 2.e0 * sfk)/3.e0
      stawsij(i,k,2) = x * (sfk   + 2.e0 * sfkp1)/3.e0
      stawsij(i,k,3) = 2.e0 * x/rgocp
      stawsij(i,k,5) = - delkp1/delk
      stawsij(i,k,6) = (shbj(i,k - 1)/shj(i,k + 1)) ** (1.e0/3.e0)
    end do
  end do ! loop 10
  !
  !     * for compatibility with earlier staggered model.
  !
  k = 1
  do i = il1,il2
    sfk        = log(shbj(i  ,k)/shbj1(i))
    sfkp1      = log(shbj(i,k + 1)/shbj(i  ,k))
    delk       =     shbj(i  ,k) - shbj1(i)
    delkp1     =     shbj(i,k + 1) - shbj(i  ,k)
    x          = 1.e0/(sfk  + sfkp1)
    stawsij(i,k,1) = x * (sfkp1 + 2.e0 * sfk)/3.e0
    stawsij(i,k,2) = x * (sfk   + 2.e0 * sfkp1)/3.e0
    stawsij(i,k,3) = 2.e0 * x/rgocp
    stawsij(i,k,5) = - delkp1/delk
    stawsij(i,k,6) = (shbj1(i)/shj(i,k + 1)) ** (1.e0/3.e0)
  end do ! loop 15
  !
  do k = 1,nk - 1
    do i = il1,il2
      stawsij(i,k,4) = 1.e0/( stawsij(i,k,2) - stawsij(i,k,3) &
                       + stawsij(i,k,5) * (stawsij(i,k,1) + stawsij(i,k,3)))
    end do
  end do ! loop 20
  !
  return
  !-----------------------------------------------------------------------
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
