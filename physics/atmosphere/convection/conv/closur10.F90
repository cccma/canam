!> \file
!> \brief  The purpose of this subroutine is to evaluate the base updraft mass
!! flux for the Zhang-McFarlane convection scheme.
!!
!! @author Norman McFarlane, John Scinocca
!

subroutine closur10(q,t,p,z,s,tp,qs,qu,su,mc,du,mu,md,qd,sd,alpha, &
                    qhat,shat,dp,qstp,zf,ql,dsubcld,mb,qdb,sdb,dsr, &
                    ug,vg,betau,betad,cape,tl,lcl,lel,jt,mx,j0, &
                    delt,ilev,ilg,il1g,il2g,msg,capelmt)
  !
  !     * apr 30/2012 - m.lazare.    new version for gcm16:
  !     *                            alf increased from 1.e8 to 5.e8.
  !     * dec 18/2007 - m.lazare     previous version closur9 for gcm15g/h/i:
  !     *                            alf decreased from 1.e9 to 1.e8.
  !     * jun 15/2006 - m.lazare.    previous version closur8 for gcm15f:
  !     *                            - now uses internal work arrays
  !     *                              instead of passed workspace.
  !     *                            - "ZERO" data constant added
  !     *                               and used in call to intrinsics.
  !     * dec 07/2004 - m.lazare.    previous version closur7 for gcm15c/d/e:
  !     *                            prognostic closure now default (iclos=1).
  !
  use phys_consts, only : grav, rgas, cpres, eps1, t1s
  use phys_parm_defs, only : ap_alf, ap_taus1
  implicit none
  real, intent(in) :: capelmt
  real, intent(in) :: delt  !< Timestep for atmospheric model \f$[seconds]\f$
  integer, intent(in) :: il1g  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2g  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: msg
  !
  !     * i/o fields:
  !
  real, intent(in)  , dimension(ilg,ilev) :: q !< Ambient water vapor mixing ratio \f$[g/g]\f$
  real, intent(in)  , dimension(ilg,ilev) :: t !< Ambient temperature \f$[K]\f$
  real, intent(in)  , dimension(ilg,ilev) :: z !< Height above the surface of mid-layer levels \f$[m]\f$
  real, intent(in)  , dimension(ilg,ilev) :: s !< Ambient dry static energy /specific heat \f$[K]\f$
  real, intent(in)  , dimension(ilg,ilev) :: p !< Ambient pressure \f$[hPa]\f$
  real, intent(in)  , dimension(ilg,ilev) :: tp !< Parcel temperature \f$[K]\f$
  real, intent(in)  , dimension(ilg,ilev) :: qs !< Ambient saturation mixing ratio \f$[g/g]\f$
  real, intent(in)  , dimension(ilg,ilev) :: qu !< Updraft mixing ratio \f$[g/g]\f$
  real, intent(in)  , dimension(ilg,ilev) :: su !< Updraft dry static energy/specific heat \f$[K]\f$
  real, intent(in)  , dimension(ilg,ilev) :: mc !< Total mass flux/unit base mass flux \f$[1]\f$
  real, intent(in)  , dimension(ilg,ilev) :: du !< Updraft detrainment rate/unit base mass flux \f$[m^{-1})]\f$
  real, intent(in)  , dimension(ilg,ilev) :: mu !< Updraft mass flux/unit base mass flux \f$[1]\f$
  real, intent(in)  , dimension(ilg,ilev) :: md !< Downdraft mass flux per unit base mass flux \f$[1]\f$
  real, intent(in)  , dimension(ilg,ilev) :: qd !< Downdraft water vapor mixing ratio \f$[g/g]\f$
  real, intent(in)  , dimension(ilg,ilev) :: sd !< Downdraft dray static energy/specific heat \f$[K]\f$
  real, intent(in)  , dimension(ilg,ilev) :: alpha !< Vertical differencing parameter (=1 for upstream) \f$[1]\f$
  real, intent(in)  , dimension(ilg,ilev) :: qhat !< Water vapor mixing ratio at layer interface levels \f$[g/g]\f$
  real, intent(in)  , dimension(ilg,ilev) :: shat !< Dry static energy/(specific heat) at layer interface levels \f$[K]\f$
  real, intent(in)  , dimension(ilg,ilev) :: dp !< Layer pressure thickness \f$[hPa]\f$
  real, intent(in)  , dimension(ilg,ilev) :: qstp !< Parcel saturation mixing ratio \f$[g/g]\f$
  real, intent(in)  , dimension(ilg,ilev) :: zf !< Interface level heights \f$[m]\f$
  real, intent(in)  , dimension(ilg,ilev) :: ql !< Updraft liquid water mixing ratio \f$[g/g]\f$
  real, intent(in)  , dimension(ilg,ilev) :: ug !< Eastward component of the ambient air velocity \f$[m/s]\f$
  real, intent(in)  , dimension(ilg,ilev) :: vg !< Northward component of the ambient air velocity \f$[m/s]\f$
  real, intent(in)  , dimension(ilg,ilev) :: dsr !< Density of ambient air to the density of dry air \f$[1]\f$

  real, intent(inout)  , dimension(ilg) :: mb !< Base level mass flux \f$[kg/m^{2}/s]\f$
  real, intent(in)  , dimension(ilg) :: qdb !< Downdraft water vapor mixing ratio at cloud base  \f$[g/g]\f$
  real, intent(in)  , dimension(ilg) :: sdb !< Downdraft dry static energy/(specific heat) at cloud base \f$[K]\f$
  real, intent(in)  , dimension(ilg) :: betau !< Updraft mass fluxes at cloud base \f$[kg/m^{2}/s]\f$
  real, intent(in)  , dimension(ilg) :: betad !< Downdraft mass fluxes at cloud base \f$[kg/m^{2}/s]\f$
  real, intent(in)  , dimension(ilg) :: cape !< Convective available potential energy \f$[J/kg]\f$
  real, intent(in)  , dimension(ilg) :: tl !< parcel saturation temperature (i.e. temperature at the parcel LCL) \f$[K]\f$
  real, intent(in)  , dimension(ilg) :: dsubcld !< pressure thickness of the layer between the base level and the parcel LCL \f$[hPa]\f$

  integer, intent(in),dimension(ilg) :: lcl !< Index of the level closest to the lifting condensation level of the parcel \f$[1]\f$
  integer, intent(in),dimension(ilg) :: lel !< Index of the highest level that can be reached by a buoyant parcel \f$[1]\f$
  integer, intent(in),dimension(ilg) :: jt !< Index of the top of the convective layer \f$[1]\f$
  integer, intent(in),dimension(ilg) :: mx !< Base level index \f$[1]\f$
  integer, intent(in),dimension(ilg) :: j0 !< Index of the level of the base of the detrainment layer \f$[1]\f$

  !==================================================================
  ! physical (adjustable) parameters
  !
  !   real :: tau !< CAPE depletion time scale \f$[s]\f%
  !            !! Its minimum/default/maximum is [1800/2400/7200]
  real, parameter :: tau = 2400.
  !
  !   real :: alf !< Proportionality parameter \f$[m^{4}kg^{-1}]\f$
  !            !! Its minimum/default/maximum is [?/\f$5\vartimes10^{8}\f$/?]
  !
  !   real :: taus1 !< Damping time scale \f$[s]\f$
  !              !! Its minimum/default/maximum is [?/21600/?]
  !
  !   real :: capelmt !< Threshold value of convective available potential energy \f$[J/kg]\f$
  !                !! Its minimum/default/maximum is [0/0/?]
  integer, parameter :: iclos = 1
  !==================================================================
  !
  !     * internal work fields:
  !
  real :: adz
  real :: alf
  real :: beta
  real :: debdt
  real :: dltaa
  real :: eb
  real :: fr
  integer :: il
  integer :: k
  real :: refm
  real :: taus1
  real :: tausmin
  real  , dimension(ilg,ilev) :: dtpdt
  real  , dimension(ilg,ilev) :: dqsdtp
  real  , dimension(ilg,ilev) :: dtmdt
  real  , dimension(ilg,ilev) :: dqmdt
  real  , dimension(ilg,ilev) :: dboydt
  real  , dimension(ilg,ilev) :: thetavp
  real  , dimension(ilg,ilev) :: thetavm
  real  , dimension(ilg)      :: dtbdt
  real  , dimension(ilg)      :: dqbdt
  real  , dimension(ilg)      :: dtldt
  real  , dimension(ilg)      :: rl
  real  , dimension(ilg)      :: pmax
  real  , dimension(ilg)      :: tmax
  real  , dimension(ilg)      :: qmax
  real  , dimension(ilg)      :: qhatmax
  real  , dimension(ilg)      :: shatmax
  real  , dimension(ilg)      :: sumax
  real  , dimension(ilg)      :: sdmax
  real  , dimension(ilg)      :: mumax
  real  , dimension(ilg)      :: mdmax
  real  , dimension(ilg)      :: qumax
  real  , dimension(ilg)      :: qdmax
  real  , dimension(ilg)      :: dadt
  real  , dimension(ilg)      :: dzt
  real  , dimension(ilg)      :: vshear
  real  , dimension(ilg)      :: ugb
  real  , dimension(ilg)      :: vgb
  real  , dimension(ilg)      :: mbtest
  !
  real, parameter :: zero = 0.
  !---------------------------------------------------------------------------
  ! cc   *************************************************************
  ! cc   change of subcloud layer properties due to convection is
  ! cc   related to cumulus updrafts and downdrafts.
  ! cc   mc(z)=f(z)*mb, mub=betau*mb, mdb=betad*mb are used
  ! cc   to define betau, betad and f(z).
  ! cc   note that this implies all time derivatives are in effect
  ! cc   time derivatives per unit cloud-base mass flux, i.e. they
  ! cc   have units of 1/mb instead of 1/sec.
  ! cc   *************************************************************
  !
  !     * constants for prognostic closure scheme.
  !
  ALF=ap_alf
  TAUS1=ap_taus1
  refm = 1./2.4e-4
  tausmin = 180.
  !
  !     * obtain gathered fields at "MX" for ensuing use.
  !     * initialize "DADT" array.
  !
  do k = msg + 1,ilev
    do il = il1g,il2g
      if (k == mx(il)) then
        pmax   (il) = p   (il,k)
        tmax   (il) = t   (il,k)
        qmax   (il) = q   (il,k)
        qhatmax(il) = qhat(il,k)
        qumax  (il) = qu  (il,k)
        qdmax  (il) = qd  (il,k)
        shatmax(il) = shat(il,k)
        sumax  (il) = su  (il,k)
        sdmax  (il) = sd  (il,k)
        mumax  (il) = mu  (il,k)
        mdmax  (il) = md  (il,k)
      end if
    end do
  end do ! loop 50
  !
  do il = il1g,il2g
    dadt   (il) = 0.
    dzt    (il) = 0.
    vshear (il) = 0.
    ugb    (il) = ug(il,lcl(il))
    vgb    (il) = vg(il,lcl(il))
    eb          = pmax(il) * qmax(il)/(eps1 + qmax(il))
    dtbdt(il)   = (1./dsubcld(il)) &
                  * (mumax(il) * (shatmax(il) - sumax(il) ) &
                  + mdmax(il) * (shatmax(il) - sdmax(il) ) )
    dqbdt(il)   = (1./dsubcld(il)) &
                  * (mumax(il) * (qhatmax(il) - qumax(il) ) &
                  + mdmax(il) * (qhatmax(il) - qdmax(il) ) )
    debdt       = eps1 * pmax(il)/(eps1 + qmax(il)) ** 2 * dqbdt(il)
    dtldt(il)   = - 2840. * (3.5/tmax(il) * dtbdt(il) - debdt/eb)/ &
                  (3.5 * log(tmax(il)) - log(eb) - 4.805) ** 2
  end do ! loop 100
  !
  ! cc   ******************************************************************
  ! cc     dtmdt and dqmdt are cumulus heating and drying.
  ! cc   ******************************************************************
  !
  do k = msg + 1,ilev
    do il = il1g,il2g
      dtmdt(il,k) = 0.
      dqmdt(il,k) = 0.
    end do
  end do ! loop 150
  !
  do k = msg + 1,ilev - 1
    do il = il1g,il2g
      if (k == jt(il)) then
        !         rl(il)=(2.501-.00237*(t(il,k+1)-t1s))*1.e6
        rl(il) = 2.501e6
        dtmdt(il,k) = (dsr(il,k)/dp(il,k)) &
                      * (mu(il,k + 1) * (su(il,k + 1) - shat(il,k + 1) &
                      - rl(il)/cpres * ql(il,k + 1) ) + &
                      md(il,k + 1) * (sd(il,k + 1) - shat(il,k + 1)) )
        dqmdt(il,k) = (dsr(il,k)/dp(il,k)) &
                      * (mu(il,k + 1) * (qu(il,k + 1) - qhat(il,k + 1) + ql(il,k + 1)) + &
                      md(il,k + 1) * (qd(il,k + 1) - qhat(il,k + 1)) )
      end if
    end do
  end do ! loop 175
  !
  beta = 1.
  do k = msg + 1,ilev - 1
    do il = il1g,il2g
      if (k > jt(il) .and. k < mx(il)) then
        !         rl(il)=(2.501-.00237*(t(il,k)-t1s))*1.e6
        rl(il) = 2.501e6
        dtmdt(il,k) = dsr(il,k) * (mc(il,k) * (shat(il,k) - s(il,k)) &
                      + mc(il,k + 1) * (s(il,k) - shat(il,k + 1)))/dp(il,k) &
                      - rl(il)/cpres * du(il,k) * (beta * ql(il,k) + (1 - beta) * ql(il,k + 1))
        dqmdt(il,k) = dsr(il,k) * (mc(il,k) * (qhat(il,k) - q(il,k)) &
                      + mc(il,k + 1) * (q(il,k) - qhat(il,k + 1)))/dp(il,k) &
                      + du(il,k) * (qs(il,k) - q(il,k)) &
                      + du(il,k) * (beta * ql(il,k) + (1 - beta) * ql(il,k + 1))
      end if
    end do
  end do ! loop 180
  !
  do k = msg + 1,ilev
    do il = il1g,il2g
      if (k >= lel(il) .and. k <= lcl(il)) then
        !         rl(il)=(2.501-.00237*(tp(il,k)-t1s))*1.e6
        rl(il) = 2.501e6
        thetavp(il,k) = tp(il,k) * (1000./p(il,k)) ** (rgas/cpres) &
                        * (1. + 1.608 * qstp(il,k) - qmax(il))
        thetavm(il,k) = t(il,k) * (1000./p(il,k)) &
                        ** (rgas/cpres) * (1. + 0.608 * q(il,k))
        dqsdtp(il,k) = qstp(il,k) * (1. + qstp(il,k)/eps1) &
                       * eps1 * rl(il)/(rgas * tp(il,k) ** 2)
        ! cc     ******************************************************************
        ! cc       dtpdt is the parcel temperature change due to change of
        ! cc       subcloud layer properties during convection.
        ! cc     ******************************************************************
        dtpdt(il,k) = tp(il,k) &
                      /(1. + rl(il)/cpres * (dqsdtp(il,k) - qstp(il,k)/tp(il,k))) * &
                      (dtbdt(il)/tmax(il) + rl(il)/cpres * &
                      (dqbdt(il)/tl(il) - qmax(il)/tl(il) ** 2 * dtldt(il) &
                      ))
        ! cc     ******************************************************************
        ! cc       dboydt is the integrand of cape change.
        ! cc     ******************************************************************
        dboydt(il,k) = ((dtpdt(il,k)/tp(il,k) &
                       + 1./(1. + 1.608 * qstp(il,k) - qmax(il)) &
                       * (1.608 * dqsdtp(il,k) * dtpdt(il,k) - dqbdt(il))) &
                       - (dtmdt(il,k)/t(il,k) + 0.608/(1. + 0.608 * q(il,k)) &
                       * dqmdt(il,k))) * grav * thetavp(il,k)/thetavm(il,k)
        ! cc     ******************************************************************
      end if
    end do
  end do ! loop 200
  !
  do k = msg + 1,ilev
    do il = il1g,il2g
      if (k > lcl(il) .and. k < mx(il)) then
        !         rl(il)=(2.501-.00237*(tp(il,k)-t1s))*1.e6
        rl(il) = 2.501e6
        thetavp(il,k) = tp(il,k) * (1000./p(il,k)) ** (rgas/cpres) &
                        * (1. + 0.608 * qmax(il))
        thetavm(il,k) = t(il,k) * (1000./p(il,k)) &
                        ** (rgas/cpres) * (1. + 0.608 * q(il,k))
        ! cc     ******************************************************************
        ! cc       dboydt is the integrand of cape change.
        ! cc     ******************************************************************
        dboydt(il,k) = (dtbdt(il)/tmax(il) &
                       + 0.608/(1. + 0.608 * qmax(il)) * dqbdt(il) &
                       - dtmdt(il,k)/t(il,k) &
                       - 0.608/(1. + 0.608 * q(il,k)) * dqmdt(il,k)) &
                       * grav * thetavp(il,k)/thetavm(il,k)
        ! cc     ******************************************************************
      end if
    end do
  end do ! loop 250
  !
  if (iclos == 0) then
    !
    do k = msg + 1,ilev
      do il = il1g,il2g
        if (k >= lel(il) .and. k < mx(il)) then
          dadt(il) = dadt(il) + dboydt(il,k) * (zf(il,k) - zf(il,k + 1))
        end if
      end do
    end do ! loop 300
    !
    do il = il1g,il2g
      mb(il)      = 0.
      dltaa = - 1. * (cape(il) - capelmt)
      if (dadt(il) /= 0.)     mb(il) = max(dltaa/tau/dadt(il),zero)
    end do ! loop 350
    !
  else
    !
    do k = msg + 1,ilev
      do il = il1g,il2g
        if (k >= lel(il) .and. k < mx(il)) then
          adz = zf(il,k) - zf(il,k + 1)
          dadt(il) = dadt(il) + dboydt(il,k) * adz
        end if
      end do
    end do ! loop 400
    !
    do il = il1g,il2g
      dltaa = (cape(il) - capelmt)
      if (dadt(il) < 0. .and. dltaa > 0.) then
        fr = - dadt(il)
        mb(il) = (dltaa/alf * delt + mb(il)) &
                 /(1. + delt/taus1)
      else
        mb(il) = 0.
      end if
    end do ! loop 450
  end if
  !
  return
end subroutine closur10

!> \file
!! \section sec_theory Theoretical formulation
!!
!! There are two closure options available, controlled by the ICLOS variable. The original
!! diagnostic CAPE tendency closure of \cite Zhang1995 (hereinafter
!! ZM) is activated with the setting ICLOS=0. The prognostic closure
!! option of \cite Scinocca2004 is accessed by setting ICLOS=1.
!! The prognostic closure is the operational default for CanAM4.
!! \n
!! \n
!! NOTE: The vertical coordinate in CanAM is pressure based. All vertical
!! derivatives are expressed in terms of a local pressure coordinate
!! using the hydrostatic relationship, and approximated by centered finite
!! differences with level indexing being from the top downward. Model
!! prognostic variables (hereinafter denoted as ambient variables) are
!! defined at the mid-levels of layers. Cumulus updraft and downdraft
!! mass fluxes, dry and moist static energy, water vapor and liquid water
!! mixing ratios are defined at layer interface levels.
!! \n
!! \subsection ssec_cape_closure_iclos_0 CAPE tendency closure (ICLOS=0)
!!
!! The formulation of this closure condition is as described in ZM. It
!! assumes that the effect of the deep convection as represented by the
!! ZM scheme is to exponentially deplete convective available potential
!! energy (CAPE) at rate that is specified in terms of a prescribed damping
!! time scale \f$\tau\f$ such that
!! \n
!! \f{equation}{
!! \left(\frac{\partial CAPE}{\partial t}\right)_{conv}=-\frac{CAPE}{\tau}\tag{1}
!! \f}
!! \n
!! The convective available potential energy is evaluated in the buoyana.f
!! routine in terms of the buoyancy of an undiluted parcel of air lifted
!! adiabatically and reversibly (i.e. all condensate is carried with
!! the parcel) from the base level to the level of neutral buoyance (LNB) (see the documentation
!! for buoyana.f). Mixed-phase and ice-phase processes are ignored in the
!! lifting process so that condensation involves production of liquid
!! water only. In addition to conservation of total water (vapor + condensate),
!! it is assumed that the equivalent potential temperature the parcel
!! is conserved during the lifting process. It is assumed that the gas
!! constants, specific heat and latent heat of vaporization are all strictly
!! constants. Mixed-phase and ice phase effects are ignored. Therefore
!! the parcel equivalent potential temperature is defined approximately
!! as
!! \n
!! \f{equation}{
!! \theta_{e}^{(p)}=\theta^{(p)}exp\left(\frac{L_{v}r^{(p)}}{c_{p}T^{(p)}}\right)\tag{2}
!! \f}
!! \n
!! As noted above, this approximation ignores temperature and humidity
!! dependance of gas constants and specific heat and also sets the relative
!! humidity factor to unity (cf. \cite Emanuel1994, sec. 4.5).
!! \n
!! For the purposes of evaluating its tendency due to deep convection,
!! the convective available potential energy is defined in terms of the
!! density potential temperatures of the parcel and environment (cf. \cite Emanuel1994, ch. 6).
!! \n
!! \f{equation}{
!! CAPE=\intop_{z_{b}}^{z_{t}}g\left(\frac{\theta_{\rho}^{(p)}-\overline{\theta}_{\rho}}{\overline{\theta}_{\rho}}\right)\tag{3}
!! \f}
!! \n
!! where \f$\theta_{\rho}=T_{\rho}\left(p_{0}/p\right)^{Rd/c_{p}}\f$ and
!! \f$p_{0}=1000hPa\f$. Density temperature and virtual temperature are
!! equivalent in the absence of condensation:
!! \n
!! \f{equation}{
!! T_{\rho}=T\frac{1+r_{v}/\epsilon}{1+r_{t}}\sim T(1+.608r_{v}-r_{l})=T(1+1.608r_{v}-r_{t})\tag{4}
!! \f}
!! \n
!! where \f$\epsilon(=.622)\f$ is the ratio of the gas constant for dry
!! air to that for water vapor. The middle quantity on the right-hand
!! side is the extended definition of the virtual temperature that is
!! frequently used in meteorological :: literature.
!! \n
!! \n
!! From the definition of the convective available potential energy the
!! tendency due to convective effects is formally given by
!! \n
!! \f{equation}{
!! \left(\frac{\partial CAPE}{\partial t}\right)_{c}=\intop_{z_{b}}^{z_{t}}\frac{\partial}{\partial t}\left(\frac{g\left(\theta_{\rho}^{(p)}-\overline{\theta}_{\rho}\right)}{\overline{\theta}_{\rho}}\right)_{c}dz\tag{5}
!! \f}
!! \n
!! In the sub-cloud layer, i.e. the region between the base level and
!! the lifting condensation level (LCL) of the parcel, its potential
!! temperature and water vapor mixing ratio are independent of height
!! and equal to that at the base level. Above the parcel LCL is assumed
!! the the pressure level of the parcel is equal to that of the ambient
!! environment and the ambient pressure is not directly affected by convection
!! so that, for coding purposes, the integrand is written as
!! \f{equation}{
!! \left(g\frac{\theta_{\rho}^{(p)}}{\overline{\theta}_{\rho}}\right)\left(\frac{\partial}{\partial t}ln\left(\frac{T_{\rho}^{(p)}}{\overline{T}_{\rho}}\right)\right)_{c}
!! \f}
!! \n
!! The time derivatives of are then formally approximated as
!! \n
!! \f{equation}{
!! \frac{\partial}{\partial t}ln\left(T_{\rho}\right)\sim\frac{1}{T}\frac{\partial T}{\partial t}+\frac{1}{1+1.608r_{v}-r_{t}}\frac{\partial}{\partial t}\left(1.608r_{v}-r_{t}\right)\tag{6}
!! \f}
!! \n
!! Contributions to individual terms for parcel and environmental tendencies
!! are then evaluated separately for the sub-cloud layer and the cloud
!! layer. Note that, by assumption total water is conserved for the parcel
!! at all levels.
!! \n
!! \subsubsection sssec_sub_cloud_t_wv Sub-cloud temperature and water vapor tendencies and their contribution to changes in the parcel density temperature
!!
!! The parcel is unsaturated in the sub-cloud layer. Therefore its water
!! vapor mixing ratio and potential temperature are separately invariant
!! with height in that region and equal to those at the base level. The
!! tendencies for the base level temperature and specific humidity (\f$q\f$)
!! water associated with convective updrafts and downdrafts are given
!! by equations 2a,b of ZM. These tendencies are approximated using centered
!! finite differences in the subroutine (Loop 100). These tendencies
!! also contribute to evaluating the tendencies of the parcel density
!! temperature in the cloud layer. Since the model vertical coordinate
!! is locally pressure based, these tendencies are expressed in terms
!! of this coordinate through the hydrostatic relationship.
!! \n
!! \subsubsection sssec_parcel_dens Parcel density temperature tendency in the cloud layer
!!
!! The parcel is saturated with respect to water vapor in the cloud layer.
!! In this circumstance the water vapor mixing ratio is a function of
!! the temperature and pressure so that
!! \n
!! \f{equation}{
!! \left(\frac{\partial r_{p}}{\partial t}\right)_{c}=\left(\frac{\partial r_{*}}{\partial T}\frac{\partial T}{\partial t}\right)_{c}^{(p)}
!! \f}
!! \n
!! where
!! \n
!! \f{equation}{
!! r_{*}(T,p)=\frac{\epsilon e_{s}(T)}{p-e_{s}(T)}\tag{7}
!! \f}
!! \n
!! where \f$e_{s}\f$ is the saturation vapor pressure and \f$\epsilon=R_{d}/R_{v}\f$
!! is the ratio of the gas constant for dry air to that for water vapor.
!! Making use of the gas law, noting that in all circumstances of interest
!! the saturation vapor pressure is much smaller in magnitude than the
!! ambient air pressure, gives rise to the following acceptably accurate
!! approximation
!! \n
!! \f{equation}{
!! \partial r_{*}/\partial T=\left(1+r_{*}/\epsilon\right)\left(\frac{r_{*}}{e_{s}}\frac{\partial e_{s}}{\partial T}\right)-r_{*}/T\tag{8}
!! \f}
!! \n
!! The usual approximation to the Clausius-Clapeyron equation (\cite Emanuel1994, Sec. 4.4,
!! AMS glossary, http://glossary.ametsoc.org/wiki/Clausius-clapeyron\_equation)
!! gives
!! \n
!! \f{equation}{
!! \frac{\partial e_{s}}{\partial T}=\frac{\epsilon L_{v}e_{s}}{R_{d}T^{2}}\tag{9}
!! \f}
!! \n
!! \n
!! The formulation for evaluating the relevant derivatives of the parcel
!! equivalent potential temperature used in the subroutine makes use
!! of the definition of the \textit{saturation temperature} (i.e. the
!! temperature of the parcel at the LCL) of the base-level air developed
!! by of \cite Bolton1980 and the following approximation for the parcel
!! equivalent potential temperature
!! \n
!! \f{equation}{
!! \theta_{e}^{(p)}\sim\theta_{b}^{(p)}exp\left(\frac{r_{*}(T_{L})L_{v}}{c_{p}T_{L}}\right)\tag{10}
!! \f}
!! \n
!! where the temperature at the LCL is approximated by
!! \n
!! \f{equation}{
!! T_{L}\sim55+\frac{2840}{3.5ln(T_{b})-ln(e_{b})-4.805}\tag{11}
!! \f}
!! \n
!! Here \f$\left(T_{b},e_{b}\right)\f$ are, respectively the temperature
!! and water vapor pressure at the base level. Substituting the definition
!! given in equation (2) and taking the logarithmic derivative of both
!! sides of equation (10) and making use of equations (8), (9), and (11)
!! gives an expression of the tendency of the parcel temperature. This
!! is evaluated in Loop 200 of the subroutine.
!! \n
!! \subsubsection ssec_t_wv_tend Contributions of ambient temperature and water vapor tendencies to the tendency of the the ambient density temperature}
!!
!! The contributions of deep convection to the ambient temperature and
!! water vapor mixing ratio tendencies, as represented by the ZM scheme,
!! can be derived by replacing specific humidity by water vapor (as a
!! is appropriate for the current operational formulation) mixing ratio
!! and combining equations (1), (5) and (6) in ZM to give the following
!! expressions
!! \n
!! \f{equation}{
!! c_{p}\left(\frac{\partial T}{\partial t}\right)_{cu}=\frac{1}{\rho_{d}}\left[M_{c}\frac{\partial S}{\partial z}+D_{u}L_{v}r_{l}^{(u)})\right]\tag{12}
!! \f}
!! \n
!! \f{equation}{
!! \left(\frac{\partial r}{\partial t}\right)_{cu}=\frac{1}{\rho_{d}}\left[M_{c}\frac{\partial r}{\partial z}+D_{u}\left(r_{*}(T,p)-r\right)\right]\tag{13}
!! \f}
!! \n
!! where \f$\rho_{d}\f$ is the dry air density, \f$M_{c}=M_{u}+M_{d}\f$ is
!! the total mass flux per unit base mass flux, \f$D_{u}\f$ is the updraft
!! detrainment rate per unite base mass flux. These expressions are used
!! to evaluate the contributions of the ambient temperature and mixing
!! ratio to the density potential temperature. The form of the detrainment
!! terms arises because it is assumed that detrainment occurs at levels
!! where the detraining updraft air is at the same temperature as the
!! environment.
!! \n
!! \section sec_prog_closure_iclos_1 Prognostic closure (ICLOS=1)
!! \n
!! The prognostic closure is based on the work of \cite Scinocca2004.
!! It is much simpler in its implementation. The essential difference
!! between the diagnostic and prognostic closure is that the base mass
!! flux is a prognostic variable in the latter and is represented by
!! the following equation
!! \n
!! \f{equation}{
!! \frac{\partial M_{b}}{\partial t}=\frac{CAPE}{\alpha}-\frac{M_{b}}{\tau}\tag{14}
!! \f}
!! \n
!! where the proportionality parameter, \f$\alpha\f$, and the damping time-scale,
!! \f$\tau\f$, are specified fixed parameters. This equation is solved numerically
!! for the base mass flux using a partially implicit Euler backward time
!! stepping scheme with a time step length \f$\delta t\f$ to give
!! \n
!! \f{equation}{
!! M_{b}^{(t+\delta t)}=\frac{\delta tCAPE^{(t)}/\alpha+M_{b}^{(t)}}{1+\delta t/\tau}\tag{15}
!! \f}
!! \section sec_adj_parameters Adjustable parameters
!!
!! **CAPE depletion time scale (TAU)**
!! \n
!! This time scale sets the exponential rate of depletion of convective available potential
!! energy for the diagnostic closure option (ICLOS=0). Typical values that have been used in
!! practical implementations range from 30 minutes to 2 hours.
!! \n
!! \n
!! **Threshold value of convective available potential energy (CAPELMT)**
!! \n
!! The sets the onset value of CAPE for activation of deep convection
!! for both the diagnostic and prognostic closure options (current default
!! value is zero).
!! \n
!! \n
!! **Proportionality parameter (ALF)**
!! \n
!! This corresponds to the parameter \f$\alpha\f$ for the prognostic closure option (default
!! value \f$5\times10^{8}m^{4}kg^{-1}\f$). Sensitivity to this parameter
!! has been documented \cite Scinocca2004.
!! \n
!! \n
!! **Damping time scale (TAUS1)**
!! \n
!! This corresponds to the time scale \f$\tau\f$ for the prognostic closure option
!! (default value 6 hr) . Sensitivity to the parameter has been documented \cite Scinocca2004.
!! Note that the ratio TAUS1/ALF determines a limiting equilibrium value of the base mass flux
!! for a given value of CAPE.
