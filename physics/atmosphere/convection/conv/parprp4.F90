!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

subroutine parprp4(buoy,tc,qstc,hmnc,hmnm,qc,qm,qf,ilev,ilevm, &
                   ilgg,msg,lmag,lcl,fcb,tdry,zf,tstar, &
                   wc,dz,pf,tfe,iskip,rrl,grav,cpres,eps1,eps2, &
                   a,b,rgas,rgasv,tvfa,adef,ltm,mb,pg)
  !-------------------------------------------------------------------------
  !     * feb 02/2017 - m.lazare.    remove (unused) capen.
  !     * may 01/2015 - k.vonsalzen: new version for gcm18+:
  !     *                            - loops 250/260 modified to handle
  !     *                              pathalogical :: case where lcl is
  !     *                              exactly at the same height as a
  !     *                              model level base (just happened
  !     *                              for the first time in almost 10 years).
  !     *                              the modifications give bit-for-bit
  !     *                              otherwise.
  !     * nov 25/2006 - m.lazare.    previous version parprp3 for gcm15f->gcm17.
  !                                  - calls new tincld3 instead of tincld2.
  !     *                            - selective promotion of variables to
  !     *                              real(8) :: to support 32-bit mode.
  !     * jun 14/2006 - m.lazare.    new version for gcm15f:
  !     *                            - "ZERO","P999999" data constants
  !     *                              added and used in call to
  !     *                              intrinsics.
  !     * may 04/2006 - m.lazare/    previous version parprp2 for gcm15e:
  !     *               k.vonsalzen. - calls new tincld2.
  !     *                              this requires passing in eps2.
  !     * may 05/2001 - k.vonsalzen/ previous version parprp for gcm15d.
  !     *               n.mcfarlane.
  !
  !     * calculates properties of undiluted air parcel ascending
  !     * from the pbl to its level of neutral buoyancy.
  !-------------------------------------------------------------------------
  implicit none
  real,    intent(in) :: a
  real,    intent(in) :: adef
  real,    intent(in) :: b
  real,    intent(in) :: cpres  !< Heat capacity of dry air \f$[J kg^{-1} K^{-1}]\f$
  real,    intent(in) :: eps1
  real,    intent(in) :: eps2
  real,    intent(in) :: grav  !< Gravity on Earth \f$[m s^{-2}]\f$
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilevm  !< Number of vertical levels minus 1 \f$[unitless]\f$
  integer, intent(in) :: ilgg    !< Total number of gathered atmospheric columns with shallow convection \f$[unitless]\f$
  integer, intent(in) :: msg
  real,    intent(in) :: rgas  !< Ideal gas constant for dry air \f$[J kg^{-1} K^{-1}]\f$
  real,    intent(in) :: rgasv  !< Ideal gas constant for moist air \f$[J kg^{-1} K^{-1}]\f$
  real,    intent(in) :: rrl
  real,    intent(in) :: tvfa
  !
  !     * work fields (first set are promoted for consistency
  !     * with rest of shallow).
  !
  real*8, intent(out),    dimension(ilgg,ilev) :: buoy !< Variable description\f$[units]\f$
  real*8, intent(in),     dimension(ilgg,ilev) :: qf !< Variable description\f$[units]\f$
  real*8, intent(in),     dimension(ilgg,ilev) :: pf !< Variable description\f$[units]\f$
  real*8, intent(out),    dimension(ilgg,ilev) :: qc !< Variable description\f$[units]\f$
  real*8, intent(inout),  dimension(ilgg,ilev) :: qstc !< Variable description\f$[units]\f$
  real*8, intent(inout),  dimension(ilgg,ilev) :: tc !< Variable description\f$[units]\f$
  real*8, intent(inout),  dimension(ilgg,ilev) :: dz   !< Layer thickness for thermodynamic layers \f$[m]\f$
  real*8, intent(inout),  dimension(ilgg,ilev) :: zf !< Variable description\f$[units]\f$
  real*8, intent(inout),  dimension(ilgg,ilev) :: tdry !< Variable description\f$[units]\f$
  real*8, intent(inout),  dimension(ilgg,ilev) :: hmnc !< Variable description\f$[units]\f$
  real*8, intent(inout),  dimension(ilgg,ilev) :: wc !< Variable description\f$[units]\f$
  real*8, intent(inout),  dimension(ilgg,ilev) :: tstar !< Variable description\f$[units]\f$


  real*8, intent(out),  dimension(ilgg)      :: fcb !< Variable description\f$[units]\f$

  real, intent(in),     dimension(ilgg,ilev) :: tfe !< Variable description\f$[units]\f$
  real, intent(in),     dimension(ilgg,ilev) :: pg !< Variable description\f$[units]\f$

  real, intent(in),     dimension(ilgg)      :: hmnm !< Variable description\f$[units]\f$
  real, intent(in),     dimension(ilgg)      :: qm !< Variable description\f$[units]\f$
  real, intent(in),     dimension(ilgg)      :: mb !< Variable description\f$[units]\f$

  integer, intent(in),  dimension(ilgg)      :: lmag !< Variable description\f$[units]\f$
  integer, intent(out), dimension(ilgg)      :: lcl !< Variable description\f$[units]\f$
  integer, intent(out), dimension(ilgg)      :: iskip !< Variable description\f$[units]\f$
  integer, intent(out), dimension(ilgg)      :: ltm !< Variable description\f$[units]\f$

  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  real*8, parameter  :: zero = 0.0
  real*8, parameter  :: p999999 = 0.999999
  !
  !-----------------------------------------------------------------------
  real :: atmp
  real :: dxdg
  integer :: il
  integer :: l
  real :: tpv
  real :: tv
  real, dimension(ilgg,ilev) :: fact
  !
  !    * calculate temperature of undiluted air parcel.
  !
  do il = 1,ilgg
    iskip(il) = 0
  end do ! loop 100
  do l = msg + 2,ilev
    do il = 1,ilgg
      qc  (il,l) = qm(il)
      hmnc(il,l) = hmnm(il)
    end do
  end do ! loop 140
  call tincld3(tc,ilgg,ilev,msg,1,ilgg,lmag,tdry,hmnc,qc,qstc,zf, &
               pf,rrl,grav,cpres,eps1,eps2,a,b,iskip)
  !
  !    * buoyancy of undiulted air parcel.
  !
  do l = msg + 1,ilev
    do il = 1,ilgg
      buoy(il,l) = - 1.e-10
    end do
  end do ! loop 200
  do l = msg + 2,ilev
    do il = 1,ilgg
      if (l <= lmag(il) ) then
        tv = tfe(il,l) * (1. + tvfa * qf(il,l))
        tpv = tc(il,l) * (1. + tvfa * qc(il,l) &
              - (1. + tvfa) * max(qc(il,l) - qstc(il,l),zero))
        buoy(il,l) = grav * ( tpv - tv)/tv
        fact(il,l) = rgas * buoy(il,l) * tfe(il,l) &
                     * log(pg(il,l)/pg(il,l - 1))/grav
      end if
    end do
  end do ! loop 210
  !
  !    * determine cloud base as lifting condensation level (lcl).
  !
  do il = 1,ilgg
    lcl(il) = msg + 2
    ltm(il) = 0
    fcb(il) = 0.
  end do ! loop 240
  do l = ilevm,msg + 2, - 1
    do il = 1,ilgg
      if (l < lmag(il) .and. ltm(il) == 0) then
        if (qc(il,l) > qstc(il,l) &
            .and. qc(il,l + 1) <= qstc(il,l + 1)) then
          ltm(il) = 1
          lcl(il) = l
          fcb(il) = (qc(il,l) - qstc(il,l)) &
                    /( (qc(il,l) - qstc(il,l)) &
                    - (qc(il,l + 1) - qstc(il,l + 1)) )
          fcb(il) = max(min(fcb(il),p999999),zero)
        else if (qc(il,l) == qstc(il,l)) then
          ltm(il) = 1
          lcl(il) = l + 1
          fcb(il) = 1.
        end if
      end if
    end do
  end do ! loop 250
  do il = 1,ilgg
    if (ltm(il) == 0 .or. lcl(il) >= ilev) then
      iskip(il) = 1
    end if
  end do ! loop 260
  do il = 1,ilgg
    if (iskip(il) == 0) then
      l = lcl(il)
      buoy(il,l + 1) = fcb(il) * buoy(il,l + 1) + (1. - fcb(il)) * buoy(il,l)
      zf  (il,l + 1) = fcb(il) * zf  (il,l + 1) + (1. - fcb(il)) * zf  (il,l)
      dz  (il,l) = zf(il,l) - zf(il,l + 1)
    end if
  end do ! loop 270
  !
  !     * vertical velocity of parcel.
  !
  do l = msg + 1,ilev
    do il = 1,ilgg
      wc(il,l) = adef
    end do
  end do ! loop 295
  do il = 1,ilgg
    if (iskip(il) == 0) then
      l = lcl(il)
      !
      !           *** set cloud-base velocity to 0.25 m/s.
      !
      wc(il,l + 1) = 0.25
    end if
  end do ! loop 300
  do l = ilevm,msg + 2, - 1
    do il = 1,ilgg
      if (l <= lcl(il) .and. iskip(il) == 0 .and. wc(il,l + 1) >= 0.) then
        dxdg = ( buoy(il,l) - buoy(il,l + 1) )/dz(il,l)
        atmp = (buoy(il,l + 1) - dxdg * zf(il,l + 1)) * dz(il,l) &
               + .5 * dxdg * (zf(il,l) ** 2 - zf(il,l + 1) ** 2)
        atmp = wc(il,l + 1) ** 2 + 2. * atmp
        wc(il,l) = sign(sqrt(abs(atmp)),atmp)
      end if
    end do
  end do ! loop 310
  do il = 1,ilgg
    ltm(il) = msg + 2
  end do ! loop 330
  do l = ilevm,msg + 2, - 1
    do il = 1,ilgg
      if (l <= lcl(il) .and. iskip(il) == 0 &
          .and. wc(il,l) /= adef) then
        ltm(il) = l
      end if
    end do
  end do ! loop 350
  !
  !     * time at which parcel reaches a certain level.
  !
  do l = msg + 1,ilev
    do il = 1,ilgg
      tstar(il,l) = adef
    end do
  end do ! loop 390
  do il = 1,ilgg
    if (iskip(il) == 0) then
      l = lcl(il) + 1
      tstar(il,l) = 0.
    end if
  end do ! loop 400
  do l = ilevm,msg + 2, - 1
    do il = 1,ilgg
      if (l <= lcl(il) .and. iskip(il) == 0 &
          .and. l >= (ltm(il) + 1) ) then
        tstar(il,l) = tstar(il,l + 1) + 2. * dz(il,l) &
                      /(wc(il,l) + wc(il,l + 1))
      end if
    end do
  end do ! loop 410
  !
  return
end subroutine parprp4

!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

