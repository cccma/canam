!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine intcac(eostrow,eostrol, &
                  il1,il2,ilg,ilev,delt,gmt,iday,mday)
  !
  !     * knut von salzen - feb 07,2009. new routine for gcm15h to do
  !     *                                interpolation of eost
  !     *                                (used to be done before
  !     *                                in intchem2).
  implicit none
  real :: day
  real :: daysm
  real, intent(in) :: delt  !< Timestep for atmospheric model \f$[seconds]\f$
  real :: fmday
  real, intent(in) :: gmt  !< Real value of number of elapsed seconds in current day \f$[seconds]\f$
  integer :: i
  integer, intent(in) :: iday  !< Julian calendar day of year \f$[day]\f$
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: istepsm
  integer, intent(in) :: mday  !< Julian calendar day of next mid-month \f$[day]\f$
  real :: stepsm
  !
  !     * surface emissions/concentrations.
  !
  real, intent(inout), dimension(ilg) :: eostrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: eostrol !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !--------------------------------------------------------------------
  !     * compute the number of timesteps from here to mday.
  !
  day = real(iday) + gmt/86400.
  fmday = real(mday)
  if (fmday < day) fmday = fmday + 365.
  daysm = fmday - day
  istepsm = nint(daysm * 86400./delt)
  stepsm = real(istepsm)
  !
  !     * general interpolation.
  !
  do i = il1,il2
    eostrow(i)  = ((stepsm - 1.) * eostrow(i) + eostrol(i)) / stepsm
  end do ! loop 210
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
