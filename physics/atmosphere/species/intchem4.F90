!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine intchem4(dmsorow,dmsorol,edmsrow,edmsrol, &
                    suz0row,suz0rol,pdsfrow,pdsfrol, &
                    ohrow,  ohrol,h2o2row,h2o2rol, &
                    o3row,  o3rol, no3row, no3rol, &
                    hno3row,hno3rol, nh3row, nh3rol, &
                    nh4row, nh4rol,                  &
                    il1,il2,ilg,levox,delt,gmt,iday,mday)
  !
  !     * apr 25,2012 - y.peng.    new version for gcm16:
  !     *                          add suz0,pdsf for new dust scheme.
  !     * feb 07,09 - k.vonsalzen. previous version intchem3 for gcm15h/i:
  !     *                          eost removed (now in separate
  !     *                          routine intcac).
  !     * jan 17/08 - x.ma/     previous version intchem2 for gcm15g:
  !     *             m.lazare.
  !     * apr 23/04 - m.lazare. previous version intchem up to gcm15f.

  !     *                       interpolation of chemistry fields, based
  !     *                       on subroutine "getgtio" (for imask=0 case).
  !
  !     * xxrow  = chemistry field    for previous timestep.
  !     * xxrol  = chemistry field    at next physics time.
  !
  !     * delt   = model timestep in seconds.
  !     * gmt    = number of seconds in current day.
  !     * iday   = current julian day.
  !     * mday   = date of target sst.
  !
  implicit none
  real :: day
  real :: daysm
  real, intent(in) :: delt  !< Timestep for atmospheric model \f$[seconds]\f$
  real :: fmday
  real, intent(in) :: gmt  !< Real value of number of elapsed seconds in current day \f$[seconds]\f$
  integer :: i
  integer, intent(in) :: iday  !< Julian calendar day of year \f$[day]\f$
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: levox      !< Number of vertical layers for oxidant input data \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: istepsm
  integer :: l
  integer, intent(in) :: mday  !< Julian calendar day of next mid-month \f$[day]\f$
  real :: stepsm
  !
  !     * surface emissions/concentrations.
  !
  real, intent(inout), dimension(ilg) :: dmsorow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: dmsorol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: edmsrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: edmsrol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: suz0row !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: suz0rol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: pdsfrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: pdsfrol !< Variable description\f$[units]\f$
  !
  !     * multi-level species.
  !
  real, intent(inout), dimension(ilg,levox) :: ohrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,levox) :: ohrol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,levox) :: h2o2row !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,levox) :: h2o2rol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,levox) :: o3row !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,levox) :: o3rol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,levox) :: no3row !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,levox) :: no3rol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,levox) :: hno3row !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,levox) :: hno3rol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,levox) :: nh3row !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,levox) :: nh3rol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,levox) :: nh4row !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,levox) :: nh4rol !< Variable description\f$[units]\f$
  !
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !--------------------------------------------------------------------
  !     * compute the number of timesteps from here to mday.
  !
  day = real(iday) + gmt/86400.
  fmday = real(mday)
  if (fmday < day) fmday = fmday + 365.
  daysm = fmday - day
  istepsm = nint(daysm * 86400./delt)
  stepsm = real(istepsm)
  !
  !     * general interpolation.
  !
  do i = il1,il2
    dmsorow(i)  = ((stepsm - 1.) * dmsorow(i) + dmsorol(i)) / stepsm
    edmsrow(i)  = ((stepsm - 1.) * edmsrow(i) + edmsrol(i)) / stepsm
    suz0row(i)  = ((stepsm - 1.) * suz0row(i) + suz0rol(i)) / stepsm
    pdsfrow(i)  = ((stepsm - 1.) * pdsfrow(i) + pdsfrol(i)) / stepsm
  end do ! loop 210
  !
  do l = 1,levox
    do i = il1,il2
      ohrow  (i,l) = ((stepsm - 1.) * ohrow  (i,l) + ohrol  (i,l)) /stepsm
      h2o2row(i,l) = ((stepsm - 1.) * h2o2row(i,l) + h2o2rol(i,l)) /stepsm
      o3row  (i,l) = ((stepsm - 1.) * o3row  (i,l) + o3rol  (i,l)) /stepsm
      no3row (i,l) = ((stepsm - 1.) * no3row (i,l) + no3rol (i,l)) /stepsm
      hno3row(i,l) = ((stepsm - 1.) * hno3row(i,l) + hno3rol(i,l)) /stepsm
      nh3row (i,l) = ((stepsm - 1.) * nh3row (i,l) + nh3rol (i,l)) /stepsm
      nh4row (i,l) = ((stepsm - 1.) * nh4row (i,l) + nh4rol (i,l)) /stepsm
    end do
  end do ! loop 250
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
