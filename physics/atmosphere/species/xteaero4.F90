!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine xteaero4(xemis,pressg,dshj,zf, &
                    ebbtrow,eobtrow,ebftrow,eoftrow,esptrow, &
                    esitrow,esdtrow,esstrow,esotrow,esrtrow, &
                    ipam,docem1,dbcem1,docem2,dbcem2,dsuem2, &
                    dsuem3,psuef2,psuef3, &
                    ioco,iocy,ibco,ibcy,iso2, &
                    ilg,il1,il2,ilev,lev,ntrac,ztmst)
  !
  !     * jun 17/2013 - k.vonsalzen/ new version for gcm17+:
  !     *               m.lazare.    - add condition to set wgt=1. for
  !     *                              special case where hso2=lso2.
  !     *                            - add support for pla option.
  !     * apr 28/2012 - k.vonsalzen/ previous version xteaero3 for gcm16:
  !     *               m.lazare.    - initialize lso2 and hso2 to
  !     *                              ilev instead of zero, to
  !     *                              avoid possible invalid
  !     *                              memory references.
  !     *                            - bugfix for incorrect memory
  !     *                              reference: lso2(l)->lso2(il).
  !     * apr 26/2010 - k.vonsalzen. previous version xteaero2 for gcm15i:
  !     *                            - zf and pf calculated one-time at
  !     *                              beginning of physics and zf
  !     *                              passed in, with internal calculation
  !     *                              thus removed and no need to pass
  !     *                              in shbj,t.
  !     *                            - emissions accumulated into general
  !     *                              "xemis" array rather than updating
  !     *                              xrow (xrow updated from xemis in
  !     *                              the physics).
  !     *                            - removal of diagnostic fields
  !     *                              calculation, so no need to pass
  !     *                              in "saverad" or "isvchem".
  !     *                            - wgt=0 (ie no emissions in layer)
  !     *                              if it is above top or below base
  !     *                              of emissions.
  !     * feb 05/09 - k.vonsalzen. previous version xteaero1 for gcm15h:
  !                                - remove natural emissions.
  !     * jan 18/08 - x.ma/     previous version xteaero for gcm15g.
  !                   m.lazare.
  !
  use phys_consts, only : grav
  implicit none
  integer, intent(in) :: ibco  !< Tracer index for black carbon (hydrophobic) \f$[unitless]\f$
  integer, intent(in) :: ibcy  !< Tracer index for black carbon (hydrophylic) \f$[unitless]\f$
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: ioco  !< Tracer index for organic carbon (hydrophobic) \f$[unitless]\f$
  integer, intent(in) :: iocy  !< Tracer index for organic carbon (hydrophylic) \f$[unitless]\f$
  integer, intent(in) :: ipam  !< Switch to use PAM or bulk aerosol scheme (0=bulk. 1=PAM) \f$[unitless]\f$
  integer, intent(in) :: iso2  !< Tracer index for sulfur dioxide \f$[unitless]\f$
  integer, intent(in) :: lev  !< Number of vertical levels plus 1 \f$[unitless]\f$
  integer, intent(in) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  real, intent(in) :: psuef2
  real, intent(in) :: psuef3
  real, intent(in) :: ztmst
  !
  !     * i/o fields.
  !
  real, intent(inout), dimension(ilg,ilev,ntrac) :: xemis !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: dshj   !< Thickness of thermodynamic layers in eta coordinates \f$[unitless]\f$
  real, intent(in), dimension(ilg) :: pressg   !< Surface pressure \f$[Pa]\f$
  real, intent(in), dimension(ilg,ilev) :: zf !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: docem1 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: dbcem1 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: docem2 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: dbcem2 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: dsuem2 !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: dsuem3 !< Variable description\f$[units]\f$
  !
  real, intent(in), dimension(ilg) :: ebbtrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: eobtrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: ebftrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: eoftrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: esptrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: esitrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: esdtrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: esstrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: esotrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: esrtrow !< Variable description\f$[units]\f$
  !
  !     * internal work fields.
  !
  real :: ebc
  real :: eoc
  real :: esdt
  real :: eso2
  real :: espi
  real :: fact
  integer :: il
  integer :: jt
  integer :: l
  real :: pom2oc
  real :: wgt
  integer, dimension(ilg) :: lso2 !< Variable description\f$[units]\f$
  integer, dimension(ilg) :: hso2
  !==================================================================
  ! physical (adjustable) parameters
  !
  real, parameter :: wso2 = 64.059
  real, parameter :: wh2so4 = 98.073
  real, parameter :: wnh3 = 17.030
  real, parameter :: wamsul = wh2so4 + 2. * wnh3
  real, parameter :: wrat = wamsul/wso2
  !==================================================================
  !
  !-------------------------------------------------------------------
  !
  !     * ebbt, ebft, eobt, and eoft
  !
  pom2oc = 1./1.4
  do il = il1,il2
    fact                 = ztmst * grav/(dshj(il,ilev) * pressg(il))
    ebc = ebbtrow(il) + ebftrow(il)
    eoc = pom2oc * (eobtrow(il) + eoftrow(il))
    if (ipam == 0) then
      xemis(il,ilev,ibco) = xemis(il,ilev,ibco) + 0.8 * ebc * fact
      xemis(il,ilev,ibcy) = xemis(il,ilev,ibcy) + 0.2 * ebc * fact
      xemis(il,ilev,ioco) = xemis(il,ilev,ioco) + 0.5 * eoc * fact
      xemis(il,ilev,iocy) = xemis(il,ilev,iocy) + 0.5 * eoc * fact
    else
      docem1(il,ilev) = docem1(il,ilev) + eobtrow(il) * fact/ztmst
      docem2(il,ilev) = docem2(il,ilev) + eoftrow(il) * fact/ztmst
      dbcem1(il,ilev) = dbcem1(il,ilev) + ebbtrow(il) * fact/ztmst
      dbcem2(il,ilev) = dbcem2(il,ilev) + ebftrow(il) * fact/ztmst
    end if
  end do ! loop 30
  !
  jt = iso2
  !
  !     * esdt, esst, esot and esrt
  !
  do il = il1,il2
    fact    = ztmst * grav/(dshj(il,ilev) * pressg(il))
    esdt = esdtrow(il) * (1. - psuef2) * 32.064/64.059
    eso2 = esstrow(il) * (1. - psuef2) * 32.064/64.059
    eso2 = eso2 + ( esotrow(il) + esrtrow(il) ) * (1. - psuef2) * 32.064/64.059
    xemis(il,ilev,jt) = xemis(il,ilev,jt) + (esdt + eso2) * fact
    if (ipam == 1) then
      dsuem2(il,ilev) = dsuem2(il,ilev) &
                        + (esdtrow(il) + esotrow(il) + esrtrow(il) + esstrow(il)) * fact &
                        * wrat * psuef2/ztmst
    end if
  end do ! loop 50
  !
  !     * determine layer corresponding to power plant (esptrow) and
  !     * industrial (esitrow) so2 emissions (100-300m).
  !
  do il = il1, il2
    lso2(il) = ilev
    hso2(il) = ilev
  end do ! loop 100
  !
  do l = ilev, 1, - 1
    do il = il1, il2
      if (zf(il,l) <= 100.)   lso2(il) = l
      if (zf(il,l) <= 300.)   hso2(il) = l
    end do
  end do ! loop 105
  !
  do l = 1, ilev
    do il = il1, il2
      espi = esptrow(il) + esitrow(il)
      fact                 = ztmst * grav/(dshj(il,l) * pressg(il))
      if (l == hso2(il) .and. l == lso2(il)) then
        wgt = 1.
      else if (l == hso2(il)) then
        wgt = (300. - zf(il,l))/200.
      else if (l > hso2(il) .and. l < lso2(il)) then
        wgt = (zf(il,l - 1) - zf(il,l))/200.
      else if (l == lso2(il)) then
        wgt = (zf(il,l - 1) - 100.)/200.
      else
        wgt = 0.
      end if
      xemis(il,l,jt) = xemis(il,l,jt) + wgt * espi * fact * 32.064/64.059 &
                       * (1. - psuef2)
      if (ipam == 1) then
        dsuem2(il,l) = dsuem2(il,l) + wgt * espi * fact * wrat * psuef2/ztmst
      end if
    end do
  end do ! loop 110
  !
  return
end subroutine xteaero4
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
