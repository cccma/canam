!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine xteant4(xemis,pressg,dshj,zf,eaocrow,eabcrow,easlrow, &
                   eashrow,ioco,iocy,ibco,ibcy,iso2,ilg,il1,il2, &
                   ilev,lev,ntrac,ztmst)
  !
  !     * jun 17/2013 - k.vonsalzen/ new version for gcm17+:
  !     *               m.lazare.    - add condition to set wgt=1. for
  !     *                              special case where hso2=lso2.
  !     * apr 28/2012 - k.vonsalzen/ previous version xteant3 for gcm16:
  !     *               m.lazare.    - initialize lso2 and hso2 to
  !     *                              ilev instead of zero, to
  !     *                              avoid possible invalid
  !     *                              memory references.
  !     *                            - bugfix for incorrect memory
  !     *                              reference: lso2(l)->lso2(il).
  !     * apr 26/2010 - k.vonsalzen. previous version xteant2 for gcm15i:
  !     *                            - zf and pf calculated one-time at
  !     *                              beginning of physics and zf
  !     *                              passed in, with internal calculation
  !     *                              thus removed and no need to pass
  !     *                              in shbj,t.
  !     *                            - emissions accumulated into general
  !     *                              "xemis" array rather than updating
  !     *                              xrow (xrow updated from xemis in
  !     *                              the physics).
  !     *                            - removal of diagnostic fields
  !     *                              calculation, so no need to pass
  !     *                              in "saverad" or "isvchem".
  !     *                            - wgt=0 (ie no emissions in layer)
  !     *                              if it is above top or below base
  !     *                              of emissions.
  !     * knut von salzen - feb 07,2009. previous routine xteant for
  !     *                                gcm15h to apply emissions for:
  !     *                                easl,eash,eaoc,eabc.
  !
  use phys_consts, only : grav
  implicit none
  integer, intent(in) :: ibco  !< Tracer index for black carbon (hydrophobic) \f$[unitless]\f$
  integer, intent(in) :: ibcy  !< Tracer index for black carbon (hydrophylic) \f$[unitless]\f$
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: ioco  !< Tracer index for organic carbon (hydrophobic) \f$[unitless]\f$
  integer, intent(in) :: iocy  !< Tracer index for organic carbon (hydrophylic) \f$[unitless]\f$
  integer, intent(in) :: iso2  !< Tracer index for sulfur dioxide \f$[unitless]\f$
  integer, intent(in) :: lev  !< Number of vertical levels plus 1 \f$[unitless]\f$
  integer, intent(in) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  real, intent(in) :: ztmst
  !
  real, intent(in), dimension(ilg) :: eaocrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: eabcrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: easlrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: eashrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: pressg   !< Surface pressure \f$[Pa]\f$
  real, intent(in), dimension(ilg,ilev) :: dshj   !< Thickness of thermodynamic layers in eta coordinates \f$[unitless]\f$
  real, intent(in), dimension(ilg,ilev) :: zf !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev,ntrac) :: xemis !< Variable description\f$[units]\f$
  !
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  real :: ebc
  real :: eoc
  real :: espi
  real :: fact
  integer :: il
  integer :: jt
  integer :: l
  real :: wgt
  integer, dimension(ilg) :: lso2 !< Variable description\f$[units]\f$
  integer, dimension(ilg) :: hso2
  !-------------------------------------------------------------------
  !
  do il = il1,il2
    fact = ztmst * grav/(dshj(il,ilev) * pressg(il))
    !
    !       * anthropogenic organic carbon.
    !
    eoc = eaocrow(il)
    xemis(il,ilev,ioco) = xemis(il,ilev,ioco) + 0.5 * eoc * fact
    xemis(il,ilev,iocy) = xemis(il,ilev,iocy) + 0.5 * eoc * fact
    !
    !       * anthropogenic black carbon.
    !
    ebc = eabcrow(il)
    xemis(il,ilev,ibco) = xemis(il,ilev,ibco) + 0.8 * ebc * fact
    xemis(il,ilev,ibcy) = xemis(il,ilev,ibcy) + 0.2 * ebc * fact
    !
    !       * anthropogenic sulphur dioxide from surface sources.
    !
    xemis(il,ilev,iso2) = xemis(il,ilev,iso2) + easlrow(il) * fact
  end do ! loop 30
  !
  !     * determine layer corresponding to power plant and industrial
  !     * so2 emissions (100-300m).
  !
  do il = il1, il2
    lso2(il) = ilev
    hso2(il) = ilev
  end do ! loop 100
  do l = ilev, 1, - 1
    do il = il1, il2
      if (zf(il,l) <= 100.) lso2(il) = l
      if (zf(il,l) <= 300.) hso2(il) = l
    end do
  end do ! loop 105
  jt = iso2
  do l = 1,ilev
    do il = il1,il2
      espi = eashrow(il)
      fact = ztmst * grav/(dshj(il,l) * pressg(il))
      if (l == hso2(il) .and. l == lso2(il)) then
        wgt = 1.
      else if (l == hso2(il)) then
        wgt = (300. - zf(il,l))/200.
      else if (l > hso2(il) .and. l < lso2(il)) then
        wgt = (zf(il,l - 1) - zf(il,l))/200.
      else if (l == lso2(il)) then
        wgt = (zf(il,l - 1) - 100.)/200.
      else
        wgt = 0.
      end if
      xemis(il,l,jt) = xemis(il,l,jt) + wgt * espi * fact
    end do
  end do ! loop 110
  !
  return
end subroutine xteant4
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
