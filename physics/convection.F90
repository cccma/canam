!>\file convection.F90
!>\brief Mass flux convergence penetrative deep scheme
!!
!! @author
!
 module convection_mod

 implicit none

 public :: convection

 contains
 subroutine convection(pa, throw, qrow, urow, vrow, xrow, pressg, tfrow, prerow,      &
                       dpog, dshj, shj, sgj, shtj, sgbj, rain, snow, rainls, depbrol, &
                       gustrol, utendcv, vtendcv, ozchm, hno3phs, nh3phs, nh4phs,     &
                       il1, il2, delt, dtadv, jlat, kount, lstag,                     &
                       saverad, save3hr, ishf, msg)
  use agcm_types_mod,   only : phys_arrays_type
  use agcm_types_mod,   only : phys_options
  use agcm_types_mod,   only : phys_diag
  use psizes_19,        only : ilg, ilev, ntrac
  use tracers_info_mod, only : iaindt, ico2
  use chem_params_mod,  only : nwflx
  use phys_consts,      only : daylnt
  use radcons_mod,      only : co2_ppm

  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ishf  !< Timestep interval to save high frequency output \f$[unitless]\f$
  integer, intent(in) :: jlat
  integer, intent(in) :: kount !< Current model timestep \f$[unitless]\f$
  logical, intent(in) :: lstag !< Loogical switch to defining whether run is staggered or unstaggered
  integer, intent(in) :: msg
  real, intent(in) :: delt  !< Timestep for atmospheric model \f$[seconds]\f$
  real, intent(in) :: dtadv  !< Timestep for atmospheric model \f$[seconds]\f$
  real, intent(in) :: save3hr  !< Reciprocal of timestep interval to save 3-hour output \f$[unitless]\f$
  real, intent(in) :: saverad  !< Reciprocal of timestep interval to save radiative transfer output \f$[unitless]\f$

  type(phys_arrays_type), intent(inout)          :: pa

  real, intent(inout), dimension(ilg,ilev,ntrac) :: xrow    !< Tracers concentration
  real, intent(in)   , dimension(ilg)            :: pressg  !< Surface pressure \f$[Pa]\f$
  real, intent(inout), dimension(ilg,ilev)       :: utendcv !< u-wind tendency (real).
  real, intent(inout), dimension(ilg,ilev)       :: vtendcv !< v-wind tendency (real).
  real, intent(inout), dimension(ilg,ilev)       :: throw !< Temperature at mid-layer.
  real, intent(inout), dimension(ilg,ilev)       :: qrow !< Specific humidity
  real, intent(inout), dimension(ilg,ilev)       :: urow !< u-wind (real).
  real, intent(inout), dimension(ilg,ilev)       :: vrow !< v-wind (real).
  real, intent(in)   , dimension(ilg,ilev+1)     :: tfrow !< Temperature at interface.
  real, intent(inout), dimension(ilg)            :: depbrol !<
  real, intent(out)  , dimension(ilg)            :: gustrol !<
  real, intent(out)  , dimension(ilg)            :: prerow !<
  real, intent(out)  , dimension(ilg)            :: rain !<
  real, intent(out)  , dimension(ilg)            :: snow !<
  real, intent(inout), dimension(ilg)            :: rainls !<
  real, intent(in)   , dimension(ilg,ilev)       :: ozchm !<
  real, intent(in)   , dimension(ilg,ilev)       :: hno3phs !<
  real, intent(in)   , dimension(ilg,ilev)       :: nh3phs !<
  real, intent(in)   , dimension(ilg,ilev)       :: nh4phs !<
  !
  !     * vertical levelling information.
  !
  real, intent(in),    dimension(ilg,ilev+1)     :: shtj   !< Eta-level for top of thermodynamic layer, plus eta-level for surface (base of lowest layer) \f$[unitless]\f$
  real, intent(in),    dimension(ilg,ilev)       :: sgj   !< Eta-level for mid-point of the momentum layer \f$[unitless]\f$
  real, intent(in),    dimension(ilg,ilev)       :: shj   !< Eta-level for mid-point of thermodynamic layer \f$[unitless]\f$
  real, intent(in),    dimension(ilg,ilev)       :: sgbj   !< Eta-level for base of momentum layers \f$[unitless]\f$
  real, intent(in),    dimension(ilg,ilev)       :: dshj   !< Thickness of thermodynamic layers in eta coordinates \f$[unitless]\f$
  real, intent(in),    dimension(ilg,ilev)       :: dpog   !< Mass of air in grid cell area \f$[kg/m2]\f$
  !
  !
  real,   dimension(ilg)            :: raindc !<
  real,   dimension(ilg)            :: rainsc !<
  real,   dimension(ilg)            :: wstrol !<
  real,   dimension(ilg,ilev)       :: dqldt !<
  !
  !     * CO2 concentration in ppv.
  real,   dimension(ilg,ilev)       :: co2chm !<

  ! Local arrays for gas-phase chemistry:
  real, dimension(ilg, nwflx) :: wflxrox
  real, dimension(ilg)        :: elccrox, elcgrox, elnirox  !< lightning diagnostics included in gas-phase chemistry
  real, dimension(ilg,ilev)   :: emisln  !< 3D distribution of NOx emissions from lightning included in gas-phase chemistry
  !
  ! Local arrays for extra chemistry diagnostic fields
  real, dimension(ilg,ilev) :: phdrox !< Variable description\f$[units]\f$
  real, dimension(ilg,ilev) :: phsrox !< Variable description\f$[units]\f$
  real, dimension(ilg) :: sdhprox !< Variable description\f$[units]\f$
  real, dimension(ilg) :: sdo3rox !< Variable description\f$[units]\f$
  real, dimension(ilg) :: sshprox !< Variable description\f$[units]\f$
  real, dimension(ilg) :: sso3rox !< Variable description\f$[units]\f$
  real, dimension(ilg) :: wdd4rox !< Variable description\f$[units]\f$
  real, dimension(ilg) :: wdd6rox !< Variable description\f$[units]\f$
  real, dimension(ilg) :: wddbrox !< Variable description\f$[units]\f$
  real, dimension(ilg) :: wdddrox !< Variable description\f$[units]\f$
  real, dimension(ilg) :: wddorox !< Variable description\f$[units]\f$
  real, dimension(ilg) :: wddsrox !< Variable description\f$[units]\f$
  real, dimension(ilg) :: wds4rox !< Variable description\f$[units]\f$
  real, dimension(ilg) :: wds6rox !< Variable description\f$[units]\f$
  real, dimension(ilg) :: wdsbrox !< Variable description\f$[units]\f$
  real, dimension(ilg) :: wdsdrox !< Variable description\f$[units]\f$
  real, dimension(ilg) :: wdsorox !< Variable description\f$[units]\f$
  real, dimension(ilg) :: wdssrox !< Variable description\f$[units]\f$
  !
  ! Local arrays for extra convection diagnostic fields
  real,   dimension(ilg,ilev)       :: smcrox
  real,   dimension(ilg,ilev)       :: dmcurox
  real,   dimension(ilg,ilev)       :: dmcdrox
  real,   dimension(ilg,ilev)       :: dmcrox
  real,   dimension(ilg)            :: cinhrox
  real,   dimension(ilg)            :: caperox
  real,   dimension(ilg)            :: bcsrox
  real,   dimension(ilg)            :: tcsrox
  real,   dimension(ilg)            :: bcdrox
  real,   dimension(ilg)            :: cdcbrox
  real,   dimension(ilg)            :: cscbrox
  real,   dimension(ilg)            :: tcdrox

  real,   dimension(ilg,ilev)       :: tprow !<
  real,   dimension(ilg,ilev)       :: tcrow !<
  real,   dimension(ilg,ilev)       :: qprow !<
  real,   dimension(ilg,ilev)       :: qcrow !<
  real,   dimension(ilg,ilev,ntrac) :: xcrow !<
  real,   dimension(ilg,ilev,ntrac) :: xprow !<
  real,   dimension(ilg,ilev)       :: ucrow !<
  real,   dimension(ilg,ilev)       :: vcrow !<

  real,   dimension(ilg)            :: voabrd1 !< Load of organic aerosol mass before convection \f$[kg/m^2]\f$
  real,   dimension(ilg)            :: vbcbrd1 !< Load of black carbon aerosol mass before convection \f$[kg/m^2]\f$
  real,   dimension(ilg)            :: vasbrd1 !< Load of ammonium sulphate aerosol mass before convection \f$[kg/m^2]\f$
  real,   dimension(ilg)            :: vmdbrd1 !< Load of mineral dust aerosol mass before convection \f$[kg/m^2]\f$
  real,   dimension(ilg)            :: vssbrd1 !< Load of seai salt aerosol mass before convection \f$[kg/m^2]\f$
  real,   dimension(ilg)            :: voabrd2 !< Load of organic aerosol mass after convection \f$[kg/m^2]\f$
  real,   dimension(ilg)            :: vbcbrd2 !< Load of black carbon aerosol mass after convection \f$[kg/m^2]\f$
  real,   dimension(ilg)            :: vasbrd2 !< Load of ammonium sulphate aerosol mass after convection \f$[kg/m^2]\f$
  real,   dimension(ilg)            :: vmdbrd2 !< Load of mineral dust aerosol mass after convection \f$[kg/m^2]\f$
  real,   dimension(ilg)            :: vssbrd2 !< Load of seai salt aerosol mass after convection \f$[kg/m^2]\f$
  integer :: ifizcon
  !
  real :: beta
  real :: duodt
  real :: dvodt
  real :: gust1
  real :: gust2
  real :: gustfac
  real :: pcptmp
  real :: rcdt
  real :: savehf
  real :: ufac
  integer :: il
  integer :: jk
  integer :: nn

  real, parameter :: air_mwg = 28.970
  real, parameter :: co2_mwg = 44.011
  real, parameter :: zero = 0.0
  !
  !     * first set appropriate co2 value in ppv for chemistry in
  !     * convection.
  !
  if (phys_options%carbon) then
     if (phys_options%specified_co2) then
        co2chm(:,:) = co2_ppm
     else
        co2chm(:,:) = (air_mwg / co2_mwg) * xrow(:,:,ico2)
     end if
  else
     co2chm(:,:) = co2_ppm
  end if
  !
  if (phys_diag%uprhsc) then
      ucrow(il1:il2, 1:ilev) = urow(il1:il2, 1:ilev)
  end if
  if (phys_diag%vprhsc) then
      vcrow(il1:il2, 1:ilev) = vrow(il1:il2, 1:ilev)
  endif
  !
  !     * set switch to control whether calculting component tendencies
  !     * in conv routine.
  !
  ifizcon = 0
  if (phys_diag%tprhsc .or. phys_diag%qprhsc .or. phys_diag%xprhsc) then
      ifizcon = 1
  end if
  !
  if (phys_options%pla .and. phys_options%pam) then
!
!     * save aerosol burdens before convection.
!
     call trburd (voabrd1,vbcbrd1,vasbrd1,vmdbrd1,vssbrd1,xrow, &
                  dpog,ntrac,iaindt,il1,il2,ilg,ilev,msg)
  end if
  !
  !     * deep and shallow convection driver.
  !
  call conv17(throw, qrow, urow, vrow, raindc,                        &
              rainsc, pressg, pa%tcvrow, pa%pbltrow, dshj, shj,       &
              sgj, shtj, sgbj, tfrow, wstrol, utendcv, vtendcv, xrow, &
              dqldt, hno3phs, nh3phs, nh4phs, co2chm, ozchm,          &
              sso3rox, sshprox, sdo3rox, sdhprox, phsrox, phdrox,     &
              depbrol, bcdrox, tcdrox,                                &
              pa%clcvrow, pa%qfxrow, pa%tfxrow,                       &
              wdd4rox, wdd6rox, wds4rox, wds6rox, wdddrox, wddbrox,   &
              wddorox, wddsrox, wdsdrox, wdsbrox, wdsorox, wdssrox,   &
              pa%cbmfrol, pa%cqfxrow, pa%chfxrow,                     &
              pa%sclfrow, pa%scdnrow, pa%slwcrow, pa%cvmcrow,         &
              smcrox, dmcurox, dmcdrox, cinhrox, caperox, bcsrox,     &
              tcsrox, cscbrox, cdcbrox, dmcrox,                       &
              tprow, tcrow, qprow, qcrow, xprow, xcrow,               &
              ilev, ilev-1, ilg, il1, il2, delt, dtadv, jlat, kount,  &
              phys_diag%xtrachem, phys_diag%xtraconv, ifizcon,        &
              ntrac, lstag, phys_options%pla,                         &
              phys_options%switch_shallow_convection,                 &
              ! Variables for gas-phase chemistry, when switched on
              phys_options%gas_chem, wflxrox, elnirox,elcgrox,elccrox, emisln)

  if (phys_diag%xtrachem) then
     pa%wdd4row(il1:il2) =  pa%wdd4row(il1:il2) + wdd4rox(il1:il2) * saverad
     pa%wds4row(il1:il2) =  pa%wds4row(il1:il2) + wds4rox(il1:il2) * saverad
     if (.not. phys_options%pla) then
        pa%phsrow = phsrox
        pa%phdrow = phdrox
        pa%sdo3row(il1:il2) = pa%sdo3row(il1:il2) + sdo3rox(il1:il2) * saverad
        pa%sdhprow(il1:il2) = pa%sdhprow(il1:il2) + sdhprox(il1:il2) * saverad
        pa%wdd6row(il1:il2) = pa%wdd6row(il1:il2) + wdd6rox(il1:il2) * saverad
        pa%wdddrow(il1:il2) = pa%wdddrow(il1:il2) + wdddrox(il1:il2) * saverad
        pa%wddbrow(il1:il2) = pa%wddbrow(il1:il2) + wddbrox(il1:il2) * saverad
        pa%wddorow(il1:il2) = pa%wddorow(il1:il2) + wddorox(il1:il2) * saverad
        pa%wddsrow(il1:il2) = pa%wddsrow(il1:il2) + wddsrox(il1:il2) * saverad
        pa%sso3row(il1:il2) = pa%sso3row(il1:il2) + sso3rox(il1:il2) * saverad
        pa%sshprow(il1:il2) = pa%sshprow(il1:il2) + sshprox(il1:il2) * saverad
        pa%wds6row(il1:il2) = pa%wds6row(il1:il2) + wds6rox(il1:il2) * saverad
        pa%wdsdrow(il1:il2) = pa%wdsdrow(il1:il2) + wdsdrox(il1:il2) * saverad
        pa%wdsbrow(il1:il2) = pa%wdsbrow(il1:il2) + wdsbrox(il1:il2) * saverad
        pa%wdsorow(il1:il2) = pa%wdsorow(il1:il2) + wdsorox(il1:il2) * saverad
        pa%wdssrow(il1:il2) = pa%wdssrow(il1:il2) + wdssrox(il1:il2) * saverad
     end if
  end if

  if (phys_diag%xtraconv) then
     pa%bcdrol (il1:il2) = bcdrox (il1:il2)
     pa%cdcbrol(il1:il2) = cdcbrox(il1:il2)
     pa%cscbrol(il1:il2) = cscbrox(il1:il2)
     pa%tcdrol (il1:il2) = tcdrox (il1:il2)
     pa%bcdrow (il1:il2) = pa%bcdrow (il1:il2) + pa%bcdrol (il1:il2) * saverad
     pa%cdcbrow(il1:il2) = pa%cdcbrow(il1:il2) + pa%cdcbrol(il1:il2) * saverad
     pa%cscbrow(il1:il2) = pa%cscbrow(il1:il2) + pa%cscbrol(il1:il2) * saverad
     pa%tcdrow (il1:il2) = pa%tcdrow (il1:il2) + pa%tcdrol (il1:il2) * saverad

     pa%bcsrow(il1:il2)  = pa%bcsrow(il1:il2)  + bcsrox(il1:il2)  * saverad
     pa%tcsrow(il1:il2)  = pa%tcsrow(il1:il2)  + tcsrox(il1:il2)  * saverad

     pa%dmcrol (il1:il2, :) = dmcrox (il1:il2, :)
     pa%smcrow (il1:il2, :) = pa%smcrow (il1:il2, :) + smcrox (il1:il2, :) * saverad
     pa%dmcurow(il1:il2, :) = pa%dmcurow(il1:il2, :) + dmcurox(il1:il2, :) * saverad
     pa%dmcdrow(il1:il2, :) = pa%dmcdrow(il1:il2, :) + dmcdrox(il1:il2, :) * saverad
     pa%dmcrow (il1:il2, :) = pa%dmcrow (il1:il2, :) + dmcrox (il1:il2, :) * saverad
     pa%caperow(il1:il2) = pa%caperow(il1:il2) + caperox(il1:il2) * saverad
     pa%cinhrow(il1:il2) = pa%cinhrow(il1:il2) + cinhrox(il1:il2) * saverad
  end if

  ! Arrays used by gas-phase chemistry in conv17.
  if (phys_options%gas_chem) then
     pa%wflxrow(il1:il2, 1:2) = pa%wflxrow(il1:il2, 1:2) + wflxrox(il1:il2, 1:2)*saverad
     pa%elccrow(il1:il2) = pa%elccrow(il1:il2) + elccrox(il1:il2)*saverad
     pa%elcgrow(il1:il2) = pa%elcgrow(il1:il2) + elcgrox(il1:il2)*saverad
     pa%elnirow(il1:il2) = pa%elnirow(il1:il2) + elnirox(il1:il2)*saverad
  end if

  if (phys_options%pla .and. phys_options%pam) then
!
! * Save aerosol burdens after convection.
!
     call trburd (voabrd2,vbcbrd2,vasbrd2,vmdbrd2,vssbrd2,xrow, &
                  dpog,ntrac,iaindt,il1,il2,ilg,ilev,msg)
!
!     * diagnose vertical integral of convective aerosol sources/sinks.
!
     voabrd2(il1:il2)=(voabrd2(il1:il2)-voabrd1(il1:il2))/(2.*delt)
     vbcbrd2(il1:il2)=(vbcbrd2(il1:il2)-vbcbrd1(il1:il2))/(2.*delt)
     vasbrd2(il1:il2)=(vasbrd2(il1:il2)-vasbrd1(il1:il2))/(2.*delt)
     vmdbrd2(il1:il2)=(vmdbrd2(il1:il2)-vmdbrd1(il1:il2))/(2.*delt)
     vssbrd2(il1:il2)=(vssbrd2(il1:il2)-vssbrd1(il1:il2))/(2.*delt)
!
!     * deposition of black carbon.
!
     depbrol(il1:il2)=max(-vbcbrd2(il1:il2),0.)
     if (phys_diag%xtrapla1) then
        do il=il1,il2
           pa%voacrow(il)=pa%voacrow(il)+min(voabrd2(il),0.)*saverad
           pa%vbccrow(il)=pa%vbccrow(il)+min(vbcbrd2(il),0.)*saverad
           pa%vascrow(il)=pa%vascrow(il)+min(vasbrd2(il),0.)*saverad
           pa%vmdcrow(il)=pa%vmdcrow(il)+min(vmdbrd2(il),0.)*saverad
           pa%vsscrow(il)=pa%vsscrow(il)+min(vssbrd2(il),0.)*saverad
        end do
     end if
  end if
  !
  ! * Store detrained water in this timestep.
  !
  pa%zdetrow(il1:il2, 1:ilev) = dtadv * dqldt(il1:il2, 1:ilev)

  rcdt    = 1./dtadv
  do jk=1,ilev
    do il=il1,il2
       if (phys_diag%tprhsc) then
          pa%ttpmrow(il,jk) = pa%ttpmrow(il,jk) + tprow(il,jk)*saverad
          pa%ttpcrow(il,jk) = pa%ttpcrow(il,jk) + tcrow(il,jk)*saverad
       end if
       if (phys_diag%qprhsc) then
          pa%qtpmrow(il,jk) = pa%qtpmrow(il,jk) + qprow(il,jk)*saverad
          pa%qtpcrow(il,jk) = pa%qtpcrow(il,jk) + qcrow(il,jk)*saverad
       end if
       if (phys_diag%uprhsc) then
          duodt         = ( urow(il,jk)-ucrow(il,jk))*rcdt
          pa%utpcrow(il,jk) = pa%utpcrow(il,jk) + duodt*saverad
       end if
       if (phys_diag%vprhsc) then
          dvodt         = ( vrow(il,jk)-vcrow(il,jk))*rcdt
          pa%vtpcrow(il,jk) = pa%vtpcrow(il,jk) + dvodt*saverad
       end if
       pa%ttpmrol(il,jk) = tprow(il,jk)
       pa%ttpcrol(il,jk) = tcrow(il,jk)
       pa%qtpmrol(il,jk) = qprow(il,jk)
       pa%qtpcrol(il,jk) = qcrow(il,jk)
    end do
  end do
  !
  if (phys_diag%xprhsc) then
      do nn=1,ntrac
        do jk=1,ilev
          do il=il1,il2
            pa%xtpmrow(il,jk,nn) = pa%xtpmrow(il,jk,nn) + xprow(il,jk,nn)*saverad
            pa%xtpcrow(il,jk,nn) = pa%xtpcrow(il,jk,nn) + xcrow(il,jk,nn)*saverad
          end do
        end do
      end do
  end if
  !=======================================================================
  !     * accumulate the latest precipitation rates (mm s-1 = kg m-2 s-1).
  !
  beta=0.55
  ufac=0.1*daylnt   ! to convert from mm/sec to cm/day
  do il=il1,il2
    raindc (il) = max (zero, raindc(il)*1000./dtadv)
    rainsc (il) = max (zero, rainsc(il)*1000./dtadv)
    rainls (il) = max (zero, rainls(il))
    rain   (il) = rainls(il) + raindc(il) + rainsc(il)
    snow   (il) = max (zero, snow(il))
    prerow (il) = rain(il) + snow(il)
    pa%pcprow (il) = pa%pcprow (il) + prerow(il)*saverad
    pa%pcpcrow(il) = pa%pcpcrow(il) + raindc(il)*saverad
    pa%pcpsrow(il) = pa%pcpsrow(il) + rainsc(il)*saverad
    !
    pa%pcp3hrow (il) = pa%pcp3hrow (il) + prerow(il)*save3hr
    pa%pcpc3hrow(il) = pa%pcpc3hrow(il) + raindc(il)*save3hr
    pa%pcpl3hrow(il) = pa%pcpl3hrow(il) + rainls(il)*save3hr
    pa%pcps3hrow(il) = pa%pcps3hrow(il) + rainsc(il)*save3hr

    !         * sample the precipitation

    pa%pcprol(il)  = prerow(il)
    pa%pcpcrol(il) = raindc(il)
    !
    !         * surface wind speed with gustiness factor (now only
    !         * done once and used else where consistently).
    !
    gust1=beta*wstrol(il)
    pcptmp=min( (raindc(il)+rainsc(il))*ufac , 6.)
    gust2=log(1.+6.69*pcptmp-0.476*pcptmp**2)
    gustfac=gust1**2+gust2**2
    gustrol(il)=sqrt(gustfac)
  end do
  !
  if (ishf/=0) then
    savehf         = 1./real(ishf)
    do il=il1,il2
      pa%pchfrow(il)    = pa%pchfrow(il) + raindc(il)*savehf
      pa%plhfrow(il)    = pa%plhfrow(il) + rainls(il)*savehf
      pa%pshfrow(il)    = pa%pshfrow(il) + rainsc(il)*savehf
    end do
  end if

  return

  end subroutine convection
 end module convection_mod
